
   $('#zn-new-loading').css({"opacity": "0","visibility":"hidden"});

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

function loadPage(page) {

    $.ajax({
        type: "GET",
        url: page,
        data: {},
        success: function (data) {
            // $('#pageLoad').html(data).fadeIn();
        },
        beforeSend: function () {
            // animateCSS('#pageLoad', 'fadeOutDown');
            $('.znPageLoad').fadeOut();
            // $("div#loading").show();
            loadingPage();
        },
    }).done(function (data) {
        endLoadingPage();
        var state = {
            name: "name",
            page: 'History',
            url: page
        };
        window.history.replaceState(state, "History", page);
        // location.reload();
        
        $('.znPageLoad').html(data).fadeIn();
    });

}

var index = 0;
function refreshScript (src) {
  var scriptElement = document.createElement('script');
  scriptElement.type = 'text/javascript';
  scriptElement.src = src + '?' + index++;
  document.getElementsByTagName('head')[0].appendChild(scriptElement);
}


function loadingPage() {
    $('#zn-new-loading').css({"opacity": "1","visibility":"unset"});
}

function endLoadingPage() {
    $('#zn-new-loading').css({"opacity": "0","visibility":"hidden"});
}

