$('select.zn-dropdown').each(function() {

    var dropdown = $('<div />').addClass('zn-dropdown selectDropdown');

    $(this).wrap(dropdown);

    var label = $('<span />').text($(this).attr('placeholder')).insertAfter($(this));
    var list = $('<ul />');
    
    var id = $(this).attr("id");
  
    var setAct = '';

    
    

    $(this).find('option').each(function() {
        
        if (id == 'utype') {
            setAct = `onclick="set_filter_u_type(`+this.value+`); get_list();"`;
        }else if (id == 'uformat'){
            setAct = `onclick="set_filter_u_format(`+this.value+`); get_list();"`;
        }else if (id == 'uori'){
            setAct = `onclick="set_filter_u_ori(`+this.value+`); get_list();"`;
        }else if (id == 'uharga'){
            setAct = `onclick="set_filter_u_harga(`+this.value+`); get_list();"`;
        }

        list.append($('<li />').append('<a '+setAct+'>'+$(this).text()+'</a>' ));
    });

    list.insertAfter($(this));

    if($(this).find('option:selected').length) {
        label.text($(this).find('option:selected').text());
        list.find('li:contains(' + $(this).find('option:selected').text() + ')').addClass('active');
        $(this).parent().addClass('filled');
    }

});

$(document).on('click touch', '.selectDropdown ul li a', function(e) {
    e.preventDefault();
    var dropdown = $(this).parent().parent().parent();
    var active = $(this).parent().hasClass('active');
    var label = active ? dropdown.find('select').attr('placeholder') : $(this).text();

    dropdown.find('option').prop('selected', false);
    dropdown.find('ul li').removeClass('active');

    dropdown.toggleClass('filled', !active);
    dropdown.children('span').text(label);

    if(!active) {
        dropdown.find('option:contains(' + $(this).text() + ')').prop('selected', true);
        $(this).parent().addClass('active');
    }

    dropdown.removeClass('open');
});

$('.zn-dropdown > span').on('click touch', function(e) {
    var self = $(this).parent();
    self.toggleClass('open');
});

$(document).on('click touch', function(e) {
    var dropdown = $('.zn-dropdown');
    if(dropdown !== e.target && !dropdown.has(e.target).length) {
        dropdown.removeClass('open');
    }
});

// light
$('.switch input').on('change', function(e) {
    $('.zn-dropdown, body').toggleClass('light', $(this).is(':checked'));
});