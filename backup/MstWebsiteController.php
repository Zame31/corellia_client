<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstWebsiteModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstWebsiteController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'website';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_website.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_website.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstWebsiteModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_website"))->first();
                $data = new MstWebsiteModel();
                $data->id = $get->max_id+1;
            }

            if($request->hasFile('img')){
                $destination_path = public_path('gallery/product');
                $files = $request->file('img');
                $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                $upload_success = $files->move($destination_path, $filename);
            
                $data->img = $filename;
            }

            $data->product_name = $request->input('product_name');
            $data->deskripsi = $request->input('deskripsi');
            $data->price = $this->clearSeparator($request->input('price'));
            $data->color = $request->input('color');
            // $data->product_id = 'WS'.$get->max_id+1;

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM mst_website where id = ".$id);
        return json_encode($data);
    }

    public function delete($id)
    {
        MstWebsiteModel::destroy($id);
    }


    

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * from mst_website order by id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
          <button class="dropdown-item" onclick="edit('.$data->id.')">
              <i class="la la-edit"></i>
              <span>Edit</span>
          </button>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('price',function($data) {
            return 'Rp '.$this->numFormat($data->price);
        })
        ->editColumn('img',function($data) {
            return '<img width="100px" src="'.asset("gallery/product/".$data->img."").'" alt="" srcset="">';
        })
        ->rawColumns(['img', 'action'])
        ->make(true);

    }



}
