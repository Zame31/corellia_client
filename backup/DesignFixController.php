<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\TraceModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

use App\Events\StatusLiked;

class DesignFixController extends Controller
{
    public function index(Request $request)
    {



        $param['tittle'] = 'design Fix';

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.design_fix.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.design_fix.list',$param);
        }
    }

    public function konfirmasi_pembayaran(Request $request)
    {
        $id = $request->get('id');

        $pembayaran = collect(\DB::select("SELECT * from mst_pembayaran where no_pembelian = '".$id."' order by id desc"))->first();


        $data = DB::table('mst_pembayaran')->where('kode_pembayaran', $pembayaran->kode_pembayaran)->first();
        $pembeli = collect(\DB::select("SELECT DISTINCT no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        join ref_bank rb on rb.id = mp.bank where no_pembelian = '".$id."'"))->first();


        $param['data'] = $data;
        $param['pembeli'] = $pembeli;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.pembelian.konfirmasi_pembayaran',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.pembelian.konfirmasi_pembayaran',$param);
        }
    }

    public function konfirmasi_pembelian(Request $request)
    {
        $id = $request->get('id');
        $data = collect(\DB::select("SELECT DISTINCT discount,note,note_undangan,status_pengerjaan_design,response_admin,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        join ref_bank rb on rb.id = mp.bank::int where no_pembelian = '".$id."'"))->first();

        $d_undangan = \DB::select("SELECT
        mw.*,md.color,md.shape,md.paper,md.image_id,product_name,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name
        from mst_pembelian mw
        left join mst_undangan mo on mo.product_id = mw.product_id
        left join mst_design md on md.id = mw.design_id
        left join ref_color rc on rc.id = md.color
        left join ref_paper rp on rp.id = mw.paper_inner
        left join ref_paper rpp on rpp.id = mw.paper_outer
        left join ref_waxseal rw on rw.id = mw.waxseal
        left join ref_foilcolor rf on rf.id = mw.foilcolor
        left join ref_type_size rts on rts.id = mw.type_size where mo.type = 1 and no_pembelian = '".$id."'");

        $d_other = \DB::select("SELECT  mw.*,product_name,price from mst_pembelian mw
        join mst_other mo on mo.product_id = mw.product_id  where mo.type = 3 and no_pembelian = '".$id."'");


        $param['data'] = $data;
        $param['d_undangan'] = $d_undangan;
        $param['d_other'] = $d_other;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.design_fix.konfirmasi_pembelian',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.design_fix.konfirmasi_pembelian',$param);
        }
    }

    public function detail_pembelian(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');

        $data = collect(\DB::select("SELECT DISTINCT discount,note,note_undangan,response_admin,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        join ref_bank rb on rb.id = mp.bank where no_pembelian = '".$id."'"))->first();


        $d_undangan = \DB::select("SELECT
        mw.*,md.color,md.shape,md.paper,md.image_id,product_name,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name
        from mst_pembelian mw
        left join mst_undangan mo on mo.product_id = mw.product_id
        left join mst_design md on md.id = mw.design_id
        left join ref_color rc on rc.id = md.color
        left join ref_paper rp on rp.id = mw.paper_inner
        left join ref_paper rpp on rpp.id = mw.paper_outer
        left join ref_waxseal rw on rw.id = mw.waxseal
        left join ref_foilcolor rf on rf.id = mw.foilcolor
        left join ref_type_size rts on rts.id = mw.type_size where mo.type = 1 and no_pembelian = '".$id."'");

        $d_other = \DB::select("SELECT  mw.*,product_name,price from mst_pembelian mw
        join mst_other mo on mo.product_id = mw.product_id  where mo.type = 3 and no_pembelian = '".$id."'");

        $param['data'] = $data;
        $param['d_undangan'] = $d_undangan;
        $param['d_other'] = $d_other;
        $param['type'] = $type;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.design_fix.detail_pembelian',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.design_fix.detail_pembelian',$param);
        }
    }

    public function pembayaran_approve(Request $request)
    {
        try{

            $pembayaran = DB::table('mst_pembayaran')->where('kode_pembayaran', $request->get('kode_pembayaran'))->first();
            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();

            $sisa_billing = $pembelian->billing - $pembayaran->nominal_transfer;

            if ($sisa_billing <= 0) {
                $status = 3;
            }else {
                $status = 2;
            }

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = '".$request->get('no_pembelian')."' "))->first();

            $data = array('pembelian' => $param_email, 'pembayaran' => $pembayaran);

          

            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'billing' => $sisa_billing,
                'status' => $status,
                'response_admin' => $request->post('SetMemo')
                ]);

                Mail::send('email.pembayaran', $data, function ($message) use ($data) {
                    $message->from('support@basys.co.id', 'Corellia - Wedding Art');
                    $message->to($data['pembelian']->email);
                    $message->subject('Konfirmasi Pembayaran');
                    });
                    
            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function design_selesai(Request $request)
    {
            $note = $request->post('note');

            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();


            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status_pengerjaan_design' => 1,
                'response_design' => $note
                ]);

                $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
                left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
                left join mst_customer mc on mc.id = mst_pembelian.custid
                where no_pembelian = '".$request->input('no_pembelian')."' "))->first();

                $get_admin = collect(\DB::select("SELECT * from admin where id::int = ".$param_email->custtempid))->first();

                $this->sendNotif($get_admin->id,"Design Telah Selesai Untuk No Pembelian : ". $request->get('no_pembelian'),$param_email->status);
                event(new StatusLiked('Kirim Notif Ke Admin'));

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

    }

    public function pembayaran_reject(Request $request)
    {
        try{

            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status' => 4,
                'response_admin' => $request->post('SetMemo')
                ]);

            DB::table('mst_pembayaran')
            ->where('kode_pembayaran', $request->get('kode_pembayaran'))
            ->update([
                'status' => 4
                ]);

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function pembelian_reject(Request $request)
    {
        try{

            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status' => 6,
                'response_admin' => $request->post('SetMemo')
                ]);

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function add_trace(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');
        $note_trace = $request->post('note_trace');

        $data = new TraceModel();
        $data->no_pembelian = $no_pembelian;
        $data->note = $note_trace;
        $data->save();

        $data_pem = DB::table('mst_pembelian')->where('no_pembelian', $no_pembelian)->first();

        $text = $no_pembelian.' - '.$note_trace;

        $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
        left join mst_customer mc on mc.id = mst_pembelian.custid
        where no_pembelian = '".$no_pembelian."' "))->first();


        $data = array(
            'sendto'=> $param_email->email,
            'pembelian' => $param_email,
            'note_trace' => $note_trace,
            'subject' => 'Status Pengiriman');


        $this->sendMail('email.lacak',$data);

        $this->sendNotif($data_pem->custid,$text);

        return response()->json([
            'rc' => 1,
            'rm' => ' Berhasil'
        ]);

    }

    public function list_trace(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');
        $data = \DB::select("SELECT * from mst_trace where no_pembelian = '".$no_pembelian."'
        order by created_at desc");

        $list_trace = '';

        foreach ($data as $key => $value) {
            $list_trace .= ' <div class="kt-list-timeline__item">
                <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                <span class="kt-list-timeline__text">'.$value->note.'</span>
                <span class="kt-list-timeline__time">'.date('d-m-Y H:i:s',strtotime($value->created_at)).'</span>
            </div>';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $list_trace
        ]);


    }


    public function data(Request $request)
    {

            $data = \DB::select("SELECT DISTINCT no_pembelian,status_pengerjaan_design,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id = mp.bank::int
            join ref_status rs on rs.id = mp.status
            where status in (1,2,3) and mp.billing < mp.total_pembelian and designer = ".Auth::user()->id."
            order by created_at desc");




       return DataTables::of($data)
       ->addColumn('action', function ($data) {





        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a onclick="loadNewPage(`'.route('design_fix.konfirmasi_pembelian').'?id='.$data->no_pembelian.'`)" class="dropdown-item">
                  <i class="la la-clipboard"></i>
                  <span>Konfirmasi Pengerjaan</span>
              </a>
            </div>
        </div>
        ';
        })


        ->editColumn('status_pengerjaan_design',function($data) {
            return ($data->status_pengerjaan_design == 1) ? '<span class="kt-badge kt-badge--success kt-badge--inline">Selesai</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">Belum Selesai</span>';

        })
        ->editColumn('total_pembelian',function($data) {
            return 'Rp '.$this->numFormat($data->total_pembelian);
        })
        ->editColumn('billing',function($data) {
            return 'Rp '.$this->numFormat($data->billing);
        })
        ->editColumn('created_at',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->created_at));
        })
        ->editColumn('end_date',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->end_date));
        })
        // ->addColumn('cek', function ($data) {
        //     return '
        //     <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
        //         <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
        //         <span></span>
        //     </label>
        //     ';
        //     })

        ->rawColumns(['status_pengerjaan_design', 'action'])
        ->make(true);

    }



}
