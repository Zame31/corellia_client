<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstOtherModel;
use App\Models\MstImageModel;
use App\Models\PembelianModel;


use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Invoice';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.invoice.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.invoice.list',$param);
        }
    }
    public function index_other(Request $request)
    {
        
        $tittle = 'Invoice Other';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.invoice.other',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.invoice.other',$param);
        }
    }

    public function get_data($type,$id)
    {
        switch ($type) {
            case 'product_id':
                if ($id == 1) {
                    $data['d'] = \DB::select("SELECT * from mst_undangan");
                }elseif ($id == 5) {
                    $data['d'] = \DB::select("SELECT * from mst_other");
                }elseif ($id == 99) {
                    $data['d'] = \DB::select("SELECT * from mst_design_order");
                } else {
                    $data['d'] = \DB::select("SELECT * from mst_other where type = ".$id);
                }
               
                break;

            case 'type_size':
                    $data['d'] = \DB::select("select * from ref_type_size where product_id ='".$id."'");
                break;
                
            case 'shape':
                    $data['d'] =  \DB::select("SELECT DISTINCT shape 
                    from mst_design mo where mo.product_id = '".$id."'");
                break;
                
            case 'color':
                    $data['d'] =  \DB::select("SELECT DISTINCT rc.color,rc.code,rc.id 
                    from mst_design mo
                                join ref_color rc on rc.id = mo.color where mo.product_id = '".$id."'");
                break;
                
            case 'paper':
                    $data['d'] = \DB::select("SELECT DISTINCT rp.desc,rp.paper_name,rp.id 
                    from mst_design mo
                                join ref_paper rp on rp.id = mo.paper where mo.product_id = '".$id."'");
                break;
                

            default:
                # code...
                break;
        }
        

        return response()->json([
            'rc' => 0,
            'type' =>$type,
            'rm' => $data,
            'id' => $id
        ]);
    }

    
    public function store(Request $request)
    {

        try{
            $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_pembelian"))->first();
            $get_design = collect(\DB::select("SELECT * FROM mst_design 
            where product_id = '".$request->input('product_id')."' and
            color = ".$request->input('color')." and
            shape = ".$request->input('shape')))->first();

            if ($get) {$idpem = $get->max_id+1;}else {$idpem = 1;}
            $design_id = ($get_design) ? $get_design->id : 1;

            $end_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
            $created_at = date('Y-m-d H:i:s');

            $data = new PembelianModel();
            $data->product_id = $request->input('product_id');
            $data->qty = $request->input('qty');
            $data->totalprice = $this->clearSeparator($request->input('totalprice'))*$request->input('qty');
            $data->discount = 0;
            $data->custid = $request->input('custid');
            $data->end_date = $end_date;
            $data->bank = $request->input('bank');
            $data->accbank = $request->input('bank');
            $data->note = '';
            $data->status = 5;
            $data->no_pembelian = 'CO'.date('Ymd').$request->input('custid').$idpem;
        }
           
            $data->created_at = $created_at;
            $data->design_id = $design_id;
            $data->waxseal = $request->input('waxseal');
            $data->foilcolor = $request->input('foilcolor');
            $data->pengiriman = $request->input('pengiriman');
           
           

            $data->type_size = $request->input('type_size');
            $data->paper_inner = $request->input('paper_inner');
            $data->paper_outer = $request->input('paper_outer');
            $data->free_finishing = $request->input('free_finishing');
            $data->free_additional = $request->input('free_additional');
            $data->additional_user = '';
            $data->additional_user_count = '';
            $data->emboss = $request->input('emboss');

            if ($request->input('pengiriman') == 'dikirim') {
                $data->ongkir = $this->clearSeparator($request->input('ongkir'));
            }else{
                $data->ongkir = 0;
            }

            $data->billing = $this->clearSeparator($request->input('total_pembelian'));
            $data->total_pembelian = $this->clearSeparator($request->input('total_pembelian'));
            $data->save();
           

            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $data->no_pembelian)->first();
         
            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = '".$data->no_pembelian."' "))->first();
    
            $datamail = array('pembelian' => $param_email, 'subject' => 'b');
            
            Mail::send('email.pembelian', $datamail, function ($message) use ($datamail) {
            $message->from('support@basys.co.id', 'Corellia - Wedding Art');
            $message->to($datamail['pembelian']->email);
            $message->subject('Menunggu Pembayaran'); 
            });

            $this->sendNotif($pembelian->custid,"Terdapat Pembelian Baru dengan nomor pembelian - ".$data->no_pembelian,1);

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Simpan Dibatalkan, Tolong Lengkapi Semua Data  !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function store_other(Request $request)
    {

        try{
            $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_pembelian"))->first();
           

            if ($get) {$idpem = $get->max_id+1;}else {$idpem = 1;}
           

            $end_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
            $created_at = date('Y-m-d H:i:s');

            $data = new PembelianModel();
            $data->product_id = $request->input('product_id');
            $data->qty = $request->input('qty');
            $data->totalprice = $this->clearSeparator($request->input('totalprice'))*$request->input('qty');
            $data->discount = 0;
            $data->custid = $request->input('custid');
            $data->end_date = $end_date;
            $data->bank = $request->input('bank');
            $data->accbank = $request->input('bank');
            $data->note = '';
            $data->status = 5;
            $data->no_pembelian = 'COA'.date('Ymd').$request->input('custid').$idpem;
           
            $data->type_id = $request->input('type');
            $data->created_at = $created_at;
          
            $data->pengiriman = $request->input('pengiriman');
            
            $data->additional_user = '';
            $data->additional_user_count = '';
        

            if ($request->input('pengiriman') == 'dikirim') {
                $data->ongkir = $this->clearSeparator($request->input('ongkir'));
            }else{
                $data->ongkir = 0;
            }

            $data->billing = $this->clearSeparator($request->input('total_pembelian'));
            $data->total_pembelian = $this->clearSeparator($request->input('total_pembelian'));
            $data->save();
           
            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $data->no_pembelian)->first();
         
            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = '".$data->no_pembelian."' "))->first();
    
            $datamail = array('pembelian' => $param_email, 'subject' => 'b');
            
            Mail::send('email.pembelian', $datamail, function ($message) use ($datamail) {
            $message->from('support@basys.co.id', 'Corellia - Wedding Art');
            $message->to($datamail['pembelian']->email);
            $message->subject('Menunggu Pembayaran'); 
            });

            $this->sendNotif($pembelian->custid,"Terdapat Pembelian Baru dengan nomor pembelian - ".$data->no_pembelian,1);

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Simpan Dibatalkan, Tolong Lengkapi Semua Data  !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data['other'] = collect(\DB::select("SELECT * FROM mst_other where id = ".$id))->first();
        $data['img'] = \DB::select("SELECT * FROM mst_image where product_id = '".$data['other']->product_id."' order by seq asc");
        
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete($id)
    {
        // MstOtherModel::destroy($id);

        DB::table('mst_other')
        ->where('product_id', $id)
        ->delete();

        DB::table('mst_image')
        ->where('product_id', $id)
        ->delete();
    }

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT *,mst_other.id as mid FROM mst_other
        join ref_type_produk on ref_type_produk.id = mst_other.type order by mst_other.id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit('.$data->mid.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del(`'.$data->product_id.'`)">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
      
        ->editColumn('price',function($data) {
            return 'Rp '.$this->numFormat($data->price);
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
