<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\UserAdminModel;

class UserController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'User List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.user.list', $param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.user.list', $param);
        }

    }

    public function data()
    {
       $data = \DB::select("SELECT * from admin join reff_user_role ru on ru.role_id::int = admin.role::int order by id desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {

        if ($data->is_active == '1') {
           $actStatus = '<a class="dropdown-item" onclick="setActive(`'.$data->id.'`,`'.$data->is_active.'`)">
                <i class="la la-times-circle-o"></i>
                <span>Deactived</span>
            </a>';
        }else{
            $actStatus = '<a class="dropdown-item" onclick="setActive(`'.$data->id.'`,`'.$data->is_active.'`)">
            <i class="la la-check-circle-o"></i>
            <span>Active</span>
        </a>';
        }

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
              <a class="dropdown-item" onclick="edit(`'.$data->id.'`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>'.$actStatus.'

              <a class="dropdown-item" onclick="resetPassword(`'.$data->id.'`)">
                <i class="la la-key"></i>
                <span>Reset Password</span>
            </a>
            </div>
        </div>
        ';
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == '1') ? '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">non-active</span>';
        })
        ->rawColumns(['is_active', 'action'])
        ->make(true);
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

         
            if ($get_id) {
                $data = UserAdminModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM admin"))->first();
                $data = new UserAdminModel();
                $data->id = $get->max_id+1;
                $data->is_active = '1';
                $data->password = Hash::make('admin');
            }

            $data->name = $request->input('name');
            $data->address = $request->input('address');
            $data->email = $request->input('email');
            $data->phone = $request->input('phone');
            $data->role = $request->input('role');
            $data->username = $request->input('username');


            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM admin where id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
        UserAdminModel::destroy($id);
    }

    public function SetActive(Request $request)
    {
        $id = $request->input('id');
        $isActive = $request->input('active');
        if ($isActive == '1') {
            DB::table('admin')->where('id', $id)->update([
                'is_active' => '0',
            ]);
            $reqMessage = 'User berhasil dinonaktifkan';
        } else {
            DB::table('admin')->where('id', $id)->update([
                'is_active' => '1',
            ]);
            $reqMessage = 'User berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    }

    public function resetPassword($id)
    {
        $user = $this->getUserAdmin($id);
        $event = "Reset Password User ".$user[0]->username;

        $data = UserAdminModel::find($id);
        $data->password = Hash::make('admin');
        $data->save();
    }


    function changePassword(Request $request)
    {
        $user = $this->getUserAdmin(Auth::user()->id);
        // $event = "Ubah Password User ".$user[0]->username;
        // $this->auditTrail($event,"User Admin");

        $data = UserAdminModel::find(Auth::user()->id);
        $data->password = Hash::make($request->input('newpassword'));
        $data->save();

        return response()->json([
            'rc' => 1,
        ]);
    }

    public function check_password()
    {
        $data = collect(\DB::select("SELECT password FROM admin where id = ".Auth::user()->id))->first();

        if(Hash::check('admin',$data->password)){
            $result = '1';
        }else {
            $result = '0';
        }

        return json_encode($result);
    }
}
