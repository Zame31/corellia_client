<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\RefColorModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class RefColorController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.ref_color.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.ref_color.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = RefColorModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM ref_color"))->first();
                $data = new RefColorModel();
                $data->id = $get->max_id+1;
            }
         
            $data->color = $request->input('color');
            $data->code = $request->input('code');

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM ref_color where id = ".$id);
        return json_encode($data);
    }

    public function delete($id)
    {
        RefColorModel::destroy($id);
    }


    

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * from ref_color");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
          <a class="dropdown-item" onclick="edit('.$data->id.')">
              <i class="la la-edit"></i>
              <span>Edit</span>
          </a>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('code',function($data) {
            return '<div style="background:'.$data->code.';width: 30px;height: 30px;border-radius: 50%;border: 1px solid #b7b7b7;" ></div>';
        })
        ->rawColumns(['code', 'action'])
        ->make(true);

    }



}
