<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstOtherModel;
use App\Models\MstImageModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstOtherController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_other.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_other.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstOtherModel::find($get_id);
                $data->product_id = 'O'.$get_id;
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_other"))->first();
                $data = new MstOtherModel();
                $data->id = $get->max_id+1;
                $data->product_id = 'O'.($get->max_id+1);
            }

            $data->product_name = $request->input('product_name');
            $data->price = $this->clearSeparator($request->input('price'));
            $data->type = $request->input('type');
            $data->desc = $request->input('desc');
            $data->berat = $this->clearSeparator($request->input('berat'));

            $data->user = Auth::user()->id;
           
            $data->product_status = 1;

            $data->save();

            

            for ($i=1; $i <= 4; $i++) { 

                if($request->hasFile('img'.$i)){
                    $destination_path = public_path('gallery/product');
                    $files = $request->file('img'.$i);

                    $setSize = 0.5 * (1024*1024);
                    if ($files->getSize() > $setSize) {
                        return response()->json([
                            'rc' => 99,
                            'rm' => 'Size File Maksimal 500 kb'
                        ]);
                    }
    
                    $exceptFile = ['jpg','jpeg','png','JPG','JPEG','PNG','pdf','PDF','xlsx','xls'];
                    if (!in_array($files->getClientOriginalExtension(), $exceptFile))
                    {
                        return response()->json([
                            'rc' => 99,
                            'rm' => 'file tidak diperbolehkan!'
                        ]);
                    }
                    
                    $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                    $upload_success = $files->move($destination_path, $filename);
                    
                    DB::table('mst_image')
                    ->where('product_id', $data->product_id)
                    ->where('seq', $i)
                    ->delete();

                    $data_img = new MstImageModel();
                    $data_img->product_id =  $data->product_id;
                    $data_img->img = $filename;
                    $data_img->seq = $i;
                    $data_img->save();
                }

            }
           

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data['other'] = collect(\DB::select("SELECT * FROM mst_other where id = ".$id))->first();
        $data['img'] = \DB::select("SELECT * FROM mst_image where product_id = '".$data['other']->product_id."' order by seq asc");
        
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete($id)
    {
        // MstOtherModel::destroy($id);

        DB::table('mst_other')
        ->where('product_id', $id)
        ->delete();

        DB::table('mst_image')
        ->where('product_id', $id)
        ->delete();
    }

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT *,mst_other.id as mid FROM mst_other
        join ref_type_produk on ref_type_produk.id = mst_other.type order by mst_other.id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit('.$data->mid.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del(`'.$data->product_id.'`)">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
      
        ->editColumn('price',function($data) {
            return 'Rp '.$this->numFormat($data->price);
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
