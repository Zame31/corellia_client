<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', sans-serif;
            text-align: center;
            color: #56565A;
            font-size: 12px;
        }

        .zn-email-bg {
            background: #f7f7f7;
            padding: 5px;
            text-align: center;
            display: inline-block;
            border-radius: 10px;
        }

        .zn-email-tittle {
            font-size: 16px;
            font-weight: bold;
            color: #c89c7d;
        }

        .zn-email-user {
            margin-top: 3px;
            font-size: 14px;
            font-weight: bold;
            color: #565656;
        }

        .zn-email-t {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
            padding-bottom: 40px;
            margin-bottom: 40px;
            border-bottom: 2px dashed #eaeaea;
        }

        .zn-email-tt {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
        }

        .zn-email-btn {
            text-decoration: none;
            display: inline-block;
            background: #b5c737;
            color: #fff;
            padding: 10px 60px;
            border-radius: 5px;
            margin-top: 10px;
            font-weight: 700;
            text-transform: uppercase;
            font-size: 14px;
        }

        .zn-email-ttt {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
        }

        .zn-email-link {
            font-size: 12px;
            display: inline-block;
            background: #EDFFAB;
            padding: 10px;
            margin-top: 20px;
            color: #b5c737;
        }

        .zn-email-bg-in {
            background: #fff;
            padding: 15px;
            border-radius: 10px;
        }

        .zn-email-cp {
            font-size: 12px;
            color: #adadad;
            padding: 20px;
        }

        .zn-product-count {
            color: #5d5c52;
            font-size: 12px;
            font-weight: 100;
            display: block;
            margin-top: 10px;
            text-align: left;
        }

        .zn-text-green {
            color: #c89c7d !important;
        }

        .zn-product-tittle {
            color: #696969;
            font-size: 12px;
            font-weight: 600;
            display: block;
            text-align: left;
        }

        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
            padding-left: 30px;
        }

        .col-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .mb-2,
        .my-2 {
            margin-bottom: .5rem !important;
        }

        .col-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }

        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }

        .col-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .text-right {
            text-align: right !important;
        }

        .mt-4,
        .my-4 {
            margin-top: 1.5rem !important;
        }

        .col-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }

               
.col-5 {
  -webkit-box-flex: 0;
  -ms-flex: 0 0 41.66667%;
  flex: 0 0 41.66667%;
  max-width: 41.66667%; }

    </style>
</head>

<body>
    <div class="zn-email-bg">
        <div class="zn-email-bg-in">
            <div class="zn-email-tittle">
                Corellia - Wedding Art
            </div>
            <br>
            <div class="zn-email-user">Pembayaran Anda <br> Telah Kami Konfirmasi</div>
            <div class="zn-email-t">Terimakasih telah menyelesaikan transaksi di corellia.id <br>
                Pembayaran menggunakan {{$pembayaran->bank}} berhasil.<br>
                </div>


            <div class="row">
                <div class="col-5" style="margin-right:40px;">
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Kode Pembayaran</span>
                        <span class="zn-product-tittle mb-2 zn-text-green">{{$pembayaran->kode_pembayaran}}</span>
                    </div>
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Nominal Transfer</span>
                        <span class="zn-product-tittle mb-2 zn-text-green">Rp
                            {{number_format($pembayaran->nominal_transfer,0,",",".")}}</span>
                    </div>
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Status Pembayaran</span>
                        <span class="zn-product-tittle mb-2">Dibayar</span>
                    </div>
                </div>
                <div class="col-5">
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Waktu Pembayaran</span>
                        <span class="zn-product-tittle mb-2">{{date('d-m-Y H:i:s', strtotime($pembayaran->created_at))}} </span>
                    </div>
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Metode Pembayaran</span>
                        <span class="zn-product-tittle mb-2">Transfer - {{$pembayaran->bank}} </span>
                    </div>
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Ditransfer Oleh</span>
                        <span class="zn-product-tittle mb-2">{{$pembayaran->no_rek}} <br> a.n. {{$pembayaran->atas_nama}} </span>
                    </div>
                    <div class="mb-4">
                        <span class="zn-product-count mb-2" style="font-weight: 400;">Transfer Ke </span>
                        <span class="zn-product-tittle mb-2">{{$pembelian->bank_acc_no}} <br> a.n. {{$pembelian->an}}
                        </span>
                    </div>
                   
                </div>
            </div>

        </div>
        <div class="zn-email-cp">Copyrights &copy; 2019 <a style="color: #c89c7d;text-decoration: none;"
                href="https://corellia.id/">Corellia.id</a>. All Rights Reserved.</div>
    </div>
</body>


</html>
