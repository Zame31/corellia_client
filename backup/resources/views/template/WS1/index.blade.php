<!DOCTYPE html>

<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Wedding Invitation {{$data->setname}} </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Spesial mengundang untuk menghadiri pernikahan kami. {{$data->setname}}" />
	<meta name="keywords" content="Wedding Invitation {{$data->setname}} " />
	<meta name="author" content="{{$data->setname}} " />
	<meta name="application-name" content="{{$data->setname}}">
	<link rel="icon" href="{{asset('gallery/website_img/')}}/{{$data->set_header}}">
	<meta property="og:title" content=""/>
	<meta property="og:image" content="{{asset('website/WS1/images/pat1.png')}}"/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('website/WS1/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('website/WS1/css/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('website/WS1/css/bootstrap.css')}}">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('website/WS1/css/magnific-popup.css')}}">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{asset('website/WS1/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('website/WS1/css/owl.theme.default.min.css')}}">
	<style media="screen">
        #bg_color {
			background: url("{{asset('website/WS1/images/pat1.png')}}");
			background-repeat: repeat;
			margin-bottom: 0px;
			opacity: 1;
		}
		.modal {
        text-align: center;
        padding: 0!important;
		}

		.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}

.mb-2, .my-2 {
    margin-bottom: 0.5rem !important;
}
.badge-primary {
    color: #fff;
    background-color: #007bff;
}

.badge-danger {
    color: #fff;
    background-color: #dc3545;
}
		
		.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 0.25rem;
}

        .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
        }

        .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        }
	</style>
	
	
	<!-- Theme style  -->
	<link rel="stylesheet" href="{{asset('website/WS1/css/mystyle.css')}}">
	
	<!-- Modernizr JS -->
	<script src="{{asset('website/WS1/js/modernizr-2.6.2.min.js')}}"></script>
	<link href="{{asset('js/sound/fraudio.min.css')}}" rel="stylesheet">
	<script src="{{asset('js/jquery.js')}}"></script>
	  <script src="{{asset('js/sound/fraudio.min.js')}}"></script>
	  
	  

	</head>
	@php
		if ($data->bg_color) {
			$bgcolor = 'background:'.$data->bg_color.'52;';
		}else{
			$bgcolor = '';
		}
	@endphp
	<body id="bg_color" class="scrollStyle">
		@if (isset($type))
		<div style="
			position: fixed;
			z-index: 10000;
			background: #000000de;
			width: 100%;
			text-align: center;
			padding: 20px;
			color: #f7b79a;
			font-weight: 500;
			font-size: 14px;
			opacity: 0.9;
			font-style: oblique;
		">
			"untuk mengaktifkan website, silahkan checkout pesanan ini dan lakukan pembayaran. Terimakasih" 
			<img src="{{asset('img/logo.png')}}" style="width: 100px;margin-left: 50px;" alt="">
		</div>
		@endif
		<div style="{{$bgcolor}}">
		<!-- <audio src="images/bg_sound.mp3" id="my_audio" preload="auto" loop="loop" autoplay="autoplay"></audio> -->
	
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="fh5co-logo"><a href="#">The  <strong>Wedding</strong></a></div>
				</div>
				
			</div>
			
		</div>
	</nav>

	
	<audio id="set_music" class="fraudio" loop>
		<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/ogg">
		<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/mpeg">
	</audio>

	<span style="
	display: inline-block;
	 position: fixed;
	 z-index: 1000;
	 color: white;
	 bottom: 88px;
	 right: 77px;
	 text-align: right;
	 font-size: 11px;
	 line-height: 13px;
	 ">
	  Music: <br> <a target="_blank" style="font-weight: 500;color: #ffffff;" href="https://www.bensound.com/">www.bensound.com</a></span>


	<header id="set_header" class="fh5co-cover" role="banner" style="background-image:url('{{asset('gallery/website_img/')}}/{{$data->set_header}}');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1 id="setName"> {{$data->setname}} </h1>
							<h2>We Are Getting Married </h2>
							<div class="simply-countdown simply-countdown-one"></div>
							{{-- @php
								$link_location = 'http://maps.google.com/maps?q='..',107.6138426245369&z=18';
							@endphp --}}
							<p>
								<a href="http://maps.google.com/maps?q={{$data->maps_lat}},{{$data->maps_lng}}&z=18" target="_blank" class="btn btn-default btn-sm">Get Location</a>
								@if ($data->link_live)
									<a href="{{$data->link_live}}" target="_blank" class="btn btn-default btn-sm">Live Streaming</a>
								@endif
							
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-couple">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h2 id="setTglRes">{{date('d F Y', strtotime($data->settglres))}}<small style="
						color: #f14e95;
						font-weight: bold;
						font-size: 20px;
					"></small> </h2>
					<h3 id="setTempatRes">{{$data->settempatres}}</h3>
					<p id="setAlamatRes">{{$data->setalamatres}}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="couple-wrap animate-box">
						<div class="couple-half">
							<div class="groom">
								<img id="foto_pria" src="{{asset('gallery/website_img/')}}/{{$data->foto_pria}}" alt="groom" class="img-responsive">
							</div>
							<div class="desc-groom">
								<h3 id="setNamePria">{{$data->setnamepria}}</h3>
								<p id="setDescPria">{{$data->setdescpria}}</p>
							</div>
						</div>
						<p class="heart text-center"><i class="icon-heart2"></i></p>
						<div class="couple-half">
							<div class="bride">
								<img id="foto_wanita" src="{{asset('gallery/website_img/')}}/{{$data->foto_wanita}}" alt="groom" class="img-responsive">
							</div>
							<div class="desc-bride">
								<h3 id="setNameWanita">{{$data->setnamewanita}}</h3>
								<p id="setDescWanita">{{$data->setdescwanita}}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			

			<div class="row">
				<div class="col-md-12">
					<span id="setKata" class="zn-quote">
						{{$data->setkata}}
					</span>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-event" class="fh5co-bg" style="background-image:url('{{asset('website/WS1/images/img_bg_3.jpg')}}');">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>Our Special Event</span>
					<h2>Wedding Events</h2>
				</div>
			</div>
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Akad Nikah</h3>
									<div class="event-col">
										<i class="icon-clock"></i>
										<span id="setJam1Akad">{{$data->setjam1akad}}</span>
										<span id="setJam2Akad">{{$data->setjam2akad}}</span>

									</div>
									<div class="event-col">
										<i class="icon-calendar"></i>
										<span id="setTglAkad">{{$data->settglakad}}</span>
										<br><br>
									</div>
									<p id="setAlamatAkad">{{$data->setalamatakad}}</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Resepsi</h3>
									<div class="event-col">
										<i class="icon-clock"></i>
										<span id="setJam1Res">{{$data->setjam1res}}</span>
										<span id="setJam2Res">{{$data->setjam2res}}</span>
									</div>
									<div class="event-col">
										<i class="icon-calendar"></i>
										<span id="setTglRes2">{{$data->settglres}}</span>
										<br>
										<br>
									</div>
									<p id="setAlamatRes2">{{$data->setalamatres}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-gallery" class="fh5co-section-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box fadeInUp animated-fast">
					<span>Our Memories</span>
					<h2>Gallery</h2>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-gallery-list">
						
					<li id="gallery_1" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_1}}'); ">
						<a class="color-2">
							
						</a>
					</li>
				


					<li id="gallery_2" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_2}}'); ">
						<a class="color-3">
						
						</a>
					</li>
					<li id="gallery_3" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_3}}'); ">
						<a class="color-4">
							
						</a>
					</li>
					<li id="gallery_4" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_4}}'); ">
						<a class="color-4">
							
						</a>
					</li>
					
					
					

					
					</ul>		
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-tamu" class="fh5co-section-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box fadeInUp animated-fast">
					<span>Kumpulan Ucapan dari Tamu</span>
					<h2>Buku Tamu</h2>
				</div>
			</div>

			<div class="row row-bottom-padded-md mb-3">
				<div class="col-md-12 text-center">	
					<button type="button" class="btn btn-danger" style="
					font-size: 13px;
					font-weight: 600;
				" data-toggle="modal" onclick="buku_tamu()">Isi Buku Tamu</button>
				</div>
			</div>
			<div class="row" id="list_buku_tamu" style="padding-bottom: 300px;">
			</div>

		</div>
	</div>

	


	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

	@include('template.buku_tamu')
	
	<!-- jQuery -->
	<script src="{{asset('website/WS1/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('website/WS1/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('website/WS1/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('website/WS1/js/jquery.waypoints.min.js')}}"></script>
	<!-- Carousel -->
	<script src="{{asset('website/WS1/js/owl.carousel.min.js')}}"></script>
	<!-- countTo -->
	<script src="{{asset('website/WS1/js/jquery.countTo.js')}}"></script>

	<!-- Stellar -->
	<script src="{{asset('website/WS1/js/jquery.stellar.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{asset('website/WS1/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('website/WS1/js/magnific-popup-options.js')}}"></script>

	<script src="{{asset('website/WS1/js/simplyCountdown.js')}}"></script>
	
	<!-- Main -->
	<script src="{{asset('website/WS1/js/main.js')}}"></script>

	<script>


		// var tomorrow = new Date();
		// tomorrow.setDate(tomorrow.getDate() + 7);

		// var d = new Date(tomorrow);
		
    var d = new Date("{{date('F d, Y', strtotime($data->settglres))}} 8:00:00 GMT+07:00");

	console.log(d);
	
    // var d = new Date('September 08, 2019 08:00:00 GMT+07:00');

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });

	function set_audio() {
            var audio = document.getElementById("set_music");
            // audio.play();
            $('.fraudio-play').click();
        }
</script>
@include('template.act_buku_tamu')
		</div>
	</body>
</html>

