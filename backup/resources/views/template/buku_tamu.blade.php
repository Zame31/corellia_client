@php
if (isset($_GET['mengundang'])) {
      $mengundang = $_GET['mengundang'];
  }else {
      $mengundang = '';
  }
@endphp
<div id="amplop" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content" style="background: inherit;border: inherit;padding: 10px;box-shadow: inherit;">
        <img src="{{asset('img/amplop.png')}}" style="width: 100%;" alt=""/>
        <div style="
        position: absolute;
        top: 15%;
        left: 0;
        width: 100%;
        text-align: center;
    ">
        @if ($mengundang)
        <div>
            <div>Dear,</div>
            <div style="
            font-size: 16px;
            font-weight: 500;
            text-transform: uppercase;
        ">{{$mengundang}}</div>
        </div>
        @endif

            @php
            $setFont = '';
                switch ($data->product_id) {
                    case 'WS1':
                        $setFont = '"Sacramento", Arial, serif';
                    break;
                    case 'WS2':
                        $setFont = '"Poppins", sans-serif';
                    break;
                    case 'WS3':
                        $setFont = '"Great Vibes", cursive';
                    break;

                    

                    
                    case 'WS4':
                        $setFont = 'JanettaSilloam';
                    break;
                    case 'WS44':
                        $setFont = 'JanettaSilloam';
                    break;
                    
                }
            @endphp
       
            <div class="mt-3">You're Invited to Our Wedding </div>
            <div style="
            font-size: 30px;
            font-weight: 600;
            margin-top: 5px;
            font-family: {{$setFont}};
            width: 100%;
        ">{{$data->setname}}</div>
        <button type="button" class="btn btn-danger" style="
        margin-top: 20%;
        font-size: 13px;
        font-weight: 600;
    " data-toggle="modal" data-target="#amplop" onclick="set_audio()">OPEN INVITATION</button>
        </div>
     
      </div>
  
    </div>
  </div>

  <div id="buku_tamu" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
           <form id="form-tamu">
                <div class="modal-header" style="background: #17a2b8;color: #fff;">
                    <h4 class="modal-title" style="
                    color: #fff !important;
                ">Isi Buku Tamu</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label >Nama</label>
                        <input type="text" maxlength="100" class="form-control" id="nama" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="kehadiran" style="width: 100%">Pilih Kehadiran</label>
                        <select class="form-control" style="width: 100%" id="kehadiran" name="kehadiran" required>
                            <option value="" selected>Pilih Kehadiran</option>
                            <option value="Berkenan Hadir">Berkenan Hadir</option>
                            <option value="Maaf Tidak bisa hadir">Maaf Tidak bisa hadir</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label style="
                        width: 100%;
                        margin-top: 15px;
                    ">Ucapan</label>
                        <textarea maxlength="500" class="form-control" rows="5" id="ucapan" name="ucapan" required></textarea>
                    </div>
                    <div class="alert alert-danger" id="alert-tamu" role="alert">
                       Lengkapi Isian Terlebih dahulu :)
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="store()">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </form>
        </div>
    </div>
  </div>
