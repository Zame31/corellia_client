<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="{{$data->setname}}">
	<meta name="keywords" content="Wedding Invitation {{$data->setname}}">
	<meta name="description" content="Spesial mengundang untuk menghadiri pernikahan kami. {{$data->setname}}" />
    <link rel="icon" href="{{asset('gallery/website_img/')}}/{{$data->set_header}}" type="image/png">
    <title>Wedding Invitation {{$data->setname}}</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('website/WS4/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/vendors/linericon/style.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/vendors/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/vendors/lightbox/simpleLightbox.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/vendors/nice-select/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/vendors/animate-css/animate.css')}}">
    <!-- main css -->
    {{-- <link rel="stylesheet" href="{{asset('website/WS4/css/fonts.css')}}"> --}}

    <link rel="stylesheet" href="{{asset('website/WS4/css/style.css?v=1.0')}}">
    <link rel="stylesheet" href="{{asset('website/WS4/css/myStyle.css?v=1.0')}}">

    <link rel="stylesheet" href="{{asset('website/WS4/css/responsive.css')}}">
    <style media="screen">
        @font-face {
            font-family: JanettaSilloam;
            src: url('{{asset("website/WS4/fonts/JanettaSilloam.otf")}}');
        }
        #bg_pat {
            background: url("{{asset('website/WS4/img/background001.jpg')}}");
            background-repeat: repeat;
            margin-bottom: 0px;
            opacity: 1;
            background-size: cover;
        }
        html, body {
        height: 100%;
        margin: 0px;
    }
    .bg-cc {
        min-height: 100vh;
    }

    .modal {
        text-align: center;
        padding: 0!important;
        }

        .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
        }

        .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        }
       
    </style>
    <link href="{{asset('js/sound/fraudio.min.css')}}" rel="stylesheet">
</head>
@php
if ($data->bg_color) {
    $bgcolor = 'background:'.$data->bg_color.'52;';
}else{
    $bgcolor = '';
}
@endphp

<body id="bg_pat" class="scrollStyle" style="overflow-x: hidden;" data-spy="scroll">
    @if (isset($type))
    <div style="
        position: fixed;
        z-index: 10000;
        background: #000000de;
        width: 100%;
        text-align: center;
        padding: 20px;
        color: #f7b79a;
        font-weight: 500;
        font-size: 14px;
        opacity: 0.9;
        font-style: oblique;
    ">
        "untuk mengaktifkan website, silahkan checkout pesanan ini dan lakukan pembayaran. Terimakasih" 
        <img src="{{asset('img/logo.png')}}" style="width: 100px;margin-left: 50px;" alt="">
    </div>
    @endif
   
    <div style="{{$bgcolor}}" class="bg-cc" >
       
            <audio class="fraudio" id="set_music" loop>
                <source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/ogg">
                <source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/mpeg">
            </audio>
            <span style="
          display: inline-block;
           position: fixed;
           z-index: 1000;
           color: white;
           bottom: 88px;
           right: 77px;
           text-align: right;
           font-size: 11px;
           line-height: 13px;
           ">
            Music: <br> <a target="_blank" style="font-weight: 500;color: #ffffff;" href="https://www.bensound.com/">www.bensound.com</a></span>
        

        
    <!--================Header Menu Area =================-->
    <ul class="nav nav-pills nav-justified zn-nav" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                aria-controls="pills-home" aria-selected="true">
                <img src="{{asset('website/WS4/img/script.svg')}}" width="35px" height="35px" alt=""></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                aria-controls="pills-profile" aria-selected="false">
                <img src="{{asset('website/WS4/img/wedding.svg')}}" width="35px" height="35px" alt=""></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                aria-controls="pills-contact" aria-selected="false">
                <img src="{{asset('website/WS4/img/live.png')}}" width="35px" height="35px" alt=""></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-gallery" role="tab"
                aria-controls="pills-gallery" aria-selected="false">
                <img src="{{asset('website/WS4/img/gallery.svg')}}" width="35px" height="35px" alt=""></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-tamu" role="tab"
                aria-controls="pills-gallery" aria-selected="false">
                <img src="{{asset('website/WS4/img/gift.png')}}" width="35px" height="35px" alt=""></a>
        </li>
    </ul>

    <div class="tab-content" id="pills-tabContent"  >
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <!--================Home Banner Area =================-->
            <section id="set_header" class="home_banner_area" style="min-height: 900px;object-fit: cover; background-image: url('{{asset('gallery/website_img/')}}/{{$data->set_header}}'); background-size:cover; background-position:center;">
                <div class="banner_inner d-flex align-items-center">
                    <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0"
                        data-background=""></div>
                    <div class="container" style="margin-top: -300px;">
                        <div class="banner_content text-center">
                            <img src="{{asset('website/WS4/img/PNGAKSEN01.png')}}" style="
                            width: 200px;
                            margin-bottom: 20px;
                        " alt="">
                            {{-- <h5>The Wedding of</h5> --}}
                            <h3 id="setName">
                                
                                {{-- @if ($data->nama_website == 'VanyaAbhip')  --}}
                                Vanya <br> & <br> Abhip
                                {{-- @else
                                    {{$data->setname}}
                                @endif --}}
                            </h3>
                            {{-- <img src="{{asset('website/WS4/img/banner/shap-2.png')}}" alt=""> --}}
                            <div style="margin-top: 40px;">
                                {{-- <a target="_blank" href="http://maps.google.com/maps?q={{$data->maps_lat}},{{$data->maps_lng}}&z=18" class="genric-btn default circle arrow">Get Location<span class="lnr lnr-arrow-right"></span></a> --}}
                                @if ($data->link_live)
                                {{-- <a target="_blank" href="{{$data->link_live}}" class="genric-btn default circle arrow">Live Streaming<span class="lnr lnr-arrow-right"></span></a> --}}
                              
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================End Home Banner Area =================-->
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <!--================Love Journey Area =================-->
            <section class="love_journey_area " style="padding: 30px 0px 100px;">
                <div class="container" >
                    <div class="love_inner">
                        <div class="main_title" style="
                        margin-bottom: 20px;
                    ">
                            <h2>Bride & Groom</h2>
                            <p id="setKata"> 
                                {{$data->setkata}}
                            </p>
                        </div>
                        <div class="love_js_items">
                          
                            <div class="love_js_item right row">
                                <div class="col-lg-5" style="
                                text-align: right;
                            ">
                                    <div class="love_img"><img id="foto_wanita" class="img-fluid" style="
                                        width: 350px;
                                        height: 350px;
                                        object-fit: cover;
                                        border: 2px solid white;
                                        /* box-shadow: 0px 0px 20px #00000066; */
                                    " src="{{asset('gallery/website_img/')}}/{{$data->foto_wanita}}"
                                        alt=""></div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="love_text_item"  style="
                                    opacity: 0.8;
                                    margin-left: 30px;
                                ">
                                        <h4 id="setNameWanita">{{$data->setnamewanita}}</h4>
                                        <p id="setDescWanita">{{$data->setdescwanita}}</p>
                                    </div>
                                </div>
                            </div>

                            <h2 style="
                                text-align: center;
                                font-size: 55px;
                                color: #222222a6;
                                margin-top: -50px;
                            ">&</h2>
                            
                            <div class="love_js_item row">
                                <div class="col-lg-7">
                                    <div class="love_text_item" style="
                                    opacity: 0.8;
                                    margin-right: 30px;
                                ">
                                        <h4 id="setNamePria">{{$data->setnamepria}}</h4>
                                        <p id="setDescPria">{{$data->setdescpria}}</p>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                 
                                    <div class="love_img"><img id="foto_pria" class="img-fluid" style="
                                        width: 350px;
                                        height: 350px;
                                        object-fit: cover;
                                        border: 2px solid white;
                                        /* box-shadow: 0px 0px 20px #00000066; */
                                    " src="{{asset('gallery/website_img/')}}/{{$data->foto_pria}}"
                                        alt=""></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!--================End Love Journey Area =================-->
        </div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <!--================Timer Area =================-->
            <section class="timer_area">
                <div class="container" style=" max-width: 100%;padding: 0;">
                    <div class="timer_inner p_120" style="padding-top: 10px;padding-bottom: 45px;">
                        <h5 style="font-size: 16px;">Until we're getting married</h5>
                        <div id="timer" class="timer">
                            <div class="timer__section days">
                                <div class="timer__number" style="color: #000000;"></div>
                                <div class="timer__label" style="font-size: 16px;color: #000000;">days</div>
                            </div>
                            <div class="timer__section hours">
                                <div class="timer__number" style="color: #000000;"></div>
                                <div class="timer__label" style="font-size: 16px;color: #000000;">hours</div>
                            </div>
                            <div class="timer__section minutes">
                                <div class="timer__number" style="color: #000000;"></div>
                                <div class="timer__label" style="font-size: 16px;color: #000000;">Minutes</div>
                            </div>
                            <div class="timer__section seconds">
                                <div class="timer__number" style="color: #000000;"></div>
                                <div class="timer__label" style="font-size: 16px;color: #000000;">seconds</div>
                            </div>
                        </div>
                        <p>are remaining</p>
                    </div>
                </div>
            </section>
            <!--================End Timer Area =================-->

      
        <!--================Journey Area =================-->
        <section class="journey_area" style="padding-top: 30px;">
        	<div class="container">
			
				<div class="row journey_inner">
					<div class="col-lg-6 col-pull-6">
                        <div class="journey_text_items">
                            <div class="journey_item" style="
                            min-height: 530px;
                        ">
                                <img src="{{asset('website/WS4/img/wedding.svg')}}" width="35px" height="35px" alt="">
                                <h4>AKAD NIKAH</h4>
                                <p>Vanya putridita <br>
                                    & <br>
                                    Mohammad fadillah abhipraya</p> <br><br>
                                {{-- <p>Acara akad nikah ini akan dilaksanakan pada :</p> --}}
								<div class="meta">
									<p><img src="{{asset('website/WS4/img/calendar.svg')}}" width="15px" height="15px" alt=""> Tanggal: <span id="setTglAkad">Sabtu, 20 Februari 2021</span></p>
									<p><img src="{{asset('website/WS4/img/calendar.svg')}}" width="15px" height="15px" alt=""> Mulai: <span id="setJam1Akad">{{$data->setjam1akad}} - Selesai</span> </p>
									{{-- <p><img src="{{asset('website/WS4/img/calendar.svg')}}" width="15px" height="15px" alt=""> Selesai: <span id="setJam2Akad">{{$data->setjam2akad}}</span> </p> --}}
                                    {{-- <p id="setTempatRes">Tempat : {{$data->settempatakad}}</p>
                                    <p>Alamat: <span id="setAlamatAkad">{{$data->setalamatakad}}</span> </p> --}}
								</div>
							</div>
                        </div>
                    </div>
					<div class="col-lg-6 col-push-6">
						<div class="journey_text_items">
							
							<div class="journey_item" style="
                            min-height: 530px;
                            padding: 40px;
                        ">
                                <img src="{{asset('website/WS4/img/script.svg')}}" width="35px" height="35px" alt="">
                                <h4>AKAD NIKAH</h4>
								{{-- <p>Acara Resepsi ini akan dilaksanakan pada :</p> --}}
                                <div class="meta">
                                    <p>
                                        Tanpa mengurangi rasa hormat, kami bermaksud memberitahukan 
                                        dan memohon doa restu penyelenggaraan akad nikah kami. Mengingat situasi dan 
                                        kondisi pandemic ini maka pihak keluarga akan mengikuti regulasi pemerintah 
                                        dengan menyelenggarakan acara sesuai protokol kesehatan dan mentaati regulasi 
                                        pembatasan tamu undangan yang akan hadir di acara akad dan resepsi
                                        <br><br>

                                        Namun, kami sangat mengharapkan kesediaan Bapak/Ibu/ Saudara untuk hadir menyaksikan secara virtual proses 
                                        akad nikah kami yang dapat di akses pada 
                                        <br><br><br>
                                        <a target="_blank" href="{{$data->link_live}}" class="genric-btn default circle arrow">Live Streaming<span class="lnr lnr-arrow-right"></span></a>
                              
                                    </p>
                                  
                                </div>
                            </div>
						</div>
					</div>
					
				</div>
        	</div>
        </section>
        <!--================End Journey Area =================-->
        </div>
        <div class="tab-pane fade" id="pills-gallery" role="tabpanel" aria-labelledby="pills-contact-tab">
          
            <section id="gallery" class="pt-100 pb-60">
				<div class="container">
					<div class="row headz justify-content-center">
						<div class="col-lg-6">
                            <div class="main_title">
                                <h2>Our Memories</h2>
                                <p>Gallery</p>
                            </div>						
						</div>
					</div>		
					<div class="row row-bottom-padded-md" style="
                    padding-bottom: 150px;
                ">
						<div class="col-md-12">
							<ul id="fh5co-gallery-list">
								
                                <li id="gallery_1" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_1}}'); ">
									<a class="color-2">
										
									</a>
								</li>
			
								<li id="gallery_2" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_2}}'); ">
									<a class="color-3">
									
									</a>
								</li>
								<li id="gallery_3" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_3}}'); ">
									<a class="color-4">
										
									</a>
								</li>
								<li id="gallery_4" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_4}}'); ">
									<a class="color-4">
										
									</a>
								</li>
							
							
							
							

							
							</ul>		
						</div>
					</div>
				</div>
			</section>
      
        </div>

        <div class="tab-pane fade" id="pills-tamu" role="tabpanel" aria-labelledby="pills-contact-tab">
          
            <section id="list-tamu" class="pt-100 pb-60">
				<div class="container">
					<div class="row headz justify-content-center">
						<div class="col-lg-6">
                            <div class="main_title">
                                {{-- <h2>Buku Tamu</h2>
                                <p>Kumpulan Ucapan dari Tamu</p> --}}
                            </div>						
						</div>
					</div>		
					<div class="row row-bottom-padded-md mb-3">
                        <div class="col-md-12">
                            <div class="journey_text_items">
							
                                <div class="journey_item" style="
                                min-height: 530px;
                                padding: 80px 80px;
                            ">
                                    <img src="{{asset('website/WS4/img/script.svg')}}" width="35px" height="35px" alt="">
                                    <h4>WEDDING GIFT</h4>
                                    {{-- <p>Acara Resepsi ini akan dilaksanakan pada :</p> --}}
                                    <div class="meta">
                                        <p>
                                            Mengingat adanya regulasi pembatasan jumlah tamu, bagi Bapak/Ibu/ Saudara 
                                            yang ingin memberikan tanda kasih kepada kedua mempelai dapat dilakukan dengan:
                                        </p>

                                        <div class="row">
                                            <div class="col-md-6" style="margin: 20px 0px;">

                                                <div style="
                                                border: 2px dashed #8a8a8a;
                                                color: #000;
                                                font-weight: 500;
                                                padding: 40px 25px;
                                                min-height: 135px;
                                            ">
                                                    <span style=" font-weight: 700;">pengiriman ke alamat</span>  : <br>
                                                    Jalan Taman Udayana VII No 6 Sentul City 16810 (Vanya)
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6" style="margin: 20px 0px;">
                                                <div style="
                                                border: 2px dashed #8a8a8a;
                                                color: #000;
                                                font-weight: 500;
                                                padding: 40px 25px;
                                                min-height: 135px;
                                            ">
                                                    <span style=" font-weight: 700;">Bank Transfer</span> : <br>
                                                    0060560692 - BCA - Vanya Putridita
                                                </div>
                                               
                                            </div>
                                        </div>

                                        <p>
                                            Kami sangat mengapresiasi dan bersyukur menerima tanda kasih dan kebaikan dari Bapak/Ibu/Saudara
                                        </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
					
                    </div>
				</div>
			</section>
      
        </div>
    </div>
    <!--================Header Menu Area =================-->

   

    

    @include('template.buku_tamu')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('website/WS4/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('website/WS4/js/popper.js')}}"></script>
    <script src="{{asset('website/WS4/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('website/WS4/js/stellar.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/lightbox/simpleLightbox.min.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/nice-select/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/isotope/isotope-min.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('website/WS4/vendors/flipclock/timer.js')}}"></script>
    <script src="{{asset('website/WS4/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('website/WS4/js/mail-script.js')}}"></script>
    <script src="{{asset('website/WS4/js/theme.js')}}"></script>
    <script src="{{asset('js/sound/fraudio.min.js')}}"></script>
    <script>
        timer.countdownDate = new Date("{{date('F d, Y', strtotime($data->settglres))}} 8:00:00 GMT+07:00");
        timer.countdownDate.setDate( timer.countdownDate.getDate());

        function set_audio() {
            var audio = document.getElementById("set_music");
            // audio.play();
            $('.fraudio-play').click();
        }
    </script>
    @include('template.act_buku_tamu')
</div>
</body>

</html>