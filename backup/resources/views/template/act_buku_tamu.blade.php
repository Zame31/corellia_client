<script>
    amplop();
     $(document).ready(function() {
        get_list_buku_tamu();
        $('#alert-tamu').hide();
        $('#form-tamu').on('keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                store();
            }
        });
    });

    function buku_tamu() {
            $("#nama").val('');
            $("#kehadiran").val('');
            $("#ucapan").val('');
            $('#alert-tamu').hide();

            $('#buku_tamu').modal('show');
        }
    
    function get_list_buku_tamu() {
          
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData();
        formData.append('website_id','{{$data->id}}');

        $.ajax({
            type: "POST",
            url: '{{ route('website.list_buku_tamu') }}',
            data : formData,
            dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
            beforeSend: function() {
            },
            success: function( msg ) {
                console.log(msg);
                $('#list_buku_tamu').html(``);
                msg.data.forEach(v => {

                    var jenis = ``;
                    if(v.kehadiran == 'Berkenan Hadir'){
                        jenis = `<span class="badge badge-primary">${v.kehadiran}</span>`;
                    }else{
                        jenis = `<span class="badge badge-danger">${v.kehadiran}</span>`;
                    }

                    $('#list_buku_tamu').append(`<div class="col-md-12 mb-2">
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-2 col-md-1 col-lg-1">
                                    <div style="
                                        background: #bb9276;
                                        text-align: center;
                                        color: #fff;
                                        font-size: 20px;
                                        font-weight: 600;
                                        border-radius: 50%;
                                        width: 50px;
                                        height: 50px;
                                        padding: 13px;
                                        text-transform: uppercase;"
                                    >${v.nama.charAt(0)}</div>
                                </div>
                                <div class="col-10 col-md-11 col-lg-11">
                                    <h5 style="
                                        margin-bottom: 0;
                                        font-weight: 600;
                                        text-transform: uppercase;
                                        color:#000;
                                    ">${v.nama}</h5>
                                    ${jenis}
                                    <h6 style="
                                        font-weight: 100;
                                        margin-top: 10px;
                                    ">${v.ucapan}</h6> 
                                </div>
                                
                            </div>
                        </div>
                    </div>`);
                });

            }
        }).done(function( msg ) {
        }).fail(function(msg) {

        });

      }

            

      function amplop() {
          $('#amplop').modal('show');
      }

      function store() {

          if($("#nama").val() == null || $("#kehadiran").val() == null || $("#ucapan").val() == null ||
             $("#nama").val() == '' || $("#kehadiran").val() == '' || $("#ucapan").val() == '' ){
              $('#alert-tamu').show();
              return false;
          }


          let myForm = document.getElementById('form-tamu');
          let formData = new FormData(myForm);
          formData.append('website_id','{{$data->id}}');

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

          $.ajax({
              type: "POST",
              url: '{{ route('website.store_buku_tamu') }}',
              data : formData,
              dataType:'JSON',
                  contentType: false,
                  cache: false,
                  processData: false,
              beforeSend: function() {
              },
              success: function( msg ) {
                  console.log(msg);
                  get_list_buku_tamu();
                  $('#buku_tamu').modal('hide');
                  
              }
          }).done(function( msg ) {
          }).fail(function(msg) {

          });
 
      }

</script>