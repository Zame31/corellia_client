@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<div class="pt-about">
				<div class="pt-img">
					<div class="pt-img-main">
						<div><img src=" {{asset('gallery/main/cdesign1.jpg')}} " class="js-init-parallax" data-orientation="up" data-overflow="true" data-scale="1.4" alt=""></div>
					</div>
					<div class="pt-img-sub">
						<div>
							<img src="{{asset('gallery/main/cdesign2.jpg')}}" class="js-init-parallax" data-orientation="down" data-overflow="true"  data-scale="1.4" alt="">
						</div>
					</div>
				</div>
				<div class="pt-description">
					<div class="pt-title">Design By Order</div>
					<p>Buat design undanganmu sendiri. sertakan judul, deskripsi, foto utama dan file tambahan lainnya. hubungi pihak corellia untuk konfirmasi design by order.</p>
                <br><br>
                    <div class="row ">
                        <div class="col-md-12 ">
                            {{-- <h2 class="pt-title-page text-center">Personal Information</h2> --}}
                            <form id="form-design" enctype="multipart/form-data" class="form-default form-layout-01" method="post" novalidate="novalidate" action="#">
                                {{-- <div class="form-group">
                                    <label for="creatFitstName">Nama Lengkap</label>
                                    <input type="text" name="name" class="form-control" id="creatFitstName" placeholder="Enter your name">
                                </div>
                                 --}}
                                 <div class="form-group">
                                    <label for="creatLastName">Judul Design</label>
                                    <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukan Judul Design">
                                </div>
                                <div class="form-group">
                                    <label for="creatEmail">Deskripsi Permintaan Design</label>
                                    <textarea maxlength="200" class="form-control" name="deskripsi" id="deskripsi" rows="3"></textarea>
                                </div>
                                <div class="form-group text-left">
                                    <label>Upload Image Utama</label>
                                    <input type="file" name="design" class="form-control" id="design"
                                         style="font-size: 12px;padding: 5px;">
                                    <small style="
                                                color: #d4d4d4;
                                            ">File yang di upload berupa JPG dan PNG. maksimal 2 Mb</small>
                                </div>
                                <div class="form-group text-left">
                                    <label>Upload File Tambahan</label>
                                    <input type="file" name="file_tambahan" class="form-control" id="file_tambahan"
                                         style="font-size: 12px;padding: 5px;">
                                    <small style="
                                                color: #d4d4d4;
                                            ">File yang di upload berupa ZIP. maksimal 10 Mb</small>
                                </div>
                              
                                <div class="row-btn">
                                    <button onclick="sendDesign()" type="submit" class="btn btn-block">KIRIM PERMINTAAN</button>
                                  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        
	</div>
</main>
<script>

    
function sendDesign() {



var validateData = $('#form-design').data('bootstrapValidator').validate();
if (validateData.isValid()) {


var formData = document.getElementById("form-design");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('undangan.set_design') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
        
    },

    success: function (response) {

        if (response.rc == 1) {
            $('#set_close_md').html('');
            $('#set_tittle_md').html('BERHASIL TERKIRIM');
            $('#set_text_md').html('Design by Order berhasil terkirim, mohon untuk menunggu konfirmasi dari pihak corellia.id');
            $('#set_action_md').html(`<a href="{{ route('customer.list_custom_design') }}" class="btn btn-block mt-4">LIHAT CUSTOM DESIGN</a>`);

            mSuccessShow();              
        }
        
    }

}).done(function (msg) {
    endLoadingPage();

}).fail(function (msg) {
    endLoadingPage();
});

}
}

$(document).ready(function () {
$("#form-design").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        deskripsi: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        judul: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        design: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                file: {
                        extension: 'jpg,jpeg,png',
                        type: 'image/jpg,image/jpeg,image/png',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        file_tambahan: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                file: {
                        extension: 'zip',
                        type: 'application/zip,application/x-compressed,application/x-zip-compressed,multipart/x-zip',
                        maxSize: 10 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});
</script>
@include('master.component.footer')
@stop