@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">Privacy Policy</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
						<div style="text-align: justify;">
							Adanya Kebijakan Privasi ini adalah komitmen nyata dari Corellia 
							untuk menghargai dan melindungi setiap data atau informasi pribadi 
							Pengguna situs www.corellia.id, situs-situs turunannya, 
							serta aplikasi gawai Corellia (selanjutnya disebut sebagai "Situs"). <br><br>

							Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs 
							Corellia sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi 
							lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, 
							pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk 
							pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan 
							kepada Corellia atau yang Corellia kumpulkan dari Pengguna, termasuk data 
							pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, 
							mengakses Situs, maupun mempergunakan layanan-layanan pada Situs 
							(selanjutnya disebut sebagai "data"). <br> <br>

							Dengan mengakses dan/atau mempergunakan layanan Corellia, Pengguna menyatakan 
							bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna 
							memberikan persetujuan kepada Corellia untuk memperoleh, mengumpulkan, menyimpan, 
							mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan 
							Privasi maupun Syarat dan Ketentuan Corellia. <br> <br>
							<ol type="A">
								<li> <b>Perolehan dan Pengumpulan Data Pengguna </b><br>
									Corellia mengumpulkan data Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:
									<ol>
										<li>Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas pada data yang diserahkan pada saat Pengguna:
										</li>
										<li>Membuat atau memperbarui akun Corellia, termasuk diantaranya nama pengguna (username), alamat email, nomor telepon, password, alamat, foto, dan lain-lain;
										</li>
										<li>
											Menghubungi Corellia, termasuk melalui layanan konsumen;</li>
										<li>
											Mengisi survei yang dikirimkan oleh Corellia atau atas nama Corellia;</li>
									</ol>
								</li>
							</ol>
							
							




						</div>
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop