<div class="modal fade" id="modal_acc_pembelian" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body kt-portlet kt-iconbox  kt-iconbox--animate-slow m-0 kt-iconbox--success">
                <div class="">
                    <div class="kt-portlet__body">
                        <div class="kt-iconbox__body">
                            <div class="kt-iconbox__icon ">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                        d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                        fill="#000000" opacity="0.3" />
                                    <path
                                        d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
                                        fill="#000000" />
                                    <path
                                        d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                        fill="#000000" />
                                </g>
                            </svg>
                            </div>
                            <div class="kt-iconbox__desc" style="width: 100%;">
                                <h3 class="kt-iconbox__title">
                                    <a class="kt-link" href="#" >
                                        Konfirmasi Pembelian
                                    </a>
                                </h3>
                                <div class="kt-iconbox__content">
                                    <span class="zn-iconbox-tittle">
                                        <div>
                                            <br>
                                            tambahkan catatan lainnya seperti estimasi selesai untuk pembelian ini. 
                                            <br>
                                         
                                            <div class="row">
                                                <div class="col-md-12 mb-3 mt-3">
                                                    <textarea name="notePembelian" placeholder="Tuliskan Estimasi Selesai dan Catatan lainnya" id="notePembelian" class="form-control" rows="4" cols="80"></textarea> 
                                                </div>
                                                <div class="col-md-12 scrollStyle">
                                                    <br><br>
                                                    tambahkan <i>additional item</i> untuk pembelian ini. 
                                                    <br><br>
                                                    @php
                                                        $dv = \DB::select("SELECT * from ref_additional");
                                                    @endphp
                                                    <div class="row">
                                                        <div onclick="add_item()" class="col-md-1">
                                                            <button type="button" class="btn btn-info btn-elevate btn-elevate-air" style="
                                                            width: 100%;
                                                        "><i class="flaticon2-plus"></i></button>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <select class="form-control" name="additional_select" id="additional_select">
                                                                <option value=""></option>
                                                                @foreach ($dv as $item)
                                                                <option id="ss{{$item->id}}" value="{{$item->id}}">{{$item->additional_name}} (Rp {{number_format($item->price,0,",",".")}})</option>
                                                              
                                                                @endforeach
                                                                </select>
                                                                <div class="invalid-feedback" id="sn">Harus Di isi</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text" id="set_jumlah" maxlength="3" placeholder="Jumlah" class="form-control">
                                                            <div class="invalid-feedback" id="sn">Harus Di isi</div>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="row zn-list-check">
                                                        <div class="col-md-2">Action</div>
                                                        <div class="col-md-8">
                                                            Additional Item
                                                        </div>
                                                        <div class="col-md-2">
                                                            Jumlah
                                                        </div>
                                                        
                                                    </div>
                                                    <div id="list_add" style="max-height: 150px;overflow-x: hidden;padding-right: 20px;overflow-y: auto;" class="scrollStyle">
                                                      

                                                    </div>
                                                   


                                                 
                                                    
                                                </div>
                                                <div class="col-md-12">

                                                </div>
                                            </div>
                                                
                                        </div>
                                        <div class="mt-3" id="zn-action-pembelian">

                                       

                                        </div>
                                    </span>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function validate_add() {
   
    let add_id = $('#additional_select').val();
    let jumlah = $('#set_jumlah').val();

    

    if(jumlah == '' || jumlah == null){
        $( "#additional_select" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#additional_select").removeClass( "is-invalid" );
    }
}
    let arr_data = [];

    function remove_item(id) {
        $("#la"+id).remove();
        
        var removeItem = id;

        arr_data = jQuery.grep(arr_data, function(value) {
            return value != removeItem;
        });

        $('#ss'+id).prop("disabled", false);
        console.log(arr_data);
        
    }

    function add_item() {

 
        let add_name = $('#additional_select option:selected').text();
        let add_id = $('#additional_select').val();

        let jumlah = $('#set_jumlah').val();

 

        if(add_id == '' || add_id == null){
            $( "#additional_select" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#additional_select").removeClass( "is-invalid" );
        }

        if(jumlah == '' || jumlah == null || jumlah == 0){
            $( "#set_jumlah" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#set_jumlah").removeClass( "is-invalid" );
        }

        $('#list_add').append(`
            <div id="la`+add_id+`" class="row zn-list-check">
                <div class="col-md-1">
                <button onclick="remove_item(`+add_id+`)" type="button" class="btn btn-danger btn-sm btn-elevate btn-elevate-air">
                <i class="flaticon-delete-1"></i></button></div>
                <div class="col-md-9">`+add_name+`</div>
                <div class="col-md-2">`+jumlah+`</div>
               
            </div>
        `);

        arr_data.push(add_id);

        console.log(arr_data);
        

        $('#additional_select option:selected').prop("disabled", true);
        $('#additional_select').val(null).trigger('change');
        $("#additional_select").select2("val", "");
        $('#set_jumlah').val(1);
    }

    $('#additional_select').select2({
        placeholder:"Pilih Additional Item"
    });
</script>