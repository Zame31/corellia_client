<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

var typeList = '{{$type}}';

console.log(typeList);

if (typeList == '0') {
    var trg = [5];
    var d_column =  [
        { data: 'action', name: 'action' },
        { data: 'no_pembelian', name: 'no_pembelian' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'phone_no', name: 'phone_no' },
        { data: 'total_pembelian', name: 'total_pembelian' }
    ]
}
else if (typeList == '9') {
    var trg = [5];
    var d_column =  [
        { data: 'action', name: 'action' },
        { data: 'no_pembelian', name: 'no_pembelian' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'phone_no', name: 'phone_no' },
        { data: 'total_pembelian', name: 'total_pembelian' },
        { data: 'disetujui_oleh', name: 'disetujui_oleh' }
    ]
}
else if (typeList == '2') {
    var trg = [5,6,7];
    var d_column =  [
        { data: 'action', name: 'action' },
        { data: 'no_pembelian', name: 'no_pembelian' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'phone_no', name: 'phone_no' },
        { data: 'total_pembelian', name: 'total_pembelian' },
        { data: 'billing', name: 'billing' },
        { data: 'hari_proses', name: 'hari_proses' },
        { data: 'disetujui_oleh', name: 'disetujui_oleh' }

    ]
}else {
    var trg = [5,6];
    var d_column =  [
        { data: 'action', name: 'action' },
        { data: 'no_pembelian', name: 'no_pembelian' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'phone_no', name: 'phone_no' },
        { data: 'total_pembelian', name: 'total_pembelian' },
        { data: 'hari_proses', name: 'hari_proses' },
        { data: 'disetujui_oleh', name: 'disetujui_oleh' }
    ]
}

var act_url = '{{ route('pembelian.data')}}?type='+typeList;
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: trg, className: 'text-right' },
        { "orderable": false, "targets": [0] }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.warning("Terjadi Kesalahan Saat Pengambilan Data,Koneksi bermasalah !");
            }
        },
    columns: d_column
});



function addTrace() {

    var no_pembelian =  $('#no_pembelian').html();
    var note_trace =  $('#note_trace').val();

    $.ajax({
        type: 'POST',
        url: '{{ route('pembelian.add_trace') }}',
        data: {
            no_pembelian:no_pembelian,
            note_trace:note_trace
        },

        beforeSend: function () {
            loadingPage();
        },
        success: function (response) {

        }

    }).done(function (msg) {
        endLoadingPage();
        showTrace(no_pembelian);
        $('#note_trace').val('');

    }).fail(function (msg) {
        endLoadingPage();
    });
}

function showTrace(no_pembelian) {

    $('#no_pembelian').html(no_pembelian);
    $.ajax({
        type: 'POST',
        url: '{{ route('pembelian.list_trace') }}',
        data: {
            no_pembelian:no_pembelian
        },

        beforeSend: function () {

        },
        success: function (response) {
            console.log(response.rm);
            $('#list_trace').html(response.rm);
        }

    }).done(function (msg) {
        $('#modal_trace').modal('show');
    }).fail(function (msg) {
    });


}

function pembelianSelesai(no_pembelian) {

    swal.fire({
            title: 'Selesaikan Pembelian',
            text: 'Yakin Pembelian Ini Telah Selesai ?',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#46C5EF",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                    loadingPage();

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('pembelian.pembelian_selesai') }}',
                        data: {
                            no_pembelian:no_pembelian
                        },

                        beforeSend: function () {

                        },
                        success: function (response) {
                            endLoadingPage();
                            toastr.success("Berhasil");
                        }

                    }).done(function (msg) {
                        location.reload();
                        // table.ajax.url(table).load();

                    }).fail(function (msg) {
                        endLoadingPage();
                        toastr.warning("Terjadi Kesalahan, Gagal Melakukan ");
                    });





            }
        });



}

</script>
