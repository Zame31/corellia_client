<script>

getData('product_id',1);

$("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                product_id: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                type_size: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                foilcolor: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                waxseal: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                emboss: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                paper_inner: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                paper_outer: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                free_finishing: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                free_additional: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                custid: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                bank: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                pengiriman: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                ongkir: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                qty: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },

            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

$('#custid').select2({
        placeholder:"Silahkan Pilih"
    });

$('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });


function setHarga() {
    
    let harga = $('#type_size').find(':selected').data('harga');
    
    $('#totalprice').val(numFormat(harga));    
}

function setTotal() {
    let ongkir = parseInt(clearNumFormat($('#ongkir').val()));
    let total = (clearNumFormat($('#totalprice').val())*$('#qty').val())+ongkir;
    $('#total_pembelian').val(numFormat(total));
}

function setOngkir(type) {
    if (type == 'dikirim') {
        $('#ongkir').prop('readonly', false); 
    }else {
        $('#ongkir').prop('readonly', true);
        $('#ongkir').val(0);
    }
    
}

function setMaterial() {
    
    let material = $('#type_size').find(':selected').data('material');
    
    if (material == 1) {
        $('#tittle_material').html('Material');
        $('#material_outer_sh').hide();
        $('#paper_inner').removeClass('zn-readonly');
    }else if (material == 2) {
        $('#tittle_material').html('Material Inner');
        $('#material_outer_sh').show();
        $('#paper_inner').removeClass('zn-readonly');
    // ONLY ARTPAPER
    } else {
        $('#tittle_material').html('Material');
        $('#material_outer_sh').hide();
        $('#paper_inner').val(1);
        $('#paper_inner').addClass('zn-readonly');

    }
}

function getData(type,id) {
   
    var act_url = '{{ route('invoice.get_data',["type"=>":type","id"=>":id"]) }}';
    act_url = act_url.replace(':type', type);
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            $('#'+res.type).html('');
            $('#'+res.type).append(`<option value="">Pilih</option>`);
            
            if (res.type == 'product_id') {
                $.each(res.rm.d, function (k,v) {
                    $('#'+res.type).append(`<option value="`+v.product_id+`">`+v.product_name+`</option>`);
                });
            }else if (res.type == 'type_size') {
                $.each(res.rm.d, function (k,v) {
                    $('#'+res.type).append(`<option data-material="`+v.material+`" data-harga="`+v.price+`" value="`+v.id+`">`+v.type+`</option>`);
                });
            }else if (res.type == 'color') {
                $.each(res.rm.d, function (k,v) {
                    $('#'+res.type).append(`<option value="`+v.id+`">`+v.color+`</option>`);
                });
            }else if (res.type == 'paper') {
                $.each(res.rm.d, function (k,v) {
                    $('#'+res.type).append(`<option value="`+v.id+`">`+v.type+`</option>`);
                });
            }else if (res.type == 'shape') {
                $.each(res.rm.d, function (k,v) {

                    $('#'+res.type).append(`<div class="col-md-4">
                        <label class="kt-radio kt-radio--bold">
                        <input  value="`+v.shape+`" type="radio" name="shape">
                            <img width="50px" src="{{ asset('gallery/shape/shape`+v.shape+`.png') }}" alt="" srcset="">
                            <span style="margin-top: 25px;"></span>
                        </label>
                    </div>`);
                });
            }
          


        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}



function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('invoice.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                        window.location.href = "{{ route('pembelian.index') }}?type=5";
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingModal();
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

</script>
