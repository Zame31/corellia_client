@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Buat Invoice kebutuhan Lainnya </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Buat Invoice kebutuhan Lainnya </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Invoice kebutuhan Lainnya </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="saveData()" class="btn kt-subheader__btn-secondary">
                    Simpan Invoice kebutuhan Lainnya
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Buat Invoice kebutuhan Lainnya
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jenis Produk</label>
                                <select onchange="getData('product_id',this.value); set_field_harga(this.value);" class="form-control" name="type" id="type">
                                    <option value="">Pilih Jenis Produk</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_type_produk where id in (2,3,4,6)");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->type_name}}</option>
                                    @endforeach
                                        <option value="99">Design By Order</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Produk</label>
                                <select onchange="setHarga();setTotal();" class="form-control" name="product_id" id="product_id">
                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="text" readonly class="form-control"  onkeyup="convertToRupiah(this)" name="totalprice" id="totalprice">
                            </div>
                            <div class="form-group">
                                <label>Jumlah</label>
                                <input type="text" value="1" maxlength="7" class="form-control" onkeyup="convertToNumber(this);setTotal();" name="qty" id="qty">
                            </div>
                            <div class="form-group">
                                <label>Total Bayar</label>
                                <input type="text" readonly class="form-control zn-readonly" name="total_pembelian" id="total_pembelian">
                            </div>
                        </div>
                        <div class="col-md-6">
                          
                            <div class="form-group">
                                <label>Pilih Customer</label>
                                <select class="form-control" name="custid" id="custid">
                                    <option value="">Pilih  Customer</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_customer ");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->cust_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Di Transfer Ke Bank</label>
                                <select class="form-control" name="bank" id="bank">
                                    <option value="">Pilih  Bank</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_bank ");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}} / {{$item->bank_acc_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Pengiriman</label>
                                <select onchange="setOngkir(this.value);setTotal();" class="form-control" name="pengiriman" id="pengiriman">
                                    <option value="diambil">Diambil Di Gallery</option>
                                    <option value="dikirim">Dikirim</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ongkos Kirim</label>
                                <input value="0" type="text" readonly maxlength="7" class="form-control" onkeyup="convertToRupiah(this);setTotal();" name="ongkir" id="ongkir">
                            </div>
                          
                        </div>
                     
                    </div>


                </form>
            </div>
        </div>
    </div>
@include('admin.invoice.action_other')
@stop
