<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jenis Produk</label>
                                <select class="form-control" name="type" id="type">
                                    <option value="">Pilih Jenis Produk</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_type_produk where id in (2,3,4)");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <input type="text" class="form-control" name="product_name" id="product_name">
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="price" id="price">
                            </div>
                           
                        </div>
                        <div class="col-md-6">
                          
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="desc" id="desc" class="form-control" style="height: 220px;" cols="30" rows="10"></textarea>
                                
                            </div>
                        </div>
                     
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-5 mt-3">
                            <h5>Upload Gambar Produk</h5>
                            <div>Format Gambar PNG,JPG maksimal 2 Mb.</div>
                            
                        </div>
                           
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                        <div id="ava1" class="kt-avatar__holder" style="width: 160px;
                                        height: 160px;background-size: 160px 160px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img1" id="img1">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                    <div>
                                        <small >Gambar Utama</small>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar2">
                                        <div id="ava2" class="kt-avatar__holder" style="width: 160px;
                                        height: 160px;background-size: 160px 160px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img2" id="img2">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar3">
                                        <div id="ava3" class="kt-avatar__holder" style="width: 160px;
                                        height: 160px;background-size: 160px 160px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img3" id="img3">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar4">
                                        <div id="ava4" class="kt-avatar__holder" style="width: 160px;
                                        height: 160px;background-size: 160px 160px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img4" id="img4">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
