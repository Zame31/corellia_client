<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});



    var d_column =  [
        { data: 'action', name: 'action' },
        { data: 'status_name', name: 'status_name' },
        { data: 'status_pengerjaan_design', name: 'status_pengerjaan_design' },
        { data: 'no_pembelian', name: 'no_pembelian' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'phone_no', name: 'phone_no' }
    ]


var act_url = '{{ route('design_fix.data')}}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": [0] }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.warning("Terjadi Kesalahan Saat Pengambilan Data,Koneksi bermasalah !");
            }
        },
    columns: d_column
});




</script>
