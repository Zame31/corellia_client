@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Konfirmasi Pembayaran </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Pembelian </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>

            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="#" onclick="confAct('pembayaran_approve','pembayaran_approve?no_pembelian={{$data->no_pembelian}}&kode_pembayaran={{$data->kode_pembayaran}}','{{ route('pembelian.index') }}?type=1');" class="btn btn-success kt-subheader__btn-options" aria-expanded="false">
                    Setujui Pembayaran
                </a>
                <a href="#" onclick="confAct('pembayaran_reject','pembayaran_reject?no_pembelian={{$data->no_pembelian}}&kode_pembayaran={{$data->kode_pembayaran}}','{{ route('pembelian.index') }}?type=1');" class="btn btn-danger kt-subheader__btn-options" aria-expanded="false">
                    Tolak Pembayaran
                </a>
                <a href="#" onclick="loadNewPage('{{ route('pembelian.index') }}?type=1')"
                    class="btn kt-subheader__btn-secondary" aria-expanded="false">
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--head-lg">
       
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{asset('gallery/pembayaran')}}/{{$data->bukti_pembayaran}}" class="img-fluid" style="
                            border: 1px solid #ebedf2;
                        " alt="">
                </div>
                <div class="col-md-7">
                    <h4 class="mb-4" style="
                    color: #1dc9b7;
                ">Detail Pembayaran</h4>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="mb-4">
                                <h6>Kode Pembayaran</h6>
                                <h5> {{$data->kode_pembayaran}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Mengirim Konfirmasi</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($data->created_at))}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Atas Nama</h6>
                                <h5> {{$data->atas_nama}} </h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-4">
                                <h6>Dari Bank</h6>
                                <h5> {{$data->bank}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Nomer Rekening</h6>
                                <h5> {{$data->no_rek}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Nominal Transfer</h6>
                                <h5>Rp {{number_format($data->nominal_transfer,0,",",".")}} </h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-4">
                                <h6>Catatan Pembeli</h6>
                                <h5> {{($data->catatan) ? $data->catatan: " Tidak ada catatan "}} </h5>
                            </div>
                        </div>
                    </div>
                   
                    <h4 class="mb-4 mt-5" style="
                    color: #1dc9b7;
                ">Detail Pembelian</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="mb-4">
                                <h6>Kode Pembelian</h6>
                                <h5> {{$pembeli->no_pembelian}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Pembelian</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($pembeli->created_at))}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Jatuh Tempo</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($pembeli->end_date))}} </h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mb-4">
                                <h6>Transfer Ke Bank</h6>
                                <h5> {{$pembeli->bank_name}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Nama Customer</h6>
                                <h5> {{$pembeli->cust_name}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>No Telpon</h6>
                                <h5> {{$pembeli->phone_no}} </h5>
                            </div>
                        </div>
                        <div class="col-md-4">

                         
                            <div class="mb-4">
                                <h6>Total Pembelian</h6>
                                <h5>Rp {{number_format($pembeli->total_pembelian,0,",",".")}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Sisa Pembayaran</h6>
                                <h5>Rp {{number_format($pembeli->billing,0,",",".")}} </h5>
                            </div>
                        </div>
                    </div>
                  
                </div>
             
            </div>
        </div>
    </div>
</div>
@stop
