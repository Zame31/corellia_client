@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Master Home </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Master Home </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Master Home </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="saveData()" class="btn kt-subheader__btn-secondary">
                    Simpan
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid" style="text-align: center;">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Master Home
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">


                <form id="form-data">
                    <input type="hidden" name="get_id" value="{{$data->id}}" id="get_id">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <h4>PROMO</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Promo Header</label>
                                <select class="form-control type" name="promo_head" id="promo_head">
                                    <option value="">Pilih Promo</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_promo");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->judul}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Promo Sub Header 1</label>
                                <select class="form-control type" name="promo_subhead1" id="promo_subhead1">
                                    <option value="">Pilih Promo</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_promo");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->judul}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Promo Sub Header 2</label>
                                <select class="form-control type" name="promo_subhead2" id="promo_subhead2">
                                    <option value="">Pilih Promo</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_promo");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->judul}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                     
                    </div>

                    <div class="row zn-border-home">
                        <div class="col-md-12 mb-3">
                            <h4>BANNER PROMO</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Banner Promo 1</label>
                                <div class="form-group mt-4">
                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                            <div id="ava1" class="kt-avatar__holder" style="width: 320px;
                                            height: 320px;background-size: 320px 320px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="img1" id="img1">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="promo_banner_desc1" id="promo_banner_desc1" cols="30" rows="5">{{$data->promo_banner_desc1}}</textarea>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Banner Promo 2</label>
                                <div class="form-group mt-4">
                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar2">
                                            <div id="ava2" class="kt-avatar__holder" style="width: 320px;
                                            height: 320px;background-size: 320px 320px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="img2" id="img2">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="promo_banner_desc2" id="promo_banner_desc2" cols="30" rows="5">{{$data->promo_banner_desc2}}</textarea>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Banner Promo 3</label>
                                <div class="form-group mt-4">
                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar3">
                                            <div id="ava3" class="kt-avatar__holder" style="width: 320px;
                                            height: 320px;background-size: 320px 320px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="img3" id="img3">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                </div>
                             </div>
                             <div class="form-group">
                                <textarea class="form-control" name="promo_banner_desc3" id="promo_banner_desc3" cols="30" rows="5">{{$data->promo_banner_desc3}}</textarea>
                           
                             </div>
                        </div>
                     
                    </div>

                    <div class="row zn-border-home">
                        <div class="col-md-12 mb-3">
                            <h4>TOP 3 PRODUK</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Produk 1</label>
                                <div class="form-group mt-3">
                                    <label>Jenis Produk</label>
                                    <select onchange="getData('product_id',this.value,'product_id1');" 
                                    class="form-control type" name="type1" id="type1">
                                        <option value="">Pilih Jenis Produk</option>
                                        @php
                                            $getJenis = \DB::select("SELECT * FROM ref_type_produk");
                                        @endphp
                                        @foreach ($getJenis as $item)
                                            <option value="{{$item->id}}">{{$item->type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Produk</label>
                                    <select class="form-control product_id" name="product_id1" id="product_id1">
                                       
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Produk 2</label>
                                <div class="form-group mt-3">
                                    <label>Jenis Produk</label>
                                    <select onchange="getData('product_id',this.value,'product_id2');" 
                                    class="form-control type" name="type2" id="type2">
                                        <option value="">Pilih Jenis Produk</option>
                                        @php
                                            $getJenis = \DB::select("SELECT * FROM ref_type_produk");
                                        @endphp
                                        @foreach ($getJenis as $item)
                                            <option value="{{$item->id}}">{{$item->type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Produk</label>
                                    <select class="form-control product_id" name="product_id2" id="product_id2">
                                       
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Produk 3</label>
                                <div class="form-group mt-3">
                                    <label>Jenis Produk</label>
                                    <select onchange="getData('product_id',this.value,'product_id3');" 
                                    class="form-control type" name="type3" id="type3">
                                        <option value="">Pilih Jenis Produk</option>
                                        @php
                                            $getJenis = \DB::select("SELECT * FROM ref_type_produk");
                                        @endphp
                                        @foreach ($getJenis as $item)
                                            <option value="{{$item->id}}">{{$item->type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Produk</label>
                                    <select class="form-control product_id" name="product_id3" id="product_id3">
                                       
                                    </select>
                                </div>
                            </div>
                        </div>
                     
                    </div>


                </form>
            </div>
        </div>
    </div>
{{-- @include('admin.invoice.modal') --}}

@include('admin.mst_home.action')
@stop
