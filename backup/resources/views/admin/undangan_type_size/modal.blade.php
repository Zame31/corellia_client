<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Undangan</label>
                                <select class="form-control" name="product_id" id="product_id">
                                    <option value="">Pilih Undangan</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_undangan");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->product_id}}">{{$item->product_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Style & Size Undangan</label>
                                <input type="text" class="form-control" name="type" id="type">
                            </div>
                           
                            <div class="form-group">
                                <label>Material</label>
                                <select class="form-control" name="material" id="material">
                                    <option value="">Pilih Material</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_material");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->material}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Harga </label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="price" id="price">
                                <small>Harga untuk 700 pcs keatas</small>
                            </div>
                            <div class="form-group">
                                <label>Minimal Pembelian </label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="min_pembelian" id="min_pembelian">
                            </div>
                           
                        </div>
                     
                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
