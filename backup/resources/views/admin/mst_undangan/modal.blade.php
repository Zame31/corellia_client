<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label>Nama Undangan</label>
                                <input type="text" class="form-control" name="product_name" id="product_name">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <input type="text" class="form-control" name="desc" id="desc">
                            </div>
                            <div class="form-group">
                                <label>Format</label>
                                <select class="form-control" name="format" id="format">
                                    <option value="">Pilih Format</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_format");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Style</label>
                                <select class="form-control" name="style" id="style">
                                    <option value="">Pilih Style</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_style");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Waxseal</label>
                                <select class="form-control" name="waxseal" id="waxseal">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Berat (Satuan Gram)</label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="berat" id="berat">
                            </div>
                           
                        </div>
                        <div class="col-md-6">
                          
                           
                        </div>
                     
                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
