<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Template Web</label>
                                <input type="text" class="form-control"  name="product_name" id="product_name">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Harga per Minggu</label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="price" id="price">
                            </div>
                            {{-- <input type="hidden" class="form-control"  name="color" id="color" value="1"> --}}
                            <div class="form-group">
                                <label>Warna</label>
                                <select class="form-control" name="color" id="color">
                                    <option value="">Pilih Warna</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_style");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 mb-5 mt-3">
                            <h5>Upload Gambar Template Web</h5>
                            <div>Format Gambar PNG,JPG maksimal 2 Mb.</div>
                            <div class="form-group mt-5">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                        <div id="ava1" class="kt-avatar__holder" style="width: 230px;
                                        height: 230px;background-size: 230px 230px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img" id="img">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                    <div>
                                        {{-- <small >Gambar Utama</small> --}}
                                    </div>
                            </div>
                        </div>
                       

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
