<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

var act_url = '{{ route('design_order.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        // { data: 'action', name: 'action' },
        { data: 'image', name: 'image' },
        { data: 'code', name: 'code' },
        { data: 'judul', name: 'judul' },
        { data: 'deskripsi', name: 'deskripsi' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'phone_no', name: 'phone_no' },

        { data: 'cust_id', name: 'cust_id' },
        { data: 'file_tambahan', name: 'file_tambahan' },
    ]
});

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#form-data').bootstrapValidator("resetForm", true);
    $('#get_id').val('');
    
    clearImage();
   
}


$('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });

function clearImage() {
    let imageUrl = '{{asset('admin/img/no_img.png')}}';

    $('.kt-avatar__holder').css("background-image", "url(" + imageUrl + ")")

    $('#kt_user_add_avatar').removeClass('kt-avatar--changed');
    $('#kt_user_add_avatar2').removeClass('kt-avatar--changed');
    $('#kt_user_add_avatar3').removeClass('kt-avatar--changed');
    $('#kt_user_add_avatar4').removeClass('kt-avatar--changed');

    $('#img1').val('');
    $('#img2').val('');
    $('#img3').val('');
    $('#img4').val('');
}

function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");
    $("#form-data")[0].reset();

    clearImage();
   

    var act_url = '{{ route('design_order.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            
            console.log(res.rm.other);
            
              $('#get_id').val(res.rm.other.id);
              $('#product_id').val(res.rm.other.product_id);
              $('#color').val(res.rm.other.color);
              $('#shape').val(res.rm.other.shape);

              $("input[value='" + res.rm.other.shape + "']").prop('checked', true);


            $.each(res.rm.img, function (k,v) {

                let imageUrl = "{{asset('gallery/product')}}/"+v.img;

                $('#ava1').css("background-image", "url(" + imageUrl + ")")

                

            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}


function del(id) {

    var act_url = '{{ route('design_order.delete', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Delete Data',
        text: "Anda Yakin Akan Menghapus Data ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $.ajax({
                    type: "POST",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Dihapus");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('design_order.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Dihapus");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Data tidak dihapus','error')
        }
    });
}



$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            product_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            color: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            shape: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            img1: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    file: {
                            extension: 'jpg,jpeg,png',
                            type: 'image/jpg,image/jpeg,image/png',
                            maxSize: 2 * (1024*1024),
                            message: 'File Tidak Sesuai'
                        }
                }
            },
           

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});


function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('design_order.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('design_order.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

</script>
