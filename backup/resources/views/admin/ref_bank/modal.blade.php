<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" enctype="multipart/form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Bank</label>
                                <input type="text" class="form-control" name="bank_name" id="bank_name">
                            </div>
                         
                            <div class="form-group">
                                <label>No Rekening</label>
                                <input type="text" maxlength="20" class="form-control" onkeyup="convertToNumber(this)" name="bank_acc_no" id="bank_acc_no">
                            </div>
                            <div class="form-group">
                                <label>Atas Nama</label>
                                <input type="text" class="form-control" name="an" id="an">
                            </div>
                            <div class="form-group">
                                <label>Logo</label>
                                <img id="set_logo" class="mt-2 mb-4" style="display:block;width: 100px;" src="" alt="">
                                <input type="file" class="form-control" name="bank_img" id="bank_img">
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
