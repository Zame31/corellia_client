<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

var act_url = '{{ route('ref_bank.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'bank_name', name: 'bank_name' },
        { data: 'bank_img', name: 'bank_img' },
        { data: 'bank_acc_no', name: 'bank_acc_no' },
        { data: 'an', name: 'an' }

    ]
});

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#form-data').bootstrapValidator("resetForm", true);
    $('#get_id').val('');
    $('#set_logo').attr("src", "");
   
}


$('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });

function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");
    $("#form-data")[0].reset();

    var act_url = '{{ route('ref_bank.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {
              $('#get_id').val(v.id);
              $('#bank_name').val(v.bank_name);
              $('#bank_acc_no').val(v.bank_acc_no);
              $('#an').val(v.an);
              $('#set_logo').attr("src", "{{asset('img')}}/"+v.bank_img);

            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}


function del(id) {

    var act_url = '{{ route('ref_bank.delete', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Delete Data',
        text: "Anda Yakin Akan Menghapus Data ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $.ajax({
                    type: "POST",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Dihapus");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('ref_bank.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Dihapus");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Data tidak dihapus','error')
        }
    });
}



$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            bank_name: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:100,
                          message: 'Maksimal 100 Karakter'
                      }
                }
            },
            bank_acc_no: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:100,
                          message: 'Maksimal 100 Karakter'
                      }
                }
            },
            an: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:100,
                          message: 'Maksimal 100 Karakter'
                      }
                }
            },
            bank_img: {
                validators: {
                    file: {
                            extension: 'jpg,jpeg,png',
                            type: 'image/jpg,image/jpeg,image/png',
                            maxSize: 2 * (1024*1024),
                            message: 'File Tidak Sesuai'
                        }
                }
            }
           

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});


function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('ref_bank.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('ref_bank.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

</script>
