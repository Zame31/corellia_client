<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Waxseal</label>
                                <input type="text" class="form-control"  name="name" id="name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Harga Waxseal</label>
                                <input type="text" onkeyup="convertToRupiah(this)" class="form-control"  name="harga" id="harga">
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <h5>Upload Gambar Waxseal</h5>
                            <div>Format Gambar PNG,JPG maksimal 2 Mb.</div>
                            <div class="form-group mt-5">
                                <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                        <div id="ava1" class="kt-avatar__holder" style="width: 230px;
                                        height: 230px;background-size: 230px 230px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="img" id="img">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                    <div>
                                        {{-- <small >Gambar Utama</small> --}}
                                    </div>
                            </div>
                        </div>
                       

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
