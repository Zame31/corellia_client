@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Referensi Waxseal </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Referensi Waxseal </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    List Waxseal </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="showModalAdd()" class="btn kt-subheader__btn-secondary">
                    Create New Data
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Waxseal List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">Action</th>
                                    <th>Nama Waxseal</th>
                                    <th>Harga</th>
                                    <th>Gambar</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
            </div>
        </div>
    </div>
@include('admin.ref_waxseal.modal')

@include('admin.ref_waxseal.action')
@stop
