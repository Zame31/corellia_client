<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kode Voucher</label>
                                <input type="text" class="form-control" name="code" id="code">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="desc" id="desc" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Potongan Harga</label>
                                <input type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this)" name="amount" id="amount">
                            </div>
                            <div class="form-group">
                                <label>Potongan Persentasi</label>
                                <input type="text" maxlength="2" class="form-control" onkeyup="convertToRupiah(this)" name="percentage" id="percentage">
                            </div>
                            <div class="form-group">
                                <label>Expired Date</label>
                                <input type="text" class="form-control" name="expdt" id="expdt">
                             
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
