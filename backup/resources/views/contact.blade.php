@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">Contact Us</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
					
						<dd>
							<p>
								Anda dapat kontak kami melalui:
								<ul class="pt-list-dot">
									<li>Admin Bandung :<br>  081 1248 6660 <br> (Call : 12.00 - 16.00 WIB) <br> (Whatsapp : 11.00 - 18.00)</li>
									<li class="mt-4">Admin Garut :<br>  0853 1117 1120 <br>(Call : 12.00 - 16.00 WIB) <br> (Whatsapp : 11.00 - 18.00)</li>
								</ul>
							</p>
						</dd>
						
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop