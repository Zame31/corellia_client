@section('content')
{{-- <div class="pt-breadcrumb">
    <div class="container-fluid">
        <ul>
            <li>List</li>
            <li><a href="#">{{$type}}</a></li>
        </ul>
    </div>
</div> --}}
<div class="container-indent" style="padding-bottom: 150px;margin-top: 10px;">
    <div class="container container-fluid-custom-mobile-padding">
            <div class="pt-layout-promo-box pt-offset-bottom">
                    <div class="row">
                        <div class="col-12 text-center">
                            <a class="pt-promo-box pt-extra-responsive">
                                <div class="image-box" style="height: 200px;">
                                    <img src="{{asset('assets/images/promo/index07-promo-06.jpg')}}" alt="">

                                </div>
                                <div class="pt-description pr-promo-type2 pt-point-h-l text-left">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-small" style="font-size:14px;"><span>SEMUA PRODUK</span></div>
                                        <div class="pt-title-large" style="text-transform: uppercase;"><span> {{$tittle}} <br></span></div>
                                    </div>
                                 </div>
                            </a>
                        </div>
                      
                    </div>
                </div>
        {{-- <h1 class="pt-title-subpages noborder">KATALOG UNDANGAN</h1> --}}
        
        <div class="row">
            <div class="col-md-4 col-lg-3 col-xl-3 leftColumn aside">
                <div class="pt-btn-col-close">
                    <a href="#">
                        <span class="pt-icon">
                            <svg>
                                <use xlink:href="#icon-close"></use>
                            </svg>
                        </span>
                        <span class="pt-text">
                            Close
                        </span>
                    </a>
                </div>
                <div class="pt-collapse open pt-filter-detach-option">
                    <div class="pt-collapse-content">
                        <div class="filters-mobile">
                            <div class="filters-row-select">

                            </div>
                        </div>
                    </div>
                </div>
                @if ($type == 'undangan')
                     @include('katalog.filter_left')
                @else
                    @include('katalog.filter_left_other')
                @endif
            </div>
            <div class="col-md-12 col-lg-9 col-xl-9">
                <div class="content-indent">
                    @if ($type == 'undangan')
                        @include('katalog.filter_top')
                    @else
                        @include('katalog.filter_top_other')
                    @endif
                   
                    <div id="list_katalog" class="pt-product-listing row">
                    </div>
       
                        
                    <div class="text-center pt_product_showmore">
                        {{-- <a href="#" class="btn btn-border">LOAD MORE</a> --}}
                        {{-- <div class="pt_item_all_js"> --}}
                            <a href="#" class="btn btn-border01">NO MORE ITEM TO SHOW</a>
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('katalog.action')
@stop
