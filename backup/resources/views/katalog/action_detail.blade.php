@php
     if(Auth::user()){
        $isLogin = 'y';
     }else {
        $isLogin = 'n';
     }

     if ($type == 1) {
        $stmp_color = $design_color[0]->id;
        $stmp_printing = $design_foilcolor[0]->id;
        $stmp_u_shape = $design_shape[0]->shape;
     }else {
        $stmp_color = 1;
        $stmp_printing = 1;
        $stmp_u_shape = 1;
     }
@endphp
<script>


let type = '{{$type}}';
if (type == 2) {
    set_qty(300);
}
if (type == 1) {

    setHarga();
    setMaterial();
    setMinPembelian();
}

function setAddHarga() {

    let qty = $('#qty').html();

    for (let ii = 0; ii < arr_data.length; ii++) {
        let stotal = arr_data[ii]['harga']*qty;
        $('#aiTotal'+arr_data[ii]['id']).html(stotal);
        arr_data[ii]['jumlah'] = qty;
        arr_data[ii]['total'] = stotal;
    }



}

function setHarga() {

    let harga = $('#type_size').find(':selected').data('harga');

    $('#get_harga').html('Rp '+numFormat(harga));
    $('#price').html(numFormat(harga));
    set_total();


}

let arr_data = [];

function remove_item(id) {
    $("#la"+id).remove();

    var removeItem = id;

    arr_data = jQuery.grep(arr_data, function(value) {
        return value.id != removeItem;
    });

    $('#ss'+id).prop("disabled", false);
    console.log(arr_data);

}

function add_item() {
    let add_name = $('#additional_select').find(':selected').data('name');
    let add_harga = $('#additional_select').find(':selected').data('harga');

    let add_id = $('#additional_select').val();

    let jumlah = $('#set_jumlah').val();

    let add_total = add_harga*jumlah;


    if(add_id == '' || add_id == null){
            $( "#additional_select" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#additional_select").removeClass( "is-invalid" );
        }

    if(jumlah == '' || jumlah == null || jumlah == 0){
        $( "#set_jumlah" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#set_jumlah").removeClass( "is-invalid" );
    }

    $('#list_add').append(`
        <tr id="la`+add_id+`">
            <td class="text-left"><a onclick="remove_item(`+add_id+`)" class="la la-minus-circle" style="font-size: 30px;display: inline;color: #ff8484;">
                    </a></td>
            <td class="text-left zn-font-table">
                    `+add_name+`
            </td>
            <td id="aiHarga`+add_id+`" class="text-right zn-font-table">`+numFormat(add_harga)+`</td>
            <td class="aiJumlah zn-font-table" id="aiJumlah`+add_id+`" >`+jumlah+`</td>
            <td id="aiTotal`+add_id+`" class="text-right zn-font-table">`+numFormat(add_total)+`</td>
        </tr>
    `);

    let sdata = [];

    sdata['id'] = add_id;
    sdata['name'] = add_name;
    sdata['jumlah'] = jumlah;
    sdata['total'] = add_total;
    sdata['harga'] = add_harga;

    arr_data.push(sdata);

    console.log(arr_data);


    $('#additional_select option:selected').prop("disabled", true);
    $("#additional_select").val("");
    // $('#set_jumlah').val(1);
}

function setMaterial() {

    let material = $('#type_size').find(':selected').data('material');

    if (material == 1) {
        $('.tittle_material').html('Material');
        $('.material_outer_sh').hide();
        $("input[name=material_inner]").prop('disabled', false);
    }else if (material == 2) {
        $('.tittle_material').html('Material Inner');
        $("input[name=material_inner]").prop('disabled', false);
        $('.material_outer_sh').show();

        // ONLY ARTPAPER
    } else {
        $('.tittle_material').html('Material');
        $('.material_outer_sh').hide();
        $("input[name=material_inner][value='1']").prop('checked', true);
        $("input[name=material_inner]").prop('disabled', true);
        set_material_paper('inner');
        // $('#material_inner').val(1);

    }
}

function setMinPembelian() {
    let minpembelian = $('#type_size').find(':selected').data('minpembelian');
    $('#set_val_qty').val(minpembelian);
    // console.log(minpembelian);

    set_qty(minpembelian);

}

function set_total_wax() {
    let harga = $('#waxseal_set').find(':selected').data('harga');
    var qty = $('#qty').html();
    $('#tmp_total_wax').html(numFormat(harga*qty));
}

function set_total() {
    var qty = $('#qty').html();

    var price = $('#price').html();
    var voucher_amount = $('#voucher_amount').html();
    var voucher_percentage = $('#voucher_percentage').html();
    var s_per = $('#set_percentage').val();

    price = clearNumFormat(price);
    voucher_amount = clearNumFormat(voucher_amount);
    voucher_percentage = clearNumFormat(voucher_percentage);

    var tmp_total = qty * price;

    var p_per = (tmp_total*s_per)/100;

    var total = tmp_total - voucher_amount;

    tmp_total = numFormat(tmp_total);
    total = numFormat(total);

    $('#voucher_percentage').html(numFormat(p_per));
    $('#tmp_total').html(tmp_total);
    $('#total').html(total);
}

function set_waxseal(id) {


    let harga = $('#waxseal_set').find(':selected').data('harga');
    $('#price_wax').html(numFormat(harga));

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_waxseal') }}',
        data: {
            id:id
        },
        beforeSend: function () {
            $('#set_waxseal').html('<div class="loader">Loading...</div>');

        },
        success: function (response) {
            console.log(response.rm);

            $('#set_waxseal').html(response.rm);
            initZoomImg();
            // endLoadingPage();

        }

    }).done(function (msg) {
        tmp_u_waxseal = id;

    }).fail(function (msg) {
        // endLoadingPage();
    });

}

function set_material_paper(type) {

    let material_inner = $(".material_inner:checked").val();
    let material_outer = $(".material_outer:checked").val();
    let material = 0;
    if (type == 'outer') {
        material = material_outer;
    } else {
        material = material_inner;

    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_paper') }}',
        data: {
            id:material
        },
        beforeSend: function () {
            if (type == 'outer') {
                $('#set_material_paper_outer').html('<div class="loader">Loading...</div>');
            } else {
                $('#set_material_paper').html('<div class="loader">Loading...</div>');
            }


        },
        success: function (response) {
            console.log(response.rm);

            if (type == 'outer') {
                $('#set_material_paper_outer').html(response.rm);
            }else {
                $('#set_material_paper').html(response.rm);
            }
            initZoomImg();

            // endLoadingPage();

        }

    }).done(function (msg) {
        // tmp_u_waxseal = id;

    }).fail(function (msg) {
        // endLoadingPage();
    });

}

function set_printing(id) {
    if (id == 0) {
        $('#set_img_custom').removeClass('foil-image');
    }else {
        $('#set_img_custom').addClass('foil-image');
    }

    tmp_printing = id;
}

function generateVoucher() {
    var voucher = $('#voucher').val();

    console.log(voucher);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_voucher') }}',
        data: {
            id:voucher
        },

        beforeSend: function () {
            // loadingPage();
        },
        success: function (response) {
            console.log(response.rm);

            var msg = response.rm;

            // endLoadingPage();
            if (response.rc == 99) {
                shownotif('Voucher Tidak Ditemukan','other');
                $('#voucher_desc').html('Voucher Tidak Ditemukan');
            }else{
                shownotif('Voucher Berhasil Digunakan','other');

                var tmp_total = $('#tmp_total').html();

                tmp_total = clearNumFormat(tmp_total);

                var p_per = (tmp_total*response.rm.percentage)/100;


                $('#set_percentage').val(response.rm.percentage);

                $('#voucher_amount').html(numFormat(response.rm.amount));
                // $('#voucher_percentage').html(numFormat(p_per));
                $('#voucher_desc').html(response.rm.desc);

                set_total();
            }
        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });


}


var tmp_color = '{{$stmp_color}}';
var tmp_printing = '{{$stmp_printing}}';
var tmp_u_shape = '{{$stmp_u_shape}}';
var tmp_u_paper = '1';
var tmp_u_waxseal = '0';

function set_filter_color(data) {
    tmp_color = data;
}
function set_filter_u_shape(data) {
    tmp_u_shape = data;
}

function set_filter_u_paper(data) {
    tmp_u_paper = data;
}

function set_design_color(product_id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.design_foilcolor') }}',
        data: {
            product_id:product_id,
            color:tmp_color
        },
        beforeSend: function () {
            // $('#list_katalog').html('<div class="loader">Loading...</div>');

        },
        success: function (response) {
            $('#set_foil_color').html(response.rm);


            tmp_printing = $('#set_foil_color').val();

            get_custom_design('{{$data->product_id}}');

            // endLoadingPage();

        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });


}


function initZoomImg() {
    $jsLayoutGallery = $('#js-layout-gallery2');
            $jsLayoutGallery.find('.pt-gallery-link').magnificPopup({
                type: 'image',
                mainClass: 'mfp-zoom-in',
                closeBtnInside: true,
                gallery: {
                    enabled: true,
                    tCounter: '<span class="mfp-counter"></span>'
                },
                callbacks: {
                    open: function () {
                        $('.mfp-gallery').append('<button title="Close (Esc)" type="button" class="mfp-close">×</button>');
                    },
                    close: function () {
                        $('.mfp-gallery .mfp-close').remove();
                        setTimeout(function () {
                            objSmallGallery.removeClass('pt-magnific-popup').find('.link-magnific-popup').remove();
                        }, 200);
                    }
                }
            });
}


function initZoomImgKat() {
    $jsLayoutGallery = $('#js-layout-gallery');
            $jsLayoutGallery.find('.pt-gallery-link').magnificPopup({
                type: 'image',
                mainClass: 'mfp-zoom-in',
                closeBtnInside: true,
                gallery: {
                    enabled: true,
                    tCounter: '<span class="mfp-counter"></span>'
                },
                callbacks: {
                    open: function () {
                        $('.mfp-gallery').append('<button title="Close (Esc)" type="button" class="mfp-close">×</button>');
                    },
                    close: function () {
                        $('.mfp-gallery .mfp-close').remove();
                        setTimeout(function () {
                            objSmallGallery.removeClass('pt-magnific-popup').find('.link-magnific-popup').remove();
                        }, 200);
                    }
                }
            });
}


function setSessionUndangan(name,datas) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_session_undangan') }}',
        data: {
            name:name,
            datas:datas
        },

        beforeSend: function () {

        },
        success: function (response) {
            console.log(response.rm);
        }

    }).done(function (msg) {
    }).fail(function (msg) {
    });


}

function get_custom_design(product_id) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_custom_design') }}',
        data: {
            color:tmp_color,
            product_id:product_id,
            printing:tmp_printing,
            u_shape:tmp_u_shape,
            u_paper:tmp_u_paper

        },
        beforeSend: function () {
            // $('#set_img_custom').fadeOut(3000);

        },
        success: function (response) {
            // console.log(response.rm);
            $('#set_img_custom').hide().html(response.rm).fadeIn('slow');

            $('.set_img_undangan_lainnya').html(response.rimg);
            initZoomImg();
            initZoomImgKat();
            // $('#set_img_custom').fadeOut(300, function(){
            //     $('#set_img_custom').html(response.rm).bind('onreadystatechange load', function(){
            //         if (this.complete) $('#set_img_custom').fadeIn(300);
            //     });
            // });

            // $('#set_img_custom').html(response.rm);

            // endLoadingPage();

        }

    }).done(function (msg) {

    }).fail(function (msg) {
        // endLoadingPage();
    });


}

function setCartlistOther(id,type){
    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') {




        var qty = $('#qty').html();
        console.log(qty);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_cartlist') }}',
        data: {
            id:id,
            type:type,
            qty:qty,
            },
        beforeSend: function () {
            loadingPage();
        },

        success: function (response) {

                if (response.rc == 1) {
                    shownotif(response.rm,'cart');
                    $('#set_count_cartlist').html(response.jumlah);
                    location.href = "{{ route('customer.cartlist') }}";
                }

            }

        }).done(function (msg) {
            endLoadingPage();

        }).fail(function (msg) {
            endLoadingPage();
        });


    }else{
        mLoginShow();
    }
}

function setCartlistUndangan(id,type){

    let tmp_add = [];
    let tmp_add_count = [];
    let tmp_finishing = [];
    let tmp_free_add = [];


    for (let ii = 0; ii < arr_data.length; ii++) {
        tmp_add.push(arr_data[ii]['id']);
        tmp_add_count.push(arr_data[ii]['jumlah']);

    }

    $("input[name='finishing[]']:checked").each( function () {
        tmp_finishing.push($(this).val());
	});

    $("input[name='free_add[]']:checked").each( function () {
        tmp_free_add.push($(this).val());
	});



    var qty = $('#qty').html();

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') {


        $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_cartlist') }}',

        data: {
            id:id,
            type:type,
            tmp_color:tmp_color,
            tmp_printing:tmp_printing,
            tmp_u_shape:tmp_u_shape,
            tmp_u_paper:tmp_u_paper,
            tmp_u_waxseal:tmp_u_waxseal,
            qty:qty,
            type_size:$('#type_size').val(),
            paper_inner:$("#material_inner:checked").val(),
            paper_outer:$("#material_outer:checked").val(),
            free_finishing:tmp_finishing,
            free_additional:tmp_free_add,
            additional_user:tmp_add,
            additional_user_count:tmp_add_count,
            emboss:$('#emboss').val(),
            },
        beforeSend: function () {
            loadingPage();
        },

        success: function (response) {

                if (response.rc == 1) {
                    shownotif(response.rm,'cart');
                    location.href = "{{ route('customer.cartlist') }}";

                }

                $('#set_count_cartlist').html(response.jumlah);

            }

        }).done(function (msg) {
            endLoadingPage();

        }).fail(function (msg) {
            endLoadingPage();
        });


    }else{

        $.ajax({
            type: 'POST',
            url: '{{ route('customer.set_session_undangan') }}',
            data: {
                id:id,
                type:type,
                tmp_color:tmp_color,
                tmp_printing:tmp_printing,
                tmp_u_shape:tmp_u_shape,
                tmp_u_paper:tmp_u_paper,
                tmp_u_waxseal:tmp_u_waxseal,
                qty:qty,
                type_size:$('#type_size').val(),
                paper_inner:$("#material_inner:checked").val(),
                paper_outer:$("#material_outer:checked").val(),
                free_finishing:tmp_finishing,
                free_additional:tmp_free_add,
                additional_user:tmp_add,
                additional_user_count:tmp_add_count
            },

            beforeSend: function () {

            },
            success: function (response) {
                console.log(response.rm);
            }

        }).done(function (msg) {
        }).fail(function (msg) {
        });
        mLoginShow();
    }
}


function set_qty(value) {

    if (type == 2) {
        if (value < 300) {
            shownotif('Jumlah Pembelian Minimal 300','error');
            $('#qty').html(300);
            $('#set_jumlah').val(300);
            $('#set_val_qty').val(300);

        }else {
            $('#qty').html(value);
            $('#set_jumlah').val(value);
            set_total();
        }
    }else if(type == 1){

        let minpembelian = $('#type_size').find(':selected').data('minpembelian');
        let harga = $('#type_size').find(':selected').data('harga');

         // SET MINIMAL PEMBELIAN
         if (value < minpembelian) {
            // console.log(minpembelian);

            shownotif('Jumlah Pembelian Minimal '+minpembelian,'error');
            $('#qty').html(minpembelian);
            $('#set_jumlah').val(minpembelian);
            $('#set_val_qty').val(minpembelian);
            $('#qty_wax').val(minpembelian);
            $('.aiJumlah').html(minpembelian);


        }else {

            let disc = 0;
            // if (value > 300 && value < 499) {
            //     disc = 500;
            // }else if(value > 500 && value < 699){
            //     disc = 1000;
            // }else if(value > 700){
            //     disc = 1500;
            // }

            if (value >= 200 && value <= 299) {
                disc = 2000;
            }else if(value >= 300 && value <= 499){
                disc = 1000;
            }else if(value >= 500 && value <= 699){
                disc = 500;
            }else if(value >= 700){
                disc = 0;
            }
            $('.aiJumlah').html(value);

            $('#get_harga').html('Rp '+numFormat(harga+disc));
            $('#price').html(numFormat(harga+disc));



            $('#qty').html(value);
            $('#set_jumlah').val(value);
            set_total();
        }







        // $('#qty').html(value);
        // $('#set_jumlah').val(value);
        // set_total();


    }else {
        $('#qty').html(value);
        $('#set_jumlah').val(value);
        set_total();
    }



}

</script>
