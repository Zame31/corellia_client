@section('content')

<div class="container container-fluid-mobile" style="padding-top:50px;padding-bottom: 150px;font-size: 14px;">
    <div class="row">
        <div class="col-6">
            <div class="pt-product-single-img no-zoom">
                <div>
                    <img class="zoom-product" src='{{asset("gallery/product/$di_u->img")}}' data-zoom-image="{{asset("gallery/product/$di_u->img")}}" alt="">
                    <button class="pt-btn-zoom js-btnzoom-slider">
                        <svg>
                            <use xlink:href="#icon-quick_view"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="product-images-carousel">
                <ul id="smallGallery" class="arrow-location-02  slick-animated-show">
                    <li><a class="zoomGalleryActive" href="#" data-image='{{asset("gallery/product/$di_u->img")}}' data-zoom-image='{{asset("gallery/product/$di_u->img")}}'><img src='{{asset("gallery/product/$di_u->img")}}' alt=""></a></li>
                   
                    @foreach ($di as $v)
                        <li><a href="#" data-image="{{asset('gallery/product/'.$v->img)}}" data-zoom-image="{{asset('gallery/product/'.$v->img)}}"><img src="{{asset('gallery/product/'.$v->img)}}" alt=""></a></li>
                    @endforeach            
                </ul>
            </div>

        </div>
        <div class="col-6">
            <div class="pt-product-single-info">
                {{-- <div class="pt-wrapper">
                    <div class="pt-label">
                        <div class="pt-label pt-label-new">Baru</div>
                    </div>
                </div> --}}
                <h1 class="pt-title">{{$data->product_name}}</h1>
                <div class="pt-price" style="font-size: 20px;">
                    Rp {{number_format($data->price,0,",",".")}}
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <b>Berat : </b> {{$data->berat}} g
                    </div>
                   
                </div>
                <div class="mt-4 text-justify">{{$data->desc}}</div>

                <div class="pt-wrapper">
                    <div class="product-information-buttons">

                        <a data-toggle="modal" data-target="#modalShipping" href="#">
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-services_delivery"></use>
                                </svg>
                            </span>
                            <span class="pt-text">
                                Pengiriman
                            </span>
                        </a>
                        {{-- <a data-toggle="modal" data-target="#modalInfo" href="#">
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-services_size_guide"></use>
                                </svg>
                            </span>
                            <span class="pt-text">
                                Informasi Tambahan
                            </span>
                        </a> --}}
                    </div>
                </div>

                
                <div class="pt-wrapper">
                    <div class="pt-row-custom-01">
                        <div class="col-item">
                            <div class="pt-input-counter style-01">
                                <span class="minus-btn">
                                    <i class="la la-minus"></i> 
                                </span>
                                
                            <input id="set_val_qty" onkeyup="set_qty(this.value);" onchange="set_qty(this.value);" 
                            type="text" value="{{($type == 2) ? '300' :'1' }}"  maxlength="4" size="9999">
                            <span class="plus-btn">
                                <i class="la la-plus"></i> 
                            </span>
                            </div>
                        </div>
                        <div class="col-item">
                            <a onclick="setWishlist('{{$data->product_id}}','{{$data->type}}')" class="btn btn-lg btn-border" style="color:#000000;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-wishlist"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO WISHLIST
                                </span>
                            </a>
                        </div>
                        <div class="col-item">
                            <a onclick="setCartlistOther('{{$data->product_id}}','{{$data->type}}')" class="btn btn-lg" style="color:#ffffff;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-cart_1"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO CART
                                </span>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="pt-wrapper mt-5 mb-1">
                    <div class="pt-table-03-indent">
                        <table class="pt-table-03">
                            <tbody>
                                <tr>
                                    <td style="width:60%">{{$data->product_name}}</td>
                                    <td width="10px">Rp</td>
                                    <td id="price" class="text-right">{{number_format($data->price,0,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td style="width:60%">Qty</td>
                                    <td width="10px"></td>
                                    <td class="text-right" id="qty">1</td>
                                </tr>
                                {{-- <tr>
                                    <td style="width:60%;font-weight: bold;color: #c89c7d;">Total Produk</td>
                                    <td width="10px" style="font-weight: bold;color: #c89c7d;">Rp</td>
                                    <td id="tmp_total" class="text-right" style="color: #c89c7d;">{{number_format($data->price,0,",",".")}}</td>
                                </tr> --}}
                                <tr style="display:none;">
                                    <td style="width:60%">Potongan Harga dari Voucher</td>
                                    <td width="10px">Rp</td>
                                    <td class="text-right" id="voucher_amount">0</td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width:60%">Diskon dari Voucher</td>
                                    <td width="10px">Rp</td>
                                    <td class="text-right" id="voucher_percentage">0</td>
                                    <input id="set_percentage" type="hidden" value="">
                                </tr>

                                <tr>
                                    <td style="width:60%;font-weight: bold;color: #c89c7d;">Total Payment</td>
                                    <td width="10px" style="font-weight: bold;color: #c89c7d;">Rp</td>
                                    <td id="total" class="text-right" style="color: #c89c7d;">{{number_format($data->price,0,",",".")}}</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="row mt-5">
                        <div class="col-md-12">
                            <div style="font-size:12px;margin-bottom:5px;">
                                    Masukan Kode Voucher Untuk Mendapatkan Potongan Harga
                            </div>
                                
                        </div>
                        <div class="col-md-9">
                            <form id="form-voucher" class="form-default form-layout-01" method="post" novalidate="novalidate" action="#">
                                <div class="form-group">
                                    <div class="pt-required mb-3">* Masukan Kode Voucher Untuk Mendapatkan Potongan Harga</div>
                                    <input type="text" maxlength="10" id="voucher" name="voucher" class="form-control" id="creatFitstName" placeholder="Kode Voucher">
                                    <div id="voucher_desc" style="font-size:12px;margin-bottom:5px;color:#c89c7d;">
                                            
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3 text-right">
                            <a onclick="generateVoucher()" class="btn btn-sm btn-border">
                                <span class="pt-text">
                                    PAKAI
                                </span>
                            </a>
                        </div>
                    </div> --}}
                    
                </div>
                {{-- <div class="pt-wrapper">
                    <div class="pt-row-custom-03">
                        <div class="col-item">
                            <a href="#" class="btn btn-dark">
                                <span class="pt-text">
                                    BELI SEKARANG
                                </span>
                            </a>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>

@include('master.component.modal_shipping')
@include('master.component.modal_info_produk')
@include('katalog.action_detail')

@stop
