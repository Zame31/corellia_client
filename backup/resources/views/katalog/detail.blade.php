@section('content')
@php
     if(Auth::user()){
        $isLogin = 'y';
     }else {
        $isLogin = 'n';
     }

     $data_session = Session::get('sess_undangan');


     if ($type == 1) {
            
            $stmp_color = $data_session['tmp_color'];
            $stmp_printing = $data_session['tmp_printing'];
            $stmp_u_shape = $data_session['tmp_u_shape'];
            $stmp_u_waxseal = $data_session['tmp_u_waxseal'];
            $stmp_paper_inner = $data_session['paper_inner'];
            $stmp_paper_outer = $data_session['paper_outer'];
            $stmp_finishing = $data_session['tmp_finishing'];
            $stmp_free_add = $data_session['tmp_free_add'];
            $stmp_qty = $data_session['qty'];
            $stmp_type_size = $data_session['type_size'];


         }else{
            $stmp_color = $design_color[0]->id;
            $stmp_printing = $design_foilcolor[0]->id;
            $stmp_u_shape = $design_shape[0]->shape;
            $stmp_u_waxseal = '';
            $stmp_paper_inner = '';
            $stmp_paper_outer = '';
            $stmp_finishing = '';
            $stmp_free_add = '';
            $stmp_qty = '';
            $stmp_type_size = '';

         }
     
@endphp

<div class="container container-fluid-mobile" style="padding-top:50px;padding-bottom: 150px;font-size: 14px;">
    <div class="row">
        <div class="col-12">

        </div>
        <div class="col-6 ">
            <div class="pt-product-single-img no-zoom">
                <div>


                        <div id="set_img_custom" class="foil-image">
                            <style>
                                .img {
                                width:100%;
                                height:600px;
                                background: url('{{asset("gallery/product/$di_u->img")}}') center center no-repeat;
                                background-size:100% auto;
                                }
                            </style>
                            <div id="set_foilcolor" class="img zoom-product">
                            </div>
                        </div>

                    {{-- <img class="zoom-product" src='{{asset("gallery/product/$di_u->img")}}' data-zoom-image="{{asset("gallery/product/$di_u->img")}}" alt=""> --}}
                    <button class="pt-btn-zoom js-btnzoom-slider">
                        <svg>
                            {{-- <use xlink:href="#icon-quick_view"></use> --}}
                        </svg>
                    </button>
                </div>
            </div>

            <div class="row mt-4 mb-4" data-scrollzoom="false" id="js-layout-gallery">
                <div class="col-md-12">
                        <div class="row set_img_undangan_lainnya">
                            <div class="col-md-3 col-xs-3">
                                @if ($di_u != '')
                                <div class="item">
                                    <a href="{{asset("gallery/product/$di_u->img")}}"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="{{asset("gallery/product/$di_u->img")}}" alt=""></a>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-3 col-xs-3">
                                @if ($img2 != '')
                                <div class="item">
                                    <a  href="{{asset("gallery/product/$img2->img")}}"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="{{asset("gallery/product/$img2->img")}}" alt=""></a>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-3 col-xs-3">
                                @if ($img3 != '')
                                <div class="item">
                                    <a href="{{asset("gallery/product/$img3->img")}}"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="{{asset("gallery/product/$img3->img")}}" alt=""></a>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-3 col-xs-3">
                                @if ($img4 != '')
                                <div class="item">
                                    <a href="{{asset("gallery/product/$img4->img")}}"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="{{asset("gallery/product/$img4->img")}}" alt=""></a>
                                </div>
                                @endif
                            </div>
                        </div>

                </div>




            </div>

            <div class="row mt-4 mb-4" data-scrollzoom="false" id="js-layout-gallery2">
                <div class="col-md-12">
                    <div class="row mt-4">
                        <div class="col-md-3 col-xs-6">
                            <div style="font-weight:bold;font-size: 12px;">Waxseal</div>
                            <div id="set_waxseal" class="zn-img-tambahan zn-fit-center">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div style="font-weight:bold;font-size: 12px;" class="tittle_material">Material</div>
                            <div id="set_material_paper" class="zn-img-tambahan zn-fit-center">
                        </div>
                        </div>
                        <div class="col-md-3 material_outer_sh">
                            <div style="font-weight:bold;font-size: 12px;">Material Outer</div>
                            <div id="set_material_paper_outer" class="zn-img-tambahan zn-fit-center">
                        </div>
                        </div>
                    </div>
                </div>
            </div>







        </div>
        <div class="col-6">
            <div class="pt-product-single-info">
                {{-- <div class="pt-wrapper">
                    <div class="pt-label">
                        <div class="pt-label pt-label-new">Baru</div>
                    </div>
                </div> --}}
                <h1 class="pt-title">{{$data->product_name}}</h1>
                <div class="row mt-5">
                    <div class="col-md-4">
                        Type & Size
                    </div>
                    <div class="col-md-8">
                        <select onchange="setHarga();setMaterial();setMinPembelian();"  class="form-control" name="type_size" id="type_size">
                            @php
                                $dw = \DB::select("select * from ref_type_size where product_id ='".$data->product_id."'");
                            @endphp
                            @foreach ($dw as $vv)
                                <option data-minpembelian="{{$vv->min_pembelian}}" data-material="{{$vv->material}}" data-harga="{{$vv->price}}" value="{{$vv->id}}">{{$vv->type}}</option>
                            @endforeach

                        </select>


                    </div>


                    <div class="col-md-12 mt-4">
                        Deskripsi <br>
                        <small>{{$data->desc}}</small>
                    </div>
                    <div class="col-md-12 mt-4">
                        {{-- <small>Undangan ini sudah dilengkapi dengan fitur emboss pada beberapa bagian undangan.</small> --}}
                    </div>
                </div>
                <div class="row" style="
                border-top: 2px dashed #ebebeb;
                padding-top: 20px;
                margin-top: 20px;
            ">
                    <div class="col-md-4">
                        Harga
                    </div>
                    <div id="get_harga" class="col-md-8 pt-price" style="font-size: 20px;">
                        Rp {{number_format($data->price,0,",",".")}}
                    </div>
                </div>
                {{-- <div class="row mt-3">
                    <div class="col-md-4">
                        Berat per item
                    </div>
                    <div class="col-md-8">
                        {{$data->berat}} g
                    </div>
                </div>              --}}
                <div class="row mt-3">
                    <div class="col-md-4">
                        Pilihan Warna
                    </div>
                    <div class="col-md-8">
                        <ul class="pt-options-swatch">
                            @foreach ($design_color as $dc)
                                <li class="{{($stmp_color == $dc->id) ? 'active':''}}"><a onclick="set_filter_color({{$dc->id}}); set_design_color('{{$data->product_id}}');" class="options-color pt-color-bg-04 pt-border" style="background: {{$dc->code}};" href="#"></a></li>

                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4">
                        Shape
                    </div>
                    <div class="col-md-8">
                        <ul class="pt-options-swatch size-middle">
                            @foreach ($design_shape as $ds)
                            <li class="{{($stmp_u_shape == $ds->shape) ? 'active':''}}" ><a href="#" onclick="set_filter_u_shape({{$ds->shape}}); get_custom_design('{{$data->product_id}}');" style="padding: 0;height:100%;">
                                <img width="50px" src="{{ asset('gallery/shape/shape'.$ds->shape.'.png') }}" alt="" srcset=""></a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4">
                        Foil Color
                    </div>
                    <div class="col-md-8">
                        <select id="set_foil_color" onchange="set_printing(this.value); get_custom_design('{{$data->product_id}}');" class="form-control">

                            @foreach ($design_foilcolor as $vv)
                                <option {{($stmp_printing == $vv->id) ? 'selected':''}} value="{{$vv->id}}">{{$vv->name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="row mt-3">
                    <div class="col-md-4">
                        Waxseal
                    </div>
                    <div class="col-md-8">
                        <select id="waxseal_set" onchange="set_waxseal(this.value);set_total_wax();" class="form-control">
                            @php
                                $dw = \DB::select('select * from ref_waxseal');
                            @endphp
                            @foreach ($dw as $vv)
                                <option data-harga="{{$vv->harga}}"  {{($stmp_u_waxseal == $vv->id) ? 'selected':''}} value="{{$vv->id}}">{{$vv->name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="row mt-3" style="display:none;">
                    <div class="col-md-4">
                        Emboss
                    </div>
                    <div class="col-md-8">
                        <select class="form-control" name="emboss" id="emboss">
                            <option value="0">Tidak Digunakan</option>
                           <option value="1">Digunakan</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4 tittle_material" id="tittle_material">
                        Material Inner
                    </div>
                    <div class="col-md-8">


                        @php
                        $d_paper = \DB::select('select * from ref_paper');
                    @endphp
                        @foreach ($d_paper as $dp)
                        <label class="radio mt-3" data-toggle="tooltip" data-placement="bottom" title="{{$dp->desc}}">
                            <input {{($stmp_paper_inner == $dp->id) ? 'checked':''}} onchange="set_material_paper('inner')" class="material_inner" id="material_inner" value="{{$dp->id}}"
                                type="radio" name="material_inner">
                            <span class="outer"><span class="inner"></span></span>
                            {{$dp->paper_name}}<br>
                            <small></small>

                        </label>
                        @endforeach


                        {{-- <input type="button" value="">
                        <select class="form-control" name="material_inner" id="material_inner">
                            @php
                            $d_paper = \DB::select('select * from ref_paper');
                        @endphp
                            @foreach ($d_paper as $dp)
                                <option value="{{$dp->id}}">{{$dp->paper_name}} </option>
                            @endforeach

                        </select> --}}
                    </div>
                </div>
                <div class="row mt-3 material_outer_sh" id="material_outer_sh">
                    <div class="col-md-4">
                        Material Outer
                    </div>
                    <div class="col-md-8">
                        @php
                            $d_paper = \DB::select('select * from ref_paper');
                        @endphp
                        @foreach ($d_paper as $dp)
                        <label class="radio mt-3" data-toggle="tooltip" data-placement="bottom" title="{{$dp->desc}}">
                            <input onchange="set_material_paper('outer')" class="material_outer" id="material_outer" value="{{$dp->id}}"
                                type="radio" name="material_outer">
                            <span class="outer"><span class="inner"></span></span>
                            {{$dp->paper_name}}
                        </label>
                        @endforeach

                        {{-- <select class="form-control" name="material_outer" id="material_outer">
                            <option value="0">Tidak Digunakan</option>
                            @php
                                $d_paper = \DB::select('select * from ref_paper');
                            @endphp
                            @foreach ($d_paper as $dp)
                                <option value="{{$dp->id}}">{{$dp->paper_name}} </option>
                            @endforeach

                        </select> --}}
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-4">
                        Free Finishing
                    </div>
                    <div class="col-md-8">
                        @php
                        $dv = \DB::select("SELECT * from ref_finishing");
                        @endphp
                            @foreach ($dv as $item)
                            <div class="checkbox-group" style="display:block;">
                                <input name="finishing[]" type="checkbox" value="{{$item->id}}" id="finishing{{$item->name}}">
                                <label for="finishing{{$item->name}}">
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    <h3 class="pt-collapse-title zn-checkbox-list">
                                        {{$item->name}}
                                    </h3>
                                </label>
                            </div>
                            @endforeach

                        {{-- <select class="form-control" name="finishing" id="finishing">
                        <option value="0">Tidak Digunakan</option>
                        @foreach ($dv as $item)
                        <option  value="{{$item->id}}">{{$item->name}}</option>

                        @endforeach
                    </select> --}}
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-4">
                        Free Additional
                    </div>
                    <div class="col-md-8">
                        @php
                        $dv = \DB::select("SELECT * from ref_additional where price = 0");
                    @endphp
                            @foreach ($dv as $item)
                            <div class="checkbox-group" style="display:block;">
                                <input name="free_add[]" type="checkbox" value="{{$item->id}}" id="free_add{{$item->additional_name}}">
                                <label for="free_add{{$item->additional_name}}">
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    <h3 class="pt-collapse-title zn-checkbox-list">
                                        {{$item->additional_name}}
                                    </h3>
                                </label>
                            </div>
                            @endforeach
                        {{-- @php
                            $dv = \DB::select("SELECT * from ref_additional where price = 0");
                        @endphp
                        <select class="form-control" name="free_add" id="free_add">
                            <option value="0">Tidak Digunakan</option>
                            @foreach ($dv as $item)
                            <option value="{{$item->id}}">{{$item->additional_name}}</option>

                            @endforeach
                        </select> --}}
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        Tambahan Additional Item
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="row">
                            <div class="col-md-10">
                                @php
                                $dv = \DB::select("SELECT * from ref_additional where price <> 0");
                            @endphp
                            <select class="form-control" name="additional_select" id="additional_select">
                                <option value="">Pilih Additional Item</option>
                                @foreach ($dv as $item)
                                <option data-harga="{{$item->price}}" data-name="{{$item->additional_name}}" id="ss{{$item->id}}" value="{{$item->id}}">{{$item->additional_name}} (Rp {{number_format($item->price,0,",",".")}})</option>

                                @endforeach
                            </select>
                            <div class="invalid-feedback" id="sn">Harus Di isi</div>
                            </div>
                            <input hidden type="text" onkeyup="convertToNumber(this)" style="text-align:center;" id="set_jumlah" maxlength="3" placeholder="Jumlah" value="1" class="form-control zn-readonly">
                                <div class="invalid-feedback" id="sn">Harus Di isi</div>

                            <div class="col-md-2">
                                <a onclick="add_item()" class="btn btn-sm" style="color:#ffffff;height: 40px;">
                                    <i class="la la-plus" style="
                                    font-size: 30px;
                                "></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="pt-wrapper">
                    <div class="product-information-buttons">

                        <a data-toggle="modal" data-target="#modalShipping" href="#">
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-services_delivery"></use>
                                </svg>
                            </span>
                            <span class="pt-text">
                                Pengiriman
                            </span>
                        </a>
                        <a data-toggle="modal" data-target="#modalInfo" href="#">
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-services_size_guide"></use>
                                </svg>
                            </span>
                            <span class="pt-text">
                                Informasi Tambahan
                            </span>
                        </a>
                    </div>
                </div>


                <div class="pt-wrapper">
                    <div class="pt-row-custom-01">
                        <div class="col-item">
                            <div class="pt-input-counter style-01">
                                <span class="minus-btn">
                                    <i class="la la-minus"></i>
                                </span>
                                <input id="set_val_qty" onkeyup="set_qty(this.value);setAddHarga();set_total_wax();"
                                onchange="set_qty(this.value);setAddHarga();set_total_wax();" type="text" value="1" maxlength="4" size="9999">
                                <span class="plus-btn">
                                    <i class="la la-plus"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-item">
                            <a onclick="setWishlist('{{$data->product_id}}','{{$data->type}}')" class="btn btn-lg btn-border" style="color:#000000;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-wishlist"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO WISHLIST
                                </span>
                            </a>
                        </div>
                        <div class="col-item">
                            <a onclick="setCartlistUndangan('{{$data->product_id}}','{{$data->type}}')" class="btn btn-lg" style="color:#ffffff;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-cart_1"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO CART
                                </span>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="pt-wrapper mt-5 mb-1">
                    <div class="pt-accordeon-content" style="display: block;">
                        <div class="tt-table-responsive-md">
                            <div class="pt-table-size">
                                <style>
                                    @media (max-width: 789px){
                                        .tt-table-responsive-md>:first-child {
                                            width: 100%;
                                        }
                                        .zn-font-table{
                                            font-size: 10px;
                                        }
                                    }

                                    /* .pt-table-size table td:first-child, .pt-table-size table th:first-child {
                                        padding-left: 20px;
                                        padding-right: 5px;
                                        font-weight: 800;
                                        width: 10px;
                                    } */
                                </style>
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="zn-font-table">Produk</th>
                                            <th class="zn-font-table">Harga</th>
                                            <th class="zn-font-table">Jumlah</th>
                                            <th class="zn-font-table">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:10%;" class="zn-font-table">{{$data->product_name}}</td>
                                            <td id="price" class="zn-font-table text-right">{{number_format($data->price,0,",",".")}}</td>
                                            <td id="qty" class="zn-font-table">20</td>
                                            <td id="tmp_total" class="text-right zn-font-table" style="color: #c89c7d;">{{number_format($data->price,0,",",".")}}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%;" class="zn-font-table">Waxseal</td>
                                            <td id="price_wax" class="text-right zn-font-table">0</td>
                                            <td id="qty_wax" class="aiJumlah zn-font-table">1</td>
                                            <td id="tmp_total_wax" class="text-right zn-font-table  " style="color: #c89c7d;">0</td>
                                        </tr>
                                        <tr style="display:none;">
                                            <td style="width:60%" class="zn-font-table">Potongan Harga dari Voucher</td>
                                            <td width="10px" class="zn-font-table">Rp</td>
                                            <td class="text-right zn-font-table" id="voucher_amount">0</td>
                                        </tr>
                                        <tr style="display:none;">
                                            <td style="width:60%" class="zn-font-table">Diskon dari Voucher</td>
                                            <td width="10px" class="zn-font-table">Rp</td>
                                            <td class="text-right zn-font-table" id="voucher_percentage">0</td>
                                            <input id="set_percentage" type="hidden" value="">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="pt-wrapper mt-5 mb-1">
                    <div class="pt-accordeon-content" style="display: block;">
                        <div class="tt-table-responsive-md">
                            <div class="pt-table-size">
                                <table>
                                    <thead>
                                        <tr>
                                            <th style="width:10%"></th>
                                            <th style="width:50%" class="text-left zn-font-table">Additional Item</th>
                                            <th class="text-right zn-font-table">Harga</th>
                                            <th class="text-right zn-font-table">Jumlah</th>
                                            <th class="text-right zn-font-table">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list_add">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
                {{-- <div class="pt-wrapper">
                    <div class="pt-row-custom-03">
                        <div class="col-item">
                            <a href="#" class="btn btn-dark">
                                <span class="pt-text">
                                    BELI SEKARANG
                                </span>
                            </a>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="pt-layout-product-info-02">
                    <h6 class="pt-title-modal text-left">Informasi Tambahan</h6>
                    <div class="pt-table-03-indent">
                            <table class="pt-table-03">
                                <tbody>

                                    <tr>
                                        <td>Style</td>
                                        <td>{{$data->name_style}}</td>
                                    </tr>
                                    <tr>
                                        <td>Orientation</td>
                                        <td>{{$data->name_orientation}}</td>
                                    </tr>
                                    <tr>
                                        <td>Format</td>
                                        <td>{{$data->name_format}}</td>
                                    </tr>
                                    <tr>
                                        <td>Design By</td>
                                        <td>Corellia</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Pembelian</td>
                                        <td>
                                            <div>
                                                <div>200pcs - 299pcs </div>
                                                <small style="font-size: 12px;color: #00000057;
                                                font-weight: 100;">Penambahan Harga Rp 2.000</small>
                                            </div>
                                            <br>
                                            <div>
                                                <div>300pcs - 499pcs </div>
                                                <small style="font-size: 12px;color: #00000057;
                                                font-weight: 100;">Penambahan Harga Rp 1.000</small>
                                            </div>
                                            <br>
                                            <div>
                                                <div>500pcs - 699pcs </div>
                                                <small style="font-size: 12px;color: #00000057;
                                                font-weight: 100;">Penambahan Harga Rp 500</small>
                                            </div>
                                            <br>
                                            <div>
                                                <div>700pcs keatas </div>
                                                <small style="font-size: 12px;color: #00000057;
                                                font-weight: 100;">Harga Normal</small>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('master.component.modal_shipping')
@include('master.component.modal_info_produk')
@include('katalog.action_detail')

@stop
