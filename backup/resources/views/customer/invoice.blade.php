@section('content')
<main id="pt-pageContent" style="padding-top:100px;padding-bottom:500px;background: #f9f9f9;">
    <div >
        <div class="container">
            <div class="text-center"><img src="{{ asset('img/logo.png') }}" style="
                width: 100px;
            " alt=""></div>
                
            <h1 class="pt-title-subpages noborder mt-2" style="
            font-size: 24px;
            margin: 0;
            padding: 0 !important;
        ">PEMBELIAN</h1>
            <div class="pt-account-layout">
                {{-- <h2 class="pt-title-page">{{$zz}}</h2> --}}

                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="pt-item">
                            {{-- <h6 class="pt-title">BATAS WAKTU PEMBAYARAN</h6> --}}
                            <div id="set_penerima_checkout" style="
                                        margin-top: 0px;
                                        font-size: 16px;
                                    " class="pt-description">
                                Pesanan paling lambat dikonfirmasi oleh pihak corellia tanggal
                                <strong
                                    style="display:block;color: #c89c7d;font-size: 27px;text-transform: uppercase;margin-top: 30px;">
                                {{date('d-m-Y', strtotime($pembelian->end_date))}} </strong>
                                <br>
                                <small> <strong
                                    style="display:block;color: #c89c7d;font-size: 15px;text-transform: uppercase;margin-top: -25px;">
                                {{date('H:i:s', strtotime($pembelian->end_date))}} </strong></small>
                                <br>
                                <small style="display: block;color: #c7c7c7;">
                                   Pihak <b>Corellia</b> akan memberitahumu apabila pesanan telah dikonfirmasi
                                </small>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="pt-wrapper">
                    <div class="zn-border-brown"></div>
                    <div class="pt-shop-info" style="padding: 50px 30px;background: white;border-radius: 5px;">
                        
                        @php
                            // dd($pembelian)
                        @endphp

                        <div class="row ">
                            <div class="col-md-4">
                                <div class="pt-item">
                                    <h6 class="pt-title">TRANSFER KE :</h6>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <img style="margin-top: 15px;" src="{{asset('img/')}}/{{$pembelian->bank_img}} " alt="">
                                        </div>
                                        <div class="col-md-8">
                                                {{$pembelian->bank_acc_no}} <br>
                                            <small>a.n {{$pembelian->an}}</small> <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="pt-item">
                                        <h6 class="pt-title">KODE PEMBELIAN :</h6>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                    {{$pembelian->no_pembelian}} <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-4">
                                <div class="pt-item">
                                    <h6 class="pt-title">TOTAL PEMBAYARAN :</h6>
                                    <h5 class="mt-3" style="
                                            font-size: 18px;
                                            font-weight: 100;
                                        ">Rp {{number_format($pembelian->total_pembelian,0,",",".")}}</h5>
                                </div>
                            </div>
                        </div>




                    </div>
                    <div class="zn-border-brown"></div>
                </div>

                <div class="row">
                    {{-- <div class="offset-3 col-md-3"> --}}
                            {{-- <a href="{{ route('customer.pay',['code'=>$pembelian->no_pembelian]) }}" class="btn btn-block mt-5">
                                    <span class="pt-text">
                                        BAYAR SEKARANG
                                    </span>
                                </a> --}}
                    {{-- </div> --}}
                    <div class="col-md-12 text-center">
                            <a href="{{ route('customer.pembelian_list') }}" class="btn btn-sm btn-border mt-5">
                                    <span class="pt-text">
                                        LIHAT PEMBELIAN
                                    </span>
                                </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
@stop
