@php
    function getUndangan($type,$no_pembelian)
    {
        $d_undangan = \DB::select("SELECT
        mw.*,md.color,md.shape,md.paper,md.image_id,product_name,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name,rrr.name as finishing,sss.additional_name as add_free,rw.harga as harga_waxseal
        from mst_pembelian mw
        left join mst_undangan mo on mo.product_id = mw.product_id
        left join mst_design md on md.id = mw.design_id
        left join ref_color rc on rc.id = md.color
        left join ref_paper rp on rp.id = mw.paper_inner
        left join ref_paper rpp on rpp.id = mw.paper_outer
        left join ref_waxseal rw on rw.id = mw.waxseal
        left join ref_foilcolor rf on rf.id = mw.foilcolor
        left join ref_type_size rts on rts.id = mw.type_size
        left join ref_finishing rrr on rrr.id::varchar = mw.free_finishing::varchar
        left join ref_additional sss on sss.id::varchar = mw.free_additional::varchar where
        mo.type = ".$type." and no_pembelian = '".$no_pembelian."'");

        return $d_undangan;
    }

    function getOther($no_pembelian)
    {
        $d_other = \DB::select("SELECT mw.*,product_name,price from
                    mst_pembelian mw
                    join mst_other mo on mo.product_id = mw.product_id where
                    no_pembelian = '".$no_pembelian."'");
        return $d_other;
    }

    function getListUndangan($d_undangan)
    {
        $list_Undangan = '';
        $material = '';


        // dd($d_undangan);

        foreach ($d_undangan as $v ) {

            $free_finishing = '';
        $free_additional = '';
        $additional = '';

            if ($v->material2) {
                $material .= '<span class="zn-point">Material Inner:<span>'.$v->material1.'</span></span>
                <span class="zn-point">Material Outer:<span>'.$v->material2.'</span></span>';
            } else {
                $material .= '<span class="zn-point">Material:<span>'.$v->material1.'</span></span>';
            }

                $add_free_finishing = explode("|",$v->free_finishing);
                $add_free_additional = explode("|",$v->free_additional);
                $len_fin = count($add_free_finishing);
                $len_add = count($add_free_additional);

                for ($i=0; $i < $len_fin-1; $i++) {
                    if ($i != $len_fin-1) {
                        $adz = collect(\DB::select("SELECT * from ref_finishing where id = '".$add_free_finishing[$i]."'"))->first();
                        $free_finishing .= "<br>- ".$adz->name;
                    }
                }

                for ($i=0; $i < $len_add-1; $i++) {
                    if ($i != $len_add-1) {
                        $adz = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_free_additional[$i]."'"))->first();
                        $free_additional .= "<br>- ".$adz->additional_name;
                    }
                }

                if ($v->qty >= 200 && $v->qty <= 299) {
                    $disc = 2000;
                }else if($v->qty >= 300 && $v->qty <= 499){
                    $disc = 1000;
                }else if($v->qty >= 500 && $v->qty <= 699){
                    $disc = 500;
                }else if($v->qty >= 700){
                    $disc = 0;
                }



            $list_Undangan .= '
            <tr>
                <td style="width: 40%;">
                    <br>'.$v->product_name.'
                    <br><br>
                    <span class="zn-point">Type & Size :'.$v->style_size.'</span>
                    <span class="zn-point">Warna :'.$v->color_name.'</span>
                    <span class="zn-point">Waxseal :'.$v->waxseal_name.'</span>
                    <span class="zn-point">Foil Color :'.$v->foilcolor_name.'</span>
                    '.$material.'
                    <span class="zn-point"> Free Finishing:<span style="font-size:12px;">'.$free_finishing.'</span></span>
                    <span class="zn-point"> Free Additional Item:<span style="font-size:12px;">'.$free_additional.'</span></span>
                </td>
                <td class="text-right">Rp '.number_format($v->price+$disc,0,",",".").'</td>
                <td class="text-right">'.number_format($v->qty,0,",",".").'</td>
                <td class="text-right">Rp '.number_format($v->totalprice,0,",",".").'</td>
            </tr>
            <tr>
                <td class="text-left">
                    <span style="font-size: 12px;" class="zn-point">Waxseal : '.$v->waxseal_name.'</span>
                    </td>
                    <td class="text-right">Rp '.number_format($v->harga_waxseal,0,",",".").'</td>
                    <td class="text-right">'.number_format($v->qty,0,",",".").'</td>
                    <td class="text-right">Rp '.number_format($v->harga_waxseal*$v->qty,0,",",".").'</td>
            </tr>';

            // {{-- ADDITIONAL ITEM --}}

            $add_item = explode("|",$v->additional_user);
            $add_item_count = explode("|",$v->additional_user_count);
            $len = count($add_item);

            foreach ($add_item as $key => $value) {
                $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$key]."'"))->first();
                if ($key != $len - 1) {
                    $additional .= '
                    <tr>
                        <td><span class="zn-point">'.$ad->additional_name.'</span></td>
                        <td class="text-right">Rp '.number_format($ad->price,0,",",".").'</td>
                        <td class="text-right">'.$add_item_count[$key].'</td>
                        <td class="text-right">Rp '.number_format($ad->price*$add_item_count[$key],0,",",".").'</td>
                    </tr>';
                }
            }

            $list_Undangan.= $additional;

        }

        return $list_Undangan;
    }

    function getListOther($d_other)
    {
        $other = '';
        foreach ($d_other as $v ) {
            $other .= '
                    <tr>
                        <td>'.$v->product_name.'</td>
                        <td class="text-right">Rp '.number_format($v->price,0,",",".").'</td>
                        <td class="text-right">'.number_format($v->qty,0,",",".").'</td>
                        <td class="text-right">Rp '.number_format($v->totalprice,0,",",".").'</td>
                    </tr>';
        }
        return $other;
    }
    function getListDesignOrder($no_pembelian)
    {
        $design_order = \DB::select("SELECT  mw.*,judul,deskripsi from mst_pembelian mw
        join mst_design_order mo on mo.code = mw.product_id  where no_pembelian = '".$no_pembelian."'");

        $d_design_order = '';
        foreach ($design_order as $v ) {
            $d_design_order .= '
                    <tr>
                        <td>('.$v->product_id.") ".$v->judul.'</td>
                        <td class="text-right">Rp '.number_format($v->totalprice/$v->qty,0,",",".").'</td>
                        <td class="text-right">'.number_format($v->qty,0,",",".").'</td>
                        <td class="text-right">Rp '.number_format($v->totalprice,0,",",".").'</td>
                    </tr>';
        }
        return $d_design_order;
    }
    function getListWebsite($no_pembelian)
    {
        $design_order = \DB::select("SELECT mw.*,product_name,date_active,deskripsi,mdw.nama_website,mdw.id as id_website,mdw.link_live
        from mst_pembelian mw
        join mst_website mo on mo.product_id = mw.product_id
		join mst_data_website mdw on mdw.id = mw.design_id where no_pembelian = '".$no_pembelian."'");

       

        $d_design_order = '';
        foreach ($design_order as $v ) {

            $enddate = strtotime($v->date_active);
            $enddate = strtotime("+".$v->qty." week", $enddate);

            $link = "'".url('/')."/".$v->nama_website."'";
            $link_view = url('/')."/".$v->nama_website;

            if ($v->link_live) {
                $link_live = ' <a onclick="copyText(`'.$v->link_live.'`)" class="btn btn-sm btn-border" style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">
                                <span class="pt-text" style="color: #333333;">
                                    COPY LINK LIVE
                                </span>
                            </a>';
            }else{
                $link_live ='';
            }

            $d_design_order .= '
                    <tr>
                        <td style="padding-bottom: 20px;">'.$v->product_name.'<br> <b style="
                                        margin-bottom: 10px;
                                        display: block;
                                        font-size: 14px;
                                    ">
                            corellia.id/'.$v->nama_website.'</b> <br>
                            Tanggal Aktif Website : <br>
                            <b>'.date('d M Y',strtotime($v->date_active)).' - '.date('d M Y',$enddate).'</b>
                            <br><br>
                            <a target="blank" href="'.$link_view.'" class="btn btn-sm" style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">
                                <span class="pt-text" style="color: #fff;">
                                    VIEW WEBSITE
                                </span>
                            </a>
                            <a href="'.route("website.edit_website",["productId"=>"$v->product_id","id"=>"$v->design_id"]).'" class="btn btn-sm" style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">
                                <span class="pt-text" style="color: #fff;">
                                    EDIT WEBSITE
                                </span>
                            </a>
                            <a onclick="copyText('.$link.')" class="btn btn-sm btn-border" style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">
                                <span class="pt-text" style="color: #333333;">
                                    COPY LINK WEBSITE
                                </span>
                            </a>
                           '.$link_live.'
                        </td>
                        <td class="text-right">Rp '.number_format($v->totalprice/$v->qty,0,",",".").'</td>
                        <td class="text-right">'.number_format($v->qty,0,",",".").'</td>
                        <td class="text-right">Rp '.number_format($v->totalprice,0,",",".").'</td>
                    </tr>';
        }

        return $d_design_order;
    }


    function getAdditionalAdmin($additional)
    {
        $add_item = explode("|",$additional);
        $len = count($add_item);
        $additional = '';

        foreach ($add_item as $key => $vitem) {
            $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$key]."'"))->first();
            if ($key != $len - 1) {
                    $additional .= '
                    <tr>
                        <td>'.$ad->additional_name.'</td>
                        <td class="text-right">Rp '.number_format($ad->price,0,",",".").'</td>
                        <td class="text-right">1</td>
                        <td class="text-right"><del> Rp '.number_format($ad->price,0,",",".").'</del></td>
                    </tr>';
                }
        }

        return $additional;
    }

    function getTotalList($v)
    {
        $total_list = '<tr>
            <td>
                Ongkos Kirim
            </td>
            <td class="text-right"> </td>
            <td class="text-right"></td>
            <td class="text-right">Rp '.number_format($v->ongkir,0,",",".").'</td>
        </tr>
        <tr>
            <td style="color: #1dc9b7;font-weight: bold;">
                Potongan Harga
            </td>
            <td class="text-right"> </td>
            <td class="text-right"></td>
            <td class="text-right" style="color: #1dc9b7;font-weight: bold;">Rp '.number_format($v->discount,0,",",".").'</td>
        </tr>
        <tr>
            <td style="font-weight:bold;">
                Total Pembayaran
            </td>
            <td class="text-right"> </td>
            <td class="text-right"></td>
            <td class="text-right" style="font-weight:bold;">Rp '.number_format($v->total_pembelian,0,",",".").'
            </td>
        </tr>';

        return $total_list;

    }

    function getDetailList($v)
    {
        $detail_list = '';
        $diambil = '';
        $sisa_bayar = '';

        if ($v->pengiriman =='diambil') {
            $diambil = 'Diambil Di Gallery';
        }else {
            $diambil = 'Dikirim';
        }

        if ($v->status == 2) {
            $sisa_bayar = '
            <div>
                <h6 class="zn-head-list">Sisa Pembayaran</h6>
                <h5 style="font-size:15px;">Rp
                    '.number_format($v->billing,0,",",".").'
                </h5>
            </div>';
        }




        $response = ($v->response_admin) ? $v->response_admin:"-";

        $detail_list .= '
        <div class="row">
            <div class="col-md-6">
                <div style="font-size:12px;">
                    <h6 class="zn-head-list">Kode Pembelian</h6>
                    <h5 style="font-size:15px;">'.$v->no_pembelian.'</h5>
                </div>
                <div>
                    <h6 class="zn-head-list">Tanggal Pembelian</h6>
                    <h5 style="font-size:15px;">
                        '.date("d M Y H:i:s", strtotime($v->created_at)).'
                    </h5>
                </div>

            </div>
            <div class="col-md-6">
                <div>
                    <h6 class="zn-head-list">Pilihan Pengiriman</h6>
                    <h5 style="font-size:15px;">
                       '.$diambil.'
                    </h5>
                </div>
                '.$sisa_bayar.'
            </div>

            <div class="col-md-12">
                <div>
                    <h6 class="zn-head-list">Catatan Pembeli
                    </h6>
                    <h5 style="font-size:15px;">
                        '.$v->note.'
                    </h5>
                </div>
            </div>
            <div class="col-md-12">
                <div>
                    <h6 class="zn-head-list">Catatan Corellia
                    </h6>
                    <h5 style="font-size:15px;">
                        '.$response.'
                    </h5>
                </div>
            </div>
        </div>';


        return $detail_list;

    }


@endphp
<script>

function copyText(text) {
    var dummyContent = text;
    var dummy = $('<input>').val(dummyContent).appendTo('body').select()
    document.execCommand('copy');
    shownotif('Berhasil Copy Link','other');

    }


function setUrlTab(page) {
    var state = { name: "name", page: 'History', url: page };
        window.history.replaceState(state, "History", page);
}

function simpanNote(no_pembelian) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_note_undangan') }}',
        data: {
            note_undangan:$('#note_undangan'+no_pembelian).val(),
            no_pembelian:no_pembelian
        },

        beforeSend: function () {
            loadingPage();
        },
        success: function (response) {

            shownotif('Catatan Undangan Berhasil Disimpan','other');

            endLoadingPage();
            $('#modalCatatanUndangan'+no_pembelian).modal('hide');
            location.reload();
        }

    }).done(function (msg) {
    }).fail(function (msg) {
        endLoadingPage();
    });

}


function mVerifBatal(id) {

    $('#set_verif_text').html('Yakin Akan Membatalkan Pembelian ?');

    $("#set_verif_action").click(function(){
        batalkanPembelian(id);
    });

$('#modalVerif').modal('show');
}

function batalkanPembelian(no_pembelian) {

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.batalkan_pembelian') }}',
    data: {
        no_pembelian:no_pembelian
    },

    beforeSend: function () {
        loadingPage();
    },
    success: function (response) {

        shownotif('Pembelian Berhasil Dibatalkan','other');

        endLoadingPage();
        location.reload();
    }

}).done(function (msg) {
}).fail(function (msg) {
    endLoadingPage();
});

}

function mCatatanUndanngan(no_pembelian) {

$('#modalCatatanUndangan'+no_pembelian).modal('show');
}

function mDetailPembayaran(no_pembelian) {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('customer.get_detail_pembayaran') }}',
        data: {
            no_pembelian:no_pembelian
        },

        beforeSend: function () {
            // loadingPage();
            $('#set_detail_pembayaran').html('');
        },
        success: function (response) {

            // var data = $.parseJSON(response.rm);


            $.each(response.rm, function (k,v) {

                var tolak = ``;
                if (v.status == 4) {
                    tolak = `style="
                            background: #ff7373;
                            color: #fff;
                        "`;
                }

                $('#set_detail_pembayaran').append(`
                    <tr `+tolak+`>
                        <td> <strong>`+v.kode_pembayaran+`</strong></td>
                        <td>`+formatDate(v.created_at)+`</td>
                        <td>`+v.bank+` - `+v.no_rek+`</td>
                        <td>`+v.atas_nama+`</td>
                        <td>Rp `+numFormat(v.nominal_transfer)+`</td>
                    </tr>
                `);

            });

        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });

$('#modalDetailPembayaran').modal('show');
}
</script>
