<script>

function set_pembelian() {
    // var bank = $('#bank').val();

    // var selectedLanguage = new Array();
   
    var bank = $(".bank:checked").val();
    var pengiriman = $(".pengiriman:checked").val();

    var note = $('#note').val();

    console.log($('#set_biaya_ongkir').val());
    
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_pembelian') }}',
        data: {
            note:note,
            bank:bank,
            pengiriman:pengiriman,
            ongkir:$('#set_biaya_ongkir').val(),
            total_add_item:$('#set_total_add_item').val()
        },

        beforeSend: function () {
            loadingPage();
        },
        success: function (response) {

            if (response.rc == 99) {
                shownotif(response.rm,'error');
                endLoadingPage();
            }else{
                window.location.href = "{{ route('customer.invoice') }}";
            }

            // loadPage('{{ route('customer.invoice') }}');

            // var msg = response.rm;
            
            // // endLoadingPage();
            // if (response.rc == 99) {
            //     shownotif('Gagal','other');
            //     $('#voucher_desc').html('Voucher Tidak Ditemukan');
            // }else{
            //     shownotif('Voucher Berhasil Digunakan','other');

            // }
        }

    }).done(function (msg) {
        endLoadingPage();
    }).fail(function (msg) {
        endLoadingPage();
    });

    
}

function setPengiriman() {
    let pengiriman = $(".pengiriman:checked").val();

    
    let total = "{{$cart['total']+$total_add_item}}";
    let ongkir = ('{{$berat_product}}'/1000)*10000;

    let total_final = 0;
    
    if (pengiriman == 'dikirim') {
        total_final = parseInt(total)+parseInt(ongkir);
    }else {
        total_final = total;
    }

    $('#get_total_final').html('Rp '+numFormat(total_final));

}
</script>