<script>
$(document).ready(function () {
$("#form-pembayaran").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        bank: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi kolom berikut'
                }
            }
        },
        no_rek: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi kolom berikut'
                }, 
                stringLength: {
                      max:40,
                      min:5,
                      message: 'Minimal 5 Karakter dan Maksimal 40 Karakter'
                  }
            }
        },
        atas_nama: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi kolom berikut'
                }, 
                stringLength: {
                      max:40,
                      min:5,
                      message: 'Minimal 5 Karakter dan Maksimal 40 Karakter'
                  }
            }
        },
        nominal_transfer: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi kolom berikut'
                }
            }
        },
        bukti_pembayaran: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi kolom berikut'
                },
                file: {
                        extension: 'jpg,jpeg,png',
                        type: 'image/jpg,image/jpeg,image/png',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        catatan: {
            validators: {
                stringLength: {
                      max:200,
                      message: 'Maksimal 200 Karakter'
                  }
            }
        }
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});

function validate_bayar() {
    let pembelian = '{{$pembelian->billing}}';
    let pembelianHalf = '{{$nom_bayar}}'; 
    let nominal = $("#nominal_transfer").val();

  
    


    if(clearNumFormat(nominal) < pembelianHalf || clearNumFormat(nominal) > parseInt(pembelian)){
        $( "#nominal_transfer" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#nominal_transfer").removeClass( "is-invalid" );

    }
}

function sendPembayaran() {



var validateData = $('#form-pembayaran').data('bootstrapValidator').validate();
if (validateData.isValid()) {

    validate_bayar();


var formData = document.getElementById("form-pembayaran");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.set_pembayaran') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
        
    },

    success: function (response) {

        if (response.rc == 1) {
            $('#set_close_md').html('');
            $('#set_tittle_md').html('BERHASIL TERKIRIM');
            $('#set_text_md').html('Konfirmasi pembayaran berhasil terkirim, mohon untuk menunggu konfirmasi dari pihak corellia.id');
            $('#set_action_md').html(`<a href="{{ route('customer.pembelian_list') }}?tab=2" class="btn btn-block mt-4">LIHAT STATUS PEMBAYARAN</a>`);

            mSuccessShow();              
        }
        
    }

}).done(function (msg) {
    endLoadingPage();

}).fail(function (msg) {
    endLoadingPage();
});

}
}
</script>