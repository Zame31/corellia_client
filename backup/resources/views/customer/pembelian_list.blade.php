@section('content')

@include('customer.action.pembelian_list_action')
<main id="pt-pageContent" style="padding-bottom:500px;">
    <div class="container-indent">
        <div class="container">
            <h1 class="pt-title-subpages noborder">DAFTAR PEMBELIAN</h1>
            <div class="pt-account-layout">
                {{-- <h2 class="pt-title-page">My Account</h2> --}}
                <div class="pt-wrapper">
                    <div class="row">
                        <div class="col-12">
                            <div class="pt-offset-35">
                                <div class="pt-product-page__tabs pt-tabs pt-tabs-gorizontal-js">
                                    <div class="pt-tabs__head" style="visibility: visible;">
                                        <ul>
                                            <li onclick="setUrlTab('?tab=1')" style="text-transform:uppercase;" id="sTab1" data-active="false" class="">
                                                <span>Pesanan Baru</span></li>
                                            <li onclick="setUrlTab('?tab=2')" style="text-transform:uppercase;" id="sTab2" data-active="false" class=""><span>Menunggu
                                                    Konfirmasi</span></li>
                                            <li onclick="setUrlTab('?tab=3')" style="text-transform:uppercase;" id="sTab3" data-active="false" class=""><span>Dibayar Sebagian</span>
                                            </li>
                                            <li onclick="setUrlTab('?tab=4')" style="text-transform:uppercase;" id="sTab4" data-active="false" class=""><span>lunas</span></li>
                                        </ul>
                                        <div class="pt-tabs__border" style="left: 0px; width: 103.281px;"></div>
                                    </div>
                                    @php $cp1 = 0;$cp2 = 0;$cp3 = 0; $cp4 = 0; @endphp
                                    @if ($pembelian)
                                        @foreach ($pembelian as $v)
                                            @if ($v->status == 0 || $v->status == 4 ||$v->status == 5 ||$v->status == 6 )
                                                @php $cp1++; @endphp
                                            @elseif($v->status == 1)
                                                @php $cp2++; @endphp
                                            @elseif($v->status == 2)
                                                @php $cp3++; @endphp
                                            @elseif($v->status == 3)
                                                @php $cp4++; @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="pt-tabs__body">
                                        <div id="cTab1" class="">
                                            <span class="pt-tabs__title">Pesanan Baru</span>
                                            <div class="pt-tabs__content" >
                                                @if ($cp1 > 0)
                                                <blockquote class="pt-blockquote">
                                                    <span class="pt-description mb-5">
                                                        Pemesanan berhasil dibuat, tunggu konfirmasi dari pihak corellia
                                                        dan segera lakukan pembayaran
                                                    </span>
                                                </blockquote>
                                                @endif

                                                <div class="pt-table-responsive">
                                                    @if ($pembelian)
                                                        
                                                    
                                                    @foreach ($pembelian as $v)
                                                    @if ($v->status == 0 || $v->status == 4 ||$v->status == 5 ||$v->status == 6 )
                                                    @php
                                                        $d_undangan = getUndangan(1,$v->no_pembelian);
                                                        $d_other = getOther($v->no_pembelian);
                                                    @endphp
                                                    <div class="row zn-border-dashed" style="padding: 50px 0px;">
                                                        <div class="col-md-4">
                                                            {!! getDetailList($v) !!}

                                                            @if ($v->status == 4)
                                                            <div class="zn-text-status">Konfirmasi Pembayaran,
                                                                <small style="font-weight: 100;margin-top: 3px;">
                                                                    {{$v->response_admin}} </small>
                                                            </div>
                                                            @elseif($v->status == 0)
                                                            <div class="zn-text-status">
                                                                <small style="font-weight: 100;margin-top: 3px;">
                                                                    Menunggu Konfirmasi Pembelian </small>
                                                            </div>
                                                            @elseif($v->status == 5)
                                                            <div class="zn-text-status mt-0"
                                                                style="background:#1dc9b7;">
                                                                <small style="font-weight: 600;margin-top: 0px;">
                                                                    TERKONFIRMASI </small>
                                                            </div>
                                                            @elseif($v->status == 6)
                                                            <div class="zn-text-status mt-0"
                                                                style="background:#2786fb;"> Pembelian Ditolak,
                                                                <small style="font-weight: 100;margin-top: 0px;">
                                                                    {{$v->response_admin}} </small>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-8 ">
                                                            <div class="pt-table-responsive">
                                                                <table class="pt-table-shop-01" style="font-size: 14px;">
                                                                    <thead style="font-size: 14px;">
                                                                        <tr>
                                                                            <th>Produk</th>
                                                                            <th class="text-right">Harga</th>
                                                                            <th class="text-right">Jumlah</th>
                                                                            <th class="text-right">Total</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {!! getListUndangan($d_undangan) !!}
                                                                        {!! getListOther($d_other) !!}
                                                                        {!! getListWebsite($v->no_pembelian) !!}
                                                                        {!! getListDesignOrder($v->no_pembelian) !!}
                                                                        {!! getAdditionalAdmin($v->additional) !!}
                                                                        {!! getTotalList($v) !!}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            
                                                          

                                                            <div class="row mt-4">
                                                                <div class="col-md-6">
                                                                </div>
                                                                <div class="col-md-6 text-right">
                                                                    <a onclick="mVerifBatal('{{$v->no_pembelian}}')" 
                                                                        class="btn btn-sm" style="color:#ffffff;">BATALKAN</a>
                                                                    @if ($v->status == 5 || $v->status == 4)
                                                                    <a href="{{ route('customer.pay',['code'=>$v->no_pembelian]) }}"
                                                                        class="btn btn-sm">BAYAR SEKARANG</a>
                                                                    @else

                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cTab2" class="">
                                            <span class="pt-tabs__title">Menunggu Konfirmasi</span>
                                            <div class="pt-tabs__content">
                                                @if ($cp2 > 0)
                                                <blockquote class="pt-blockquote">
                                                    <span class="pt-description mb-5">
                                                        Pembayaran sedang diproses, Menunggu konfirmasi dari pihak
                                                        Corellia.id
                                                    </span>
                                                </blockquote>
                                                @endif
                                                {{-- <small style="display:block;margin-bottom:20px;">Pemesanan sedang diproses, Menunggu konfirmasi admin</small> --}}
                                                <div class="pt-table-responsive">

                                                    @foreach ($pembelian as $v)
                                                    @if ($v->status == 1)
                                                    @php
                                                        $d_undangan = getUndangan(1,$v->no_pembelian);
                                                        $d_other = getOther($v->no_pembelian);
                                                    @endphp
                                                    <div class="row zn-border-dashed" style="padding: 50px 0px;">
                                                        <div class="col-md-4">
                                                            {!! getDetailList($v) !!}
                                                        </div>

                                                        

                                                        <div class="col-md-8">
                                                            <div class="pt-table-responsive">
                                                            <table class="pt-table-shop-01" style="font-size: 14px;">
                                                                <thead style="font-size: 14px;">
                                                                    <tr>
                                                                        <th>Produk</th>
                                                                        <th class="text-right">Harga</th>
                                                                        <th class="text-right">Jumlah</th>
                                                                        <th class="text-right">Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {!! getListUndangan($d_undangan) !!}
                                                                    {!! getListOther($d_other) !!}
                                                                    {!! getListWebsite($v->no_pembelian) !!}
                                                                    {!! getListDesignOrder($v->no_pembelian) !!}
                                                                    {!! getAdditionalAdmin($v->additional) !!}
                                                                    {!! getTotalList($v) !!}
                                                                </tbody>
                                                            </table>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-6">
                                                                </div>
                                                                <div class="col-md-6 text-right">
                                                                    <button
                                                                        onclick="mDetailPembayaran('{{$v->no_pembelian}}')"
                                                                        class="btn btn-sm">DETAIL PEMBAYARAN</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cTab3" class="">
                                            <span class="pt-tabs__title">Dibayar Sebagian</span>
                                            <div class="pt-tabs__content">
                                                @if ($cp3 > 0)
                                                    <blockquote class="pt-blockquote">
                                                        <span class="pt-description mb-5">
                                                            Pemesanan Sudah Dibayar Sebagian, Silahkan Lunasi Pesanan Anda.
                                                        </span>
                                                    </blockquote>
                                                @endif
                                                {{-- <small style="display:block;margin-bottom:20px;">Pemesanan sedang diproses, Menunggu konfirmasi admin</small> --}}
                                                <div class="pt-table-responsive">
                                                    @foreach ($pembelian as $v)
                                                    @if ($v->status == 2)
                                                    @php
                                                        $d_undangan = getUndangan(1,$v->no_pembelian);
                                                        $d_other = getOther($v->no_pembelian);
                                                    @endphp
                                                    <div class="row zn-border-dashed" style="padding: 50px 0px;">
                                                        <div class="col-md-4">
                                                            {!! getDetailList($v) !!}

                                                            @if (count($d_undangan) > 0)
                                                            
                                                                @if ($v->is_note_undangan == '1')
                                                                <small style=" display: block;text-align: justify;">Catatan Telah Terisi.</small>
                                                                <button onclick="mCatatanUndanngan('{{$v->no_pembelian}}')"
                                                                    class="btn btn-border btn-sm"
                                                                    style="width: 100%;background:#1dc9b7;color:#ffffff;">LIHAT ISI CATATAN UNDANGAN 
                                                            </button>
                                                                @else
                                                                <small style=" display: block;text-align: justify;">Silahkan Isi Catatan Undangan Seperti alamat, nama orangtua, nama mempelai, turut menundang dan lainnya .</small>
                                                                <br>
                                                                <small style="
                                                                color: #ff4545;
                                                                display: block;
                                                                text-align: justify;
                                                                ">Isi Catatan Undangan hanya bisa 1 kali pengisian, mohon lebih teliti dalam pengisian. apabila ada revisi untuk selanjutnya bisa hubungi pihak corellia </small>
                                                       
                                                                <br>
                                                                <button onclick="mCatatanUndanngan('{{$v->no_pembelian}}')"
                                                                    class="btn btn-border btn-sm"
                                                                    style="width: 100%;background:#1dc9b7;color:#ffffff;">ISI CATATAN UNDANGAN 
                                                            </button>

                                                                @endif
                                                          
                                                              
                                                            
                                                                <br><br>
                                                             
                                                       
                                                            @endif
                                                        </div>



                                                        <div class="col-md-8">
                                                            <div class="pt-table-responsive">
                                                            <table class="pt-table-shop-01" style="font-size: 14px;">
                                                                <thead style="font-size: 14px;">
                                                                    <tr>
                                                                        <th>Produk</th>
                                                                        <th class="text-right">Harga</th>
                                                                        <th class="text-right">Jumlah</th>
                                                                        <th class="text-right">Total</th>
                                                                    </tr>
                                                                </thead>   
                                                                <tbody>
                                                                    {!! getListUndangan($d_undangan) !!}
                                                                    {!! getListOther($d_other) !!}
                                                                    {!! getListWebsite($v->no_pembelian) !!}
                                                                    {!! getListDesignOrder($v->no_pembelian) !!}
                                                                    {!! getAdditionalAdmin($v->additional) !!}
                                                                    {!! getTotalList($v) !!}
                                                                </tbody>
                                                            </table>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-8">
                                                                   

                                                                </div>
                                                                <div class="col-md-4 text-right">
                                                                    <a href="{{ route('customer.pay',['code'=>$v->no_pembelian]) }}"
                                                                        class="btn btn-sm"
                                                                        style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">LUNASI</a>
                                                                    <button
                                                                        onclick="mDetailPembayaran('{{$v->no_pembelian}}')"
                                                                        class="btn btn-border btn-sm"
                                                                        style="height: 28px;width: 100%;">DETAIL
                                                                        PEMBAYARAN</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cTab4" class="">
                                            <span class="pt-tabs__title">Lunas</span>
                                            <div class="pt-tabs__content">
                                                @if ($cp4 > 0)
                                                    <blockquote class="pt-blockquote">
                                                        <span class="pt-description mb-5">
                                                            Selamat, pembayaran telah selesai dilakukan.
                                                        </span>
                                                    </blockquote>
                                                @endif
                                                {{-- <small style="display:block;margin-bottom:20px;">Pemesanan sedang diproses, Menunggu konfirmasi admin</small> --}}
                                                <div class="pt-table-responsive">
                                                    @foreach ($pembelian as $v)
                                                    @if ($v->status == 3)
                                                    @php
                                                        $d_undangan = getUndangan(1,$v->no_pembelian);
                                                        $d_other = getOther($v->no_pembelian);
                                                    @endphp
                                                    <div class="row zn-border-dashed" style="padding: 50px 0px;">
                                                        <div class="col-md-4">
                                                            {!! getDetailList($v) !!}
                                                            @if (count($d_undangan) > 0)

                                                            
                                                            @if ($v->is_note_undangan == '1')
                                                            <small style=" display: block;text-align: justify;">Catatan Telah Terisi.</small>
                                                            <button onclick="mCatatanUndanngan('{{$v->no_pembelian}}')"
                                                                class="btn btn-border btn-sm"
                                                                style="width: 100%;background:#1dc9b7;color:#ffffff;">LIHAT ISI CATATAN UNDANGAN 
                                                        </button>
                                                            @else
                                                          
                                                                <small style="text-align: justify;">Silahkan Isi Catatan Undangan Seperti alamat, nama orangtua, nama mempelai, turut menundang dan lainnya .</small>
                                                                <br><br>
                                                                {{-- @if ($v->is_catatan_diisi == null) --}}
                                                                    <button onclick="mCatatanUndanngan('{{$v->no_pembelian}}')"
                                                                        class="btn btn-border btn-sm"
                                                                        style="width: 100%;background:#1dc9b7;color:#ffffff;">ISI CATATAN UNDANGAN 
                                                                </button>
                                                                {{-- @endif --}}
                                                               @endif
                                                       
                                                       
                                                            @endif
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h6 class="zn-head-list mt-5">Progres Pembelian </h6>
                                                                    <div class="zn-timeline ">
                                                                        <ul class="scrollStyle">
                                                                            @php
                                                                                $d_trace = \DB::select("SELECT * from
                                                                                mst_trace
                                                                                where no_pembelian = '".$v->no_pembelian."' order by created_at desc");
                                                                            @endphp
                                                                            @foreach ($d_trace as $dtv)
                                                                            <li>
                                                                                <span></span>
                                                                                <div>
                                                                                    <div class="info"> {{$dtv->note}} </div>
                                                                                    <div class="type m-0" > {{date('d M Y H:i:s', strtotime($dtv->created_at))}}</div>
                                                                                </div> 
                                                                            </li>
                                                                            @endforeach
                                                                            <li>
                                                                                <span></span>
                                                                                <div>
                                                                                    <div class="info">Sedang Di Proses Oleh Pihak Corellia</div>
                                                                                    {{-- <div class="type m-0" >20-10-2019 20:10:10</div> --}}
                                                                                </div> 
                                                                            </li>
                                                                          
                                                                          
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         

                                                        </div>



                                                        <div class="col-md-8">
                                                            <div class="pt-table-responsive">
                                                            <table class="pt-table-shop-01" style="font-size: 14px;">
                                                                <thead style="font-size: 14px;">
                                                                    <tr>
                                                                        <th>Produk</th>
                                                                        <th class="text-right">Harga</th>
                                                                        <th class="text-right">Jumlah</th>
                                                                        <th class="text-right">Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {!! getListUndangan($d_undangan) !!}
                                                                    {!! getListOther($d_other) !!}
                                                                    {!! getListWebsite($v->no_pembelian) !!}
                                                                    {!! getListDesignOrder($v->no_pembelian) !!}
                                                                    {!! getAdditionalAdmin($v->additional) !!}
                                                                    {!! getTotalList($v) !!}
                                                                </tbody>
                                                            </table>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-8">
                                                                    
                                                                </div>
                                                                <div class="col-md-4 text-right">
                                                                  
                                                                    <button
                                                                        onclick="mDetailPembayaran('{{$v->no_pembelian}}')"
                                                                        class="btn btn-sm"
                                                                        style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">DETAIL
                                                                        PEMBAYARAN</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <h3 class="pt-title">Order History</h3> --}}

                </div>
            </div>
        </div>
    </div>
    {{-- SET TAB --}}
    @php
         if (isset($_GET['tab'])) {
            $getTab = $_GET['tab'];
        }else {
            $getTab = '1';
        }
    @endphp
    <script>
        let get_url = "{{$getTab}}";
        console.log(get_url);

        $('#sTab'+get_url).addClass('active');
        $('#sTab'+get_url).attr("data-active","true");

        $('#cTab'+get_url).addClass('active');
        $('#cTab'+get_url+" .pt-tabs__content").css('display','block');

    </script>
</main>


@foreach ($pembelian as $v)
<div class="modal fade" id="modalCatatanUndangan{{$v->no_pembelian}}" tabindex="-1" role="dialog" aria-label="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body" style="background: #f8f8f8;">
                <div class="pt-shopcart-box">
                    <h6 class="pt-title p-0">Catatan Undangan</h6>

                    @if ($v->is_note_undangan == '1')
                    <br>
                    {!! nl2br($v->note_undangan) !!}
                    @else
                    <small>
                     
                        Tuliskan lengkap deskripsi undangan seperti alamat, nama orangtua, nama mempelai, turut menundang dan lainnya.</small>
                    <br><br><div class="form-default form-wrapper">
                        <textarea style="font-size: 13px;font-weight: 100;height: 900px;" name="note_undangan" class="form-control" rows="7"
                            placeholder="Catatan Undangan" 
                            id="note_undangan{{$v->no_pembelian}}">@if ($v->note_undangan == '')Inisial nama CPP CPW : 
Nama panggilan CPW & CPP :
Nama lengkap CPW & CPP :

(Optional : anak ke-..) 

AKAD : 
Hari, tanggal : 
Lokasi & alamat lokasi :
Pukul :

RESEPSI:
Hari, tanggal :
Lokasi & alamat lokasi :
Pukul :

Ortu CPW :
1.
2.

Alamat ortu CPW :


Ortu CPP :
1.
2.

Alamat ortu CPP:

Turut mengundang (kalau ada)   
1.
2.
3.                            
                            @else
                                {{$v->note_undangan}}
                            @endif
                        </textarea>
                        
                    </div>
                    <br>
                    <a onclick="simpanNote('{{$v->no_pembelian}}')"
                        class="btn btn-sm"
                        style="height: 30px;display: block;margin-bottom: 5px;width: 100%;color: white;padding: 8px;">SIMPAN CATATAN</a>
              
                    @endif
                   
                </div>

            </div>
        </div>
    </div>
</div>
@endforeach

<div class="modal fade" id="modalDetailPembayaran" tabindex="-1" role="dialog" aria-label="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body" style="
			padding: 60px 0px 60px;
		">
                <div class="container-indent">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <style>
                                    .pt-table-04 tbody td,
                                    .pt-table-04 tbody th,
                                    .pt-table-04 thead td,
                                    .pt-table-04 thead th {
                                        padding: 12px 19px 11px;
                                        font-size: 13px;
                                        line-height: 1.1;
                                    }

                                </style>
                                <table class="pt-table-04 pt-table-zebra">
                                    <thead>
                                        <tr>
                                            <th>Kode Pembayaran</th>
                                            <th>Tanggal Transfer</th>
                                            <th>Bank / No Rekening</th>
                                            <th>Atas Nama</th>
                                            <th width="200px">Nominal Transfer</th>
                                        </tr>
                                    </thead>
                                    <tbody id="set_detail_pembayaran">

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@stop
