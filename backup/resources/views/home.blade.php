@section('content')

<div class="container-indent nomargin">
        @include('master.component.promo_head')
    </div>
    <div class="pt-offset-small container-indent">
        @include('master.component.promo_head_sub')
    </div>

    {{-- SERVICES --}}
    @include('master.component.services')

    {{-- FITUR --}}
    @include('master.component.fitur')

    @include('master.component.testimoni')

    @include('master.component.footer')

@stop
