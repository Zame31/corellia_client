<head>
	<meta charset="utf-8">
	<title>Corellia - Wedding Art</title>
	<meta name="keywords" content="Corellia,corellia,wedding,wedding art,design, wedding invitation">
	<meta name="description" content="Corellia - Wedding Art">
	<meta name="author" content="Corellia">
	<link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
	<meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" type="text/css">
 
    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon2/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
   
    <link rel="stylesheet" href="{{ asset('css/myStyle.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dropdown.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrapvalidator.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/notif.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ns-default.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ns-style-bar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foil-image.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" />
         
    <link href="{{asset('js/sound/fraudio_view.min.css')}}" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/esri-leaflet-geocoder.css') }}">
    <link rel="stylesheet" href="{{ asset('css/leaflet.css') }}" />
    
    <style media="screen">
        #login_pat {
            background: url('{{ asset('img/pat6.png') }}');
            background-repeat: repeat;
            margin-bottom: 0px;
            opacity: 1;
        }
        
   
    </style>

<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>
<script src="{{asset('admin/assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js')}}" type="text/javascript"></script>
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<script src="{{asset('js/sound/fraudio.min.js')}}"></script>
    

@include('master.global')
    
    
  
</head>
