<div class="modal fade" id="modalVerif" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-small">
        <div class="modal-content ">
            <div class="modal-body">
                <div class="pt-modal-verifyage">
                    <p id="set_verif_text">Yakin Akan Menghapus Cart ?</p>
                    <br>
                    <div class="row-btn">
                        <a id="set_verif_action" class="btn btn-border">YES</a>
                        <a onclick="mVerifHide()" class="btn btn-border">NO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalSuccess" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm ">
        <div class="modal-content ">
            <div id="set_close_md" class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="p-4">
                                    <img src="{{ asset('img/logo.png') }}" alt="" srcset="">
                                </div>
                                <h2 id="set_tittle_md" class="pt-title-page">BERHASIL MEMBUAT AKUN</h2>
                                <label id="set_text_md" style="font-size: 13px !important;">
                                    Selamat kamu berhasil membuat akun di corellia.id, terima kasih telah
                                    mendaftar.</label>
                                
                                <div id="set_action_md">
                                        <button onclick="mSuccessHide(); mLoginShow();" class="btn btn-block mt-4">LOGIN
                                                SEKARANG</button>                
                                </div>

                                
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">
                        {{-- <h1 class="pt-title-subpages noborder text-left">Already Registered?</h1> --}}
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="pt-title-page text-left">Login</h2>
                                <form id="form-login" class="form-default form-layout-01" method="post">
                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Username</label>
                                        <input type="text" name="username" class="form-control" id="username"
                                            placeholder="Masukan Username">
                                    </div>
                                    <div class="form-group text-left zn_sh_password">
                                        <label for="inputLastName">Password</label>
                                        <input type="password" name="password" class="form-control" id="password"
                                            placeholder="Masukan Password">
                                            <div class="input-group-addon zn-icon-eye">
                                                <a href=""><i class="la la-eye-slash" aria-hidden="true"></i></a>
                                              </div>
                                    </div>
                                    {{-- <div class="input-group ">
                                        <input class="form-control" type="password">
                                        <div class="input-group-addon">
                                          <a href=""><i class="la la-eye" aria-hidden="true"></i></a>
                                        </div>
                                      </div> --}}

                                    <!-- Alert Error -->
                                    <div class="zn-alert-text alert alert-danger alert-dismissible d-none"
                                        id="alertError">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <!-- Alert Info -->
                                    <div class="zn-alert-text alert alert-warning alert-dismissible d-none"
                                        id="alertInfo">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <!-- Alert Success -->
                                    <div class="zn-alert-text alert alert-success alert-dismissible d-none"
                                        id="alertSuccess">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <div class="row-btn">
                                        <button type="submit" onclick="sendLogin();" class="btn btn-block">LOGIN</button>
                                        {{-- <button type="submit" class="btn-link btn-block btn-lg"><span class="pt-text">Lost your password?</span></button> --}}
                                    </div>

                                </form>
                            </div>
                            <div class="offset-1 col-md-5" style="border-left: 1px dashed #f1f1f1;">
                                <div class="form-content text-left" style="font-size: 13px;">
                                    <h3 class="pt-title-page text-left">Pengguna Baru ?</h3>
                                    Dengan Membuat Akun di toko kami, kamu akan diperbolehkan melakukan proses
                                    'checkout' lebih cepat, menyimpan design undangan dan template website, melihat and
                                    melacak pesanan di akunmu dan lebih banyak lagi.
                                    <button type="submit" onclick="mLoginHide(); mRegisterShow();"
                                        class="btn btn-dark btn-block btn-top mt-4">BUAT AKUN
                                        BARU</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">
                        {{-- <h1 class="pt-title-subpages noborder text-left">Already Registered?</h1> --}}
                        <form id="form-register" class="form-default form-layout-01">
                            <div class="row">

                                <div class="col-md-12">
                                    <h2 class="pt-title-page text-left">Buat Akun Baru</h2>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Nama Lengkap</label>
                                        <input type="text" name="r_nama" class="form-control" id="r_nama"
                                            placeholder="Masukan Nama Lengkap">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="inputFitstName">E-Mail</label>
                                        <input type="text" name="r_email" class="form-control" id="r_email"
                                            placeholder="Masukan E-Mail">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Username</label>
                                        <input type="text" name="r_username" class="form-control" id="r_username"
                                            placeholder="Masukan Username">
                                    </div>

                                    <!-- Alert Error -->
                                    <div class="zn-alert-text alert alert-danger alert-dismissible d-none"
                                        id="alertError">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <!-- Alert Info -->
                                    <div class="zn-alert-text alert alert-warning alert-dismissible d-none"
                                        id="alertInfo">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <!-- Alert Success -->
                                    <div class="zn-alert-text alert alert-success alert-dismissible d-none"
                                        id="alertSuccess">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </div>

                                    <div class="row-btn">
                                        <button type="submit" onclick="sendRegister();" class="btn btn-block">BUAT AKUN BARU</button>
                                        {{-- <button type="submit" class="btn-link btn-block btn-lg"><span class="pt-text">Lost your password?</span></button> --}}
                                    </div>


                                </div>
                                <div class="col-md-6">

                                    <div class="form-group text-left zn_sh_password">
                                        <label for="inputLastName">Password</label>
                                        <input type="password" name="r_password" class="form-control" id="r_password"
                                            placeholder="Masukan Password">
                                            <div class="input-group-addon zn-icon-eye">
                                                <a href=""><i class="la la-eye-slash" aria-hidden="true"></i></a>
                                              </div>
                                    </div>
                                    <div class="form-group text-left zn_sh_password">
                                        <label for="inputLastName">Ketik Ulang Password</label>
                                        <input type="password" name="r_repassword" class="form-control"
                                            id="r_repassword" placeholder="Ketik Ulang Password">
                                            <div class="input-group-addon zn-icon-eye">
                                                <a href=""><i class="la la-eye-slash" aria-hidden="true"></i></a>
                                              </div>
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="inputLastName" style="font-size: 13px !important;">Dengan
                                            mendaftar,
                                            Anda setuju dengan Syarat, Ketentuan dan Kebijakan dari Corellia &
                                            Kebijakan
                                            Privasi</label>
                                    </div>

                                </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
