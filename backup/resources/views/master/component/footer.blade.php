<footer id="pt-footer">
        <div class="pt-footer-col tt-color-scheme-01">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-4 pt-mobile-offsetdown">
                        <a class="pt-logo pt-logo-alignment" href="index.html">
                            Corellia Wedding Art
                        </a>
                        <div class="pt-text-info">
                            Your one stop wedding stuff
                            Costumize Design Wedding Invitation
                        </div>
                        <div class="pt-social-box">
                            <div class="pt-social-box-title">Follow Us:</div>
                            <ul class="pt-social-box-icon">
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/by.corellia/">
                                        <svg width="18" height="18" viewBox="0 0 18 18">
                                            <use xlink:href="#icon-social_icon_instagram"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.bridestory.com/id/corellia-invitation">
                                        BrideStory
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="pt-mobile-collapse">
                            <h4 class="pt-footer-title pt-collapse-title">
                                Gallery Bandung
                                <i class="pt-icon">
                                    <svg viewBox="0 0 16 16" fill="none">
                                        <use xlink:href="#icon-close"></use>
                                    </svg>
                                </i>
                            </h4>
                            <div class="pt-collapse-content mb-4">
                                <address class="pt-address">
                                    <p>
                                        Jl. Depok No.29, Antapani Tengah, Kec. Antapani, Kota Bandung, Jawa Barat 40291
                                    </p>

                                </address>
                            </div>

                            <h4 class="pt-footer-title pt-collapse-title">
                                Gallery Garut
                                <i class="pt-icon">
                                    <svg viewBox="0 0 16 16" fill="none">
                                        <use xlink:href="#icon-close"></use>
                                    </svg>
                                </i>
                            </h4>
                            <div class="pt-collapse-content">
                                <address class="pt-address">
                                    <p>
                                        Komplek PLN, Jl. Paseban No.20, Kec. Tarogong Kidul, 44151
                                    </p>

                                </address>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-lg-2">
                        <div class="pt-mobile-collapse mb-5">
                            <h4 class="pt-footer-title pt-collapse-title">
                                Info
                                <i class="pt-icon">
                                    <svg viewBox="0 0 16 16" fill="none">
                                        <use xlink:href="#icon-close"></use>
                                    </svg>
                                </i>
                            </h4>
                            <div class="pt-collapse-content">
                                <ul class="pt-list">
                                    <li><a href="{{ route('home.about') }}">About Us</a></li>
                                    <li><a href="{{ route('home.contact') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-2">

                        <div class="pt-mobile-collapse">
                            <h4 class="pt-footer-title pt-collapse-title">
                                Our Policies
                                <i class="pt-icon">
                                    <svg viewBox="0 0 16 16" fill="none">
                                        <use xlink:href="#icon-close"></use>
                                    </svg>
                                </i>
                            </h4>
                            <div class="pt-collapse-content">
                                <ul class="pt-list">
                                    <li><a href="{{ route('home.faq') }}">FAQs</a></li>
                                    {{-- <li><a href="{{ route('home.privacy') }}">Privacy Policy</a></li> --}}
                                    <li><a href="{{ route('home.term') }}">Terms and Conditions</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pt-footer-custom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6">
                        <!-- copyright -->
                        <div class="pt-box-copyright">
                            &copy; 2020 Corellia. All Rights Reserved by <a href="#">Corellia Wedding Art.</a> Created by <a href="#">BASYS.</a>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-auto ml-auto">

                    </div>
                </div>
            </div>
        </div>

    </footer>