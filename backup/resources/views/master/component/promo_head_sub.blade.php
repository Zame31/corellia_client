@php
   if ($data_home) {
        $data_product1 = getProductHome($data_home->type1,$data_home->product_id1);
    $data_product2 = getProductHome($data_home->type2,$data_home->product_id2);
    $data_product3 = getProductHome($data_home->type3,$data_home->product_id3);

   }
   

    function getProductHome($type,$product)
    {
        if ($type == 1) {
            $data_produk = collect(\DB::select("SELECT * from mst_undangan mu
            join mst_image mi on mi.product_id = mu.product_id
            where mu.product_id = '".$product."' and seq = 1"))->first();
        }else {
            $data_produk = collect(\DB::select("SELECT * from mst_other mu
            join mst_image mi on mi.product_id = mu.product_id
            where mu.product_id = '".$product."' and seq = 1"))->first();
        }

        

        return $data_produk;
    }
    
@endphp

@if ($data_home)
<div class="container-fluid">
    <div class="pt-layout-promo-box">
        <div class="row js-init-carousel js-align-arrow row arrow-location-center js-promo-align-arrow slickDots-indent-1"
            data-item="3" data-itemmobile="1">
            <div class="col-sm-6 col-md-4">
                <div class="pt-promo-card movecontent">
                    <a href="{{route("katalog.detail",["id"=>"$data_product1->product_id","type"=>"$data_product1->type"])}}" class="image-box">
                        <img class="lazyload zn-fit-center" data-src="{{asset('gallery/product/'.$data_product1->img.'')}}" alt="NEW COLLETION">
                    </a>
                    <div class="pt-description">
                        <a href="{{route("katalog.detail",["id"=>"$data_product1->product_id","type"=>"$data_product1->type"])}}" class="pt-title">
                            <div class="pt-title-small"> {{$data_product1->product_name}} </div>
                        </a>
                        <p>{{$data_product1->desc}}</p>
                        <a href="{{route("katalog.detail",["id"=>"$data_product1->product_id","type"=>"$data_product1->type"])}}" class="btn">BELI SEKARANG!</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="pt-promo-card movecontent">
                    <a href="{{route("katalog.detail",["id"=>"$data_product2->product_id","type"=>"$data_product2->type"])}}" class="image-box">
                        <img class="lazyload zn-fit-center" data-src="{{asset('gallery/product/'.$data_product2->img.'')}}" alt="NEW COLLETION">
                    </a>
                    <div class="pt-description">
                        <a href="{{route("katalog.detail",["id"=>"$data_product2->product_id","type"=>"$data_product2->type"])}}" class="pt-title">
                            <div class="pt-title-small">{{$data_product2->product_name}}</div>
                        </a>
                        <p>{{$data_product1->desc}}</p>
                        <a href="{{route("katalog.detail",["id"=>"$data_product2->product_id","type"=>"$data_product2->type"])}}" class="btn">BELI SEKARANG!</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="pt-promo-card movecontent">
                    <a href="{{route("katalog.detail",["id"=>"$data_product3->product_id","type"=>"$data_product3->type"])}}" class="image-box">
                        <img class="lazyload zn-fit-center" data-src="{{asset('gallery/product/'.$data_product3->img.'')}}" alt="NEW COLLETION">
                    </a>
                    <div class="pt-description">
                        <a href="{{route("katalog.detail",["id"=>"$data_product3->product_id","type"=>"$data_product3->type"])}}" class="pt-title">
                            <div class="pt-title-small">{{$data_product3->product_name}}</div>
                        </a>
                        <p>{{$data_product1->desc}}</p>
                        <a href="{{route("katalog.detail",["id"=>"$data_product3->product_id","type"=>"$data_product3->type"])}}" class="btn">BELI SEKARANG!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endif
