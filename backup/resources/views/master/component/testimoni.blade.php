<div class="container-indent">
		<div class="container">
			<div class="pt-block-title">
				<h4 class="pt-title">TESTIMONI</h4>
			</div>
			<div class="pt-reviewsbox-listing js-carousel-reviewsbox row">
				<div class="col-md-6">
					<div class="pt-reviewsbox">
						<div class="pt-reviewsbox-description">
							<div class="pt-reviewsbox-title">
								<h5 class="pt-title">Wedding Website </h5>
								<div class="pt-rating">
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
								</div>
							</div>
							<img style="width: 100%;height: 200px;object-fit: cover;" src="{{asset('website/WS5/images/bg_1.png')}}" alt="">
							<p style="
							font-size: 13px;
							text-align: justify;
						">
								Undangan Pernikahan ku dibuat dengan sangat apik oleh tim corellia, Mereka kasih alcohol swab ditiap undangannya tanpa diminta di masa pandemi. Amaze banget sama service nya, thanks corellia dan tim
							</p>
						</div>
						<div class="pt-reviewsbox-author">
							<div class="pt-img"><img src="images/blog/reviewsbox/reviewsbox-img01.jpg" alt=""></div>
							<div class="pt-title"><strong>Retno & Dena</strong></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pt-reviewsbox">
						<div class="pt-reviewsbox-description">
							<div class="pt-reviewsbox-title">
								<h5 class="pt-title">Undangan Syara fina </h5>
								<div class="pt-rating">
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
								</div>
							</div>
							<img style="width: 100%;height: 200px;object-fit: cover;" src="{{asset('img/testi3.png')}}" alt="">
							<p style="
							font-size: 13px;
							text-align: justify;
						">
								Ga kapok order order di corelia, karena buat aku dg harga terjangkau punya kualitas yg oke banget !dan yg paling oke sih ga kebingungan sama sekali buat nyari template dasar yg udh di siapin di corelia, tapi udh gtu kita bakal dibkinin yg agak berbeda dr template yg kita pilih, ngga ngerasa rugi bgt order banyak, puas, cepat tanggap lagi. Thankyou coreliaaaaa

							</p>
						</div>
						<div class="pt-reviewsbox-author">
							<div class="pt-img"><img src="images/blog/reviewsbox/reviewsbox-img01.jpg" alt=""></div>
							<div class="pt-title"><strong>Syara fina </strong></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pt-reviewsbox">
						<div class="pt-reviewsbox-description">
							<div class="pt-reviewsbox-title">
								<h5 class="pt-title">Undangan Retno & Dena </h5>
								<div class="pt-rating">
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
									<i class="pt-star">
										<svg>
											<use xlink:href="#icon-review"></use>
										</svg>
									</i>
								</div>
							</div>
							<img style="width: 100%;height: 200px;object-fit: cover;" src="{{asset('img/testi2.png')}}" alt="">
							<p style="
							font-size: 13px;
							text-align: justify;
						">
								Aku cuma tentuin design nya mau yg mana, warna nya mau apa, kemudian selang beberapa hari langsung ada. tanpa banyak revisi ini itu (bukti kalo mereka tau banget keinginan client) Langsung naik cetak.
							</p>
						</div>
						<div class="pt-reviewsbox-author">
							<div class="pt-img"><img src="images/blog/reviewsbox/reviewsbox-img01.jpg" alt=""></div>
							<div class="pt-title"><strong>Retno & Dena</strong></div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>