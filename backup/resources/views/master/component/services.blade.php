<div class="container-indent">
    <div class="container-fluid" style="padding: 0px 90px;">
        {{-- <div class="pt-block-title">
            <h4 class="pt-title">Our Services</h4>
            <div class="pt-description">Visit our Services to see amazing creations from our designers.</div>
        </div> --}}
        <div class="pt-layout-promo-box">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-sm-6 col-md-12">
                                    <a href="{{ route('katalog.index',['type'=>'mahar','type_id'=>'3']) }}?menu=3" class="pt-promo-box">
                                        <div class="image-box">
                                            <img src="{{asset('assets/images/promo/MAHAR.jpg')}}" alt="">
                                        </div>
                                        <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                            <div class="pt-description-wrapper">
                                                <div class="pt-title-large"><span>Mahar.</span></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-md-12">
                                    <a href="{{ route('katalog.index',['type'=>'souvenir','type_id'=>'2']) }}?menu=3" class="pt-promo-box">
                                        <div class="image-box">
                                            <img src="{{asset('assets/images/promo/SOUVENIR.jpg')}}" alt="">
                                        </div>
                                        <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                            <div class="pt-description-wrapper">
                                                <div class="pt-title-large"><span>souvenir.</span></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <a href="{{ route('katalog.index',['type'=>'undangan','type_id'=>'1']) }}?menu=2" class="pt-promo-box">
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/KATALOG_UNDANGAN.jpg')}}" alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Katalog Undangan.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <a href="{{ route('undangan.custom_design') }}" class="pt-promo-box">
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/CUSTOME_UNDANGAN.jpg')}}" alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Custom Undangan.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            @if (Auth::user())
                                <a class="pt-promo-box" href="{{ route('website.index',['type'=>'website','type_id'=>'5']) }}?menu=4">
                            @else
                                <a class="pt-promo-box" onclick="mLoginShow();">
                            @endif
                            {{-- <a  href="#" class="pt-promo-box"> --}}
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/index07-promo-07.jpg')}}" alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Website Invitattion.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <a href="{{ route('home.promo')}}" class="pt-promo-box">
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/PROMO.jpg')}}" alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Promo.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <a onclick="mRegisterShow();"  class="pt-promo-box">
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/REGISTRASI.jpg')}}"  alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Registrasi.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <a href="{{ route('home.contact') }}" class="pt-promo-box">
                                <div class="image-box">
                                    <img src="{{asset('assets/images/promo/CONTACT_US.jpg')}}"  alt="">
                                </div>
                                <div class="pt-description pr-promo-type1 pt-point-v-t pt-point-h-l">
                                    <div class="pt-description-wrapper">
                                        <div class="pt-title-large"><span>Contact Us.</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
