<div class="modal fade" id="modalShipping" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="pt-layout-product-info-02">
                    <h6 class="pt-title-modal text-left">Pengiriman</h6>
                    <ul>
                        <li>Pengiriman Paling Lambat 2x24 Jam</li>
                    </ul>
                    <h6 class="pt-title-modal text-left">Pengembalian dan Pertukaran</h6>
                    <ul>
                        <li>Pengembalian dilakukan paling lambah setelah 7 hari pengiriman.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
