@if ($data_home)
<div class="slider-revolution revolution-default" data-fullscreen="false" data-width="1920" data-height="650">
    <div class="tp-banner-container">
        <div class="tp-banner revolution">
            <ul>
                
                
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"
                    data-title="Slide">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAKKAQMAAADmz9REAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAMdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWlPTgkAAAAABD0/7Ub7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDZAgAAd/2YNgAAAAASUVORK5CYII="
                        data-lazyload="{{asset('gallery/home/'.$data_home->promo_banner_img1.'')}}" alt="slide1"
                        data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
                    <div class="tp-caption tp-caption1 lft stb text-left" data-x="left" data-y="center" data-hoffset="0"
                        data-voffset="-3" data-speed="600" data-start="900" data-easing="Power4.easeOut"
                        data-endeasing="Power4.easeIn">
                        {{-- <div class="tp-caption1-wd-1"> {{$data_home->promo_banner_desc1}} </div> --}}
                        <div class="tp-caption1-wd-2 pt-white-color" style="font-size: 24px;">{{$data_home->promo_banner_desc1}} </div>
                        {{-- <div class="tp-caption1-wd-3 pt-text-color">pilih design undangan sesuka hati.</div> --}}
                        {{-- <div class="tp-caption1-wd-4"><a href="listing-left-column.html" target="_blank" class="btn"
                                data-text="PESAN SEKARANG!">PESAN SEKARANG!</a></div> --}}
                    </div>
                </li>
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"
                data-title="Slide">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAKKAQMAAADmz9REAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAMdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWlPTgkAAAAABD0/7Ub7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDZAgAAd/2YNgAAAAASUVORK5CYII="
                    data-lazyload="{{asset('gallery/home/'.$data_home->promo_banner_img2.'')}}" alt="slide1"
                    data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                <div class="tp-caption tp-caption1 text-center lft stb" data-x="center" data-y="center"
                    data-hoffset="0" data-voffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut"
                    data-endeasing="Power4.easeIn">
                    <div class="tp-caption1-wd-2 pt-white-color" style="font-size: 24px;">{{$data_home->promo_banner_desc2}} </div>
                </div>
            </li>
            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"
                data-title="Slide">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAKKAQMAAADmz9REAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAMdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWlPTgkAAAAABD0/7Ub7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDZAgAAd/2YNgAAAAASUVORK5CYII="
                    data-lazyload="{{asset('gallery/home/'.$data_home->promo_banner_img3.'')}}" alt="slide1" data-bgposition="center center"
                    data-bgfit="cover" data-bgrepeat="no-repeat">
                <div class="tp-caption tp-caption1 lfr stb" data-x="right" data-y="center" data-hoffset="0"
                    data-voffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut"
                    data-endeasing="Power4.easeIn">
                    <div class="tp-caption1-wd-2 pt-white-color" style="font-size: 24px;">{{$data_home->promo_banner_desc3}} </div>
                </div>
            </li>
               
               
               
            </ul>
        </div>
    </div>
</div>
@endif