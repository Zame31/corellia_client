<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="{{ asset('assets/js/bundle.js') }}"></script>

<script src="{{ asset('js/dropdown.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="{{ asset('js/classie.js') }}"></script>
<script src="{{ asset('js/notificationFx.js') }}"></script>

{{-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCvGd3nqk6_WsMY-ljjdWatOIJjMgUuOKI&signed_in=true&libraries=places'></script> --}}


@php
     if(Auth::user()){
        $isLogin = 'y';
        $custId = Auth::user()->id;
     }else {
        $isLogin = 'n';
        $custId = '';
     }
@endphp

<script>

var pusher = new Pusher('b82d8d49fc44a567b6fe', {
          cluster: 'ap3',
          forceTLS: true
        });
    
    var channel = pusher.subscribe('status-liked');
    channel.bind('App\\Events\\StatusLiked', function(data) {
        console.log('wadaawww');
        
        getNotifCustomer('{{$custId}}');
        
    });

        function getNotifCustomer(id) {
            
            $.ajax({
                type: 'POST',
                url: '{{ route('customer.customer_notif') }}',
                data: {id : id},
                beforeSend: function () {
                    // loadingModal();
                },
                success: function (response) {
                    
                    // toastr.success("Ada "+response.jumlah+" Pemberitahuan Baru");
                    if (response.jumlah == 0) {
                        $('#set_count_notif').fadeOut();
                    }else {
                        $('#set_count_notif').fadeIn();
                    }

                    $('#set_count_notif').html(response.jumlah);

                    console.log(response.rm);
                   
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                // endLoadingModal();
                // $('#modal').modal('hide');
                // toastr.error("Terjadi Kesalahan");
            });
            
        }

    
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$('#wawww').tooltip('show');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#form-login').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        sendLogin();
    }
});


function loadPage(page) {

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: "GET",
    url: page,
    data: {},
    success: function (data) {
        // $('#pageLoad').html(data).fadeIn();
    },
    beforeSend: function () {
        // animateCSS('#pageLoad', 'fadeOutDown');
        $('.znPageLoad').fadeOut();
        // $("div#loading").show();
        loadingPage();
    },
}).done(function (data) {
    endLoadingPage();
    var state = {
        name: "name",
        page: 'History',
        url: page
    };
    window.history.replaceState(state, "History", page);
    // location.reload();
    
    $('.znPageLoad').html(data).fadeIn();
});

}

function loadingPage() {
$('#zn-new-loading').css({"opacity": "1","visibility":"unset"});
}

function endLoadingPage() {
$('#zn-new-loading').css({"opacity": "0","visibility":"hidden"});
}



function mRegisterHide() {
$('#modalRegister').modal('hide');
}
function mLoginHide() {
$('#modalLogin').modal('hide');
}
function mSuccessHide() {
$('#modalSuccess').modal('hide');
}
function mVerifHide() {
$('#modalVerif').modal('hide');
}

function mProfilHide() {
$('#modalProfil').modal('hide');
}


function mProfilShow(id) {
$("#form-profil")[0].reset();
$('#form-profil').bootstrapValidator("resetForm", true);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.get_profil') }}',
    data: {
        id:id
    },

    beforeSend: function () {
        // loadingPage();
    },
    success: function (response) {
        console.log(response.rm);

        var msg = response.rm;
        
        $('#profil_nama').val(response.rm.cust_name);
        $('#profil_email').val(response.rm.email);
        $('#profil_username').val(response.rm.username);
        $('#profil_telepon').val(response.rm.phone_no);
        $('#profil_alamat').val(response.rm.address);
        $('#profil_pos').val(response.rm.postal_code);

       
    }

}).done(function (msg) {
    $('#modalProfil').modal('show');
}).fail(function (msg) {
    // endLoadingPage();
});



}

function mRegisterShow() {
$("#form-register")[0].reset();
$('#form-register').bootstrapValidator("resetForm", true);
$('#modalRegister').modal('show');
}
function mLoginShow() {
$("#form-login")[0].reset();
$('#form-login').bootstrapValidator("resetForm", true);
$('#modalLogin').modal('show');
}
function mSuccessShow() {
$('#modalSuccess').modal('show');
}

function mVerifShow(id,type) {
if (type == 'cart') {
    $('#set_verif_text').html('Yakin Akan Menghapus Cart ?');

    $("#set_verif_action").click(function(){ 
        removeCartlist(id); 
    });
}
$('#modalVerif').modal('show');
}




///////////////////////////////////////////////////////////////////////////

// LOGIN
$(document).ready(function () {
$("#form-login").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        username: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        }
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});


function sendLogin() {
var validateData = $('#form-login').data('bootstrapValidator').validate();
if (validateData.isValid()) {

var formData = document.getElementById("form-login");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('login_customer') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        $("#alertInfo").addClass('d-none');
        $("#alertError").addClass('d-none');
        $("#alertSuccess").addClass('d-none');
        loadingPage();
    },

    success: function (response) {
        console.log(response);
        $("#loading").css('display', 'none');
        switch (response.rc) {
            // password / username invalid
            case 0:
                $("#inputUsername").val('');
                $("#inputPassword").val('');
                $("#alertError").removeClass('d-none');
                $("#alertError").text(response.rm);
            break;

            // akun tidak aktif
            case 1:
                $("#inputUsername").val('');
                $("#inputPassword").val('');
                $("#alertInfo").removeClass('d-none');
                $("#alertInfo").text(response.rm);
            break;

            // reset password
            case 2:
                $("#inputUsername").val('');
                $("#inputPassword").val('');
                $("#form-data")[0].reset();
                $('#form-data').bootstrapValidator("resetForm", true);
                $("#modal").modal('show');
                $("#id").val(response.id_user);
            break;

            case 3:
                // console.log(window.location.href);
                location.reload();
                
                // window.location.href = '{{ route('home') }}';
                // window.location.href = window.location.href;
            break;
        }
    }

}).done(function (msg) {
    endLoadingPage();
    
}).fail(function (msg) {
    endLoadingPage();
});
}
} 


// REGISTER
$(document).ready(function () {
$("#form-register").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        r_nama: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        r_email: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                emailAddress: {
                    message: 'format email salah'
                },
                remote: {
                    message: 'Email Telah Digunakan',
                    url: "{{ route('valid.checking','r_email') }}",
                    data: function(validator) {
                            return {
                                isEdit: ''
                            };
                        }
                }
            }
        },
        r_username: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                      max:40,
                      min:5,
                      message: 'Minimal 5 Karakter dan Maksimal 40 Karakter'
                  },
                  remote: {
                    message: 'Username Telah Digunakan',
                    url: "{{ route('valid.checking','r_username') }}",
                    data: function(validator) {
                            return {
                                isEdit: ''
                            };
                        }
                }
            }
        },
        r_password: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                regexp: {
                    regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#.,$@!%?&]).{8,}$/,
                    message: 'Password minimal 8 karakter terdiri dari huruf, huruf Kapital, angka dan spesial karakter. Contoh : Corellia2@'
                }
            }
        },
        r_repassword: {
            validators: {
                notEmpty: {
                message: 'Mohon isi di kolom berikut'
            },
                identical: {
                    field: 'r_password',
                    message: 'Tidak Sama Dengan Password Yang Dibuat'
                }
            }
        }
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});

$('#form-register').on('keyup keypress', function (e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) {
    sendRegister();
}
});


function sendRegister() {

var validateData = $('#form-register').data('bootstrapValidator').validate();
if (validateData.isValid()) {


var formData = document.getElementById("form-register");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('register_customer') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
        
    },

    success: function (response) {

        if (response.rc == 1) {
            mRegisterHide();               
            mSuccessShow();
        }
        
    }

}).done(function (msg) {
    endLoadingPage();

}).fail(function (msg) {
    endLoadingPage();
});

}
}


$(document).ready(function () {
$("#form-profil").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        profil_nama: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        profil_telepon: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                      max:15,
                      message: 'Maksimal 15 Karakter'
                  }
            }
        },
        profil_pos: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                      max:5,
                      message: 'Maksimal 5 Karakter'
                  }
            }
        },
        profil_alamat: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                      max:200,
                      message: 'Maksimal 200 Karakter'
                  }
            }
        }
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});

$('#form-profil').on('keyup keypress', function (e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) {
    sendProfil();
    sendProfil2();
}
});


function sendProfil2() {



var validateData = $('#form-profil').data('bootstrapValidator').validate();
if (validateData.isValid()) {

    

var formData = document.getElementById("form-profil");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.set_profil') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
          
    },

    success: function (response) {

        if (response.rc == 1) {
            shownotif('Profil Berhasil Disimpan','other');
            location.reload();
        }
        
    }

}).done(function (msg) {
    endLoadingPage();
  
}).fail(function (msg) {
    endLoadingPage();
});

}
}

function sendProfil() {



var validateData = $('#form-profil').data('bootstrapValidator').validate();
if (validateData.isValid()) {

    

var formData = document.getElementById("form-profil");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.set_profil') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
          
    },

    success: function (response) {

        if (response.rc == 1) {
            shownotif('Profil Berhasil Disimpan','other');

            mProfilHide();
            $('#set_penerima_checkout').html(response.rm.cust_name+`
                                    <br>
                                    <small style="margin-top: -5px;display: block;color: #c7c7c7;">`+response.rm.email+`</small>
                                    <small><strong>`+response.rm.phone_no+`</strong></small>`);     
            $('#set_alamat_checkout').html(response.rm.address+`
                                        <br> 
                                        `+response.rm.postal_code);              

        }
        
    }

}).done(function (msg) {
    endLoadingPage();
  
}).fail(function (msg) {
    endLoadingPage();
});

}
}

 $('#zn-new-loading').css({"opacity": "0","visibility":"hidden"});



 function shownotif(text,type) {

    if (type == 'wishlist') {
        var icon = `<i class="flaticon-black" style="
                   font-size: 21px;
                   display: inline-block;
                   padding-top: 4px;
               "></i>`;
    }else if(type == 'cart'){
        var icon = ` <svg width="24" height="24" viewBox="0 0 24 24">
                <use xlink:href="#icon-cart_1"></use>
            </svg>`;
    }else if(type == 'error'){
        var icon = ` <i class="flaticon2-cross" style="
                   font-size: 21px;
                   display: inline-block;
                   padding-top: 4px;
               "></i>`;
    }
    else {
        var icon = `<i class="flaticon2-check-mark" style="
                   font-size: 21px;
                   display: inline-block;
                   padding-top: 4px;
               "></i>`;
    }

    let set_type = 'notice';
    
    if (type == 'error') {
        set_type ='error';
    }

        // create the notification
        var notification = new NotificationFx({
            message : icon+`<p>`+text+`</p>`,
            layout : 'bar',
            ttl : 2000,
            effect : 'slidetop',
            type : set_type, // notice, warning or error
            onClose : function() {
                // bttn.disabled = false;
            }
        });

        // show the notification
        notification.show();

    
    // disable the button (for demo purposes only)
    this.disabled = true;
}

function get_list_notif() {
    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        type: 'POST',
        url: '{{ route('customer.get_list_notif') }}',
        data: {},
        beforeSend: function () {
            // loadingPage();
        },

        success: function (response) {

                if (response.rc == 1) {    
                $('#get_list_notif').html(response.rm);
                $('#get_list_notif_mobile').html(response.rm);

                }

                $('#set_count_notif').html(response.jumlah);
                $('#set_count_notif_mobile').html(response.jumlah);

                
            }

        }).done(function (msg) {
            // endLoadingPage();
        
        }).fail(function (msg) {
            // endLoadingPage();
        });
        
        
    }else{
        mLoginShow();
    }
}

function setCartlist(id,type){
    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_cartlist') }}',
        data: {
            id:id,
            type:type
            },
        beforeSend: function () {
            // loadingPage();
        },

        success: function (response) {

                if (response.rc == 1) {    
                    shownotif(response.rm,'cart');      
                }

                $('#set_count_cartlist').html(response.jumlah);
                
            }

        }).done(function (msg) {
            // endLoadingPage();
        
        }).fail(function (msg) {
            // endLoadingPage();
        });
        
        
    }else{
        mLoginShow();
    }
}

function setWishlist(id,type) {
    var isLogin = '{{$isLogin}}';

    if (isLogin == 'y') {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        type: 'POST',
        url: '{{ route('customer.set_wishlist') }}',
        data: {
            id:id,
            type:type
            },
        beforeSend: function () {
            // loadingPage();
        },

        success: function (response) {

           
            

                if (response.rc == 1) {    
                    shownotif(response.rm,'wishlist');
                    $('#love'+response.rd_id+'_'+response.rd_type).html(`
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>
                    `);              
                }else if(response.rc == 2){
                   
                    shownotif(response.rm,'wishlist');
                    $('#love'+response.rd_id+'_'+response.rd_type).html(`
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                    `);   
                }

                $('#set_count_wishlist').html(response.jumlah);
                
            }

        }).done(function (msg) {
            // endLoadingPage();
        
        }).fail(function (msg) {
            // endLoadingPage();
        });
        
        
    }else{
        mLoginShow();
    }
    
}

function removeCartlist(id) {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        type: 'POST',
        url: '{{ route('customer.remove_cartlist') }}',
        data: {
            id:id
            },
        beforeSend: function () {
            // loadingPage();
        },

        success: function (response) {

                if (response.rc == 1) {    
                    shownotif(response.rm,'cart');      
                }

             mVerifHide();
                
            }

        }).done(function (msg) {
            location.reload(true);
        
        }).fail(function (msg) {
            // endLoadingPage();
        });
    
    
}
</script>
@include('master.component.icon')
