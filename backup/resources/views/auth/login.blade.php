<!DOCTYPE html>

<html lang="en">
<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Corellia</title>
    <meta name="description" content="Login HRIS">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages/login/login-6.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />

    <script src="{{asset('assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript">
    </script>

     <style media="screen">
        #login_pat {
        background: url('{{ asset('img/pat1.png') }}');
        background-repeat: repeat;
        margin-bottom: 0px;
        opacity: 1;
        }
    </style>
</head>
<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    <!-- Loading -->
    <div id="loading">
        <div class="lds-facebook">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <div id="login_pat" class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                    <div class="kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__body">
                                <div class="kt-login__logo mb-2">
                                    <a href="#">
                                        {{-- logo --}}
                                        {{-- <img src="{{asset('img/logo.jpg')}}" style="width: 100px;"> --}}
                                    </a>
                                </div>
                                <div class="kt-login__signin">
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">Corellia</h3>
                                         <div class="kt-login__desc">
                                "Corellia Management System"
                            </div>
                                    </div>
                                    <div class="kt-login__form">
                                        {{-- <form class="kt-form" method="POST" action="{{ route('login') }}"> --}}
                                        <form class="kt-form" id="loginForm">

                                                <!-- Alert Error -->
                                                <div class="alert alert-danger alert-dismissible d-none" id="alertError">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                </div>

                                                <!-- Alert Info -->
                                                <div class="alert alert-warning alert-dismissible d-none" id="alertInfo">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                </div>

                                                <!-- Alert Success -->
                                                <div class="alert alert-success alert-dismissible d-none" id="alertSuccess">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                </div>

                                            <div class="form-group">
                                                <input class="form-control" style="background: transparent;" type="text" placeholder="Username" name="username" autocomplete="off" id="inputUsername">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control form-control-last" style="background: transparent;" type="password" placeholder="Password" name="password" id="inputPassword">
                                            </div>
                                            <div class="kt-login__extra">

                                            </div>
                                            <div class="kt-login__actions">
                                                <a class="btn btn-brand btn-pill btn-elevate text-white" onclick="login();" style="cursor: pointer;">Sign In</a>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content"
                style="background-image: url({{asset('img/bg.jpg')}});">
                    <div class="kt-login__section">
                        <div class="kt-login__block">
                            <h3 class="kt-login__title">Corellia</h3>
                            <div class="kt-login__desc">
                                "Corellia Management System"
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--end:: Vendor Plugins -->
    <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        // enable enter in form login
        $('#loginForm').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                login();
            }
        });

        // disable enter in form reset password
        $('#form-data').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                return false;
            }
        });

        $(document).ready(function () {

            // validate reset password
            $("#form-data").bootstrapValidator({
                excluded: [':disabled'],
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    new_password: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Silahkan isi minimal 6 karakter'
                            }
                        }
                    },
                }
            }).on('success.field.bv', function (e, data) {
                var $parent = data.element.parents('.form-group');
                $parent.removeClass('has-success');
                $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
            });

            // validate login
            $("#loginForm").bootstrapValidator({
                excluded: [':disabled'],
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    username: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi'
                            },
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi'
                            },
                        }
                    },
                }
            }).on('success.field.bv', function (e, data) {
                var $parent = data.element.parents('.form-group');
                $parent.removeClass('has-success');
                $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
            });
        });

        function login(){
            var validateLogin = $('#loginForm').data('bootstrapValidator').validate();
            if (validateLogin.isValid()) {

                var formData = document.getElementById("loginForm");
                var objData = new FormData(formData);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('login') }}',
                    data: objData,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,

                    beforeSend: function () {
                        $("#alertInfo").addClass('d-none');
                        $("#alertError").addClass('d-none');
                        $("#alertSuccess").addClass('d-none');
                        $("#loading").css('display', 'block');
                    },

                    success: function (response) {
                        console.log(response);
                        $("#loading").css('display', 'none');
                        switch (response.rc) {
                            // password / username invalid
                            case 0:
                                $("#inputUsername").val('');
                                $("#inputPassword").val('');
                                $("#alertError").removeClass('d-none');
                                $("#alertError").text(response.rm);
                            break;

                            // akun tidak aktif
                            case 1:
                                $("#inputUsername").val('');
                                $("#inputPassword").val('');
                                $("#alertInfo").removeClass('d-none');
                                $("#alertInfo").text(response.rm);
                            break;

                            // reset password
                            case 2:
                                $("#inputUsername").val('');
                                $("#inputPassword").val('');
                                $("#form-data")[0].reset();
                                $('#form-data').bootstrapValidator("resetForm", true);
                                $("#modal").modal('show');
                                $("#id").val(response.id_user);
                            break;

                            // login success
                            case 3:
                                window.location.href = '{{ route('home') }}';
                            break;
                        }
                    }

                }).done(function (msg) {
                    $("#loading").css('display', 'none');
                }).fail(function (msg) {
                    $("#loading").css('display', 'none');
                    // toastr.error("Terjadi Kesalahan");
                });
            }
        }


    </script>



</body>
</html>
