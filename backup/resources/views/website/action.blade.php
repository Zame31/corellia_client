<script>

function set_active_product(menu) {
    $("[id^='p_']").removeClass('active');
    $('#p_'+menu).addClass('active');
}
function set_active_price(menu) {
    $("[id^='pr_']").removeClass('active');
    $('#pr_'+menu).addClass('active');
}


var tmp_type = '{{$type}}';
var tmp_type_id = '{{$type_id}}';
var tmp_u_color = '0';

function set_filter_u_harga(data) {
    tmp_u_harga = data;
}

function set_filter_u_color(data) {
    tmp_u_color = data;
}

get_list();

function get_list() {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    $.ajax({
        type: 'POST',
        url: '{{ route('website.get_list') }}',
        data: {
            type:tmp_type,
            type_id:tmp_type_id,
            u_color:tmp_u_color
        },
        beforeSend: function () {
            $('#list_katalog').html('<div class="loader">Loading...</div>');
            
        },
        success: function (response) {
            $('#list_katalog').html(response.rm);

            // endLoadingPage();

        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });
        

} 


</script>