@php
     if(Auth::user()){
        $isLogin = 'y';
     }else {
        $isLogin = 'n';
     }

   
@endphp


<script type="text/javascript">

$(document).ready(function () {
$("#form-data").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nama_website: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                },
                remote: {
                    message: 'Nama Website Telah Digunakan',
                    url: "{{ route('valid.checking','nama_website') }}",
                    data: function(validator) {
                            return {
                                isEdit: validator.getFieldElements('nama_website').val()
                            };
                        }
                }
            }
        },
        setName: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setNamePria: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setDescPria: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setNameWanita: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setDescWanita: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setTglRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam1Res: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam2Res: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setTempatRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setAlamatRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        date_active: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        bg_color: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        setKata: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:400,
                    message: 'Maksimal 400 Karakter'
                }
            }
        },
        setTglAkad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam1Akad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam2Akad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setAlamatAkad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        set_header: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        foto_pria: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        foto_wanita: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_1: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_2: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_3: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_4: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },

    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});

$("#form-data-edit").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nama_website: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                },
                remote: {
                    message: 'Nama Website Telah Digunakan',
                    url: "{{ route('valid.checking','nama_website') }}",
                    data: function(validator) {
                            return {
                                isEdit: validator.getFieldElements('nama_website').val()
                            };
                        }
                }
            }
        },
        setName: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setNamePria: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setDescPria: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setNameWanita: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setDescWanita: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setTglRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam1Res: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam2Res: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setTempatRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setAlamatRes: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        date_active: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        bg_color: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                }
            }
        },
        setKata: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:400,
                    message: 'Maksimal 400 Karakter'
                }
            }
        },
        setTglAkad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam1Akad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setJam2Akad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        setAlamatAkad: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                stringLength: {
                    max:100,
                    message: 'Maksimal 100 Karakter'
                }
            }
        },
        set_header: {
            validators: {
                file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        foto_pria: {
            validators: {
                file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        foto_wanita: {
            validators: {
               file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_1: {
            validators: {
              file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_2: {
            validators: {
              file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_3: {
            validators: {
                file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },
        gallery_4: {
            validators: {
                
                file: {
                        extension: 'jpg,jpeg,png,gif',
                        type: 'image/jpg,image/jpeg,image/png,image/gif',
                        maxSize: 2 * (1024*1024),
                        message: 'File Tidak Sesuai'
                    }
            }
        },

    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});
 


function set_qty(value) {
    
    $('#qty').html(value);
        $('#set_jumlah').val(value);
        set_total();
   
}


function set_total() {
    var qty = $('#qty').html();

    var price = $('#price').html();
    var voucher_amount = $('#voucher_amount').html();
    var voucher_percentage = $('#voucher_percentage').html();
    var s_per = $('#set_percentage').val();

    price = clearNumFormat(price);
    voucher_amount = clearNumFormat(voucher_amount);
    voucher_percentage = clearNumFormat(voucher_percentage);

    var tmp_total = qty * price;

    var p_per = (tmp_total*s_per)/100; 
    
    var total = tmp_total - voucher_amount;
    
    tmp_total = numFormat(tmp_total);
    total = numFormat(total);
    
    $('#voucher_percentage').html(numFormat(p_per));
    $('#tmp_total').html(tmp_total);
    $('#total').html(total);
}

function generateVoucher() {
    var voucher = $('#voucher').val();

    console.log(voucher);
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_voucher') }}',
        data: {
            id:voucher
        },

        beforeSend: function () {
            // loadingPage();
        },
        success: function (response) {
            console.log(response.rm);

            var msg = response.rm;
            
            // endLoadingPage();
            if (response.rc == 99) {
                shownotif('Voucher Tidak Ditemukan','other');
                $('#voucher_desc').html('Voucher Tidak Ditemukan');
            }else{
                shownotif('Voucher Berhasil Digunakan','other');

                var tmp_total = $('#tmp_total').html();

                tmp_total = clearNumFormat(tmp_total);

                var p_per = (tmp_total*response.rm.percentage)/100; 

                
                $('#set_percentage').val(response.rm.percentage);

                $('#voucher_amount').html(numFormat(response.rm.amount));
                // $('#voucher_percentage').html(numFormat(p_per));
                $('#voucher_desc').html(response.rm.desc);
                
                set_total();
            }
        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });

    
}

function setSize() {
   
    $('#sDesktop').hide();
    $('#sMobile').show();
    $('#set_size_frame').removeClass('col-6');
    $('#set_size_frame').addClass('col-12');
}

function setSizeM() {
   
    $('#sMobile').hide();
    $('#sDesktop').show();
    $('#set_size_frame').removeClass('col-12');
    $('#set_size_frame').addClass('col-6');
}

function setCartlistOther(id,type){
    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') { 

        
        var validateData = $('#form-data').data('bootstrapValidator').validate();
        if (validateData.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);
            
            var qty = $('#qty').html();
            console.log(qty);

            objData.append('id', id);
            objData.append('type', type);
            objData.append('qty', qty);


          
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $.ajax({
            type: 'POST',
            url: '{{ route('website.set_cartlist') }}',
          
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
            beforeSend: function () {
                loadingPage();
            },

            success: function (response) {

                    if (response.rc == 1) {    
                        shownotif(response.rm,'cart');      
                        $('#set_count_cartlist').html(response.jumlah);
                        location.href = "{{ route('customer.cartlist') }}";

                    }
                    
                }

            }).done(function (msg) {
                endLoadingPage();
            
            }).fail(function (msg) {
                endLoadingPage();
            });
        
    }else{
        shownotif('Data Belum Lengkap','error');
    }


    }else{
        mLoginShow();
    }
}


function setEditWebsite(productId,Id){
    var isLogin = '{{$isLogin}}';
    if (isLogin == 'y') { 


        var validateData = $('#form-data-edit').data('bootstrapValidator').validate();
        if (validateData.isValid()) {
            var formData = document.getElementById("form-data-edit");
            var objData = new FormData(formData);
            
            objData.append('Id', Id);
            objData.append('productId', productId);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $.ajax({
            type: 'POST',
            url: '{{ route('website.set_edit') }}',
          
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
            beforeSend: function () {
                loadingPage();
            },

            success: function (response) {

                    if (response.rc == 1) {    
                        shownotif(response.rm,'cart');    
                        location.href = "{{ route('customer.pembelian_list') }}";
                    }
                    
                }

            }).done(function (msg) {
                endLoadingPage();
            
            }).fail(function (msg) {
                endLoadingPage();
            });
        
    }else{
        shownotif('Data Belum Lengkap','error');
    }


    }else{
        mLoginShow();
    }
}

function changeHeader(id) {

    var file_size = $('#'+id)[0].files[0].size;
	if(file_size>2097152) {
        $('#form-data').bootstrapValidator('revalidateField', id);
		return false;
	} 
            
            
        var formData = document.getElementById("form-data");
        var objData = new FormData(formData);

        objData.append('id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{ route('website.set_image') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                loadingPage();
            },
            success: function (response) {
                console.log(response.rm);

                var web_img = "{{asset('gallery/website_img')}}/"+response.rm;
                var iframe = document.getElementById("myFrame");
                var elmnt = iframe.contentWindow.document.getElementById(id);
                
                if (id == 'foto_pria' || id == 'foto_wanita') {
                    elmnt.src = web_img;
                }else {
                    elmnt.style.backgroundImage = "url('"+web_img+"')";
                }

                

            }

        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
        });  
        
}


$('.tgl').datepicker({
        format: 'dd M yyyy',
        autoclose: true,
    });
    

function convertDateFormat(date) {

    var new_date = new Date(date);

    var bulan = new_date.getMonth()+1;
    var tanggal = ((new_date.getDate().toString().length == 1) ? '0' : '') + new_date.getDate();
    var tahun = new_date.getFullYear();
    var jam = ((new_date.getHours().toString().length == 1) ? '0' : '') + new_date.getHours();
    var minutes = ((new_date.getMinutes().toString().length == 1) ? '0' : '') + new_date.getMinutes();

    var bil_bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    return tanggal + " " + bil_bulan[bulan-1] + " " + tahun;
}

function setToFrame(id) {

   
     var text_head = $('#'+id).val();
     
     console.log(id);
     console.log(text_head);
     

     var iframe = document.getElementById("myFrame");
     
     if (id == 'setTglRes') {
        var elmnt2 = iframe.contentWindow.document.getElementById('setTglRes2');
        elmnt2.innerHTML = convertDateFormat(text_head);
     }

     if (id == 'setAlamatRes') {
        var elmnt3 = iframe.contentWindow.document.getElementById('setAlamatRes2');
        elmnt3.innerHTML = text_head;
     }

     var elmnt = iframe.contentWindow.document.getElementById(id);
     if (id == 'setTglRes' || id == 'setTglAkad' ) {
        elmnt.innerHTML = convertDateFormat(text_head);
     }else if(id == 'bg_color'){
        
        elmnt.style.background = text_head+'52';
       

     }else{
        elmnt.innerHTML = text_head;
     }
   }

  

</script>

<script type="text/javascript">
 function set_audio() {
     let music = $('#music').find(':selected').data('music');

    var audio = document.getElementById("set_music");

    audio.pause();
    audio.setAttribute('src',music);
    audio.load();
    audio.play();
 }
</script>


<script type="text/javascript">


// Initialize the map and assign it to a variable for later use
// var map = L.map('map', {
//     // Set latitude and longitude of the map center (required)
//     center: [37.7833, -122.4167],
//     // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
//     zoom: 10
// });

// L.control.scale().addTo(map);

// // Create a Tile Layer and add it to the map
// //var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
// L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
//   }).addTo(map);

//   var searchControl = new L.esri.Controls.Geosearch().addTo(map);

//   var results = new L.LayerGroup().addTo(map);

//   searchControl.on('results', function(data){
//     results.clearLayers();
//     for (var i = data.results.length - 1; i >= 0; i--) {
//       results.addLayer(L.marker(data.results[i].latlng));
//     }
//   });

// setTimeout(function(){$('.pointer').fadeOut('slow');},3400);

    
function updatePosition(lat,lng)
  {
    $('#maps_lat').val(lat);
    $('#maps_lng').val(lng);

    // console.log(latLng.lat());
    // console.log(latLng.lng());
    
  }

  </script>

  
<script src="{{asset('js/leaflet.js')}}"></script>
<script src="{{asset('js/esri-leaflet.js')}}"></script>
<script src="{{asset('js/esri-leaflet-geocoder.js')}}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.js"></script> --}}
{{-- <script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script> --}}



<script>
    // Initialize the map and assign it to a variable for later use
var map = L.map('map', {
    // Set latitude and longitude of the map center (required)
    center: [-6.908139, 107.608487],
    // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
    zoom: 13
});

L.control.scale().addTo(map);

// Create a Tile Layer and add it to the map
//var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var searchControl = new L.esri.Controls.Geosearch().addTo(map);

  var results = new L.LayerGroup().addTo(map);

  searchControl.on('results', function(data){
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {
      results.addLayer(L.marker(data.results[i].latlng));

      var coord = data.results[i].latlng.toString().split(',');
        var lat = coord[0].split('(');
        var lng = coord[1].split(')');

        updatePosition(lat[1],lng[0]);

        console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
    }
   

  });

setTimeout(function(){$('.pointer').fadeOut('slow');},3400);

map.on('click',function(e){
    var coord = e.latlng.toString().split(',');
    var lat = coord[0].split('(');
    var lng = coord[1].split(')');

    results.clearLayers();
    results.addLayer(L.marker([lat[1], lng[0]]));

    updatePosition(lat[1],lng[0]);
    
    console.log("You clicked the map at latitude: " + lat[1] + " and longitude:" + lng[0]);
});



</script>
  
  {{-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCvGd3nqk6_WsMY-ljjdWatOIJjMgUuOKI&signed_in=true&libraries=places'></script> --}}

{{-- <script src="https://maps.googleapis.com/maps/api/js??v=3.exp&key=AIzaSyCvGd3nqk6_WsMY-ljjdWatOIJjMgUuOKI&libraries=places&callback=initMap"></script> --}}