<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstDesignModel;
use App\Models\MstImageModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstDesignController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'z';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_design.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_design.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstDesignModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_design"))->first();
                $data = new MstDesignModel();
                $data->id = $get->max_id+1;
            }

            $data->product_id = $request->input('product_id');
            $data->color = $request->input('color');
            $data->shape = $request->input('shape');
            $data->foilcolor = $request->input('foilcolor');
            $data->keterangan = 1;
            $data->paper = 1;
           
            $data->cust_id = Auth::user()->id;

            for ($i=1; $i <= 4; $i++) { 

                if($request->hasFile('img'.$i)){
                    $destination_path = public_path('gallery/product');
                    $files = $request->file('img'.$i);
                    $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                    $upload_success = $files->move($destination_path, $filename);
                    
                    // DB::table('mst_image')
                    // ->where('product_id', $data->product_id)
                    // ->where('seq', $i)
                    // ->delete();

                    $data_img = new MstImageModel();
                    $data_img->product_id =  $data->product_id;
                    $data_img->img = $filename;
                    $data_img->seq = $i;
                    $data_img->save();

                    if ($i == 1) {
                        $data->image_id = $data_img->id;
                    }elseif ($i == 2) {
                        $data->image2 =  $data_img->id;
                    }elseif ($i == 3) {
                        $data->image3 =  $data_img->id;
                    }elseif ($i == 4) {
                        $data->image4 =  $data_img->id;
                    }
                   
                }

            }

            $data->save();
           

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data['other'] = collect(\DB::select("SELECT * FROM mst_design where id = ".$id))->first();
        $data['img'] = \DB::select("SELECT * FROM mst_image where id::varchar = '".$data['other']->image_id."' order by seq asc");
        $data['img2'] = \DB::select("SELECT * FROM mst_image where id::varchar = '".$data['other']->image2."' order by seq asc");
        $data['img3'] = \DB::select("SELECT * FROM mst_image where id::varchar = '".$data['other']->image3."' order by seq asc");
        $data['img4'] = \DB::select("SELECT * FROM mst_image where id::varchar = '".$data['other']->image4."' order by seq asc");
        
        // $data['img'] = \DB::select("SELECT * FROM mst_image where product_id = '".$data['other']->product_id."' order by seq asc");
       
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete($id)
    {
        // MstDesignModel::destroy($id);
        $get = collect(\DB::select("SELECT * from mst_design where id = ".$id))->first();

        DB::table('mst_image')
        ->where('id', $get->image_id)
        ->delete();

        DB::table('mst_design')
        ->where('id', $id)
        ->delete();

      
    }

    public function data(Request $request)
    {
     
        $data = \DB::select("select md.*,md.id as mid,rcs.name as foil_name, rc.color as color_name,rc.code,product_name 
        from mst_design md
        left join ref_color rc on rc.id = md.color
        left join ref_foilcolor rcs on rcs.id = md.foilcolor
        left join mst_undangan mu on mu.product_id = md.product_id
        ");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit('.$data->mid.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del('.$data->mid.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('shape',function($data) {
            return '<img width="50px" src="'.asset("gallery/shape/shape".$data->shape.".png").'" alt="" srcset="">';
        })
        ->editColumn('image_id',function($data) {

            $get = collect(\DB::select("SELECT * from mst_image where id = ".$data->image_id))->first();
            // return $data->image_id;

            // dd($get);
            return '<img width="50px" src="'.asset("gallery/product/".$get->img."").'" alt="" srcset="">';
        })
      
        ->rawColumns(['image_id','shape', 'action'])
        ->make(true);

    }



}
