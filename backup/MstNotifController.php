<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstNotifModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstNotifController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_notif.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_notif.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstNotifModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_notif"))->first();
                $data = new MstNotifModel();
                $data->id = $get->max_id+1;
            }
            $data->cust_id = 0;

            $data->notif = $request->input('notif');
            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM mst_notif where id = ".$id);
        return json_encode($data);
    }

    public function delete($id)
    {
        MstNotifModel::destroy($id);
    }


    

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT *,mst_notif.id as mid, mst_notif.created_at as dibuat FROM mst_notif 
        left join mst_customer on mst_notif.cust_id = mst_customer.id
        where cust_id = 0 order by mst_notif.created_at desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
          <a class="dropdown-item" onclick="edit('.$data->mid.')">
              <i class="la la-edit"></i>
              <span>Edit</span>
          </a>
          <a class="dropdown-item" onclick="del('.$data->mid.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('dibuat',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->dibuat));
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
