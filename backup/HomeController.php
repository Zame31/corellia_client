<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use App\Models\MstHomeModel;

use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {

        $param['aa'] = '';

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.home',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.home',$param);
        }
    }
    public function data($filter)
    {
  

        if ($filter == "all") {
            $f = "";
            $fEvent = "";
        }elseif ($filter == "t_year") {
            $f = " and EXTRACT(YEAR FROM transaction_time) = ".date('Y');
            $fEvent = " and EXTRACT(YEAR FROM crtdt) = ".date('Y');

        }elseif ($filter == "t_month") {
            $f = " and EXTRACT(MONTH FROM transaction_time) = ".date('m')." and EXTRACT(YEAR FROM transaction_time) = ".date('Y');
            $fEvent = " and EXTRACT(MONTH FROM crtdt) = ".date('m')." and EXTRACT(YEAR FROM crtdt) = ".date('Y');

        }else {
            $f = "";
            $fEvent = "";

        }

        $d_undangan = collect(\DB::select("select COALESCE(sum(mp.total_pembelian),0) as d from mst_pembelian mp
        join mst_undangan mu on mu.product_id= mp.product_id".$f))->first();

        $d_produk_lain = collect(\DB::select("select COALESCE(sum(mp.total_pembelian),0) as d from mst_pembelian mp
        join mst_other mu on mu.product_id= mp.product_id".$f))->first();

        $d_website = collect(\DB::select("select COALESCE(sum(mp.total_pembelian),0) as d from mst_pembelian mp
        join mst_website mu on mu.product_id= mp.product_id".$f))->first();

        $d_proses = collect(\DB::select("select COALESCE(sum(mp.id),0) as d 
        from mst_pembelian mp where mp.status <> 3".$f))->first();

        $d_pembayaran = collect(\DB::select("select COALESCE(sum(mp.total_pembelian),0) as d 
        from mst_pembelian mp where mp.status = 3".$f))->first();

        $d_p1 = collect(\DB::select("select count(id) as d from mst_pembelian mp where status = 0".$f))->first();
        $d_p2 = collect(\DB::select("select count(id) as d from mst_pembelian mp where status = 5".$f))->first();
        $d_p3 = collect(\DB::select("select count(id) as d from mst_pembelian mp where status = 1".$f))->first();
        $d_p4 = collect(\DB::select("select count(id) as d from mst_pembelian mp where status = 2".$f))->first();
        $d_p5 = collect(\DB::select("select count(id) as d from mst_pembelian mp where status = 3".$f))->first();

        $d_terlaris = \DB::select("select product_name, mp.product_id,count(mp.product_id) as d from mst_pembelian mp
        join mst_other mo on mo.product_id = mp.product_id GROUP BY product_name,mp.product_id
        UNION
        select product_name, mp.product_id,count(mp.product_id) from mst_pembelian mp
        join mst_website mw on mw.product_id = mp.product_id GROUP BY product_name,mp.product_id
        UNION
        select product_name, mp.product_id,count(mp.product_id) from mst_pembelian mp
        join mst_undangan mu on mu.product_id = mp.product_id GROUP BY product_name,mp.product_id
        order by d desc limit 5 ");


        

        // LINE GRAFIK
        $comma = ",";

        for ($bulan=1; $bulan <= 12; $bulan++) {
            if ($bulan == 12) {$comma = "";} else {$comma = ",";}

            $c_grafik_undangan = collect(\DB::select("select COALESCE(count(mp.id),0) as d from mst_pembelian mp
            join mst_undangan mu on mu.product_id= mp.product_id where EXTRACT(MONTH FROM mp.created_at) = ".$bulan))->first();

            $temp_grafik_undangan[] = $c_grafik_undangan->d;

            $c_grafik_lainnya = collect(\DB::select("select COALESCE(count(mp.id),0) as d from mst_pembelian mp
            join mst_other mu on mu.product_id= mp.product_id where EXTRACT(MONTH FROM mp.created_at) = ".$bulan))->first();

            $temp_grafik_lainya[] = $c_grafik_lainnya->d;

            $c_grafik_website = collect(\DB::select("select COALESCE(count(mp.id),0) as d from mst_pembelian mp
            join mst_website mu on mu.product_id= mp.product_id where EXTRACT(MONTH FROM mp.created_at) = ".$bulan))->first();

            $temp_grafik_website[] = $c_grafik_website->d;
        }

        $data_grafik['Undangan'] = $temp_grafik_undangan;
        $data_grafik['Kebutuhan Lainnya'] = $temp_grafik_lainya;
        $data_grafik['Website'] = $temp_grafik_website;

        $data['d_undangan'] = $d_undangan;
        $data['d_produk_lain'] = $d_produk_lain;
        $data['d_proses'] = $d_proses;
        $data['d_pembayaran'] =$d_pembayaran;
        $data['d_website'] =$d_website;
        $data['d_p1'] =$d_p1;
        $data['d_p2'] =$d_p2;
        $data['d_p3'] =$d_p3;
        $data['d_p4'] =$d_p4;
        $data['d_p5'] =$d_p5;
        $data['d_terlaris'] =$d_terlaris;

        

        //LINE GRAFIK
        $data['grafik'] = $data_grafik;

    return json_encode($data);
    }

    public function audit_trail(Request $request)
    {
        $tittle = 'Audit Trail';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.audit_trail.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.audit_trail.list',$param);
        }
    }

    public function data_audit_trail(Request $request)
    {
        
        $data = \DB::select("SELECT * from audits order by created_at desc");  
       return DataTables::of($data) 
       ->editColumn('created_at',function($data) {
        return date('d M Y, h:i',strtotime($data->created_at));
    })
        ->rawColumns(['cek', 'action'])
        ->make(true);
    }

    public function index_mst_home(Request $request)
    {
        
        $data = collect(\DB::select("SELECT * FROM mst_home"))->first();
     
        $param['data'] = $data;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_home.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_home.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstHomeModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_home"))->first();
                $data = new MstHomeModel();
                $data->id = $get->max_id+1;
            }

            $data->promo_head = $request->input('promo_head');
            $data->promo_subhead1 = $request->input('promo_subhead1');
            $data->promo_subhead2 = $request->input('promo_subhead2');
            $data->type1 = $request->input('type1');
            $data->type2 = $request->input('type2');
            $data->type3 = $request->input('type3');
            $data->product_id1 = $request->input('product_id1');
            $data->product_id2 = $request->input('product_id2');
            $data->product_id3 = $request->input('product_id3');
            $data->promo_banner_desc1 = $request->input('promo_banner_desc1');
            $data->promo_banner_desc2 = $request->input('promo_banner_desc2');
            $data->promo_banner_desc3 = $request->input('promo_banner_desc3');


            for ($i=1; $i <= 3; $i++) { 

                if($request->hasFile('img'.$i)){
                    $destination_path = public_path('gallery/home');
                    $files = $request->file('img'.$i);
                    $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                    $upload_success = $files->move($destination_path, $filename);

                    if ($i == 1) {
                        $data->promo_banner_img1 = $filename;
                    }elseif ($i == 2) {
                        $data->promo_banner_img2 = $filename;
                    }elseif ($i == 3) {
                        $data->promo_banner_img3 = $filename;
                    }
                   
                }

            }

            $data->save();
           

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


}
