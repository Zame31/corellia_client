<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstCustomerModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstCustomerController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_customer.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_customer.list',$param);
        }
    }


    public function delete($id)
    {
        MstCustomerModel::destroy($id);
    }

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT cust_name,mc.id,email,phone_no,address,count(mp.id) as jumlah_pembelian 
        FROM mst_customer mc
        left join mst_pembelian mp on mp.custid = mc.id 
        GROUP BY cust_name,mc.id,email,phone_no,address,mc.created_at
        order by mc.created_at desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {

       

        if ($data->jumlah_pembelian == 0) {
            return '
            <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
             
              <a class="dropdown-item" onclick="del('.$data->id.')">
                  <i class="la la-trash"></i>
                  <span>Delete</span>
              </a>
            </div>
        </div>
            ';
        }else{
            return '-';
        }
  
        
        })
      
        ->editColumn('jumlah_pembelian',function($data) {
            return $data->jumlah_pembelian.' Produk';
        })
        
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
