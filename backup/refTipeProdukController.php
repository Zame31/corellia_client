<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\refTipeProdukModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class refTipeProdukController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.ref_tipe_produk.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.ref_tipe_produk.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = refTipeProdukModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM ref_type_produk"))->first();
                $data = new refTipeProdukModel();
                $data->id = $get->max_id+1;
            }

            $data->type_name = $request->input('type_name');
            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM ref_type_produk where id = ".$id);
        return json_encode($data);
    }


    public function delete($id)
    {
        refTipeProdukModel::destroy($id);
    }

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * FROM ref_type_produk");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
         
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
      
        
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
