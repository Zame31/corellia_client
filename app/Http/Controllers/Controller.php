<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\MasterTxModel;
use Auth;
use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\BDDModel;
use App\Models\BDDScModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;
use App\Models\NotifModel;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function numFormat($nominal)
    {
        return number_format($nominal,0,",",".");
    }

    public function sendNotif($cust_id,$text,$type)
    {
        $data = new NotifModel();
        $data->cust_id = $cust_id;
        $data->notif = $text;
        $data->type = $type;

        $data->save();
    }

    
    public function sendMail($view,$data)
    {

        Mail::send($view, $data, function ($message) use ($data) {
            $message->from('znurzamanz@gmail.com', 'Corellia - Wedding Art');
            $message->to($data['sendto']);
            $message->subject($data['subject']); 
        });
        // return redirect()->back()->with('message','Successfully Send Your Mail Id.');

    // Mail::to('znurzamanz@gmail.com')->send(new SendMailable());
    
    // return 'Email was sent';
    }


    public function clearSeparator($nominal)
    {
        return str_replace('.','',$nominal);
    }


    public function MonthIndo($data)
    {
        $bulan = array (
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $bulan[$data];
    }

    public function endSession()
    {
        Auth::logout();
        // return redirect()->route('login');
        return response()->json([
            'rc' => 0,
            'rm' => "sukses"
        ]);
    }

    
    public function checking($type)
    {
        if ($_GET['isEdit']) {

         
            if ($type == "nama_website") {
                $data = collect(\DB::select("SELECT * FROM mst_data_website where nama_website = ?",[$_GET['isEdit']]))->first();

                // $valid = true;

                if($data){
                    if ($_GET['nama_website'] == $data->nama_website) {
                        $isAvailable = true;
                    }
                }else{
                    $data = \DB::select("SELECT * FROM mst_data_website where nama_website = ?",[$_GET['nama_website']]);
                    $isAvailable = (count($data) > 0) ? false : true ;
                }
                
                    

            }

        }else{
            if ($type == "r_username") {
                $data = \DB::select("SELECT * FROM mst_customer where username = ?",[$_GET['r_username']]);
            }elseif ($type == "r_email") {
                $data = \DB::select("SELECT * FROM mst_customer where email = ?",[$_GET['r_email']]);
            }elseif ($type == "nama_website") {
    
                // DELETE WEBSITE KADALUARSA
                $dataw = \DB::select("SELECT * from mst_data_website");
                if ($dataw) {
                    foreach ($dataw as $key => $value) {
                        $d_lama = collect(\DB::select("SELECT * FROM mst_pembelian where design_id = ?",[$value->id]))->first();
                        
                        if ($d_lama) {
                            $set_lama = "+".$d_lama->qty." week";
                            $enddate = strtotime($value->date_active);
                            $enddate = strtotime($set_lama, $enddate);
                            if (date("Y-m-d H:i:s") >= date('Y-m-d H:i:s',$enddate)) {
                                DB::table('mst_data_website')
                                ->where('id', $value->id)
                                ->delete();
                            }
                        }
    
                       
                    }
                }
    
                $data = \DB::select("SELECT * FROM mst_data_website where nama_website = ?",[$_GET['nama_website']]);
            } 

            $isAvailable = (count($data) > 0) ? false : true ;
        }
      


      

        echo json_encode(array(
            'valid' => $isAvailable,
        ));

        


    }


}
