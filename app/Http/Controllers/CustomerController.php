<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Session;
use Request as Req;
use Illuminate\Support\Collection;
use App\Models\WishlistModel;
use App\Models\CartlistModel;
use App\Models\UserModel;
use App\Models\PembelianModel;
use App\Models\PembayaranModel;

use App\Events\StatusLiked;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    public function customer_notif(Request $request)
    {
        $data = \DB::select("SELECT * FROM mst_notif where cust_id = ? order by id desc",[$request->post('id')]);

        $c_notif = collect(\DB::select("SELECT count(cust_id) as j FROM mst_notif where is_read is null and cust_id = ? GROUP BY cust_id",[$request->post('id')]))->first();

        $numNotif = 0;
        if ($c_notif) {
            $numNotif = $c_notif->j;
        }

        return response()->json([
            'rc' => 1,
            'rm' => $data,
            'jumlah' => $numNotif
        ]);
    }


    public function list_custom_design(Request $request)
    {
        return view('master.master')->nest('child', 'customer.list_custom_design');
    }

    public function pembelian_list(Request $request)
    {
        $pembelian = \DB::select("SELECT DISTINCT additional,is_note_undangan,pengiriman,ongkir,note,response_admin,note_undangan,billing,discount,response_admin,created_at,end_date,status,status_name,no_pembelian,total_pembelian
        from mst_pembelian
        join ref_status on ref_status.id = mst_pembelian.status where custid = ".Auth::user()->id." order by created_at desc");

        $param['pembelian'] = $pembelian;


        return view('master.master')->nest('child', 'customer.pembelian_list',$param);
    }

    public function account(Request $request)
    {
        $profile = collect(\DB::select("SELECT * FROM mst_customer where id = ".Auth::user()->id))->first();

        $param['profile'] = $profile;
        return view('master.master')->nest('child', 'customer.account',$param);
    }

    public function get_list_notif(Request $request)
    {
        $notif =\DB::select("SELECT * FROM mst_notif where cust_id = ".Auth::user()->id." or cust_id = 0 order by created_at desc");

        $list_notif = '';
        $code = '';

        foreach ($notif as $key => $value) {
            if ($value->cust_id == 0) {
                $link_get = '#';
            }else{
                if ($value->type == '5' || $value->type == '6') {
                    $code = '?tab=1';
                }elseif ($value->type == '2') {
                    $code = '?tab=3';
                }elseif ($value->type == '3') {
                    $code = '?tab=4';
                }

                $link_get = route('customer.pembelian_list').$code;
            }
            $list_notif .= ' <a href="'.$link_get.'" class="zn-list-notif">
                                <div style="display: inline-block;width: 11%;margin-top: 15px;vertical-align: top;">
                                    <i class="flaticon2-bell-4 zn-notif-icon" ></i>
                                </div>
                                <div  style="display:inline-block;width: 80%;">
                                    <div>'.$value->notif.'</div>
                                    <small style="color: #dadada;">'.date('d-m-Y H:i:s', strtotime($value->created_at)).'</small>
                                </div>
                            </a>';
        }

        $list_notif .= '<div class="zn-list-notif">
        <div style="display: inline-block;width: 11%;margin-top: 15px;vertical-align: top;">
            <i class="flaticon2-bell-4 zn-notif-icon" ></i>
        </div>
        <div style="display:inline-block;width: 80%;">
            <div>Selamat Datang '.Auth::user()->cust_name.', Selamat berbelanja di toko kami</div>
            <small style="color: #dadada;">by Corellia</small>
        </div>
    </div>';

    DB::table('mst_notif')
            ->where('cust_id', Auth::user()->id)
            ->update(['is_read' => 1]);

    $c_notif = collect(\DB::select("SELECT count(id) as j from mst_notif where is_read IS NULL and cust_id = ".Auth::user()->id))->first();


        return response()->json([
            'rc' => 1,
            'rm' => $list_notif,
            'jumlah' => $c_notif->j

        ]);

    }

    public function passwordbaru(Request $request)
    {
        try{
            $data = UserModel::find(Auth::user()->id);
            $data->password = Hash::make($request->input('pro_r_password'));

            $data->save();

            return response()->json([
                'rc' => 1,
                'rm' => 'Berhasil'
            ]);

        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function batalkan_pembelian(Request $request)
    {
        DB::table('mst_pembelian')
            ->where('no_pembelian', $request->input('no_pembelian'))
            ->update(['status' => 7]);

            $getNoPembelian = $request->input('no_pembelian');

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = ?",[$getNoPembelian]))->first();

        // ADMIN SEND
        $get_admin = \DB::select("SELECT * from admin where role::int in (1,2)");
        foreach ($get_admin as $key => $value) {
            $toEmail[] = $value->email;
            $this->sendNotif($value->id,"Pesanan Dibatalkan Dengan No Pembelian : ".$getNoPembelian,"7");
        }

        $data_admin = array(
            'sendto'=> $toEmail,
            'pembelian' => $param_email,
            'note_trace' => "Pesanan Dibatalkan Dengan No Pembelian : ".$getNoPembelian,
            'subject' => 'Pesanan Dibatalkan');


        $this->sendMail('email.lacak',$data_admin);

        event(new StatusLiked('Kirim Notif Ke Admin'));


            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);
    }

    public function wishlist(Request $request)
    {
        $dm = \DB::select("SELECT * from mst_wishlist mw
        join mst_other mo on mo.product_id = mw.product_id where cust_id = ".Auth::user()->id);

        $d_undangan = \DB::select("SELECT * from mst_wishlist mw
        join mst_undangan mo on mo.product_id = mw.product_id where cust_id = ".Auth::user()->id);

        $d_website = \DB::select("SELECT * from mst_wishlist mw
        join mst_website mo on mo.product_id = mw.product_id where cust_id = ".Auth::user()->id);

        $param['dm'] = $dm;
        $param['d_undangan'] = $d_undangan;
        $param['d_website'] = $d_website;

        return view('master.master')->nest('child', 'customer.wishlist',$param);
    }

    public function cartlist(Request $request)
    {
        $dm = \DB::select("SELECT  mw.*,mw.id as cartlist_id,product_name,price
        from mst_cartlist mw
        join mst_other mo on mo.product_id = mw.product_id  where mw.type IN (2,3,4) and cust_id = ".Auth::user()->id." order by mw.created_at desc");

        $d_undangan = \DB::select("SELECT  mw.*,mw.id as cartlist_id,md.color,md.shape,md.paper,md.image_id,product_name,rts.min_pembelian,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name,rrr.name as finishing,sss.additional_name as add_free,rw.harga as harga_waxseal
                from mst_cartlist mw
                 left join mst_undangan mo on mo.product_id = mw.product_id
                        left join mst_design md on md.id = mw.design_id
                        left join ref_color rc on rc.id = md.color
                        left join ref_paper rp on rp.id = mw.paper_inner
                        left join ref_paper rpp on rpp.id = mw.paper_outer
                        left join ref_waxseal rw on rw.id = mw.waxseal
                        left join ref_foilcolor rf on rf.id = mw.foilcolor
                        left join ref_type_size rts on rts.id = mw.type_size
                        left join ref_finishing rrr on rrr.id::varchar = mw.free_finishing::varchar
                        left join ref_additional sss on sss.id::varchar = mw.free_additional::varchar

                 where mw.type = 1 and mw.cust_id = ".Auth::user()->id." order by mw.created_at desc");

        $d_website = \DB::select("SELECT  mw.*,mw.id as cartlist_id,product_name,price,img,nama_website,mdw.id as id_website
        from mst_cartlist mw
        left join mst_website mo on mo.product_id = mw.product_id
		left join mst_data_website mdw on mdw.cartlist_id::int = mw.id::int  
                where mw.cust_id = ".Auth::user()->id." and mw.type = 5 order by mw.created_at desc");


        $param['dm'] = $dm;
        $param['d_undangan'] = $d_undangan;
        $param['d_website'] = $d_website;

        return view('master.master')->nest('child', 'customer.cartlist',$param);
    }

    public function checkout(Request $request)
    {
        $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();

        $param['cart'] = Session::get('my_cart');
        $param['cust'] = $cust;



        if (Req::ajax()) {
            return view('master.only_content')->nest('child',  'customer.checkout',$param);
        }else {
            return view('master.master')->nest('child',  'customer.checkout',$param);
        }
    }

    public function invoice(Request $request)
    {
        $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();
        $pembelian = collect(\DB::select("SELECT * from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int where custid = ".Auth::user()->id." order by mst_pembelian.id desc"))->first();

        $param['cart'] = Session::get('my_cart');
        $param['cust'] = $cust;
        $param['pembelian'] = $pembelian;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.invoice',$param);
        }else {
            return view('master.master')->nest('child', 'customer.invoice',$param);
        }
    }


    public function pay($code,Request $request)
    {
        $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();
        $pembelian = collect(\DB::select("SELECT * from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int where no_pembelian = ?",[$code]))->first();

        $pembelian_list = \DB::select("SELECT * from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int where no_pembelian = ?",[$code]);

        $param['cart'] = Session::get('my_cart');
        $param['cust'] = $cust;
        $param['pembelian'] = $pembelian;
        $param['pembelian_list'] = $pembelian_list;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.pay',$param);
        }else {
            return view('master.master')->nest('child', 'customer.pay',$param);
        }
    }

    public function get_detail_pembayaran(Request $request)
    {
        $datas = \DB::select("SELECT * from mst_pembayaran where no_pembelian = ? order by id desc",[$request->input('no_pembelian')]);

        return response()->json([
            'rc' => 1,
            'rm' => $datas

        ]);
    }

    public function get_profil(Request $request)
    {
        $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();

        return response()->json([
            'rc' => 1,
            'rm' => $cust

        ]);
    }

    public function set_pembelian(Request $request)
    {

        try{
            $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();

            if ($cust->address == null || $cust->phone_no == null || $cust->postal_code == null) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Data Profil Belum Lengkap, Silahkan Lengkapi Terlebih Dahulu'
                ]);
            }

            if ($request->input('note') == null || $request->input('note') == '') {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Catatan Tidak Boleh Kosong'
                ]);
            }


            $cart = Session::get('my_cart');
            $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_pembelian"))->first();

            if ($get) {
                $idpem = $get->max_id+1;
            }else {
                $idpem = 1;
            }

            $end_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
            $created_at = date('Y-m-d H:i:s');



            foreach ($cart['product_name'] as $key => $v) {

                $data = new PembelianModel();
                $data->product_id = $cart['product_id'][$key];
                $data->qty = $cart['count_product'][$key];
                $data->totalprice = $cart['total_per'][$key];
                $data->discount = $cart['voucher_percentage']+$cart['voucher_amount'];
                $data->custid = $cust->id;
                $data->end_date = $end_date;
                $data->bank = $request->input('bank');
                $data->accbank = $request->input('bank');
                $data->note = $request->input('note');

                if (Auth::user()->id == '9999') {
                    $data->status = 3;
                }else{
                    $data->status = 0;
                }

                $data->no_pembelian = 'CO'.date('Ymd').$cust->id.$idpem;
                $data->created_at = $created_at;

                $data->pengiriman = $request->input('pengiriman');

                // JIKA UNDANGAN
                if ($cart['info_type'][$key] == 1) {
                    $data->type_id = 1;
                    $data->design_id = $cart['design_id'][$key];
                    $data->waxseal = $cart['waxseal_id'][$key];
                    $data->foilcolor = $cart['foilcolor_id'][$key];
                    $data->type_size = $cart['type_size_id'][$key];
                    $data->paper_inner = $cart['info_kertas_id'][$key];
                    $data->paper_outer = $cart['info_kertas2_id'][$key];
                    $data->free_finishing = $cart['free_finishing_id'][$key];
                    $data->free_additional = $cart['free_additional_id'][$key];
                    $data->additional_user = $cart['additional_item'][$key];
                    $data->additional_user_count = $cart['additional_item_count'][$key];
                    $data->emboss = $cart['emboss'][$key];
                }

                if ($cart['info_type'][$key] == 5) {
                    $data->design_id = $cart['design_id'][$key];
                    $data->type_id = 5;
                }



                if ($request->input('pengiriman') == 'dikirim') {
                    $data->ongkir = $request->input('ongkir');
                    $total_final = $cart['total']+$request->input('total_add_item')+$request->input('ongkir');
                }else{
                    $data->ongkir = 0;
                    $total_final = $cart['total']+$request->input('total_add_item');
                }

                $data->billing = $total_final;

                $data->total_pembelian = $total_final;


                $data->save();

                DB::table('mst_cartlist')
                ->where('cust_id', Auth::user()->id)
                ->where('product_id', $cart['product_id'][$key])
                ->where('design_id', $cart['design_id'][$key])
                ->delete();

            }

            DB::table('mst_voucher')
            ->where('code', $cart['voucher'])
            ->update(['vouchersts' => 1]);


            //SEND MAIL

            $getNoPembelian = $data->no_pembelian;

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = ?",[$data->no_pembelian]))->first();


            $data = array('sendto'=> $param_email->email,'pembelian' => $param_email, 'subject' => 'Pembelian');

            $this->sendMail('email.checkout',$data);

            // ADMIN SEND
            $get_admin = \DB::select("SELECT * from admin where role::int in (1,2)");
            foreach ($get_admin as $key => $value) {
                $toEmail[] = $value->email;
                $this->sendNotif($value->id,"Ada Pesanan Baru Dengan No Pembelian : ".$getNoPembelian,"0");
            }

            $data_admin = array('sendto'=> $toEmail,'pembelian' => $param_email, 'subject' => 'Pembelian');

            // $this->sendMail('email.checkout',$data_admin);

            $this->sendMail('email.checkout',$data_admin);



            event(new StatusLiked('Kirim Notif Ke Admin'));

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);
        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }


    }


    public function set_pembayaran(Request $request)
    {
        try{

            $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_pembayaran"))->first();

            if ($get) {
                $getid = $get->max_id+1;
            }else {
                $getid = 1;
            }

            $file = $request->file('bukti_pembayaran');
            
            $setSize = 0.5 * (1024*1024);
            if ($file->getSize() > $setSize) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Size File Maksimal 500 kb'
                ]);
            }

            $exceptFile = ['jpg','jpeg','png','JPG','JPEG','PNG','pdf','PDF','xlsx','xls'];
            if (!in_array($file->getClientOriginalExtension(), $exceptFile))
            {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'file tidak diperbolehkan!'
                ]);
            }

            $imgname = date('dmYhis').preg_replace("/\s+/","",$file->getClientOriginalName());
            $file->move(public_path('/gallery/pembayaran/'), $imgname);

            $data = new PembayaranModel();
            $data->no_pembelian = $request->input('no_pembelian');
            $data->nominal_transfer = $this->clearSeparator($request->input('nominal_transfer'));
            $data->bukti_pembayaran = $imgname;
            $data->bank = $request->input('bank');
            $data->atas_nama = $request->input('atas_nama');
            $data->no_rek = $request->input('no_rek');
            $data->catatan = $request->input('catatan');
            $data->status = 0;
            $data->kode_pembayaran = 'PO'.$getid.date('ymdhis');

            $data->save();

            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->input('no_pembelian'))
            ->update(['status' => 1]);

             // ADMIN SEND
             $getNoPembelian = $request->input('no_pembelian');

             $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = ?",[$getNoPembelian]))->first();


            //  $get_admin = \DB::select("SELECT * from admin where role in (1,2)");
             $get_admin = \DB::select("SELECT * from admin where id = ".$param_email->custtempid);

             foreach ($get_admin as $key => $value) {
                 $toEmail[] = $value->email;
                 $this->sendNotif($value->id,"No Pembelian : ".$getNoPembelian." Melakukan Pembayaran","1");
             }

             $data_admin = array('sendto'=> $toEmail,'pembelian' => $param_email, 'subject' => 'Pembelian');

             // $this->sendMail('email.checkout',$data_admin);

            //  $this->sendMail('email.checkout',$data_admin);



             event(new StatusLiked('Kirim Notif Ke Admin'));



            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);
        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }


    }


    public function set_profil(Request $request)
    {
        try{
            $data = UserModel::find(Auth::user()->id);
            $data->cust_name = $request->input('profil_nama');
            // $data->email = $request->input('profil_email');
            // $data->username = $request->input('profil_username');
            $data->phone_no = $request->input('profil_telepon');
            $data->address = $request->input('profil_alamat');
            $data->postal_code = $request->input('profil_pos');

            $data->save();

            $cust = collect(\DB::select("SELECT * from mst_customer where id = ".Auth::user()->id))->first();

            return response()->json([
                'rc' => 1,
                'rm' => $cust
            ]);

        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }



    }

    public function set_note_undangan(Request $request)
    {
        $stat = '';
        $note_staff = '';


        $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
        left join mst_customer mc on mc.id = mst_pembelian.custid
        where no_pembelian = '".$request->input('no_pembelian')."' "))->first();

        //  $get_admin = \DB::select("SELECT * from admin where role in (1,2)");
        $get_admin = collect(\DB::select("SELECT * from admin where id = ".$param_email->custtempid))->first();

        // if (Auth::user()->role) {
            if ($request->input('is_admin')) {
                    $stat = ' Staff '.$get_admin->name;
                    $note_staff = '

                    '.$get_admin->name." - ".date('d-m-Y H:i:s');
            }
        // }

        // dd(Auth::user()->role);


        DB::table('mst_pembelian')
            ->where('no_pembelian', $request->input('no_pembelian'))
            ->update([
                'note_undangan' => $request->input('note_undangan').$note_staff,
                'is_note_undangan' => '1'
                ]);



            // foreach ($get_admin as $key => $value) {
            //     // $toEmail[] = $value->email;

            // }

            $this->sendNotif($get_admin->id,"No Pembelian : ". $request->input('no_pembelian').$stat." Melakukan Pengisian Catatan Undangan",$param_email->status);

            if ($param_email->designer != null) {
                $this->sendNotif($param_email->designer,"No Pembelian : ". $request->input('no_pembelian').$stat." Melakukan Pengisian Catatan Undangan","77");

            }

            $this->sendNotif($param_email->custid,"No Pembelian : ". $request->input('no_pembelian').$stat." Melakukan Pengisian Catatan Undangan",$param_email->status);

            // $data_admin = array('sendto'=> $toEmail,'pembelian' => $param_email, 'subject' => 'Pembelian');

            // $this->sendMail('email.checkout',$data_admin);

            // $this->sendMail('email.checkout',$data_admin);



            event(new StatusLiked('Kirim Notif Ke Admin'));

        return response()->json([
            'rc' => 1,
            'rm' => 'berhasil'

        ]);
    }

    public function set_session_undangan(Request $request)
    {
       $sess_undangan['tmp_color'] = $request->input('tmp_color');
       $sess_undangan['tmp_u_waxseal'] = $request->input('tmp_u_waxseal');
       $sess_undangan['tmp_printing'] = $request->input('tmp_printing');
       $sess_undangan['type_size'] = $request->input('type_size');
       $sess_undangan['paper_inner'] = $request->input('paper_inner');
       $sess_undangan['paper_outer'] = $request->input('paper_outer');
       $sess_undangan['tmp_u_shape'] = $request->input('tmp_u_shape');
       $sess_undangan['tmp_finishing'] = $request->input('free_finishing');
       $sess_undangan['tmp_free_add'] = $request->input('free_additional');
       $sess_undangan['qty'] = $request->input('qty');
       $sess_undangan['type_size'] = $request->input('type_size');


       Session::flush();

       Session::put('sess_undangan', $sess_undangan);

       return response()->json([
        'rc' => 1,
        'rm' => $sess_undangan

    ]);

    }

    public function set_checkout(Request $request)
    {
        $my_cart['p_per'] = $request->input('p_per');
        $my_cart['product_name'] = $request->input('product_name');
        $my_cart['count_product'] = $request->input('count_product');
        $my_cart['price'] = $request->input('price');
        $my_cart['total_per'] = $request->input('total_per');
        $my_cart['sub_total'] = $request->input('sub_total');
        $my_cart['voucher_amount'] = $request->input('voucher_amount');
        $my_cart['voucher_percentage'] = $request->input('voucher_percentage');
        $my_cart['total'] = $request->input('total');
        $my_cart['product_id'] = $request->input('product_id');
        $my_cart['info_warna'] = $request->input('info_warna');
        $my_cart['info_waxseal'] = $request->input('info_waxseal');
        $my_cart['info_kertas'] = $request->input('info_kertas');
        $my_cart['info_foilcolor'] = $request->input('info_foilcolor');
        $my_cart['info_type'] = $request->input('info_type');
        $my_cart['design_id'] = $request->input('design_id');
        $my_cart['waxseal_id'] = $request->input('waxseal_id');
        $my_cart['foilcolor_id'] = $request->input('foilcolor_id');
        $my_cart['voucher'] = $request->input('voucher');
        $my_cart['emboss'] = $request->input('emboss');
        $my_cart['free_finishing'] = $request->input('free_finishing');
        $my_cart['free_additional'] = $request->input('free_additional');
        $my_cart['additional_item'] = $request->input('additional_item');
        $my_cart['additional_item_count'] = $request->input('additional_item_count');
        $my_cart['info_kertas2'] = $request->input('info_kertas2');
        $my_cart['info_kertas_id'] = $request->input('info_kertas_id');
        $my_cart['info_kertas2_id'] = $request->input('info_kertas2_id');
        $my_cart['free_finishing_id'] = $request->input('free_finishing_id');
        $my_cart['free_additional_id'] = $request->input('free_additional_id');
        $my_cart['type_size_id'] = $request->input('type_size_id');
        $my_cart['harga_waxseal'] = $request->input('harga_waxseal');

        Session::flush();

        Session::put('my_cart', $my_cart);



        return response()->json([
            'rc' => 1,
            'rm' => 'berhasil'

        ]);

    }


    public function set_wishlist(Request $request)
    {
        try{

            $dm = \DB::select("SELECT * from mst_wishlist where product_id = ? and cust_id = ?",[$request->input('id'),Auth::user()->id]);

            if ( $request->input('type') == 1) {
                $d_name = collect(\DB::select("SELECT * from mst_undangan where product_id = ?",[$request->input('id')]))->first();
            }
            elseif ( $request->input('type') == 5) {
                $d_name = collect(\DB::select("SELECT * from mst_website where product_id = ?",[$request->input('id')]))->first();
            }
            else{
                $d_name = collect(\DB::select("SELECT * from mst_other where product_id = ?",[$request->input('id')]))->first();
            }

            if (count($dm) > 0 ) {

                WishlistModel::destroy($dm[0]->id);

                $c_wish = collect(\DB::select("SELECT count(id) as j from mst_wishlist where cust_id = ".Auth::user()->id))->first();

                return response()->json([
                    'rc' => 2,
                    'rm' => $d_name->product_name.' Berhasil Dihilangkan dari Wishlist',
                    'rd_id' => $dm[0]->product_id,
                    'rd_type' => $dm[0]->type,
                    'jumlah' => $c_wish->j

                ]);

            }else {

                $data = new WishlistModel();
                $data->type = $request->input('type');
                $data->product_id = $request->input('id');
                $data->cust_id = Auth::user()->id;
                $data->save();

                $c_wish = collect(\DB::select("SELECT count(id) as j from mst_wishlist where cust_id = ".Auth::user()->id))->first();

                return response()->json([
                    'rc' => 1,
                    'rm' => $d_name->product_name.' Berhasil Ditambahkan ke Wishlist',
                    'rd_id' => $request->input('id'),
                    'rd_type' => $request->input('type'),
                    'jumlah' => $c_wish->j
                ]);

            }




        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function set_cartlist(Request $request)
    {
        try{

            $dm = \DB::select("SELECT *
            from mst_cartlist where product_id = ? and cust_id = ?",[$request->input('id'),Auth::user()->id]);

            if ( $request->input('type') == 1) {
                $d_name = collect(\DB::select("SELECT * from mst_undangan where product_id = ?",[$request->input('id')]))->first();

                if ($request->input('tmp_color')) {
                    $design = collect(\DB::select("SELECT * from mst_design
                    where color = ".$request->input('tmp_color')." and
                    shape = ".$request->input('tmp_u_shape')." and
                    paper = ".$request->input('tmp_u_paper')." and
                    foilcolor = ".$request->input('tmp_printing')." and
                    product_id = '".$request->input('id')."'"))->first();
                }


            }elseif ( $request->input('type') == 5) {
                $d_name = collect(\DB::select("SELECT * from mst_website where product_id = ?",[$request->input('id')]))->first();

            }else{
                $d_name = collect(\DB::select("SELECT * from mst_other where product_id = ?",[$request->input('id')]))->first();

            }


            if (count($dm) > 0 ) {

                if ( $request->input('type') == 1) {

                    if ($request->input('tmp_color')) {
                        $design_id = $design->id;
                        $tmp_u_waxseal = $request->input('tmp_u_waxseal');
                        $tmp_printing = $request->input('tmp_printing');

                    }else{
                        $design_id = 1;
                        $tmp_u_waxseal = 0;
                        $tmp_printing = 1;

                    }

                    DB::table('mst_cartlist')->where(
                    [
                        'product_id' => $request->input('id'),
                        'design_id' => $design_id,
                        'waxseal' => $tmp_u_waxseal,
                        'foilcolor' => $tmp_printing,

                    ]
                    )->delete();

                }else {
                    CartlistModel::destroy($dm[0]->id);
                }

            }

                if ($request->input('qty')) {
                    $qty = $request->input('qty');
                }else {
                   if ($request->input('type') == 2) {
                        $qty = 300;
                   }else {
                        $qty = 1;
                   }

                }


                // dd($qty);



                $data = new CartlistModel();
                $data->type = $request->input('type');
                $data->product_id = $request->input('id');
                $data->cust_id = Auth::user()->id;
                $data->qty = $qty;


                if ( $request->input('type') == 1) {
                    if ($request->input('tmp_color')) {

                        $add_item = $request->post('additional_user');
                        $add_item_count = $request->post('additional_user_count');
                        $add_free_finishing = $request->post('free_finishing');
                        $add_free_additional = $request->post('free_additional');

                        // dd($add_item);

                        $additional = '';
                        if ($add_item) {
                            foreach ($add_item as $key => $value) {
                                $additional .= $add_item[$key]."|";
                            }
                        }


                        $additional_count = '';
                        if ($add_item_count) {
                            foreach ($add_item_count as $key => $value) {
                                $additional_count .= $add_item_count[$key]."|";
                            }
                        }

                        $free_finishing = '';
                        if ($add_free_finishing) {
                            foreach ($add_free_finishing as $key => $value) {
                                $free_finishing .= $add_free_finishing[$key]."|";
                            }
                        }

                        $free_additional = '';
                        if ($add_free_additional) {
                            foreach ($add_free_additional as $key => $value) {
                                $free_additional .= $add_free_additional[$key]."|";
                            }
                        }


                        $design_id = $design->id;
                        $tmp_u_waxseal = $request->input('tmp_u_waxseal');
                        $tmp_printing = $request->input('tmp_printing');
                        $type_size = $request->input('type_size');
                        $paper_inner = $request->input('paper_inner');
                        $paper_outer = $request->input('paper_outer');
                        $free_finishing = $free_finishing;
                        $free_additional = $free_additional;
                        $additional_user = $additional;
                        $additional_user_count = $additional_count;

                        $emboss = $request->input('emboss');

                    }else{
                        $design_id = 1;
                        $tmp_u_waxseal = 0;
                        $tmp_printing = 1;
                        $type_size = 1;
                        $paper_inner = 1;
                        $paper_outer = null;
                        $free_finishing = 0;
                        $free_additional = 0;
                        $additional_user = '';
                        $additional_user_count = '';
                        $emboss = 0;

                    }

                    $data->design_id = $design_id;
                    $data->waxseal = $tmp_u_waxseal;
                    $data->foilcolor = $tmp_printing;
                    $data->type_size = $type_size;
                    $data->paper_inner = $paper_inner;
                    $data->paper_outer = $paper_outer;
                    $data->free_finishing = $free_finishing;
                    $data->free_additional = $free_additional;
                    $data->additional_user = $additional_user;
                    $data->additional_user_count = $additional_user_count;
                    $data->emboss = $emboss;

                    // $data->waxseal = $request->input('tmp_u_waxseal');
                    // $data->foilcolor = $request->input('tmp_printing');
                }

                $data->save();

                $c_cart = collect(\DB::select("SELECT count(id) as j from mst_cartlist where cust_id = ".Auth::user()->id))->first();


                return response()->json([
                    'rc' => 1,
                    'rm' => $d_name->product_name.' Berhasil Ditambahkan ke Cart',
                    'rd_id' => $request->input('id'),
                    'rd_type' => $request->input('type'),
                    'jumlah' => $c_cart->j
                ]);



        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function remove_cartlist(Request $request)
    {
        // $dm = collect(\DB::select("SELECT * from mst_cartlist where id =".$request->input('id')))->first();
        // $d_name = collect(\DB::select("SELECT * from mst_other where product_id = '".$dm->product_id."'"))->first();

        CartlistModel::destroy($request->input('id'));
        return response()->json([
            'rc' => 1,
            'rm' => 'Berhasil dihapus dari Cart List'
        ]);

    }

}
