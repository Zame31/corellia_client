<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\RefBankModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class refBankController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.ref_bank.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.ref_bank.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = RefBankModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM ref_bank"))->first();
                $data = new RefBankModel();
                $data->id = $get->max_id+1;
            }

            if($request->hasFile('bank_img')){
                $destination_path = public_path('img');
                $files = $request->file('bank_img');

                $setSize = 0.5 * (1024*1024);
                if ($files->getSize() > $setSize) {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'Size File Maksimal 500 kb'
                    ]);
                }

                $exceptFile = ['jpg','jpeg','png','JPG','JPEG','PNG','pdf','PDF','xlsx','xls'];
                if (!in_array($files->getClientOriginalExtension(), $exceptFile))
                {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'file tidak diperbolehkan!'
                    ]);
                }
                
                $filename = $files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $data->bank_img =  $filename;
            }
    

            $data->bank_name = $request->input('bank_name');
            $data->bank_acc_no = $request->input('bank_acc_no');
            $data->an = $request->input('an');

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM ref_bank where id = ?",[$id]);
        return json_encode($data);
    }


    public function delete($id)
    {
        RefBankModel::destroy($id);
    }

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * FROM ref_bank order by id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
        <a class="dropdown-item" onclick="edit('.$data->id.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('bank_img',function($data) {
            return '<img style="width:50px;" src="'.asset('img/').'/'.$data->bank_img.'" alt="">';
        })
        
        ->rawColumns(['bank_img', 'action'])
        ->make(true);

    }



}
