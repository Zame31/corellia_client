<?php

namespace App\Http\Controllers\admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\MasterTxModel;
use Auth;
use App\Models\NotifModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function numFormat($nominal)
    {
        return number_format($nominal,0,",",".");
    }

    public function clearSeparator($nominal)
    {
        return str_replace('.','',$nominal);
    }
    public function convertDate($date)
    {
        return date('Y-m-d',strtotime($date));
    }
    

    public function sendNotif($cust_id,$text,$type)
    {
        $data = new NotifModel();
        $data->cust_id = $cust_id;
        $data->notif = $text;
        $data->type = $type;

        $data->save();
    }

    public function sendMail($view,$data)
    {
        Mail::send($view, $data, function ($message) use ($data) {
            $message->from('official@corellia.id', 'Corellia - Wedding Art');
            $message->to($data['sendto']);
            $message->subject($data['subject']); 
        });
    }

    public function admin_notif(Request $request)
    {
        $data = \DB::select("SELECT * FROM mst_notif where cust_id = ? order by id desc",[$request->post('id')]);
        
        $c_notif = collect(\DB::select("SELECT count(cust_id) as j FROM mst_notif where is_read is null and cust_id = ? GROUP BY cust_id",[$request->post('id')]))->first();

        $numNotif = 0;
        if ($c_notif) {
            $numNotif = $c_notif->j;
        }

        return response()->json([
            'rc' => 1,
            'rm' => $data,
            'jumlah' => $numNotif
        ]);
    }

    public function read_notif(Request $request)
    {
        DB::table('mst_notif')
            ->where('cust_id', $request->post('id'))
            ->update(['is_read' => 1]);
        
        $c_notif = collect(\DB::select("SELECT count(cust_id) as j FROM mst_notif where is_read is null and cust_id = ? GROUP BY cust_id",[$request->post('id')]))->first();

        $numNotif = 0;
        if ($c_notif) {
            $numNotif = $c_notif->j;
        }

        return response()->json([
            'rc' => 1,
            'jumlah' => $numNotif
        ]);
    }



    public function MonthIndo($data)
    {
        $bulan = array (
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $bulan[$data];
    }

    public function endSession()
    {
        Auth::logout();
        // return redirect()->route('login');
        return response()->json([
            'rc' => 0,
            'rm' => "sukses"
        ]);
    }

    public function getUserAdmin($id)
    {
       return \DB::select("SELECT * FROM admin where id = ?",[$id]);
    }

    
    public function checking($type)
    {
        // dd($_GET['isEdit']);
        if ($_GET['isEdit']) {

            $data = collect(\DB::select("SELECT * FROM admin where id = ?",[$_GET['isEdit']]))->first();

            if ($type == "username") {

                if ($_GET['username'] == $data->username) {
                    $valid = true;
                }else{
                    $data = \DB::select("SELECT * FROM admin where username = ?",[$_GET['username']]);
                    $valid = (count($data) > 0) ? false : true ;
                }

            }
            elseif ($type == "email") {

                if ($_GET['email'] == $data->email) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM admin where email = ?",[$_GET['email']]);
                    $valid = (count($data) > 0) ? false : true ;
                }
            }
            elseif ($type == "phone") {

                if ($_GET['phone'] == $data->phone) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM admin where phone = ?",[$_GET['phone']]);
                    $valid = (count($data) > 0) ? false : true ;
                }
            }



            echo json_encode(array(
                'valid' => $valid,
            ));




        }else {

            if ($type == "username") {
                $data = \DB::select("SELECT * FROM admin where username = ?",[$_GET['username']]);
            }elseif ($type == "email") {
                $data = \DB::select("SELECT * FROM admin where email = ?",[$_GET['email']]);
            }elseif ($type == "phone") {
                $data = \DB::select("SELECT * FROM admin where phone = ?",[$_GET['phone']]);
            }

            $isAvailable = (count($data) > 0) ? false : true ;

            echo json_encode(array(
                'valid' => $isAvailable,
            ));

        }


    }


}
