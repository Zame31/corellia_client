<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\RefWaxsealModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class refWaxsealController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = '';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.ref_waxseal.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.ref_waxseal.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = RefWaxsealModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM ref_waxseal"))->first();
                $data = new RefWaxsealModel();
                $data->id = $get->max_id+1;
            }
         
            if($request->hasFile('img')){
                $destination_path = public_path('gallery/waxseal');
                $files = $request->file('img');

                $setSize = 0.5 * (1024*1024);
                if ($files->getSize() > $setSize) {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'Size File Maksimal 500 kb'
                    ]);
                }

                $exceptFile = ['jpg','jpeg','png','JPG','JPEG','PNG','pdf','PDF','xlsx','xls'];
                if (!in_array($files->getClientOriginalExtension(), $exceptFile))
                {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'file tidak diperbolehkan!'
                    ]);
                }
                
                $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                $upload_success = $files->move($destination_path, $filename);
                $data->image = $filename;
            }

            $data->name = $request->input('name');
            $data->harga = $this->clearSeparator($request->input('harga'));

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM ref_waxseal where id = ?",[$id]);
        return json_encode($data);
    }

    public function delete($id)
    {
        RefWaxsealModel::destroy($id);
    }


    

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * from ref_waxseal order by id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
          <a class="dropdown-item" onclick="edit('.$data->id.')">
              <i class="la la-edit"></i>
              <span>Edit</span>
          </a>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })->editColumn('image',function($data) {
            return '<img width="100px" src="'.asset("gallery/waxseal/".$data->image."").'" alt="" srcset="">';
        })
        ->editColumn('harga',function($data) {
            return $this->numFormat($data->harga);
        })
        ->rawColumns(['image', 'action'])
        ->make(true);

    }



}
