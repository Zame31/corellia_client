<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\TraceModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;
use App\Events\StatusLiked;

class PembelianController extends Controller
{
    public function index(Request $request)
    {


        if ($request->get('type') == "0") {
            $tittle = 'Baru';
        }elseif ($request->get('type') == "1") {
            $tittle = 'Menunggu Konfirmasi Pembayaran';

        }elseif ($request->get('type') == "2") {
            $tittle = 'Dibayar Sebagian';

        }elseif ($request->get('type') == "3") {
            $tittle = 'Dibayar Lunas';
        }
        elseif ($request->get('type') == "5") {
            $tittle = 'Menunggu Pembayaran';
        }
        elseif ($request->get('type') == "6") {
            $tittle = 'Ditolak';
        }
        elseif ($request->get('type') == "7") {
            $tittle = 'Dibatalkan Customer';
        }
        elseif ($request->get('type') == "9") {
            $tittle = 'Selesai';
        }

        $param['type'] = $request->get('type');
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.pembelian.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.pembelian.list',$param);
        }
    }

    public function konfirmasi_pembayaran(Request $request)
    {
        $id = $request->get('id');

        $pembayaran = collect(\DB::select("SELECT * from mst_pembayaran where no_pembelian = ? order by id desc",[$id]))->first();


        $data = DB::table('mst_pembayaran')->where('kode_pembayaran', $pembayaran->kode_pembayaran)->first();
        $pembeli = collect(\DB::select("SELECT DISTINCT no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        join ref_bank rb on rb.id::int = mp.bank::int where no_pembelian = ?",[$id]))->first();


        $param['data'] = $data;
        $param['pembeli'] = $pembeli;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.pembelian.konfirmasi_pembayaran',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.pembelian.konfirmasi_pembayaran',$param);
        }
    }

    public function konfirmasi_pembelian(Request $request)
    {
        $id = $request->get('id');
        $data = collect(\DB::select("SELECT DISTINCT discount,pengiriman,ongkir,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        join ref_bank rb on rb.id::int = mp.bank::int where no_pembelian = ?",[$id]))->first();

        $d_undangan = \DB::select("SELECT
        mw.*,md.color,md.shape,md.paper,md.image_id,product_name,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name,rrr.name as finishing,sss.additional_name as add_free,rw.harga as harga_waxseal
        from mst_pembelian mw
        left join mst_undangan mo on mo.product_id = mw.product_id
        left join mst_design md on md.id = mw.design_id
        left join ref_color rc on rc.id = md.color
        left join ref_paper rp on rp.id = mw.paper_inner
        left join ref_paper rpp on rpp.id = mw.paper_outer
        left join ref_waxseal rw on rw.id = mw.waxseal
        left join ref_foilcolor rf on rf.id = mw.foilcolor
        left join ref_type_size rts on rts.id = mw.type_size
        left join ref_finishing rrr on rrr.id::varchar = mw.free_finishing::varchar
        left join ref_additional sss on sss.id::varchar = mw.free_additional::varchar where mo.type = 1 and no_pembelian = ?",[$id]);

        $d_other = \DB::select("SELECT  mw.*,product_name,price from mst_pembelian mw
        join mst_other mo on mo.product_id = mw.product_id  where no_pembelian = ?",[$id]);



$d_website = \DB::select("SELECT mw.*,product_name,deskripsi,mdw.nama_website,mdw.id as id_website
from mst_pembelian mw
join mst_website mo on mo.product_id = mw.product_id
join mst_data_website mdw on mdw.id = mw.design_id where no_pembelian = ?",[$id]);

$param['d_website'] = $d_website;

        $param['data'] = $data;
        $param['d_undangan'] = $d_undangan;
        $param['d_other'] = $d_other;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.pembelian.konfirmasi_pembelian',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.pembelian.konfirmasi_pembelian',$param);
        }
    }

    public function detail_pembelian(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');

        $data = collect(\DB::select("SELECT DISTINCT a.name as designer_name,designer,discount,note,pengiriman,ongkir,note_undangan,response_admin,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name
        from mst_pembelian mp
        join mst_customer mc on mc.id = mp.custid
        left join admin a on a.id = mp.designer
        join ref_bank rb on rb.id::int = mp.bank::int where no_pembelian = ?",[$id]))->first();


        $d_undangan = \DB::select("SELECT
        mw.*,md.color,md.shape,md.paper,md.image_id,product_name,rts.price,rts.type as style_size,
        rw.name as waxseal_name, rc.color as color_name,rp.paper_name as material1,rpp.paper_name as material2,
                rf.name as foilcolor_name,rrr.name as finishing,sss.additional_name as add_free,rw.harga as harga_waxseal
        from mst_pembelian mw
        left join mst_undangan mo on mo.product_id = mw.product_id
        left join mst_design md on md.id = mw.design_id
        left join ref_color rc on rc.id = md.color
        left join ref_paper rp on rp.id = mw.paper_inner
        left join ref_paper rpp on rpp.id = mw.paper_outer
        left join ref_waxseal rw on rw.id = mw.waxseal
        left join ref_foilcolor rf on rf.id = mw.foilcolor
        left join ref_type_size rts on rts.id = mw.type_size
        left join ref_finishing rrr on rrr.id::varchar = mw.free_finishing::varchar
        left join ref_additional sss on sss.id::varchar = mw.free_additional::varchar where mo.type = 1 and no_pembelian = ?",[$id]);

        $d_other = \DB::select("SELECT  mw.*,product_name,price from mst_pembelian mw
        join mst_other mo on mo.product_id = mw.product_id  where no_pembelian = ?",[$id]);

        $d_design_order = \DB::select("SELECT  mw.*,judul,deskripsi from mst_pembelian mw
        join mst_design_order mo on mo.code = mw.product_id  where no_pembelian = ?",[$id]);

        $d_website = \DB::select("SELECT mw.*,product_name,deskripsi,mdw.nama_website,mdw.id as id_website
        from mst_pembelian mw
        join mst_website mo on mo.product_id = mw.product_id
        join mst_data_website mdw on mdw.id = mw.design_id where no_pembelian = ?",[$id]);

       $param['d_website'] = $d_website;
       $param['d_design_order'] = $d_design_order;
        $param['data'] = $data;
        $param['d_undangan'] = $d_undangan;
        $param['d_other'] = $d_other;
        $param['type'] = $type;

        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.pembelian.detail_pembelian',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.pembelian.detail_pembelian',$param);
        }
    }

    public function pembayaran_approve(Request $request)
    {
        try{

            $pembayaran = DB::table('mst_pembayaran')->where('kode_pembayaran', $request->get('kode_pembayaran'))->first();
            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();

            $sisa_billing = $pembelian->billing - $pembayaran->nominal_transfer;

            if ($sisa_billing <= 0) {
                $status = 3;
            }else {
                $status = 2;
            }

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = ?",[$request->get('no_pembelian')]))->first();

            $data = array('pembelian' => $param_email, 'pembayaran' => $pembayaran);

          

            // $email_designer = \DB::select("SELECT * from admin where role = 3");

            // foreach ($email_designer as $key => $value) {
            //     $toEmail[] = $value->email;

            //     $this->sendNotif($value->id,'Ada Pesanan Undangan Baru, Silahkan Mulai Pengerjaan Design','77');

            // }

            //     $data = array(
            //         'sendto'=> $toEmail,
            //         'pembelian' => $param_email,
            //         'note_trace' => 'Ada Pesanan Undangan Baru, Silahkan Mulai Pengerjaan Design',
            //         'subject' => 'Pesanan Undangan');

            //     $this->sendMail('email.lacak',$data);



            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'billing' => $sisa_billing,
                'status' => $status,
                'custtempid' => Auth::user()->id,
                'response_admin' => $request->post('SetMemo')
                ]);

                DB::table('mst_data_website')
                ->where('id', $pembelian->design_id)
                ->update([
                    'is_active' => 1
                    ]);

                    Mail::send('email.pembayaran', $data, function ($message) use ($data) {
                        $message->from('official@corellia.id', 'Corellia - Wedding Art');
                        $message->to($data['pembelian']->email);
                        $message->subject('Konfirmasi Pembayaran');
                        });
                $this->sendNotif($pembelian->custid,$request->post('SetMemo'),$status);


                event(new StatusLiked('Kirim Notif Ke Admin'));

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function pembelian_approve(Request $request)
    {
        // try{
            $add_item = $request->post('value');
            $note = $request->post('note');

            $additional = '';
            if ($add_item) {
                foreach ($add_item as $key => $value) {
                    $additional .= $add_item[$key]."|";
                }
            }


            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = ?",$request->get('no_pembelian')))->first();

            $data = array('pembelian' => $param_email, 'subject' => 'b');

           


            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status' => 5,
                'response_admin' => $note,
                'custtempid' => Auth::user()->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'additional' => $additional
                ]);

                event(new StatusLiked('Kirim Notif Ke Admin'));

                Mail::send('email.pembelian', $data, function ($message) use ($data) {
                    $message->from('support@basys.co.id', 'Corellia - Wedding Art');
                    $message->to($data['pembelian']->email);
                    $message->subject('Menunggu Pembayaran');
                    });
        
                    $this->sendNotif($pembelian->custid,$note,'5');

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);



        // }catch (QueryException $e){

        //     if($e->getCode() == '23505'){
        //         $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
        //     }else{
        //         $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
        //     }
        //     return response()->json([
        //         'rc' => 99,
        //         'rm' => $response,
        //         'msg' => $e->getMessage()
        //     ]);
        // }
    }

    public function pembayaran_reject(Request $request)
    {
        try{


            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status' => 4,
                'response_admin' => $request->post('SetMemo')
                ]);

            DB::table('mst_pembayaran')
            ->where('kode_pembayaran', $request->get('kode_pembayaran'))
            ->update([
                'status' => 4
                ]);

            $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();

            $this->sendNotif($pembelian->custid,$request->post('SetMemo'),'4');


                $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
                left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
                left join mst_customer mc on mc.id = mst_pembelian.custid
                where no_pembelian = ?",[$request->get('no_pembelian')]))->first();


                $data = array(
                    'sendto'=> $param_email->email,
                    'pembelian' => $param_email,
                    'note_trace' => $request->post('SetMemo'),
                    'subject' => 'Status Pembelian');

                $this->sendMail('email.lacak',$data);
                event(new StatusLiked('Kirim Notif Ke Admin'));
            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function pembelian_reject(Request $request)
    {
        try{

            DB::table('mst_pembelian')
            ->where('no_pembelian', $request->get('no_pembelian'))
            ->update([
                'status' => 6,
                'response_admin' => $request->post('SetMemo')
                ]);

                $pembelian = DB::table('mst_pembelian')->where('no_pembelian', $request->get('no_pembelian'))->first();

                $this->sendNotif($pembelian->custid,$request->post('SetMemo'),'6');

                $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
                left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
                left join mst_customer mc on mc.id = mst_pembelian.custid
                where no_pembelian = ?",[$request->get('no_pembelian')]))->first();


                $data = array(
                    'sendto'=> $param_email->email,
                    'pembelian' => $param_email,
                    'note_trace' => $request->post('SetMemo'),
                    'subject' => 'Status Pembelian');

                $this->sendMail('email.lacak',$data);

                event(new StatusLiked('Kirim Notif Ke Admin'));
            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function set_designer(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');
        $pilihdesigner = $request->post('pilihdesigner');

        DB::table('mst_pembelian')
        ->where('no_pembelian', $no_pembelian)
        ->update([
            'designer' =>$pilihdesigner
            ]);

            $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
            left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
			left join mst_customer mc on mc.id = mst_pembelian.custid
            where no_pembelian = '".$no_pembelian."' "))->first();

             $email_designer = collect(\DB::select("SELECT * from admin where role = 3 and id = ?",[$pilihdesigner]))->first();

            $this->sendNotif($pilihdesigner,'Ada Pesanan Undangan Baru, Silahkan Mulai Pengerjaan Design','77');


                $data = array(
                    'sendto'=> $email_designer->email,
                    'pembelian' => $param_email,
                    'note_trace' => 'Ada Pesanan Undangan Baru, Silahkan Mulai Pengerjaan Design',
                    'subject' => 'Pesanan Undangan');

                $this->sendMail('email.lacak',$data);


            // $this->sendNotif($pilihdesigner,"No Pembelian : ". $request->input('no_pembelian').$stat." Melakukan Pengisian Catatan Undangan",$param_email->status);
            event(new StatusLiked('Kirim Notif Ke Admin'));

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

    }

    public function add_trace(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');
        $note_trace = $request->post('note_trace');

        $data = new TraceModel();
        $data->no_pembelian = $no_pembelian;
        $data->note = $note_trace;
        $data->save();

        $data_pem = DB::table('mst_pembelian')->where('no_pembelian', $no_pembelian)->first();

        $text = $no_pembelian.' - '.$note_trace;

        $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
        left join mst_customer mc on mc.id = mst_pembelian.custid
        where no_pembelian = ?",[$no_pembelian]))->first();


        $data = array(
            'sendto'=> $param_email->email,
            'pembelian' => $param_email,
            'note_trace' => $note_trace,
            'subject' => 'Status Pengiriman');


        $this->sendMail('email.lacak',$data);

        $this->sendNotif($data_pem->custid,$text,'3');
        event(new StatusLiked('Kirim Notif Ke Admin'));
        return response()->json([
            'rc' => 1,
            'rm' => ' Berhasil'
        ]);

    }

    public function list_trace(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');
        $data = \DB::select("SELECT * from mst_trace where no_pembelian = ?
        order by created_at desc",[$no_pembelian]);

        $list_trace = '';

        foreach ($data as $key => $value) {
            $list_trace .= ' <div class="kt-list-timeline__item">
                <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                <span class="kt-list-timeline__text">'.$value->note.'</span>
                <span class="kt-list-timeline__time">'.date('d-m-Y H:i:s',strtotime($value->created_at)).'</span>
            </div>';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $list_trace
        ]);


    }


    public function data(Request $request)
    {
        $type = $request->get('type');

        $btn_konfirmasi = '';
        $filter = (Auth::user()->role == 2) ? "and custtempid = ".Auth::user()->id:"";

        if ($type == "0") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status in (0)
            order by created_at desc");
        }elseif ($type == "1") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status = 1 ".$filter."
            order by created_at desc");


        }elseif ($type == "2") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status = 2 ".$filter."
            order by created_at desc");
        }elseif ($type == "3") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status = 3 ".$filter."
            order by created_at desc");
        }elseif ($type == "5") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status in (5,4) ".$filter."
            order by created_at desc");
        }elseif ($type == "6") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status in (6)
            order by created_at desc");
        }
        elseif ($type == "7") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status in (7)
            order by created_at desc");
        }
        elseif ($type == "9") {
            $data = \DB::select("SELECT DISTINCT DATE_PART('day',CURRENT_DATE - mp.updated_at) as hari_proses,no_pembelian,total_pembelian,billing,status,note,mp.created_at,end_date,bank,cust_name,phone_no,bank_name,status_name, a.name as disetujui_oleh
            from mst_pembelian mp
            join mst_customer mc on mc.id = mp.custid
            join ref_bank rb on rb.id::int = mp.bank::int
            join ref_status rs on rs.id = mp.status
			left join admin a on a.id = mp.custtempid
            where status in (9)
            order by created_at desc");
        }



       return DataTables::of($data)
       ->addColumn('action', function ($data) use($btn_konfirmasi,$type) {

        if ($type == "1") {

            $btn_konfirmasi = '<a onclick="loadNewPage(`'.route('pembelian.konfirmasi_pembayaran').'?id='.$data->no_pembelian.'`)" class="dropdown-item" style="cursor: pointer;">
                    <i class="la la-money"></i>
                    <span>Konfirmasi Pembayaran</span>
                </a>';
        }

        if ($type == "0") {

            $btn_konfirmasi = '<a  onclick="loadNewPage(`'.route('pembelian.konfirmasi_pembelian').'?id='.$data->no_pembelian.'`)" class="dropdown-item" style="cursor: pointer;">
                    <i class="la la-check"></i>
                    <span>Konfirmasi Pembelian</span>
                </a>';
        }

        if ($type == "9") {
            $btn_konfirmasi = '<a onclick="showTrace(`'.$data->no_pembelian.'`)" class="dropdown-item" style="cursor: pointer;">
                <i class="la la-truck"></i>
                <span>Progres Pembelian</span>
            </a>';
        }

        if ($type == "3") {

            $btn_konfirmasi = '<a onclick="showTrace(`'.$data->no_pembelian.'`)" class="dropdown-item" style="cursor: pointer;">
                    <i class="la la-truck"></i>
                    <span>Progres Pembelian</span>
                </a>
                <a onclick="pembelianSelesai(`'.$data->no_pembelian.'`)" class="dropdown-item" style="cursor: pointer;">
                    <i class="la la-check"></i>
                    <span>Pembelian Selesai</span>
                </a>';
        }
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
            '.$btn_konfirmasi.'
              <a onclick="loadNewPage(`'.route('pembelian.detail_pembelian').'?id='.$data->no_pembelian.'&type='.$type.'`)" class="dropdown-item" style="cursor: pointer;">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->editColumn('total_pembelian',function($data) {
            return 'Rp '.$this->numFormat($data->total_pembelian);
        })
        ->editColumn('billing',function($data) {
            return 'Rp '.$this->numFormat($data->billing);
        })
        ->editColumn('created_at',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->created_at));
        })
        ->editColumn('end_date',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->end_date));
        })
        // ->addColumn('cek', function ($data) {
        //     return '
        //     <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
        //         <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
        //         <span></span>
        //     </label>
        //     ';
        //     })

        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function pembelian_selesai(Request $request)
    {
        $no_pembelian = $request->post('no_pembelian');

        DB::table('mst_pembelian')
        ->where('no_pembelian', $no_pembelian)
        ->update([
            'status' => 9,
            ]);

        return response()->json([
            'rc' => 1,
            'rm' => ' Berhasil'
        ]);


    }



}
