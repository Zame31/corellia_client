<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstDesignModel;
use App\Models\MstImageModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class DesignOrderController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'z';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.design_order.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.design_order.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstDesignModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_design"))->first();
                $data = new MstDesignModel();
                $data->id = $get->max_id+1;
            }

            $data->product_id = $request->input('product_id');
            $data->color = $request->input('color');
            $data->shape = $request->input('shape');
            $data->foilcolor = 1;
            $data->keterangan = 1;
            $data->paper = 1;
           
            $data->cust_id = Auth::user()->id;

            
            if($request->hasFile('img1')){
                $destination_path = public_path('gallery/product');
                $files = $request->file('img1');

                $setSize = 0.5 * (1024*1024);
                if ($files->getSize() > $setSize) {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'Size File Maksimal 500 kb'
                    ]);
                }

                $exceptFile = ['jpg','jpeg','png','JPG','JPEG','PNG','pdf','PDF','xlsx','xls'];
                if (!in_array($files->getClientOriginalExtension(), $exceptFile))
                {
                    return response()->json([
                        'rc' => 99,
                        'rm' => 'file tidak diperbolehkan!'
                    ]);
                }
                
                $filename = date('dmYhis').preg_replace("/\s+/","",$files->getClientOriginalName());
                $upload_success = $files->move($destination_path, $filename);
                
                $get_img = collect(\DB::select("SELECT max(seq) as max_id FROM mst_image where product_id = ?",[$request->input('product_id')]))->first();

                $data_img = new MstImageModel();
                $data_img->product_id =  $data->product_id;
                $data_img->img = $filename;
                $data_img->seq = $get_img->max_id+1;
                $data_img->save();

                $data->image_id =  $data_img->id;
            }

            $data->save();
           

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data['other'] = collect(\DB::select("SELECT * FROM mst_design where id = ".$id))->first();
        $data['img'] = \DB::select("SELECT * FROM mst_image where id = ? order by seq asc",[$data['other']->image_id]);
        
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete($id)
    {
        // MstDesignModel::destroy($id);
        $get = collect(\DB::select("SELECT * from mst_design where id = ?",[$id]))->first();

        DB::table('mst_image')
        ->where('id', $get->image_id)
        ->delete();

        DB::table('mst_design')
        ->where('id', $id)
        ->delete();

      
    }

    public function data(Request $request)
    {
     
        $data = \DB::select("select md.*,mc.phone_no,mc.cust_name from mst_design_order md
        join mst_customer mc on mc.id = md.cust_id
        ");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit('.$data->id.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('image',function($data) {
            return ' <a href="'.asset("gallery/design_order/".$data->image."").'" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
                        <img src="'.asset("gallery/design_order/".$data->image."").'" class="img-fluid rounded">
                    </a>';
         })
        ->editColumn('file_tambahan',function($data) {
            return '<a href="'.asset("public/gallery/design_order/".$data->file_tambahan."").'" class="btn btn-sm btn-label-success btn-bold">Download</a>
            ';
        })

        ->rawColumns(['file_tambahan','image','action'])
        ->make(true);

    }



}
