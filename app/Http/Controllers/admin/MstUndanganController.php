<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstUndanganModel;
use App\Models\MstImageModel;
use App\Models\RefTypeSizeModel;


use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstUndanganController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_undangan.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_undangan.list',$param);
        }
    }

    
    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstUndanganModel::find($get_id);
                $data->product_id = 'U'.$get_id;
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_undangan"))->first();
                $data = new MstUndanganModel();
                $data->id = $get->max_id+1;
                $data->product_id = 'U'.($get->max_id+1);
            }

            $data->product_name = $request->input('product_name');
            $data->format = $request->input('format');
            $data->style = $request->input('style');
            $data->waxseal = $request->input('waxseal');
            $data->desc = $request->input('desc');

            
            $data->berat = $this->clearSeparator($request->input('berat'));

            $data->product_status = 1;
            $data->type = 1;
            $data->orientation = 1;
            $data->dimension = 1;

            $data->save();

                   

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data['datas'] = collect(\DB::select("SELECT * FROM mst_undangan where product_id = ?",[$id]))->first();
        
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete($id)
    {
        // MstUndanganModel::destroy($id);

        DB::table('mst_undangan')
        ->where('product_id', $id)
        ->delete();

        DB::table('mst_image')
        ->where('product_id', $id)
        ->delete();
    }

    public function data(Request $request)
    {
     
        $data = \DB::select("SELECT *,mst_undangan.id as mid,ref_format.name as format_name, ref_style.name as style_name 
        from mst_undangan 
        join ref_format on ref_format.id = mst_undangan.format 
        join ref_style on ref_style.id = mst_undangan.style 
        order by created_at desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit(`'.$data->product_id.'`)">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del(`'.$data->product_id.'`)">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('berat',function($data) {
            return $this->numFormat($data->berat)." Gram";
        })
        ->editColumn('waxseal',function($data) {
            return ($data->waxseal == 1) ? "tersedia":"tidak tersedia";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    // TYPE SIZE

    public function index_type_size(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.undangan_type_size.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.undangan_type_size.list',$param);
        }
    }

    
    public function store_type_size(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = RefTypeSizeModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM ref_type_size"))->first();
                $data = new RefTypeSizeModel();
                $data->id = $get->max_id+1;
            }

            $data->product_id = $request->input('product_id');
            $data->type = $request->input('type');
            $data->material = $request->input('material');
            $data->price = $this->clearSeparator($request->input('price'));
            $data->min_pembelian = $this->clearSeparator($request->input('min_pembelian'));

            $data->save();

            $checkHarga = DB::table('mst_undangan')->where('product_id', $request->input('product_id'))->first();

            if ($checkHarga->price == null) {
                DB::table('mst_undangan')
                ->where('product_id', $request->input('product_id'))
                ->update(['price' => $data->price]);
            }
            elseif ($data->price < $checkHarga->price) {
                DB::table('mst_undangan')
                ->where('product_id', $request->input('product_id'))
                ->update(['price' => $data->price]);
            }

           

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit_type_size($id)
    {
        $data['datas'] = collect(\DB::select("SELECT * FROM ref_type_size where id = ?",[$id]))->first();
        
        return response()->json([
            'rc' => 0,
            'rm' => $data
        ]);
    }


    public function delete_type_size($id)
    {
        RefTypeSizeModel::destroy($id);
    }

    public function data_type_size(Request $request)
    {
     
        $data = \DB::select("SELECT ref_type_size.*,ref_type_size.id as mid,ref_material.material as material_name,mst_undangan.product_name 
        from ref_type_size 
        join ref_material on ref_material.id = ref_type_size.material 
        join mst_undangan on mst_undangan.product_id = ref_type_size.product_id
        order by ref_type_size.id desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
            <a class="dropdown-item" onclick="edit('.$data->mid.')">
            <i class="la la-edit"></i>
            <span>Edit</span>
        </a>
          <a class="dropdown-item" onclick="del('.$data->mid.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        })
        ->editColumn('price',function($data) {
            return "Rp ".$this->numFormat($data->price);
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    // END TYPE SIZE


}
