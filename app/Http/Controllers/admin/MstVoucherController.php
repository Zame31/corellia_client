<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\MstVoucherModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;

class MstVoucherController extends Controller
{
    public function index(Request $request)
    {
        
        $tittle = 'Menunggu Pembayaran';
     
        $param['tittle'] = $tittle;
        
        if (Req::ajax()) {
            return view('admin.master.only_content')->nest('child', 'admin.mst_voucher.list',$param);
        }else {
            return view('admin.master.master')->nest('child', 'admin.mst_voucher.list',$param);
        }
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = MstVoucherModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_voucher"))->first();
                $data = new MstVoucherModel();
                $data->id = $get->max_id+1;
            }

            $data->vouchersts = 0;
            $data->code = $request->input('code');
            $data->desc = $request->input('desc');
            $data->amount = $this->clearSeparator($request->input('amount'));
            $data->percentage = $request->input('percentage');
            $data->expdt = $this->convertDate($request->input('expdt'));

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM mst_voucher where id = ?",[$id]);
        return json_encode($data);
    }

    public function delete($id)
    {
        MstVoucherModel::destroy($id);
    }


    

    public function data(Request $request)
    {
     

      
        $data = \DB::select("SELECT * FROM mst_voucher order by created_at desc");
          
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
  
        return '
        <div class="dropdown dropdown-inline">
        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flaticon-more"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-center">
          <a class="dropdown-item" onclick="edit('.$data->id.')">
              <i class="la la-edit"></i>
              <span>Edit</span>
          </a>
          <a class="dropdown-item" onclick="del('.$data->id.')">
              <i class="la la-trash"></i>
              <span>Delete</span>
          </a>
        </div>
    </div>
        ';
        }) ->editColumn('vouchersts',function($data) {
            return ($data->vouchersts == '1') ? '<span class="kt-badge kt-badge--danger kt-badge--inline">Telah Digunakan</span>':'<span class="kt-badge kt-badge--success kt-badge--inline">Aktif</span>';
        })
        ->editColumn('amount',function($data) {
            return 'Rp '.$this->numFormat($data->amount);
        })
        ->editColumn('created_at',function($data) {
            return date('d-m-Y h:i:s',strtotime($data->created_at));
        })
        ->editColumn('expdt',function($data) {
            return date('d-m-Y',strtotime($data->expdt));
        })
        ->rawColumns(['vouchersts', 'action'])
        ->make(true);

    }



}
