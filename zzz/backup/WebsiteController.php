<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use App\Models\CartlistModel;
use App\Models\MstDataWebsiteModel;
use App\Models\BukuTamuModel;

class WebsiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index($type,$type_id,Request $request)
    {
     

        $tittle = 'BUAT WEBSITE';

        $param['type'] = $type;
        $param['type_id'] = $type_id;
        $param['tittle'] = $tittle;
        
        return view('master.master')->nest('child', 'website.index',$param);

    }

    public function detail($id,$color)
    {
       
        $data = collect(\DB::select("SELECT * 
        from mst_website mo where mo.product_id = '".$id."'"))->first();

        $param['data'] = $data;
        // return view('template.WS1.index');

        
        return view('master.master')->nest('child', 'website.detail',$param);
        
    }

    public function edit_website($productId,$Id)
    {
       
        $data = collect(\DB::select("SELECT * 
        from mst_website mo where mo.product_id = '".$productId."'"))->first();

        $data_website = collect(\DB::select("SELECT * 
        from mst_data_website mo where mo.id = '".$Id."'"))->first();

        $param['data'] = $data;
        $param['dw'] = $data_website;
        
        return view('master.master')->nest('child', 'website.edit_website',$param);
        
    }

    public function get_list(Request $request)
    {
        $type = $request->input('type');
        $type_id = $request->input('type_id');
        $u_color = $request->input('u_color');
      
        $whereParts = array();
        
            $ifLeftFilter = '';

            if($u_color) { 
                if ($u_color == 0) {
                } else {
                    $whereParts[] = "color = $u_color "; 
                }
            }
            

            $sql_data = "SELECT DISTINCT product_id,product_name,img,color,deskripsi,price from mst_website mu ".$ifLeftFilter;
            if(count($whereParts)) {
                $sql_data .= "WHERE " . implode('AND ', $whereParts);
            }
           
        $data = \DB::select($sql_data);

        // dd($sql_data);

        
        
        $list = '';

        foreach ($data as $key => $value) {
            
                if (Auth::user()) {
                    $d_wish = \DB::select("SELECT * 
                    from mst_wishlist where product_id = '".$value->product_id."' and cust_id = ".Auth::user()->id);
                    if (count($d_wish) > 0 ) {
                        $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>
                        <svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>';
                    }else {
                        $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                        <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>';
                    }
        
                }else {
                    $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>';
                }

                $list .= '<div class="col-6 col-md-4 pt-col-item">
                                <div class="pt-product">
                                    <div class="pt-image-box">
                                        <div class="pt-app-btn">
                                            <a href="#" id="love'.$value->product_id.'_5" onclick="setWishlist(`'.$value->product_id.'`,5)" class="pt-btn-wishlist" data-tposition="left">
                                                '.$love.'
                                            </a>
                                        
                                        </div>
                                        <a href="'.route("website.detail",["id"=>"$value->product_id","type"=>"$value->color"]).'">
                                            <span class="pt-img">
                                                <img class="zn-fit-center" style="height: 250px;" src="'.asset("gallery/product/$value->img").'" alt="image">
                                            </span>
                                            <span class="pt-label-location">
                                            </span>
                                        </a>
                                    </div>
                                    <div class="pt-description">
                                        <div class="pt-col">
                                            
                                            <h2 class="pt-title"><a href="'.route("website.detail",["id"=>"$value->product_id","type"=>"$value->color"]).'">'.$value->product_name.'</a></h2>
                                            <ul class="pt-add-info">
                                                <li>
                                                    <a>By Corellia</a>
                                                
                                                </li>
                                            </ul>
                                            <div class="pt-content">
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                </div>
                                        </div>
                                        <div class="pt-col">
                                            <div class="pt-row-hover">
                                                <a href="'.route("website.detail",["id"=>"$value->product_id","type"=>"$value->color"]).'" class="pt-btn-addtocart">
                                                    <div class="pt-icon">
                                                        <svg><use xlink:href="#icon-cart_1"></use></svg>
                                                        <svg><use xlink:href="#icon-cart_1_plus"></use></svg>
                                                        <svg><use xlink:href="#icon-cart_1_disable"></use></svg>
                                                    </div>
                                                    <span class="pt-text" >BUY WEBSITE</span>
                                                </a>
                                                <div class="pt-price">
                                                    <span class="new-price">Rp '.$this->numFormat($value->price).'</span>
                                                </div>
                                                <div class="pt-wrapper-btn">
                                                    <a href="#" class="pt-btn-wishlist">
                                                        <span class="pt-icon">
                                                            <svg><use xlink:href="#icon-wishlist"></use></svg>
                                                            <svg><use xlink:href="#icon-wishlist-add"></use></svg>
                                                        </span>
                                                        <span class="pt-text">Add to wishlist</span>
                                                    </a>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
        
            
           
    
            
            
        }

        return response()->json([
            'rc' => 0,
            'rm' => $list
        ]);
    }

    public function get_voucher(Request $request)
    {
        $voucher = $request->input('id'); 
        $dv = collect(\DB::select("SELECT * from mst_voucher where code = '".$voucher."'"))->first();
        
        if ($dv) {
            if (date('Y-m-d') > $dv->expdt) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Voucher Sudah Tidak Berlaku'
                ]);
            }elseif ($dv->vouchersts > 0) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Voucher Telah Digunakan'
                ]);
            } else {
                return response()->json([
                    'rc' => 0,
                    'rm' => $dv
                ]);
            }
            
        
        } else {
            return response()->json([
                'rc' => 99,
                'rm' => 'Voucher Tidak Ditemukan'
            ]);
        }

    }

    public function set_image(Request $request)
    {
        $id = $request->input('id');

        $filename = 'no_img.png';
        try{
            if($request->hasFile($id)){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file($id);
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
            }
    
            return response()->json([
                'rc' => 0,
                'rm' => $filename
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }

      
    }

    public function preview(Request $request,$id,$name)
    {
        $data = collect(\DB::select("SELECT * 
        from mst_data_website mo where mo.id = '".$id."' and  mo.nama_website = '".$name."'"))->first();

        $param['data'] = $data;
        $param['type'] = 'preview';

        $d_view = 'template.'.$data->product_id.'.index';
        
        return view($d_view,$param);
    }

    public function set_edit(Request $request)
    {
        try{

            $dweb = MstDataWebsiteModel::find($request->input('Id'));
      

            if($request->hasFile('set_header')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('set_header');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->set_header = $filename;
            }

            if($request->hasFile('foto_pria')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('foto_pria');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->foto_pria = $filename;
            }

            if($request->hasFile('foto_wanita')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('foto_wanita');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->foto_wanita = $filename;
            }

            for ($i=1; $i <= 4; $i++) { 
                if($request->hasFile('gallery_'.$i)){
                    $destination_path = public_path('gallery/website_img');
                    $files = $request->file('gallery_'.$i);
                    $filename = date('dmY').$files->getClientOriginalName();
                    $upload_success = $files->move($destination_path, $filename);
                
                    switch ($i) {
                        case 1:
                            $dweb->gallery_1 = $filename;
                            break;
                        case 2:
                            $dweb->gallery_2 = $filename;
                            break;
                        case 3:
                            $dweb->gallery_3 = $filename;
                            break;
                        case 4:
                            $dweb->gallery_4 = $filename;
                            break;
                    }
                }
            }

            // dd($request->all());
            // dd($request->input('nama_website'));
            $dweb->nama_website = $request->input('nama_website');
            $dweb->cust_id = Auth::user()->id;
            $dweb->setname = $request->input('setName');
            $dweb->setnamepria = $request->input('setNamePria');
            $dweb->setdescpria = $request->input('setDescPria');
            $dweb->setnamewanita = $request->input('setNameWanita');
            $dweb->setdescwanita = $request->input('setDescWanita');
            $dweb->settglres = $request->input('setTglRes');
            $dweb->setjam1res = $request->input('setJam1Res');
            $dweb->setjam2res = $request->input('setJam2Res');
            $dweb->settempatres = $request->input('setTempatRes');
            $dweb->setalamatres = $request->input('setAlamatRes');
            $dweb->settglakad = $request->input('setTglAkad');
            $dweb->setjam1akad = $request->input('setJam1Akad');
            $dweb->setjam2akad = $request->input('setJam2Akad');
            $dweb->setalamatakad = $request->input('setAlamatAkad');
            $dweb->maps_lat = $request->input('maps_lat');
            $dweb->maps_lng = $request->input('maps_lng');
            $dweb->setkata = $request->input('setKata');
            $dweb->bg_color = $request->input('bg_color');
            $dweb->music = $request->input('music');
            $dweb->settempatakad = $request->input('setTempatAkad');
            $dweb->link_live = $request->input('link_live');

            

            $dweb->date_active = date('Y-m-d', strtotime($request->input('date_active')));
            $dweb->save();

               // dd('berhasil');

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil Di Ubah'
            ]);
    
            

        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function set_cartlist(Request $request)
    {
        try{

            $dm = \DB::select("SELECT * from mst_cartlist where product_id = '".$request->input('id')."' and cust_id = ".Auth::user()->id);
            $d_name = collect(\DB::select("SELECT * from mst_website where product_id = '".$request->input('id')."'"))->first();

            if (count($dm) > 0 ) {
                CartlistModel::destroy($dm[0]->id); 
            }

            if ($request->input('qty')) {
                $qty = $request->input('qty');
            }else {
                $qty = 1;
            }

            $data = new CartlistModel();
            $data->type = $request->input('type');
            $data->product_id = $request->input('id');
            $data->cust_id = Auth::user()->id;
            $data->qty = $qty;
            $data->save();

            // if (Auth::user()->id == '9999') {
            //     $data = new PembelianModel();
            //     $data->product_id = $request->input('id');
            //     $data->qty = 9999;
            //     $data->totalprice = $cart['total_per'][$key];
            //     $data->discount = $cart['voucher_percentage']+$cart['voucher_amount'];
            //     $data->custid = $cust->id;
            //     $data->end_date = $end_date;
            //     $data->bank = $request->input('bank');
            //     $data->accbank = $request->input('bank');
            //     $data->note = $request->input('note');
            //     $data->status = 3;
            //     $data->no_pembelian = 'CO'.date('Ymd').$cust->id.$idpem;
            //     $data->created_at = $created_at;

            //     $data->pengiriman = $request->input('pengiriman');
            //     $data->design_id = $cart['design_id'][$key];
            //     $data->type_id = 5;
            //     $data->ongkir = 0;
            //     $total_final = $cart['total']+$request->input('total_add_item');
            //     $data->billing = $total_final;

            //     $data->total_pembelian = $total_final;
            //     $data->save();

            // }

            // DB::table('mst_data_website')
            // ->where('cartlist_id', $data->id)
            // ->delete();

            $dweb = new MstDataWebsiteModel();
            $dweb->cartlist_id = $data->id;

            if (Auth::user()->id == '9999') {
                $dweb->is_active = 1;
            }

            if($request->hasFile('set_header')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('set_header');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->set_header = $filename;
            }

            if($request->hasFile('foto_pria')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('foto_pria');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->foto_pria = $filename;
            }

            if($request->hasFile('foto_wanita')){
                $destination_path = public_path('gallery/website_img');
                $files = $request->file('foto_wanita');
                $filename = date('dmY').$files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $filename);
                $dweb->foto_wanita = $filename;
            }

            for ($i=1; $i <= 4; $i++) { 
                if($request->hasFile('gallery_'.$i)){
                    $destination_path = public_path('gallery/website_img');
                    $files = $request->file('gallery_'.$i);
                    $filename = date('dmY').$files->getClientOriginalName();
                    $upload_success = $files->move($destination_path, $filename);
                
                    switch ($i) {
                        case 1:
                            $dweb->gallery_1 = $filename;
                            break;
                        case 2:
                            $dweb->gallery_2 = $filename;
                            break;
                        case 3:
                            $dweb->gallery_3 = $filename;
                            break;
                        case 4:
                            $dweb->gallery_4 = $filename;
                            break;
                        default:
                            $dweb->gallery_1 = $filename;
                            break;
                    }
                }
            }
            
            $dweb->product_id = $request->input('id');
            $dweb->nama_website = $request->post('nama_website');
            // $dweb->no_pembelian = $request->post('no_pembelian');
            $dweb->nama_website = $request->post('nama_website');
            $dweb->cust_id = Auth::user()->id;
            $dweb->setname = $request->post('setName');
            $dweb->setnamepria = $request->post('setNamePria');
            $dweb->setdescpria = $request->post('setDescPria');
            $dweb->setnamewanita = $request->post('setNameWanita');
            $dweb->setdescwanita = $request->post('setDescWanita');
            $dweb->settglres = $request->post('setTglRes');
            $dweb->setjam1res = $request->post('setJam1Res');
            $dweb->setjam2res = $request->post('setJam2Res');
            $dweb->settempatres = $request->post('setTempatRes');
            $dweb->setalamatres = $request->post('setAlamatRes');
            $dweb->settglakad = $request->post('setTglAkad');
            $dweb->setjam1akad = $request->post('setJam1Akad');
            $dweb->setjam2akad = $request->post('setJam2Akad');
            $dweb->setalamatakad = $request->post('setAlamatAkad');
            $dweb->maps_lat = $request->post('maps_lat');
            $dweb->maps_lng = $request->post('maps_lng');
            $dweb->setkata = $request->post('setKata');
            $dweb->bg_color = $request->post('bg_color');
            $dweb->music = $request->post('music');
            $dweb->settempatakad = $request->post('setTempatAkad');
            $dweb->link_live = $request->post('link_live');

            

            $dweb->date_active = date('Y-m-d', strtotime($request->post('date_active')));
            $dweb->save();

            // $data_upd = CartlistModel::find($data->id);
            // $data_upd->design_id = $dweb->id;
            // $data_upd->save();

           

            $c_cart = collect(\DB::select("SELECT count(id) as j from mst_cartlist where cust_id = ".Auth::user()->id))->first();

            // dd('berhasil');

            return response()->json([
                'rc' => 1,
                'rm' => $d_name->product_name.' Berhasil Ditambahkan ke Cart',
                'rd_id' => $request->input('id'),
                'rd_type' => $request->input('type'),
                'jumlah' => $c_cart->j
            ]);
    
            

        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function store_buku_tamu(Request $request)
    {
        try{
         
            $get = collect(\DB::select("SELECT max(id) as max_id FROM buku_tamu"))->first();
            $data = new BukuTamuModel();

            $data->id = $get->max_id+1;
            $data->website_id = $request->input('website_id');
            $data->nama = $request->input('nama');
            $data->kehadiran = $request->input('kehadiran');
            $data->ucapan = $request->input('ucapan');
            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function list_buku_tamu(Request $request)
    {
        $data = \DB::select("SELECT * from buku_tamu where website_id = '".$request->input('website_id')."' order by id desc");
        return response()->json([
            'rc' => 0,
            'data' => $data
        ]);
    }


}
