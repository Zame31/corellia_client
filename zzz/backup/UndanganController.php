<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use App\Models\DesignOrderModel;

class UndanganController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function custom_design(Request $request)
    {
        return view('master.master')->nest('child', 'undangan.custom_design');
    }

    public function set_design(Request $request)
    {
        try{

            $file = $request->file('design');
            $imgname = date('dmyhis').$file->getClientOriginalName();
            $file->move(public_path('/gallery/design_order/'), $imgname);

            $file_tambahan = $request->file('file_tambahan');
            $file_tambahan_name = date('dmyhis').$file_tambahan->getClientOriginalName();
            $file_tambahan->move(public_path('/gallery/design_order/'), $file_tambahan_name);

            $get = collect(\DB::select("SELECT max(id) as max_id FROM mst_design_order"))->first();

            $data = new DesignOrderModel();
            $data->cust_id = Auth::user()->id;
            $data->image = $imgname;
            $data->file_tambahan = $file_tambahan_name;

            $data->deskripsi = $request->input('deskripsi');
            $data->judul = $request->input('judul');
            $data->code = 'DBO'.$get->max_id.Auth::user()->id;
            $data->save();

            return response()->json([
                'rc' => 1,
                'rm' => ' Berhasil'
            ]);

        }catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Koneksi Bermasalah !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


}
