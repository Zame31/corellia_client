<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use Session;

class KatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index($type,$type_id,Request $request)
    {
        if ($type == 'undangan') {
            $tittle = 'KATALOG UNDANGAN';
        } else {
            $tittle = 'KEBUTUHAN LAINNYA';
        }

        $param['type'] = $type;
        $param['type_id'] = $type_id;
        $param['tittle'] = $tittle;




        // if (Req::ajax()) {
        //     return view('master.only_content')->nest('child', 'katalog.index',$param);
        // }else {
            return view('master.master')->nest('child', 'katalog.index',$param);
        // }
    }

    public function detail($id,$type)
    {
        // UNDANGAN
        if ($type == 1) {
            $data = collect(\DB::select("SELECT mo.*,ro.name as name_orientation, rf.name as name_format, rs.name as name_style, rd.name as name_dimension from mst_undangan mo
            left join ref_orientation ro on ro.id = mo.orientation
            left join ref_format rf on rf.id = mo.format
            left join ref_style rs on rs.id = mo.style
            left join ref_dimension rd on rd.id = mo.dimension where mo.product_id = '".$id."'"))->first();

            $di = \DB::select("SELECT * from mst_image where seq <> 1 and product_id = '".$id."'");

            $design = collect(\DB::select("SELECT * from mst_design where product_id = '".$id."'"))->first();

            $img2 = '';
            $img3 = '';
            $img4 = '';

            // $di_u = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id = '".$id."'"))->first();

            if ($design->image_id) {
                $di_u = collect(\DB::select("SELECT * from mst_image where id = '".$design->image_id."'"))->first();
            }

            if ($design->image2) {
                $img2 = collect(\DB::select("SELECT * from mst_image where id = '".$design->image2."'"))->first();
            }

            if ($design->image3) {
                $img3 = collect(\DB::select("SELECT * from mst_image where id = '".$design->image3."'"))->first();
            }
            if ($design->image4) {
                $img4 = collect(\DB::select("SELECT * from mst_image where id = '".$design->image4."'"))->first();
            }





            $design_shape = \DB::select("SELECT DISTINCT shape
            from mst_design mo where mo.product_id = '".$id."'");

            $design_color = \DB::select("SELECT DISTINCT rc.color,rc.code,rc.id
            from mst_design mo
						join ref_color rc on rc.id = mo.color where mo.product_id = '".$id."'");

            $design_paper = \DB::select("SELECT DISTINCT rp.desc,rp.paper_name,rp.id
            from mst_design mo
                        join ref_paper rp on rp.id = mo.paper where mo.product_id = '".$id."'");

            $design_foilcolor = \DB::select("SELECT DISTINCT rp.name,rp.id
            from mst_design mo
                        join ref_foilcolor rp on rp.id = mo.foilcolor where mo.product_id = '".$id."'");

            $param['design_img'] = $design;
            $param['img2'] = $img2;
            $param['img3'] = $img3;
            $param['img4'] = $img4;

            $param['design_shape'] = $design_shape;
            $param['design_color'] = $design_color;
            $param['design_paper'] = $design_paper;
            $param['design_foilcolor'] = $design_foilcolor;

        // OTHER
        }else {
            $data = collect(\DB::select("SELECT *
            from mst_other mo where mo.product_id = '".$id."'"))->first();

            $di = \DB::select("SELECT * from mst_image where seq <> 1 and product_id = '".$id."'");
            $di_u = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id = '".$id."'"))->first();

        }


        $param['type'] = $type;
        $param['id'] = $id;
        $param['data'] = $data;
        $param['di'] = $di;
        $param['di_u'] = $di_u;

        // Session::flush();
        // Session::regenerate();

        if ($type == 1) {
            return view('master.master')->nest('child', 'katalog.detail',$param);
        }else {
            return view('master.master')->nest('child', 'katalog.detail_other',$param);
        }


    }

    public function design_foilcolor(Request $request)
    {

        $design_foilcolor = \DB::select("SELECT DISTINCT rp.name,rp.id
            from mst_design mo
            join ref_foilcolor rp on rp.id = mo.foilcolor where mo.product_id = '".$request->input('product_id')."'
            and color = ".$request->input('color'));

        // dd($request);

        $view = '';
        foreach ($design_foilcolor as $key => $vv) {
            $view .= '<option value="'.$vv->id.'">'.$vv->name.'</option>';
        }

        return response()->json([
            'rc' => 0,
            'rm' => $view,
            'data' => $design_foilcolor
        ]);
    }

    public function get_list(Request $request)
    {
        $type = $request->input('type');
        $type_id = $request->input('type_id');
        $price_a = $request->input('price_a');
        $price_b = $request->input('price_b');

        // UNDANGAN
        $u_type = $request->input('u_type');
        $u_format = $request->input('u_format');
        $u_ori = $request->input('u_ori');
        $u_harga = $request->input('u_harga');
        $u_color = $request->input('u_color');
        $u_foilcolor = $request->input('u_foilcolor');
        $u_shape = $request->input('u_shape');
        $u_paper = $request->input('u_paper');
        $u_waxseal = $request->input('u_waxseal');

        if ($u_harga) {
            if ($u_harga == 0) {
                $price_u_a = 0;
                $price_u_b = 0;

            } elseif ($u_harga == 1) {
                $price_u_a = 5000;
                $price_u_b = 10000;
            } elseif ($u_harga == 2) {
                $price_u_a = 10500;
                $price_u_b = 20000;
            }
            elseif ($u_harga == 3) {
                $price_u_a = 21000;
                $price_u_b = 50000;
            }

        }

        if ($request->input('price_a')) {
            $price_a = $request->input('price_a');

        }

        $whereParts = array();
        $ifLeftFilter = '';

        if ($type == 'undangan') {

            $ifLeftFilter = '';

            if($u_ori) { $whereParts[] = "orientation = $u_ori "; }
            if($u_format) { $whereParts[] = "format = $u_format "; }
            if($u_type) { $whereParts[] = "style = $u_type "; }
            if($u_waxseal) { $whereParts[] = "waxseal <> 0 "; }

            if($u_harga) { $whereParts[] = "price >= '$price_u_a' AND price <= '$price_u_b' "; }
            if($u_color) {
                if ($u_color == 1) {
                } else {
                    $whereParts[] = "color = $u_color ";
                    $ifLeftFilter = 'join mst_design on mst_design.product_id = mu.product_id ';
                }
            }

            if($u_foilcolor) {
                if ($u_foilcolor == 0) {
                } else {
                    $whereParts[] = "foilcolor = $u_foilcolor ";
                    $ifLeftFilter = 'join mst_design on mst_design.product_id = mu.product_id ';
                }
            }

            if($u_shape) {
                if ($u_shape == 0) {
                } else {
                    $whereParts[] = "shape = $u_shape ";
                    $ifLeftFilter = 'join mst_design on mst_design.product_id = mu.product_id ';
                }
            }

            if($u_paper) {
                if ($u_paper == 0) {
                } else {
                    $whereParts[] = "paper = $u_paper ";
                    $ifLeftFilter = 'join mst_design on mst_design.product_id = mu.product_id ';
                }
            }

            // dd($ifLeftFilter);

            $sql_data = "SELECT DISTINCT mu.product_id,type,mu.product_name,mu.price,mu.desc from mst_undangan mu ".$ifLeftFilter;
            if(count($whereParts)) {
                $sql_data .= "WHERE " . implode('AND ', $whereParts);
            }



        }elseif ($type == 'website') {

        }else{


            if($type != 'semua') { $whereParts[] = "type = '$type_id' "; }
            if($price_a) { $whereParts[] = "price >= '$price_a' AND price <= '$price_b' "; }

            $sql_data = "SELECT * from mst_other ";
            if(count($whereParts)) {
                $sql_data .= "WHERE " . implode('AND ', $whereParts);
            }

        }
        $data = \DB::select($sql_data);



        $list = '';


        foreach ($data as $key => $value) {

            if ($type == 'undangan') {

                $design = collect(\DB::select("SELECT * from mst_design where product_id = '".$value->product_id."'"))->first();

                // dd($design->image_id);
                if ($design) {
                    $dm = collect(\DB::select("SELECT * from mst_image where id = '".$design->image_id."'"))->first();
                }else{
                    $dm = null;
                }

            }else{
                $dm = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id = '".$value->product_id."'"))->first();

            }




            if ($dm) {
                if (Auth::user()) {
                    $d_wish = \DB::select("SELECT *
                    from mst_wishlist where product_id = '".$value->product_id."' and cust_id = ".Auth::user()->id);
                    if (count($d_wish) > 0 ) {
                        $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>
                        <svg><use xlink:href="#icon-wishlist-add" style="color:#ff6363;"></use></svg>';
                    }else {
                        $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                        <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>';
                    }

                }else {
                    $love = '<svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>
                    <svg><use xlink:href="#icon-wishlist-add" style="color:#ffffff;"></use></svg>';
                }

                $list .= '<div class="col-6 col-md-4 pt-col-item">
                                <div class="pt-product">
                                    <div class="pt-image-box">
                                        <div class="pt-app-btn">
                                            <a href="#" id="love'.$value->product_id.'_'.$value->type.'" onclick="setWishlist(`'.$value->product_id.'`,'.$value->type.')" class="pt-btn-wishlist" data-tposition="left">
                                                '.$love.'
                                            </a>

                                        </div>
                                        <a href="'.route("katalog.detail",["id"=>"$value->product_id","type"=>"$value->type"]).'">
                                            <span class="pt-img">
                                                <img class="zn-fit-center" style="height: 250px;" src="'.asset("gallery/product/$dm->img").'" alt="image">
                                            </span>
                                            <span class="pt-label-location">
                                            </span>
                                        </a>
                                    </div>
                                    <div class="pt-description">
                                        <div class="pt-col">

                                            <h2 class="pt-title"><a href="'.route("katalog.detail",["id"=>"$value->product_id","type"=>"$value->type"]).'">'.$value->product_name.'</a></h2>
                                            <ul class="pt-add-info">
                                                <li>
                                                    <a>By Corellia</a>

                                                </li>
                                            </ul>
                                            <div class="pt-content">
                                                '.$value->desc.'
                                            </div>
                                        </div>
                                        <div class="pt-col">
                                            <div class="pt-row-hover">
                                                <a href="'.route("katalog.detail",["id"=>"$value->product_id","type"=>"$value->type"]).'" class="pt-btn-addtocart">
                                                    <div class="pt-icon">
                                                        <svg><use xlink:href="#icon-cart_1"></use></svg>
                                                        <svg><use xlink:href="#icon-cart_1_plus"></use></svg>
                                                        <svg><use xlink:href="#icon-cart_1_disable"></use></svg>
                                                    </div>
                                                    <span class="pt-text" >BUY</span>
                                                </a>
                                                <div class="pt-price">
                                                    <span class="new-price">Rp '.$this->numFormat($value->price).'</span>
                                                </div>
                                                <div class="pt-wrapper-btn">
                                                    <a href="#" class="pt-btn-wishlist">
                                                        <span class="pt-icon">
                                                            <svg><use xlink:href="#icon-wishlist"></use></svg>
                                                            <svg><use xlink:href="#icon-wishlist-add"></use></svg>
                                                        </span>
                                                        <span class="pt-text">Add to wishlist</span>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }






        }

        return response()->json([
            'rc' => 0,
            'rm' => $list,
            'where' => $whereParts,
            'ifLeftFilter' => $ifLeftFilter,
            'sql_data' => $sql_data

        ]);
    }

    public function get_voucher(Request $request)
    {
        $voucher = $request->input('id');
        $dv = collect(\DB::select("SELECT * from mst_voucher where code = '".$voucher."'"))->first();

        if ($dv) {
            if (date('Y-m-d') > $dv->expdt) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Voucher Sudah Tidak Berlaku'
                ]);
            }elseif ($dv->vouchersts > 0) {
                return response()->json([
                    'rc' => 99,
                    'rm' => 'Voucher Telah Digunakan'
                ]);
            } else {
                return response()->json([
                    'rc' => 0,
                    'rm' => $dv
                ]);
            }


        } else {
            return response()->json([
                'rc' => 99,
                'rm' => 'Voucher Tidak Ditemukan'
            ]);
        }

    }

    public function get_waxseal(Request $request)
    {
        $id = $request->input('id');

        if ($id == '0') {
            $data_img = '';
        }else {
            $dm = collect(\DB::select("SELECT * from ref_waxseal where id = '".$id."'"))->first();
            // $data_img = '<img src="'.asset("gallery/waxseal/$dm->image").'" alt="">';

            $data_img = ' <div class="item">
                <a href="'.asset("gallery/waxseal/$dm->image").'"
                class="pt-gallery-link">
                <img class="zn-fit-center" src="'.asset("gallery/waxseal/$dm->image").'" alt=""></a>
            </div>';
        }



        return response()->json([
            'rc' => 0,
            'rm' => $data_img
        ]);
    }

    public function get_paper(Request $request)
    {
        $id = $request->input('id');

        if ($id == '0') {
            $data_img = '';
        }else {
            $dm = collect(\DB::select("SELECT * from ref_paper where id = '".$id."'"))->first();
            // $data_img = '<img  src="'.asset("gallery/paper/$dm->img").'" alt="">';

            $data_img = ' <div class="item">
            <a href="'.asset("gallery/paper/$dm->img").'"
            class="pt-gallery-link">
            <img class="zn-fit-center" src="'.asset("gallery/paper/$dm->img").'" alt=""></a>
        </div>';
        }



        return response()->json([
            'rc' => 0,
            'rm' => $data_img
        ]);
    }

    public function get_custom_design(Request $request)
    {
        $color = $request->input('color');
        $product_id = $request->input('product_id');
        $printing = $request->input('printing');
        $u_shape = $request->input('u_shape');
        $u_paper = $request->input('u_paper');


        $whereParts = array();

        if($product_id) { $whereParts[] = "product_id = '$product_id' "; }
        if($color) { $whereParts[] = "color = $color "; }
        if($u_shape) { $whereParts[] = "shape = $u_shape "; }
        if($u_paper) { $whereParts[] = "paper = $u_paper "; }
        if($printing) { $whereParts[] = "foilcolor = $printing "; }


        $sql_data = "SELECT * from mst_design ";
        if(count($whereParts)) {
            $sql_data .= "WHERE " . implode('AND ', $whereParts);
        }


        $data = collect(\DB::select($sql_data))->first();

        $dm = collect(\DB::select("SELECT * from mst_image where id = '".$data->image_id."'"))->first();

        // if ($printing) {
        //     $df = collect(\DB::select('select * from ref_foilcolor where id ='.$printing))->first();
        // }else {
        //     $df = collect(\DB::select('select * from ref_foilcolor where id = 3'))->first();
        // }






        $data_img = ' <style>
            .img {
            width:100%;
            height:600px;
            background: url('.asset("gallery/product/$dm->img").') center center no-repeat;
            background-size:100% auto;
            }
            </style>
        <div class="img">
        </div>';

        $img2 = '';
        $img3 = '';
        $img4 = '';
        $set_img2 = '';
        $set_img3 = '';
        $set_img4 = '';
        $set_btn = '';

        if ($data->image2) {
            $img2 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image2."'"))->first();
        }
        if ($data->image3) {
            $img3 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image3."'"))->first();
        }
        if ($data->image4) {
            $img4 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image4."'"))->first();
        }

        if ($dm != ''){
            $set_img1 = '<div class="col-md-3">
                             <div class="item">
                                 <a href="'.asset("gallery/product/$dm->img").'"
                                 class="pt-gallery-link">
                                 <img class="zn-fit-center" src="'.asset("gallery/product/$dm->img").'" alt=""></a>
                             </div>
                         </div>';
         }

        if ($img2 != ''){
               $set_img2 = '<div class="col-md-3">
                                <div class="item">
                                    <a href="'.asset("gallery/product/$img2->img").'"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="'.asset("gallery/product/$img2->img").'" alt=""></a>
                                </div>
                            </div>';
            }
        if ($img3 != ''){
            $set_img3 = '<div class="col-md-3">
                                <div class="item">
                                    <a href="'.asset("gallery/product/$img3->img").'"
                                    class="pt-gallery-link">
                                    <img class="zn-fit-center" src="'.asset("gallery/product/$img3->img").'" alt=""></a>
                                </div>
                            </div>';
            }
        if ($img4 != ''){
        $set_img4 = '<div class="col-md-3">
                            <div class="item">
                                <a href="'.asset("gallery/product/$img4->img").'"
                                class="pt-gallery-link">
                                <img class="zn-fit-center" src="'.asset("gallery/product/$img4->img").'" alt=""></a>
                            </div>
                        </div>';
        }
        // if ($img3 != ''){
        //     $set_img2 = '<div class="item">
        //         <a target="blank" href="'.asset("gallery/product/$img3->img").'"
        //         class="pt-gallery-link">
        //         <img src="'.asset("gallery/product/$img3->img").'" alt=""></a>
        //     </div>';
        // }

        // if ($img4 != ''){
        //     $set_img3 = '<div class="item">
        //         <a target="blank" href="'.asset("gallery/product/$img4->img").'"
        //         class="pt-gallery-link">
        //         <img src="'.asset("gallery/product/$img4->img").'" alt=""></a>
        //     </div>';
        // }






            $data_img_other = $set_img1.$set_img2.$set_img3.$set_img4;

        return response()->json([
            'rc' => 0,
            'rm' => $data_img,
            'rimg' => $data_img_other

        ]);
    }

    public function maintance(Request $request)
    {
        return view('maintance');
    }

}
