<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Session;
use Request as Req;
use App\Models\UserModel;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Redirect;
use View;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

   

    public function getdomain($domain)
    {
        $data = collect(\DB::select("SELECT * 
        from mst_data_website mo where mo.nama_website = ?",[$domain]))->first();
        if ($data) {

            if ($data->is_active == 1) {
                $dataPembelian = \DB::selectOne("SELECT * from mst_pembelian where design_id = ?",[$data->id]);

                $enddate = strtotime($data->date_active);
                $enddate = strtotime("+".$dataPembelian->qty." week", $enddate);

                if (date("Y-m-d H:i:s") >= $data->date_active && date("Y-m-d H:i:s") <= date('Y-m-d',$enddate) ) {
                    $param['data'] = $data;
                    // $param['buku_tamu'] = \DB::select("SELECT * from buku_tamu where website_id = '".$data->id."'");
            
                    $d_view = 'template.'.$data->product_id.'.index';
                    return view($d_view,$param);
                } else {
                    return view('suspend');
                }
            }else{
                return view('suspend');
            }

            
            
          
        }else {
            return view('404');
        }
        

    }

    public function mail()
    {
        // $pembelian = collect(\DB::select("SELECT * from mst_pembelian
        // left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
        // where no_pembelian = 'CO20191218231' "))->first();

        // $data = array('pembelian' => $pembelian, 'subject' => 'b');
        
        // Mail::send('email.pembayaran', $data, function ($message) use ($data) {
        // $message->from('zame@corellia.id', 'Corellia - Wedding Art');
        // $message->to('zamzam_nurzaman@outlook.com');
        // $message->subject('Selesaikan Pembayaran'); 
        // });

        $pembelian = DB::table('mst_pembelian')->where('no_pembelian', 'CO20191220291')->first();
         
        $param_email = collect(\DB::select("SELECT mst_pembelian.*,ref_bank.*,mc.email from mst_pembelian
        left join ref_bank on ref_bank.id::int = mst_pembelian.bank::int
        left join mst_customer mc on mc.id = mst_pembelian.custid
        where no_pembelian = 'CO20191220291' "))->first();

        $data = array('pembelian' => $param_email, 'subject' => 'b');
        
        Mail::send('email.pembelian', $data, function ($message) use ($data) {
        $message->from('official@corellia.id', 'Corellia - Wedding Art');
        $message->to($data['pembelian']->email);
        $message->subject('Selesaikan Pembayaran'); 
        });

        return 'Email was sent';
    }

    public function index(Request $request)
    {

        $data_home = collect(\DB::select("SELECT mp.judul as promo1,ms1.judul as promo2,ms2.judul as promo3, mh.* 
        FROM mst_home mh
        join mst_promo mp on mp.id::INTEGER = mh.promo_head::INTEGER
        join mst_promo ms1 on ms1.id::INTEGER = mh.promo_subhead1::INTEGER
        join mst_promo ms2 on ms2.id::INTEGER = mh.promo_subhead2::INTEGER
        "))->first();

        $param['data_home'] = $data_home;


        
            return view('master.master')->nest('child', 'home',$param);
        
    }

    public function faq(Request $request)
    {
        return view('master.master')->nest('child', 'faq');
    }

    public function promo(Request $request)
    {
        $data = \DB::select("SELECT * from mst_promo order by id desc");
        $datas['data'] = $data;

        return view('master.master')->nest('child', 'promo',$datas);
    }

    public function privacy(Request $request)
    {
        return view('master.master')->nest('child', 'privacy');
    }
    public function term(Request $request)
    {
        return view('master.master')->nest('child', 'term');
    }
    public function about(Request $request)
    {
        return view('master.master')->nest('child', 'about');
    }
    public function contact(Request $request)
    {
        return view('master.master')->nest('child', 'contact');
    }

    public function maintance(Request $request)
    {
        return view('maintance');
    }

    public function login_customer(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = DB::table('mst_customer')->where('username', $request->input('username'))->first();
        if (!is_null($user)) {
            
            if (!Hash::check($request->input('password'), $user->password)){
                return response()->json([
                    'rc' => 0,
                    'rm' => 'Username atau Password salah'
                ]);
            }elseif (!$user->cust_status){
                return response()->json([
                    'rc' => 1,
                    'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin corellia.id.'
                ]);
            } else {

                Session::put('id', $user->id);
                Auth::loginUsingId($user->id,true);
    
                return response()->json([
                    'rc' => 3,
                    'rm' => 'success'
                ]);
            }

        } else {
            // login failed
            return response()->json([
                'rc' => 0,
                'rm' => 'Username Belum Terdaftar'
            ]);
        }
    }

    public function register_customer(Request $request)
    {
        try{
            $data = new UserModel();
            $data->cust_name = $request->input('r_nama');
            $data->email = $request->input('r_email');
            $data->username = $request->input('r_username');
            $data->password = Hash::make($request->input('r_password'));
            $data->cust_status = 1;
            $data->save();

            return response()->json([
                'rc' => 1,
                'rm' => 'Berhasil'
            ]);

        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
       

       
    }

}
