<?php
Route::get('/admin_login', 'admin\Auth\LoginController@login_admin')->name('admin.login');
Route::post('/admin_action', 'admin\Auth\LoginController@authenticate')->name('admin.login.action');

Route::get('admin/valid/checking/{id}', 'admin\Controller@checking')->name('admin.valid.checking');

Route::post('admin_notif/get', 'admin\Controller@admin_notif')->name('admin_notif.get');

Route::post('admin_notif/read_notif', 'admin\Controller@read_notif')->name('admin_notif.read_notif');

Route::get('data/dashboard/{id}', 'admin\HomeController@data')->name('data.dashboard');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/admin/dashboard','admin\HomeController@index')->name('admin.dashboard');
    Route::post('/admin/logout_admin', 'admin\Auth\LoginController@logout_admin')->name('admin.logout');

    // Route::get('','admin\HomeController@index')->name('admin.index');

    Route::get('admin/audit_trail','admin\HomeController@audit_trail')->name('admin.audit_trail');
    Route::get('admin/data_audit_trail','admin\HomeController@data_audit_trail')->name('audit.data_audit_trail');


    // VALID


    Route::prefix('admin/pembelian')->group(function () {
        Route::get('index', 'admin\PembelianController@index')->name('pembelian.index');
        Route::get('data', 'admin\PembelianController@data')->name('pembelian.data');
        Route::get('konfirmasi_pembayaran', 'admin\PembelianController@konfirmasi_pembayaran')->name('pembelian.konfirmasi_pembayaran');
        Route::get('konfirmasi_pembelian', 'admin\PembelianController@konfirmasi_pembelian')->name('pembelian.konfirmasi_pembelian');
        Route::get('detail_pembelian', 'admin\PembelianController@detail_pembelian')->name('pembelian.detail_pembelian');
        Route::post('list_trace', 'admin\PembelianController@list_trace')->name('pembelian.list_trace');
        Route::post('add_trace', 'admin\PembelianController@add_trace')->name('pembelian.add_trace');
        Route::post('pembelian_selesai', 'admin\PembelianController@pembelian_selesai')->name('pembelian.pembelian_selesai');

    });

    Route::prefix('admin/design_fix')->group(function () {
        Route::get('index', 'admin\DesignFixController@index')->name('design_fix.index');
        Route::get('data', 'admin\DesignFixController@data')->name('design_fix.data');
        Route::get('konfirmasi_pembayaran', 'admin\DesignFixController@konfirmasi_pembayaran')->name('design_fix.konfirmasi_pembayaran');
        Route::get('konfirmasi_pembelian', 'admin\DesignFixController@konfirmasi_pembelian')->name('design_fix.konfirmasi_pembelian');
        Route::get('detail_pembelian', 'admin\DesignFixController@detail_pembelian')->name('design_fix.detail_pembelian');
        Route::post('list_trace', 'admin\DesignFixController@list_trace')->name('design_fix.list_trace');
        Route::post('add_trace', 'admin\DesignFixController@add_trace')->name('design_fix.add_trace');
    });

    Route::prefix('admin/notif')->group(function () {
        Route::get('index', 'admin\MstNotifController@index')->name('mst_notif.index');
        Route::get('data', 'admin\MstNotifController@data')->name('mst_notif.data');
        Route::get('edit/{id}', 'admin\MstNotifController@edit')->name('mst_notif.edit');
        Route::post('delete/{id}', 'admin\MstNotifController@delete')->name('mst_notif.delete');
        Route::post('store', 'admin\MstNotifController@store')->name('mst_notif.store');
    });

    Route::prefix('admin/mst_website')->group(function () {
        Route::get('index', 'admin\MstWebsiteController@index')->name('mst_website.index');
        Route::get('data', 'admin\MstWebsiteController@data')->name('mst_website.data');
        Route::get('edit/{id}', 'admin\MstWebsiteController@edit')->name('mst_website.edit');
        Route::post('delete/{id}', 'admin\MstWebsiteController@delete')->name('mst_website.delete');
        Route::post('store', 'admin\MstWebsiteController@store')->name('mst_website.store');
    });

    Route::prefix('admin/ref_color')->group(function () {
        Route::get('index', 'admin\RefColorController@index')->name('ref_color.index');
        Route::get('data', 'admin\RefColorController@data')->name('ref_color.data');
        Route::get('edit/{id}', 'admin\RefColorController@edit')->name('ref_color.edit');
        Route::post('delete/{id}', 'admin\RefColorController@delete')->name('ref_color.delete');
        Route::post('store', 'admin\RefColorController@store')->name('ref_color.store');
    });

    Route::prefix('admin/ref_waxseal')->group(function () {
        Route::get('index', 'admin\refWaxsealController@index')->name('ref_waxseal.index');
        Route::get('data', 'admin\refWaxsealController@data')->name('ref_waxseal.data');
        Route::get('edit/{id}', 'admin\refWaxsealController@edit')->name('ref_waxseal.edit');
        Route::post('delete/{id}', 'admin\refWaxsealController@delete')->name('ref_waxseal.delete');
        Route::post('store', 'admin\refWaxsealController@store')->name('ref_waxseal.store');
    });

    Route::prefix('admin/ref_foilcolor')->group(function () {
        Route::get('index', 'admin\refFoilcolorController@index')->name('ref_foilcolor.index');
        Route::get('data', 'admin\refFoilcolorController@data')->name('ref_foilcolor.data');
        Route::get('edit/{id}', 'admin\refFoilcolorController@edit')->name('ref_foilcolor.edit');
        Route::post('delete/{id}', 'admin\refFoilcolorController@delete')->name('ref_foilcolor.delete');
        Route::post('store', 'admin\refFoilcolorController@store')->name('ref_foilcolor.store');
    });

    Route::prefix('admin/mst_promo')->group(function () {
        Route::get('index', 'admin\MstPromoController@index')->name('mst_promo.index');
        Route::get('data', 'admin\MstPromoController@data')->name('mst_promo.data');
        Route::get('edit/{id}', 'admin\MstPromoController@edit')->name('mst_promo.edit');
        Route::post('delete/{id}', 'admin\MstPromoController@delete')->name('mst_promo.delete');
        Route::post('store', 'admin\MstPromoController@store')->name('mst_promo.store');
    });

    Route::prefix('admin/ref_paper')->group(function () {
        Route::get('index', 'admin\refPaperController@index')->name('ref_paper.index');
        Route::get('data', 'admin\refPaperController@data')->name('ref_paper.data');
        Route::get('edit/{id}', 'admin\refPaperController@edit')->name('ref_paper.edit');
        Route::post('delete/{id}', 'admin\refPaperController@delete')->name('ref_paper.delete');
        Route::post('store', 'admin\refPaperController@store')->name('ref_paper.store');
    });


    Route::prefix('admin/voucher')->group(function () {
        Route::get('index', 'admin\MstVoucherController@index')->name('mst_voucher.index');
        Route::get('data', 'admin\MstVoucherController@data')->name('mst_voucher.data');
        Route::get('edit/{id}', 'admin\MstVoucherController@edit')->name('mst_voucher.edit');
        Route::post('delete/{id}', 'admin\MstVoucherController@delete')->name('mst_voucher.delete');
        Route::post('store', 'admin\MstVoucherController@store')->name('mst_voucher.store');
    });

    Route::prefix('admin/customer')->group(function () {
        Route::get('index', 'admin\MstCustomerController@index')->name('mst_customer.index');
        Route::get('data', 'admin\MstCustomerController@data')->name('mst_customer.data');
        Route::post('delete/{id}', 'admin\MstCustomerController@delete')->name('mst_customer.delete');
    });

    Route::prefix('admin/ref_tipe_produk')->group(function () {
        Route::get('index', 'admin\refTipeProdukController@index')->name('tipe_produk.index');
        Route::get('data', 'admin\refTipeProdukController@data')->name('tipe_produk.data');
        Route::get('edit/{id}', 'admin\refTipeProdukController@edit')->name('tipe_produk.edit');
        Route::post('delete/{id}', 'admin\refTipeProdukController@delete')->name('tipe_produk.delete');
        Route::post('store', 'admin\refTipeProdukController@store')->name('tipe_produk.store');
    });

    Route::prefix('admin/ref_additional')->group(function () {
        Route::get('index', 'admin\refAdditionalController@index')->name('ref_additional.index');
        Route::get('data', 'admin\refAdditionalController@data')->name('ref_additional.data');
        Route::get('edit/{id}', 'admin\refAdditionalController@edit')->name('ref_additional.edit');
        Route::post('delete/{id}', 'admin\refAdditionalController@delete')->name('ref_additional.delete');
        Route::post('store', 'admin\refAdditionalController@store')->name('ref_additional.store');
    });

    Route::prefix('admin/ref_bank')->group(function () {
        Route::get('index', 'admin\refBankController@index')->name('ref_bank.index');
        Route::get('data', 'admin\refBankController@data')->name('ref_bank.data');
        Route::get('edit/{id}', 'admin\refBankController@edit')->name('ref_bank.edit');
        Route::post('delete/{id}', 'admin\refBankController@delete')->name('ref_bank.delete');
        Route::post('store', 'admin\refBankController@store')->name('ref_bank.store');
    });

    Route::prefix('admin/ref_finishing')->group(function () {
        Route::get('index', 'admin\refFinishingController@index')->name('ref_finishing.index');
        Route::get('data', 'admin\refFinishingController@data')->name('ref_finishing.data');
        Route::get('edit/{id}', 'admin\refFinishingController@edit')->name('ref_finishing.edit');
        Route::post('delete/{id}', 'admin\refFinishingController@delete')->name('ref_finishing.delete');
        Route::post('store', 'admin\refFinishingController@store')->name('ref_finishing.store');
    });

    Route::prefix('admin/mst_other')->group(function () {
        Route::get('index', 'admin\MstOtherController@index')->name('mst_other.index');
        Route::get('data', 'admin\MstOtherController@data')->name('mst_other.data');
        Route::get('edit/{id}', 'admin\MstOtherController@edit')->name('mst_other.edit');
        Route::post('delete/{id}', 'admin\MstOtherController@delete')->name('mst_other.delete');
        Route::post('store', 'admin\MstOtherController@store')->name('mst_other.store');
    });

    Route::prefix('admin/mst_undangan')->group(function () {
        Route::get('index', 'admin\MstUndanganController@index')->name('mst_undangan.index');
        Route::get('data', 'admin\MstUndanganController@data')->name('mst_undangan.data');
        Route::get('edit/{id}', 'admin\MstUndanganController@edit')->name('mst_undangan.edit');
        Route::post('delete/{id}', 'admin\MstUndanganController@delete')->name('mst_undangan.delete');
        Route::post('store', 'admin\MstUndanganController@store')->name('mst_undangan.store');
    });

    Route::prefix('admin/undangan_type_size')->group(function () {
        Route::get('index', 'admin\MstUndanganController@index_type_size')->name('undangan_type_size.index');
        Route::get('data', 'admin\MstUndanganController@data_type_size')->name('undangan_type_size.data');
        Route::get('edit/{id}', 'admin\MstUndanganController@edit_type_size')->name('undangan_type_size.edit');
        Route::post('delete/{id}', 'admin\MstUndanganController@delete_type_size')->name('undangan_type_size.delete');
        Route::post('store', 'admin\MstUndanganController@store_type_size')->name('undangan_type_size.store');
    });

    Route::prefix('admin/mst_design')->group(function () {
        Route::get('index', 'admin\MstDesignController@index')->name('mst_design.index');
        Route::get('data', 'admin\MstDesignController@data')->name('mst_design.data');
        Route::get('edit/{id}', 'admin\MstDesignController@edit')->name('mst_design.edit');
        Route::post('delete/{id}', 'admin\MstDesignController@delete')->name('mst_design.delete');
        Route::post('store', 'admin\MstDesignController@store')->name('mst_design.store');
    });

    Route::prefix('admin/design_order')->group(function () {
        Route::get('index', 'admin\DesignOrderController@index')->name('design_order.index');
        Route::get('data', 'admin\DesignOrderController@data')->name('design_order.data');
        Route::get('edit/{id}', 'admin\DesignOrderController@edit')->name('design_order.edit');
        Route::post('delete/{id}', 'admin\DesignOrderController@delete')->name('design_order.delete');
        Route::post('store', 'admin\DesignOrderController@store')->name('design_order.store');
    });

    Route::prefix('admin/user')->group(function () {
        Route::get('index', 'admin\UserController@index')->name('user.index');
        Route::get('data', 'admin\UserController@data')->name('user.data');
        Route::get('edit/{id}', 'admin\UserController@edit')->name('user.edit');
        Route::post('store', 'admin\UserController@store')->name('user.store');
        Route::delete('delete/{id}', 'admin\UserController@delete')->name('user.delete');
        Route::get('/active','admin\UserController@SetActive')->name('user.setActive');
        Route::post('user/resetpassword/{id}', 'admin\UserController@resetPassword')->name('user.resetPassword');
        Route::post('user/changePassword', 'admin\UserController@changePassword')->name('user.changePassword');
        Route::get('check/password', 'admin\UserController@check_password')->name('password.check');
    });

    Route::prefix('admin/invoice')->group(function () {
        Route::get('index', 'admin\InvoiceController@index')->name('invoice.index');
        Route::get('index_other', 'admin\InvoiceController@index_other')->name('invoice.index_other');
        Route::post('store', 'admin\InvoiceController@store')->name('invoice.store');
        Route::post('store_other', 'admin\InvoiceController@store_other')->name('invoice.store_other');

        Route::get('data', 'admin\InvoiceController@data')->name('invoice.data');
        Route::get('edit/{id}', 'admin\InvoiceController@edit')->name('invoice.edit');
        Route::post('delete/{id}', 'admin\InvoiceController@delete')->name('invoice.delete');


        Route::get('get_data/{type}/{id}', 'admin\InvoiceController@get_data')->name('invoice.get_data');

    });

    Route::prefix('admin/mst_home')->group(function () {
        Route::get('index', 'admin\HomeController@index_mst_home')->name('mst_home.index_mst_home');
        Route::post('store', 'admin\HomeController@store')->name('mst_home.store');
    });

    // PEMBAYARAN APPROVE
    Route::post('pembayaran_approve', 'admin\PembelianController@pembayaran_approve')->name('pembelian.pembayaran_approve');
    Route::post('pembayaran_reject', 'admin\PembelianController@pembayaran_reject')->name('pembelian.pembayaran_reject');

    // PEMBELIAN APPROVE
    Route::post('pembelian_approve', 'admin\PembelianController@pembelian_approve')->name('pembelian.pembelian_approve');
    Route::post('pembelian_reject', 'admin\PembelianController@pembelian_reject')->name('pembelian.pembelian_reject');

    //DESIGN SELESAI
    Route::post('design_selesai', 'admin\DesignFixController@design_selesai')->name('design_fix.design_selesai');

    Route::post('set_designer', 'admin\PembelianController@set_designer')->name('staff.set_designer');


});
