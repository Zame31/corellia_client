<?php
include_once 'admin.php';

// Route::get('/','HomeController@maintance')->name('maintance');
Auth::routes();

Route::get('/{domain}','HomeController@getdomain')->name('getdomain');

Route::post('/v/set_session_undangan','CustomerController@set_session_undangan')->name('customer.set_session_undangan');



Route::get('/','HomeController@index')->name('home');
Route::get('/undangan/custom_design','UndanganController@custom_design')->name('undangan.custom_design');
Route::post('/undangan/set_design','UndanganController@set_design')->name('undangan.set_design');

Route::post('/katalog/design_foilcolor','KatalogController@design_foilcolor')->name('katalog.design_foilcolor');

Route::get('/v/faq','HomeController@faq')->name('home.faq');
Route::get('/v/privacy','HomeController@privacy')->name('home.privacy');
Route::get('/v/term','HomeController@term')->name('home.term');
Route::get('/v/about','HomeController@about')->name('home.about');
Route::get('/v/contact','HomeController@contact')->name('home.contact');
Route::get('/v/promo','HomeController@promo')->name('home.promo');

Route::prefix('customer')->group(function () {
    Route::post('login', 'HomeController@login_customer')->name('login_customer');
    Route::post('register_customer', 'HomeController@register_customer')->name('register_customer');

    Route::get('account', 'CustomerController@account')->name('customer.account');
    Route::post('passwordbaru','CustomerController@passwordbaru')->name('customer.passwordbaru');

    Route::post('get_list_notif','CustomerController@get_list_notif')->name('customer.get_list_notif');

    Route::get('pembelian_list', 'CustomerController@pembelian_list')->name('customer.pembelian_list');
    Route::post('set_pembayaran', 'CustomerController@set_pembayaran')->name('customer.set_pembayaran');
    Route::post('get_detail_pembayaran', 'CustomerController@get_detail_pembayaran')->name('customer.get_detail_pembayaran');

    // WISHLIST
    Route::get('wishlist', 'CustomerController@wishlist')->name('customer.wishlist');
    Route::post('set_wishlist', 'CustomerController@set_wishlist')->name('customer.set_wishlist');

    // CARTLIST
    Route::get('cartlist', 'CustomerController@cartlist')->name('customer.cartlist');
    Route::post('set_cartlist', 'CustomerController@set_cartlist')->name('customer.set_cartlist');
    Route::post('remove_cartlist', 'CustomerController@remove_cartlist')->name('customer.remove_cartlist');

    // CHECKOUT
    Route::get('checkout', 'CustomerController@checkout')->name('customer.checkout');
    Route::post('set_checkout', 'CustomerController@set_checkout')->name('customer.set_checkout');

    Route::post('get_profil', 'CustomerController@get_profil')->name('customer.get_profil');
    Route::post('set_profil', 'CustomerController@set_profil')->name('customer.set_profil');

    Route::post('set_pembelian', 'CustomerController@set_pembelian')->name('customer.set_pembelian');

    Route::get('invoice', 'CustomerController@invoice')->name('customer.invoice');
    Route::get('pay/{code}', 'CustomerController@pay')->name('customer.pay');

    Route::get('list_custom_design','CustomerController@list_custom_design')->name('customer.list_custom_design');

    Route::post('set_note_undangan', 'CustomerController@set_note_undangan')->name('customer.set_note_undangan');
    Route::post('batalkan_pembelian', 'CustomerController@batalkan_pembelian')->name('customer.batalkan_pembelian');

    Route::post('customer_notif', 'CustomerController@customer_notif')->name('customer.customer_notif');




});

Route::prefix('katalog')->group(function () {
    Route::get('list/{type}/{type_id}', 'KatalogController@index')->name('katalog.index');
    Route::get('detail/{id}/{type}', 'KatalogController@detail')->name('katalog.detail');
    Route::post('get_list', 'KatalogController@get_list')->name('katalog.get_list');
    Route::post('get_voucher', 'KatalogController@get_voucher')->name('katalog.get_voucher');
    Route::post('get_custom_design', 'KatalogController@get_custom_design')->name('katalog.get_custom_design');
    Route::post('get_waxseal', 'KatalogController@get_waxseal')->name('katalog.get_waxseal');
    Route::post('get_paper', 'KatalogController@get_paper')->name('katalog.get_paper');
});

Route::prefix('website')->group(function () {
    Route::get('list/{type}/{type_id}', 'WebsiteController@index')->name('website.index');
    Route::get('detail/{id}/{type}', 'WebsiteController@detail')->name('website.detail');
    Route::post('get_list', 'WebsiteController@get_list')->name('website.get_list');
    Route::post('set_image', 'WebsiteController@set_image')->name('website.set_image');
    Route::post('set_cartlist', 'WebsiteController@set_cartlist')->name('website.set_cartlist');
    Route::get('preview/OsNhPJkVHzaqBkTYiA1bafTW/{id}/{name}', 'WebsiteController@preview')->name('website.preview');

    Route::get('edit_website/{productId}/{id}', 'WebsiteController@edit_website')->name('website.edit_website');
    Route::post('set_edit', 'WebsiteController@set_edit')->name('website.set_edit');

    Route::post('list_buku_tamu','WebsiteController@list_buku_tamu')->name('website.list_buku_tamu');
    

});

Route::post('/store_buku_tamu', 'WebsiteController@store_buku_tamu')->name('website.store_buku_tamu');


Route::get('/send/email', 'HomeController@mail');
// VALID
Route::get('valid/checking/{id}', 'Controller@checking')->name('valid.checking');
