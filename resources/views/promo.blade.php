@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">Promo</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
						
						@foreach ($data as $item)
						<div class="zn-box-promo">
							<dt style="font-size: 15px;padding-bottom: 0;"> {{$item->judul}} </dt>
							<dd>
								<p style="font-size: 14px;">
									{{$item->desc}}
								</p>
							</dd>
						</div>
						@endforeach
						
						
					
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop