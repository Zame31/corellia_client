@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">About Us</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
						<div style="text-align: justify;">
							Corellia merupakan  salah satu vendor pernikahan yang menciptakan karyanya melalui sebuah kartu undangan, guna menympaikan pesan bahagia melalui sebuah ucapan tertulis dan pesan online yang dikemas pada masa kini. 
							<br><br> 
							Dengan seiringnya waktu corellia tumbuh untuk memudahkan pasangan dalam pemilihan desain kartu undangan serta budgeting, tanpa harus bertatap muka ataupun konsultasi, dengan kemudahan akses dalam mencoba dan merasakan sensasi pemilihan warna , bentuk dan desain secara realtime dengan fitur yang ada di website corellia ini.
						</div>
					
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop