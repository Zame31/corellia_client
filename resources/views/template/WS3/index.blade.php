<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wedding Invitation {{$data->setname}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="{{$data->setname}}">
	<meta name="keywords" content="Wedding Invitation {{$data->setname}}">
	<meta name="description" content="Spesial mengundang untuk menghadiri pernikahan kami. {{$data->setname}}" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Libre+Caslon+Text:400,400i,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('website/WS3/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS3/css/animate.css')}}">
	<link rel="icon" href="{{asset('gallery/website_img/')}}/{{$data->set_header}}">
    <link rel="stylesheet" href="{{asset('website/WS3/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS3/css/fowl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS3/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('website/WS3/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('website/WS3/css/ionicons.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('website/WS3/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('website/WS3/css/icomoon.css')}}">
	<link rel="stylesheet" href="{{asset('website/WS3/css/style.css')}}">
	<style media="screen">
		#bg_color {
			background: url("{{asset('website/WS1/images/pat7.png')}}");
			background-repeat: repeat;
			margin-bottom: 0px;
			opacity: 1;
		}
		.modal {
        text-align: center;
        padding: 0!important;
        }

        .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
        }

        .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        }
	</style>
		<link href="{{asset('js/sound/fraudio.min.css')}}" rel="stylesheet">
		
  </head>
  @php
  if ($data->bg_color) {
	  $bgcolor = 'background:'.$data->bg_color.'52;';
  }else{
	  $bgcolor = '';
  }
@endphp
  <body class="scrollStyle" id="bg_color" style="overflow-x: hidden;">
	@if (isset($type))
	<div style="
	position: fixed;
	z-index: 10000;
	background: #000000de;
	width: 100%;
	text-align: center;
	padding: 20px;
	color: #f7b79a;
	font-weight: 500;
	font-size: 14px;
	opacity: 0.9;
	font-style: oblique;
">
	"untuk mengaktifkan website, silahkan checkout pesanan ini dan lakukan pembayaran. Terimakasih" 
	<img src="{{asset('img/logo.png')}}" style="width: 100px;margin-left: 50px;" alt="">
</div>
    @endif
	<div style="{{$bgcolor}}">

	<audio class="fraudio" loop>
		<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/ogg">
		<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/mpeg">
	</audio>
	<span style="
	display: inline-block;
	 position: fixed;
	 z-index: 1000;
	 color: white;
	 bottom: 88px;
	 right: 77px;
	 text-align: right;
	 font-size: 11px;
	 line-height: 13px;
	 ">
	  Music: <br> <a target="_blank" style="font-weight: 500;color: #ffffff;" href="https://www.bensound.com/">www.bensound.com</a></span>

	
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">The Wedding</a>
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	          <li class="nav-item"><a href="#set_header" class="nav-link"><span>Home</span></a></li>
	          <li class="nav-item"><a href="#groom-bride-section" class="nav-link"><span>Groom &amp; Bride</span></a></li>
	          <li class="nav-item"><a href="#when-where-section" class="nav-link"><span>When &amp; Where</span></a></li>
	          <li class="nav-item"><a href="#gallery-section" class="nav-link"><span>Gallery</span></a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>

	  <section id="set_header" class="video-hero js-fullheight" style="height: 700px; background-image: url('{{asset('gallery/website_img/')}}/{{$data->set_header}}'); background-size:cover; background-position: top center;" data-stellar-background-ratio="0.5">
	  	<div class="overlay"></div>
			<!-- <a class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=Mjjw19B7rMk',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'default',optimizeDisplay:true}"></a> -->
			<div class="container">
				<div class="row js-fullheight justify-content-center d-flex align-items-center">
					<div class="col-md-12">
						<div class="text text-center">
							<div class="icon">
								<span class="flaticon-rose-outline-variant-with-vines-and-leaves"></span>
							</div>
							<span class="subheading">The Wedding of</span>
							<h1 id="setName">{{$data->setname}}</h1>
							<div id="timer" class="d-flex">
							  <div class="time" id="days"></div>
							  <div class="time pl-3" id="hours"></div>
							  <div class="time pl-3" id="minutes"></div>
							  <div class="time pl-3" id="seconds"></div>
							</div>
							<a target="_blank" href="http://maps.google.com/maps?q={{$data->maps_lat}},{{$data->maps_lng}}&z=18" value="Get Location" class="mt-5 btn btn-primary py-3 px-4">Get Location</a>
							@if ($data->link_live)
								<a href="{{$data->link_live}}" target="_blank"  value="Live Streaming" class="mt-5 btn btn-primary py-3 px-4">Live Streaming</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-section ftco-about ftco-no-pt ftco-no-pb" id="groom-bride-section">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-12">
    				<div class="wrap">
			    		<div class="row d-flex">
			    		
			    			<div class="col-md-12 py-md-5 text">
			    				<div class="py-md-4">
				    				<div class="row justify-content-start pb-3">
									<div class="col-md-12 ftco-animate p-5 p-lg-5 text-center">
										<span class="subheading mb-4">Join us to celebrate <br>the wedding day of</span>
										<h2 class="mb-4" id="setTglRes">{{date('d F Y', strtotime($data->settglres))}}</h2>
										<span class="icon flaticon-rose-variant-outline-with-vines"></span>
										<span class="subheading" id="setTempatRes">{{$data->settempatres}}</span>
										<span class="subheading mb-5" id="setAlamatRes">{{$data->setalamatres}}</span>
									</div>
						        </div>
					        </div>
				        </div>
			        </div>
			      </div>
		      </div>
		    </div>
    	</div>
	</section>
	
	<div class="row">
		<div class="col-md-12">
			<span id="setKata" class="zn-quote">
				{{$data->setkata}}	
			</span>
		</div>
	</div>

    <section class="ftco-section bg-section">
    	<div class="overlay-top" style="background-image: url(images/top-bg.jpg);"></div>
    	<div class="overlay-bottom" style="background-image: url(images/bottom-bg.jpg);"></div>
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 text-center heading-section ftco-animate">
          	{{-- <span class="clone">Bride &amp; Groom</span> --}}
            <h2 class="mb-3">Bride &amp; Groom</h2>
          </div>
        </div>
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
		        	<div class="col-md-6 text-center d-flex align-items-stretch">
		        		<div class="bride-groom ftco-animate">
		        			<img id="foto_pria" class="img" src="{{asset('gallery/website_img/')}}/{{$data->foto_pria}}"/>
		        			<div class="text mt-4 px-4">
		        				<h2 id="setNamePria">{{$data->setnamepria}}</h2>
		        				<p id="setDescPria">{{$data->setdescpria}}</p>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-md-6 text-center d-flex align-items-stretch">
		        		<div class="bride-groom ftco-animate">
							<img id="foto_wanita" class="img" src="{{asset('gallery/website_img/')}}/{{$data->foto_wanita}}"/>
		        			<div class="text mt-4 px-4">
		        				<h2 id="setNameWanita">{{$data->setnamewanita}}</h2>
		        				<p id="setDescWanita">{{$data->setdescwanita}}</p>
		        			</div>
		        		</div>
		        	</div>
		        </div>
		      </div>
        </div>
    	</div>
    </section>
   

    <section class="ftco-section bg-light" id="when-where-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 text-center heading-section ftco-animate">
          	{{-- <span class="clone">Place</span> --}}
            <h2 class="mb-3">Place &amp; Time</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-6 ftco-animate">
        		<div class="place img" style="background-image: url(images/place-2.jpg);">
        			<div class="text text-center">
	        			<span class="icon flaticon-wedding-kiss"></span>
	        			<h3>The Ceremony</h3>
						<p><span id="setTglAkad">{{$data->settglakad}}</span>
							<br>
							<span><span id="setJam1Akad">{{$data->setjam1akad}}</span>-<span id="setJam2Akad">{{$data->setjam2akad}}</span></span></p>
	        			<p><span id="setAlamatAkad">{{$data->setalamatakad}}</span></p>
	        			
	        		</div>
        		</div>
        	</div>
        	<div class="col-md-6 ftco-animate">
        		<div class="place img" style="background-image: url(images/place-3.jpg);">
        			<div class="text text-center">
	        			<span class="icon flaticon-cake"></span>
	        			<h3>The Party</h3>
						<p><span id="setTglRes2">{{$data->settglres}}</span>
							<br><span><span id="setJam1Res">{{$data->setjam1res}}</span> - <span id="setJam2Res">{{$data->setjam2res}}</span></span></p>
	        			<p><span id="setAlamatRes2">{{$data->setalamatres}}</span></p>
	        		</div>
        		</div>
        	</div>
        </div>
    	</div>
    </section>


    <section class="ftco-section" id="gallery-section">
    	<div class="container-fluid px-md-4">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 text-center heading-section ftco-animate">
          	{{-- <span class="clone">Photos</span> --}}
            <h2 class="mb-3">Gallery</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-3 ftco-animate">
        		<a id="gallery_1" class="gallery img d-flex align-items-center justify-content-center" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_1}}');">
        		</a>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<a id="gallery_2" class="gallery img d-flex align-items-center justify-content-center" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_2}}');">
        		</a>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<a id="gallery_3" class="gallery img d-flex align-items-center justify-content-center" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_3}}');">
        		</a>
        	</div>
        	<div class="col-md-3 ftco-animate">
        		<a id="gallery_4" class="gallery img d-flex align-items-center justify-content-center" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_4}}');">
        		</a>
        	</div>
        	
        </div>
    	</div>
	</section>
	@if (URL::to('/') != 'https://wedding.bukutamuku.com')
	<section class="ftco-section" id="tamu-section">
    	<div class="container-fluid px-md-4">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 text-center heading-section ftco-animate">
          	{{-- <span class="clone">Kumpulan Ucapan dari Tamu</span> --}}
            <h2 class="mb-3">Buku Tamu</h2>
          </div>
		</div>
		<div class="row row-bottom-padded-md mb-3">
			<div class="col-md-12 text-center">	
				<button type="button" class="btn btn-danger" style="
				font-size: 13px;
				font-weight: 600;
			" data-toggle="modal" onclick="buku_tamu()">Isi Buku Tamu</button>
			</div>
		</div>
		<div class="row" id="list_buku_tamu" style="padding-bottom: 300px;">
		</div>
    	</div>
    </section>
	@endif
    
  	<footer class="ftco-footer ftco-section">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row mb-5">
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
	
            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved 
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  @include('template.buku_tamu')

  <script src="{{asset('website/WS3/js/jquery.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/popper.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/aos.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/jquery.mb.YTPlayer.min.js')}}"></script>
  <script src="{{asset('website/WS3/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('website/WS3/js/google-map.js')}}"></script>
  
  <script src="{{asset('website/WS3/js/main.js')}}"></script>

  <script src="{{asset('js/sound/fraudio.min.js')}}"></script>
  <script>
  function makeTimer(date) {
		var endTime = new Date(date);			
		endTime = (Date.parse(endTime) / 1000);

		var now = new Date();
		now = (Date.parse(now) / 1000);

		var timeLeft = endTime - now;

		var days = Math.floor(timeLeft / 86400); 
		var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
		var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
		var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

		if (hours < "10") { hours = "0" + hours; }
		if (minutes < "10") { minutes = "0" + minutes; }
		if (seconds < "10") { seconds = "0" + seconds; }

		$("#days").html(days + "<span>Days</span>");
		$("#hours").html(hours + "<span>Hours</span>");
		$("#minutes").html(minutes + "<span>Minutes</span>");
		$("#seconds").html(seconds + "<span>Seconds</span>");		
	}

	function set_audio() {
            var audio = document.getElementById("set_music");
            // audio.play();
            $('.fraudio-play').click();
        }
	
	var d = new Date("{{date('F d, Y', strtotime($data->settglres))}} 8:00:00 GMT+07:00");
	// var tomorrow = new Date();
	// tomorrow.setDate(tomorrow.getDate() + 7);
	
	setInterval(function() { makeTimer(d); }, 1000);
	// setInterval(function() { makeTimer(tomorrow); }, 1000);
	</script>
	@include('template.act_buku_tamu')
    </div>
  </body>
</html>