	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<link rel="icon" href="{{asset('gallery/website_img/')}}/{{$data->set_header}}">
		<!-- Author Meta -->
		<meta name="author" content="{{$data->setname}}">
		<!-- Meta Description -->
		<meta name="description" content="Spesial mengundang untuk menghadiri pernikahan kami. {{$data->setname}}" />
		<!-- Meta Keyword -->
		<meta name="keywords" content="Wedding Invitation {{$data->setname}}">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Wedding Invitation {{$data->setname}}</title>
		<style media="screen">
			#bg_color {
				background: url("{{asset('website/WS1/images/pat8.png')}}");
				background-repeat: repeat;
				margin-bottom: 0px;
				opacity: 1;
			}
			.zn-fit-center {
				object-fit: cover;
				width: 250px;
				height: 300px !important;
				border: 2px solid #e0e0e0;
				padding: 4px;
				border-radius: 4px;
			}
			.modal {
        text-align: center;
        padding: 0!important;
        }

        .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
        }

        .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        }
		</style>
		
		
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="{{asset('website/WS2/css/linearicons.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/font-awesome.min.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/availability-calendar.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/magnific-popup.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/nice-select.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/owl.carousel.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/bootstrap.css')}}">
			<link rel="stylesheet" href="{{asset('website/WS2/css/main.css')}}">
			<link href="{{asset('js/sound/fraudio.min.css')}}" rel="stylesheet">
		</head>
		@php
		if ($data->bg_color) {
			$bgcolor = 'background:'.$data->bg_color.'52;';
		}else{
			$bgcolor = '';
		}
	@endphp
		<body id="bg_color" class="scrollStyle">
			@if (isset($type))
			<div style="
			position: fixed;
			z-index: 10000;
			background: #000000de;
			width: 100%;
			text-align: center;
			padding: 20px;
			color: #f7b79a;
			font-weight: 500;
			font-size: 14px;
			opacity: 0.9;
			font-style: oblique;
		">
			"untuk mengaktifkan website, silahkan checkout pesanan ini dan lakukan pembayaran. Terimakasih" 
			<img src="{{asset('img/logo.png')}}" style="width: 100px;margin-left: 50px;" alt="">
		</div>
			@endif
			<div style="{{$bgcolor}}">
			<!-- Start Header Area -->
			<header>
				<div class="container">
					<div class="header-wrap">
						<div class="header-top d-flex justify-content-between align-items-center">
							<div class="col menu-left">
								<!-- <a href="#">+880 1234 654 953</a>
								<a href="#">support@colorlib.com</a> -->
							</div>
							<div class="col-3 logo">
								<a href="#" style="
								font-size: 19px;
								color: #222;
								font-weight: bold;
							">The Wedding</a>
							</div>
							 <nav class="col navbar navbar-expand-md justify-content-end">

							  <!-- Toggler/collapsibe Button -->
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
							    <span class="lnr lnr-menu"></span>
							  </button>

							  <!-- Navbar links -->
							  <div class="collapse navbar-collapse menu-right" id="collapsibleNavbar">
							    <ul class="navbar-nav">
							      <li class="nav-item">
							        <a class="nav-link" href="#set_header">Home</a>
							      </li>
							      <li class="nav-item">
							        <a class="nav-link" href="#about">About us</a>
								  </li>
								  <li class="nav-item">
							        <a class="nav-link" href="#event">Event</a>
							      </li>	
							      <li class="nav-item">
							        <a class="nav-link" href="#gallery">Gallery</a>
							      </li>						     					      
							    </ul>
							  </div>
							</nav> 
						</div>
					</div>
				</div>
			</header>
			<!-- End Header Area -->

			<audio class="fraudio" id="set_music" loop>
				<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/ogg">
				<source src="{{asset('gallery/music')}}/{{$data->music}}" type="audio/mpeg">
			</audio>

			<span style="
			display: inline-block;
			 position: fixed;
			 z-index: 1000;
			 color: white;
			 bottom: 88px;
			 right: 77px;
			 text-align: right;
			 font-size: 11px;
			 line-height: 13px;
			 ">
			  Music: <br> <a target="_blank" style="font-weight: 500;color: #ffffff;" href="https://www.bensound.com/">www.bensound.com</a></span>

			  
			<!-- start banner Area -->
			<section id="set_header" class="banner-area relative" style="background: url('{{asset('gallery/website_img/')}}/{{$data->set_header}}') no-repeat center center/cover">
				<div class="overlay overlay-bg"></div>
				<div class="container">
						<div class="row fullscreen align-items-center justify-content-center" style="height: 915px;">
							<div class="banner-content col-lg-12 col-md-12">
								<h1>
								 	<div id="setName">{{$data->setname}}</div> 
									are Getting Married
								</h1>
								<a href="http://maps.google.com/maps?q={{$data->maps_lat}},{{$data->maps_lng}}&z=18" target="_blank" class="submit-btn primary-btn mt-50 text-uppercase ">get location<span class="lnr lnr-arrow-right"></span></a>
								@if ($data->link_live)
									<a href="{{$data->link_live}}" target="_blank" class="submit-btn primary-btn mt-50 text-uppercase "><span class="lnr lnr-arrow-right"></span>Live Streaming</a>
								@endif
							</div>
						</div>
				</div>
			</section>
			<!-- End banner Area -->

			<!-- Start About Area -->
			<section id="about" class="About-area pt-100 pb-60">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 mt-5">
							<div class="row justify-content-center align-items-center">
								<div class="col-lg-6" >
									<h1>About the <br>
									Sweet <span id="setNameWanita">{{$data->setnamewanita}}</span></h1>
									<p id="setDescWanita">
										{{$data->setdescwanita}}
									</p>
								</div>
								<div class="col-lg-6">
									<img id="foto_wanita" class="img-fluid mx-auto zn-fit-center" src="{{asset('gallery/website_img/')}}/{{$data->foto_wanita}}" alt="">
								</div>										
							</div>
						</div>
						<div class="col-lg-6 mt-5">
							<div class="row justify-content-center align-items-center">
								<div class="col-lg-6">
									<h1>About the <br>
									Handsome <span id="setNamePria">{{$data->setnamepria}}</span></h1>
									<p id="setDescPria">
										{{$data->setdescpria}}
									</p>
									
								</div>
								<div class="col-lg-6">
									<img id="foto_pria" class="img-fluid mx-auto zn-fit-center" src="{{asset('gallery/website_img/')}}/{{$data->foto_pria}}" alt="">
								</div>										
							</div>
						</div>
					</div>
					
				</div>	
			</section>
			<!-- End About Area -->
			
			<!-- Start date Area -->
			<section class="date-area">
				<div class="container">
					<div class="row justify-content-between date-section flex-row">
							<h3 class="text-white" >
								<span id="setTglRes">{{date('d F Y', strtotime($data->settglres))}}</span>,
								<span id="setTempatRes">{{$data->settempatres}}</span>
								<span id="setAlamatRes" style="
								display: block;
								font-size: 13px;
								font-weight: 400;
							">{{$data->setalamatres}}</span>
							</h3>	
											
					</div>
				</div>	
			</section>
			<!-- End date Area -->
			
			<!-- Start gallery Area -->
			<section class="gallery-area pt-100" >
				<div class="container-fluid">
					<div class="row headz justify-content-center">
						<div class="col-lg-8">
							<!-- <h1>Our Pre Wedding Photo Gallery</h1> -->
							<p id="setKata">
								{{$data->setkata}}	
							</p>							
						</div>
					</div>
				</div>	
			</section>
			<!-- End gallery Area -->

			<!-- Start countdown Area -->
			<section class="countdown-area pt-100">
				<div class="container">
					<div class="row justify-content-center no-padding">
						<div class="col-lg-10">
							<div class="row clock_sec align-items-end clockdiv" id="clockdiv">
				                    <div class="col clockinner1 clockinner">
				                        <span class="days">10</span>
				                        <div class="smalltext">Days</div>
				                    </div>
				                    <div class="col clockinner clockinner1">
				                        <span class="hours">22</span>
				                        <div class="smalltext">Hours</div>
				                    </div>
				                    <div class="col clockinner clockinner1">
				                        <span class="minutes">23</span>
				                        <div class="smalltext">Minutes</div>
				                    </div>
				                    <div class="col clockinner clockinner1">
				                        <span class="seconds">12</span>
				                        <div class="smalltext">Seconds</div>
				                    </div>

				                </div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End countdown Area -->
			
			<!-- Start information Area -->

			<section id="event" class="section-gap info-area" style="background: #f5f5f5;">
				<div class="container">
					<div class="row headz justify-content-center">
						<div class="col-lg-6">
							<h1>Our Special Event</h1>
							<p>
								Wedding Events
							</p>							
						</div>
					</div>					
					<div class="single-info row">
						<div class="col-lg-6 col-md-12 text-center no-padding">
							<div class="info-thumb">
								<!-- <img src="img/info1.jpg" class="img-fluid" alt=""> -->
							</div>
						</div>
						<div class="col-lg-6 col-md-12 mt-60 no-padding">
							<div class="info-content">
								<h1>Akad Nikah </h1>
								<p>Acara akad nikah ini akan dilaksanakan pada</p>
								<div class="meta">
									<p>Tanggal: <span id="setTglAkad">{{$data->settglakad}}</span></p>
									<p>Mulai: <span id="setJam1Akad">{{$data->setjam1akad}}</span> </p>
									<p>Selesai: <span id="setJam2Akad">{{$data->setjam2akad}}</span> </p>
									<p>Alamat: <span id="setAlamatAkad">{{$data->setalamatakad}}</span> </p>
								</div>
							</div>
						</div>
					</div>
					<div class="single-info row mt-50">
						<div class="col-lg-6 col-md-12 mt-60 no-padding">
							<div class="info-content2">
								<h1>Resepsi</h1>
								<p>Acara Resepsi ini akan dilaksanakan pada</p>
								<div class="meta">
									<p>Tanggal: <span id="setTglRes2">{{$data->settglres}}</span></p>
									<p>Mulai: <span id="setJam1Res">{{$data->setjam1res}}</span> </p>
									<p>Selesai: <span id="setJam2Res">{{$data->setjam2res}}</span> </p>
									<p>Alamat: <span id="setAlamatRes2">{{$data->setalamatres}}</span> </p>
								</div>
							</div>
						</div>	
						<div class="col-lg-6 col-md-12 text-center no-padding">
							<div class="info-thumb">
								<!-- <img src="img/info2.jpg" class="img-fluid" alt=""> -->
							</div>
						</div>											
					</div>
				</div>
			</section>			
			<!-- End information Area -->
			
		
			
			<section id="gallery" class="pt-100 pb-60">
				<div class="container">
					<div class="row headz justify-content-center">
						<div class="col-lg-6">
							<h1>Our Memories</h1>
							<p>
								Gallery
							</p>							
						</div>
					</div>		
					<div class="row row-bottom-padded-md">
						<div class="col-md-12">
							<ul id="fh5co-gallery-list">
								
								<li id="gallery_1" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_1}}'); ">
									<a class="color-2">
										
									</a>
								</li>
							
			
			
								<li id="gallery_2" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_2}}'); ">
									<a class="color-3">
									
									</a>
								</li>
								<li id="gallery_3" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_3}}'); ">
									<a class="color-4">
										
									</a>
								</li>
								<li id="gallery_4" class="one-third animate-box fadeIn animated-fast" data-animate-effect="fadeIn" style="background-image: url('{{asset('gallery/website_img/')}}/{{$data->gallery_4}}'); ">
									<a class="color-4">
										
									</a>
								</li>
							
							
							

							
							</ul>		
						</div>
					</div>
				</div>
			</section>


			<section id="tamu" class="pt-100 pb-60">
				<div class="container">
					<div class="row headz justify-content-center">
						<div class="col-lg-6">
							<h1>Buku tamu</h1>
							<p>
								Kumpulan Ucapan dari Tamu
							</p>							
						</div>
					</div>

					<div class="row row-bottom-padded-md mb-3">
						<div class="col-md-12 text-center">	
							<button type="button" class="btn btn-danger" style="
							font-size: 13px;
							font-weight: 600;
						" data-toggle="modal" onclick="buku_tamu()">Isi Buku Tamu</button>
						</div>
					</div>
					<div class="row" id="list_buku_tamu" style="padding-bottom: 300px;">
					</div>
					
				</div>

			
	
			</section>
			@php
				$now = time(); // or your date as well
				$your_date = strtotime($data->settglres);
				$datediff = $your_date - $now;

				if ($datediff < 0) {
					$setDate = 0.0001;
				}else {
					$setDate = round($datediff / (60 * 60 * 24));
				}

				
			@endphp

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row justify-content-center">
										
					</div>
					<div class="row footer-bottom justify-content-center">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						<p class="col-lg-8 col-sm-12 footer-text">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</div>
				</div>
			</footer>
			
			@include('template.buku_tamu')
			<!-- End footer Area -->		

			<script src="{{asset('website/WS2/js/vendor/jquery-2.2.4.min.js')}}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="{{asset('website/WS2/js/vendor/bootstrap.min.js')}}"></script>
			<script src="{{asset('website/WS2/js/owl.carousel.min.js')}}"></script>
			<script src="{{asset('website/WS2/js/jquery.sticky.js')}}"></script>
			<script src="{{asset('website/WS2/js/parallax.min.js')}}"></script>
			<script src="{{asset('website/WS2/js/jquery.nice-select.min.js')}}"></script>			
			<script src="{{asset('website/WS2/js/countdown.js')}}"></script>
			<script src="{{asset('website/WS2/js/jquery.magnific-popup.min.js')}}"></script>
			<script src="{{asset('website/WS2/js/main.js')}}"></script>	

			<script src="{{asset('js/sound/fraudio.min.js')}}"></script>
			<script>

				console.log('{{$setDate}}');
				
			
				var deadline = new Date(Date.parse(new Date()) + '{{$setDate}}' * 24 * 60 * 60 * 1000);
				initializeClock('clockdiv', deadline);
			</script>
			<script>
			function set_audio() {
				var audio = document.getElementById("set_music");
				// audio.play();
				$('.fraudio-play').click();
			}

			</script>

			
			</div>
			@include('template.act_buku_tamu')
		</body>
	</html>