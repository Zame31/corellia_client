<nav class="panel-menu mobile-main-menu">
    <ul>
       
        <li id="bm1" class="dropdown">
            <a href="{{ route('home') }}?menu=1"><span>HOME </span></a>
        </li>
        <li id="bm2" class="dropdown">
            <a href="#"><span>UNDANGAN </span></a>
            <div class="dropdown-menu">
                <div class="container">
                    <div class="row pt-col-list">
                        <div class="col">
                            {{-- <h6 class="pt-title-submenu">MENU UNDANGAN</h6> --}}
                            <ul class="pt-megamenu-submenu">
                                <li><a href="{{ route('katalog.index',['type'=>'undangan','type_id'=>'1']) }}?menu=2">Katalog</a></li>
                                {{-- <li><a href="{{ route('undangan.custom_design') }}">Custom Design</a></li> --}}
                                <li>
                                    @if (Auth::user())
                                        <a href="{{ route('undangan.custom_design') }}?menu=2">Design by order</a>
                                    @else
                                        <a onclick="mLoginShow();">Design by order</a>  
                                    @endif
                                    
                                
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="dropdown pt-megamenu-col-01">
            <a href="#"><span>WEBSITE</span></a>
            <div class="dropdown-menu">
                <div class="container">
                    <div class="row pt-col-list">
                        <div class="col">
                            {{-- <h6 class="pt-title-submenu">MENU WEBSITE</h6> --}}
                            <ul class="pt-megamenu-submenu">
                                {{-- <li><a href="{{ route('katalog.index',['type'=>'website','type_id'=>'5']) }}">Template Wedding</a></li> --}}
                                <li><a href="{{ route('website.index',['type'=>'website','type_id'=>'5']) }}">Create Wedding Website</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li id="bm3" class="dropdown pt-megamenu-col-01">
            <a href="#"><span>KEBUTUHAN LAINNYA</span></a>
            <div class="dropdown-menu">
                    <div class="container">
                        <div class="row pt-col-list">
                            <div class="col">
                                {{-- <h6 class="pt-title-submenu">KEBUTUHAN LAINNYA</h6> --}}
                                <ul class="pt-megamenu-submenu">
                                    <li><a href="{{ route('katalog.index',['type'=>'souvenir','type_id'=>'2']) }}?menu=3">Souvenir</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'seserahan','type_id'=>'4']) }}?menu=3">Seserahan</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'mahar','type_id'=>'3']) }}?menu=3">Mahar</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'lainnya','type_id'=>'6']) }}?menu=3">Lainnya</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </li>
        @if (Auth::user())
            <li class="dropdown pt-megamenu-col-01">
                <a href="#"><span>MY PROFILE</span></a>
                <div class="dropdown-menu">
                    <div class="container">
                        <div class="row pt-col-list">
                            <div class="col">
                                <h6 class="pt-title-submenu"> 
                                </h6>
                                <ul class="pt-megamenu-submenu">
                                    @php
                                        $c_wish = collect(\DB::select("SELECT count(id) as j from mst_wishlist where cust_id = ".Auth::user()->id))->first();
                                        $c_notif = collect(\DB::select("SELECT count(id) as j from mst_notif where is_read IS NULL and cust_id = ".Auth::user()->id))->first();
                                    
                                    @endphp
                                    <li><a href="{{ route('customer.account') }}"> {{Auth::user()->cust_name}}</a></li>
                                    <li><a href="{{ route('customer.list_custom_design') }}">Custom Design</a></li>
                                    <li><a href="{{ route('customer.pembelian_list') }}">Pembelian</a></li>
                                    <li><a href="{{ route('customer.wishlist') }}">Wishlist<span id="set_count_wishlist" class="pt-badge" style="
                                        background: #C07B5B;
                                        border-radius: 2px;
                                        font-weight: bold;
                                    "> {{$c_wish->j}} </span>
                                    </a></li>
                                    <li><a class="pt-dropdown-toggle" onclick="get_list_notif()">Notifikasi<span id="set_count_notif_mobile" class="pt-badge" style="
                                        background: #C07B5B;
                                        border-radius: 2px;
                                        font-weight: bold;
                                    "> {{$c_notif->j}} </span></a>
                                    <ul>
                                        <li>
                                            <div id="get_list_notif_mobile" class="pt-dropdown-inner zn-con-notif scrollStyle" style="background: #fbfbfb;">
            

                                            </div>    
                                        </li>
                                    </ul>
                                        
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <form action="{{ route('logout') }}" method="post">
                    {{ csrf_field() }}
                    <input type="submit" value="SIGN OUT" class="btn btn-label btn-label-brand btn-sm btn-bold" style="
                   font-size: 14px;
                    padding: 0px 30px;
                    margin-left: 17px;
                    margin-top: 20px;
                ">
                </form>
            </li>
        @else
        <li class="dropdown">
            <a onclick="mLoginShow();" href="#"><span>SIGN IN </span></a>
            <a onclick="mRegisterShow();" href="#"><span>REGISTER</span></a>
        </li>
        @endif
        
    </ul>
    <div class="mm-navbtn-names">
        <div class="mm-closebtn">Close</div>
        <div class="mm-backbtn">Back</div>
    </div>
</nav>
<div class="pt-mobile-header">
    <div class="container-fluid">
        <div class="pt-header-row">
            <!-- mobile menu toggle -->
            <div class="pt-mobile-parent-menu">
                <div class="pt-menu-toggle">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                        <use xlink:href="#icon-mobile-menu-toggle"></use>
                    </svg>
                </div>
            </div>
            <!-- /mobile menu toggle -->
            <div class="pt-logo-container">
                <!-- mobile logo -->
                <div class="pt-logo pt-logo-alignment" itemscope itemtype="http://schema.org/Organization">
                    <a href="{{ route('home') }}?menu=1" src="{{ asset('img/logo.png') }}" itemprop="url">
                            <img src="{{ asset('img/logo.png') }}" alt="" srcset="">
                        {{-- <h2 class="pt-title">Corelliazzz</h2> --}}
                    </a>
                </div>
                <!-- /mobile logo -->
            </div>
            <!-- search -->
            {{-- <div class="pt-mobile-parent-search pt-parent-box"></div> --}}
            <!-- /search -->
            <!-- cart -->

            

            <div class="pt-mobile-parent-cart pt-parent-box"></div>
            <!-- /cart -->
        </div>
    </div>
</div>