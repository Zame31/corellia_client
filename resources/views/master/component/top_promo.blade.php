@php
      $data_home_head = collect(\DB::select("SELECT mp.judul as promo1,ms1.judul as promo2,ms2.judul as promo3, mh.* 
        FROM mst_home mh
        join mst_promo mp on mp.id::INTEGER = mh.promo_head::INTEGER
        join mst_promo ms1 on ms1.id::INTEGER = mh.promo_subhead1::INTEGER
        join mst_promo ms2 on ms2.id::INTEGER = mh.promo_subhead2::INTEGER
        "))->first();
@endphp
@if ($data_home_head)
<div class="pt-top-panel">
    <div class="container">
        <div class="pt-row">
            <div class="pt-description">
                <strong>{{$data_home_head->promo1}}</strong>
                
            </div>
            <button class="pt-btn-close js-removeitem">
                <svg fill="none">
                    <use xlink:href="#icon-close"></use>
                </svg>
            </button>
        </div>
    </div>
</div>
<div class="headerunderline">
    <div class="container-fluid">
        <div class="pt-header-row pt-top-row no-gutters">
            <div class="pt-col-left col-3">
                <div class="pt-box-info">
                    <ul>
                        <li>Call Us: <strong class="pt-base-dark-color">081-1248-6660</strong></li>
                    </ul>
                </div>
            </div>
            <div class="pt-col-center col-6">
                <div class="pt-box-info">
                    <ul class="js-header-slider pt-slider-smoothhiding slick-animated-show">
                        <li>
                            {{$data_home_head->promo2}} <a href="{{ route('home.promo') }}"  class="pt-link-underline">Selengkapnya</a>
                        </li>
                        <li>
                            {{$data_home_head->promo3}} <a href="{{ route('home.promo') }}"  class="pt-link-underline">Selengkapnya</a>
                        </li>
                    </ul>
                </div>
            </div>
            {{-- <div class="pt-col-right col-3 ml-auto">
					<div class="pt-desctop-parent-submenu pt-parent-box">
						<ul class="submenu">
                        	<li><a target="_blank" href="https://www.instagram.com">
								<span class="icon">
									<svg width="18" height="19" viewBox="0 0 18 19">
										<use xlink:href="#icon-social_icon_instagram"></use>
									</svg>
								</span>
								<span class="text">Instagram</span>
							</a></li>
						</ul>
					</div>
				</div> --}}
        </div>
    </div>
</div>

@endif
