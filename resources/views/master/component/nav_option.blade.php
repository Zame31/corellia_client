	<!-- pt-search -->
    <div style="display:none;" class="pt-desctop-parent-search pt-parent-box">
        <div class="pt-search pt-dropdown-obj js-dropdown">
            <button class="pt-dropdown-toggle" data-tooltip="Search" data-tposition="bottom">
                <svg width="24" height="24" viewBox="0 0 24 24">
                    <use xlink:href="#icon-search"></use>
                </svg>
            </button>
            <div class="pt-dropdown-menu">
                <div class="container">
                    <form>
                        <div class="pt-col">
                            <input type="text" class="pt-search-input" placeholder="Search products...">
                            <button class="pt-btn-search" type="submit">
                                <svg width="24" height="24" viewBox="0 0 24 24">
                                    <use xlink:href="#icon-search"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="pt-col">
                            <button class="pt-btn-close">
                                <svg width="16" height="16" viewBox="0 0 16 16">
                                    <use xlink:href="#icon-close"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="pt-info-text">
                            What are you Looking for?
                        </div>
                        <div class="search-results">
                            <ul>
                                <li>
                                    <a href="product.html">
                                        <div class="thumbnail">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAOCAQMAAACvc5NpAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAHBJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADg9uCABAAAAEDQ/9f9CBUAAAAAAAAAAAAAAAAAAAAAAGAiOEEAAVstZ/kAAAAASUVORK5CYII=" class="lazyload" data-src="images/product/product-01.jpg" alt="image">
                                        </div>
                                        <div class="pt-description">
                                            <div class="pt-title">Midi button-up denim skirt</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                   <a href="product.html">
                                        <div class="thumbnail">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAOCAQMAAACvc5NpAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAHBJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADg9uCABAAAAEDQ/9f9CBUAAAAAAAAAAAAAAAAAAAAAAGAiOEEAAVstZ/kAAAAASUVORK5CYII=" class="lazyload" data-src="images/product/product-02.jpg" alt="image">
                                        </div>
                                        <div class="pt-description">
                                            <div class="pt-title">Printed dress</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="product.html">
                                        <div class="thumbnail">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAOCAQMAAACvc5NpAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAHBJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADg9uCABAAAAEDQ/9f9CBUAAAAAAAAAAAAAAAAAAAAAAGAiOEEAAVstZ/kAAAAASUVORK5CYII=" class="lazyload" data-src="images/product/product-03.jpg" alt="image">
                                        </div>
                                        <div class="pt-description">
                                            <div class="pt-title">Corduroy backpack</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <button type="button" class="pt-view-all btn-link btn-lg"><span class="pt-text">View All Products</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /pt-search -->
    <!-- pt-account -->
<div class="pt-desctop-parent-account pt-parent-box">
    <div class="pt-account pt-dropdown-obj js-dropdown">
        <button class="pt-dropdown-toggle" data-tooltip="My Profile" data-tposition="bottom">
            <svg width="24" height="24" viewBox="0 0 24 24">
                <use xlink:href="#icon-user"></use>
            </svg>
        </button>
        <div class="pt-dropdown-menu">
            <div class="pt-mobile-add">
                <button class="pt-close">
                    <svg>
                        <use xlink:href="#icon-close"></use>
                    </svg>Close
                </button>
            </div>
            <div class="pt-dropdown-inner">
               
                @if (Auth::user())
                <ul>
                    <li class="mb-3">
                        <span class="pt-text"> <b style="text-transform: uppercase;">{{Auth::user()->cust_name}}</b></span>
                    </li>
                    <li>
                            <a href="{{ route('customer.account') }}">
                                <i class="pt-icon pt-align-icon">
                                    <svg width="24" height="24">
                                        <use xlink:href="#icon-user"></use>
                                    </svg>
                                </i>
                                <span class="pt-text">Profile</span>
                            </a>
                        </li>
                    <li>
                        <a href="{{ route('customer.list_custom_design') }}">
                            <i class="pt-icon pt-align-icon">
                                <svg width="24" height="24">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                            </i>
                            <span class="pt-text">Custom Design</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('customer.pembelian_list') }}">
                            <i class="pt-icon pt-align-icon">
                                <svg width="24" height="24">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                            </i>
                            <span class="pt-text">Pembelian</span>
                        </a>
                    </li>

                    <li style="
                    border-top: 1px solid #efefef;
                ">
                    <form action="{{ route('logout') }}" method="post">
                            {{ csrf_field() }}
                            <input type="submit" value="Sign Out" class="btn btn-label btn-label-brand btn-sm btn-bold" style="
                            font-size: 11px;
                            padding: 0px 10px;
                            height: 25px;
                            margin-top: 10px;
                            border-radius: 4px;
                        ">
                        </form>
                        {{-- <a href="#">
                            <i class="pt-icon pt-align-icon">
                                <svg width="24" height="24">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                            </i>
                            <span class="pt-text">Logout</span>
                        </a> --}}
                    </li>

                </ul>
                @else
                <ul>
                    <li><a onclick="mLoginShow();" href="#">
                            <i class="pt-icon">
                                <svg width="18" height="23">
                                    <use xlink:href="#icon-lock"></use>
                                </svg>
                            </i>
                            <span class="pt-text">Sign In</span>
                        </a></li>
                    <li><a onclick="mRegisterShow();" href="#">
                            <i class="pt-icon pt-align-icon">
                                <svg width="24" height="24">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                            </i>
                            <span class="pt-text">Register</span>
                        </a></li>
                </ul>
                @endif


            </div>
        </div>
    </div>
</div>
<!-- /pt-account -->

   
<!-- pt-notif -->
<div class="pt-desctop-parent-compare pt-parent-box">
    @if (Auth::user())
            <div onclick="get_list_notif()" class="pt-compare pt-dropdown-obj js-dropdown">   
                <a class="pt-dropdown-toggle la la-bell zn-icon-notif"  data-tooltip="Notifikasi" data-tposition="bottom">
                    @php
                        $c_notif = collect(\DB::select("SELECT count(id) as j from mst_notif where is_read IS NULL and cust_id = ".Auth::user()->id))->first();
                    @endphp
                    <span id="set_count_notif" class="pt-badge"> {{$c_notif->j}} </span>
                </a>
                
              
                <div class="pt-dropdown-menu" style="width: 400px;">
                    
                    <div class="pt-mobile-add">
                        <button class="pt-close">
                            <svg>
                                <use xlink:href="#icon-close"></use>
                            </svg>Close
                        </button>
                    </div>
                    <div id="get_list_notif" class="pt-dropdown-inner zn-con-notif scrollStyle">
                

                    </div>
                </div>
            </div>
       
    
    @else
     
    <div onclick="mLoginShow()" class="pt-account pt-dropdown-obj js-dropdown">
        <a class="pt-dropdown-toggle la la-bell zn-icon-notif"  data-tooltip="Notifikasi" data-tposition="bottom">
        </a>
        <div class="pt-dropdown-menu" style="width: 400px;">
            <div class="pt-mobile-add">
                <button class="pt-close">
                    <svg>
                        <use xlink:href="#icon-close"></use>
                    </svg>Close
                </button>
            </div>
            <div id="get_list_notif" class="pt-dropdown-inner zn-con-notif scrollStyle">
        

            </div>
        </div>
    </div>
       
    @endif
   
</div>

<!-- /pt-notif -->
   
<!-- pt-wishlist -->
<div class="pt-desctop-parent-wishlist pt-parent-box">
    <div class="pt-wishlist pt-dropdown-obj">
            @if (Auth::user())
                <a href="{{ route('customer.wishlist') }}" class="pt-dropdown-toggle" data-tooltip="Wishlist" data-tposition="bottom">
                @php
                    $c_wish = collect(\DB::select("SELECT count(id) as j from mst_wishlist where cust_id = ".Auth::user()->id))->first();
                @endphp
                    <span id="set_count_wishlist" class="pt-badge"> {{$c_wish->j}} </span>
            @else
                <a onclick="mLoginShow();" class="pt-dropdown-toggle" data-tooltip="Wishlist" data-tposition="bottom">       
            @endif
          <span class="pt-icon">
                <svg width="24" height="24" viewBox="0 0 24 24">
                    <use xlink:href="#icon-wishlist"></use>
                </svg>
            </span>
            <span class="pt-text">Wishlist</span>
        </a>
    </div>
</div>
<!-- /pt-wishlist -->
  <!-- pt-cart -->
<div class="pt-desctop-parent-cart pt-parent-box">
    <div class="pt-cart pt-dropdown-obj">
        @if (Auth::user())
            <a href="{{ route('customer.cartlist') }}" class="pt-dropdown-toggle" data-tooltip="cart" data-tposition="bottom">
            @php
                $c_cart = collect(\DB::select("SELECT count(id) as j from mst_cartlist where cust_id = ".Auth::user()->id))->first();
          @endphp
            <span id="set_count_cartlist" class="pt-badge"> {{$c_cart->j}} </span>
        
        @else
            <a onclick="mLoginShow();" class="pt-dropdown-toggle" data-tooltip="cart" data-tposition="bottom">       
        @endif
            <span class="pt-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24">
                            <use xlink:href="#icon-cart_1"></use>
                        </svg>
            </span>
            <span class="pt-text"></span>
           
        </a>
    </div>
</div>
<!-- /pt-cart -->