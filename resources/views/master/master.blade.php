<!DOCTYPE html>
<html lang="en">
@include('master.head')

<body>
    
    <div id="loader-wrapper" class="zn-loader">
        <div class="loader">Loading...</div>
    </div>

    <div id="zn-new-loading" class="zn-loader-dark">
            <div class="loader">Loading...</div>
        </div>
    <header id="pt-header">

       
        @include('master.component.top_promo')
        @include('master.menu_mobile')

        <!-- pt-desktop-header -->
        <div class="pt-desktop-header">
            <div class="container-fluid">
                <div class="headinfo-box form-inline">
                    <!-- logo -->
                    <div class="pt-logo pt-logo-alignment" >
                        <a href="{{ route('home') }}" itemprop="url">
                            <img src="{{ asset('img/logo.png') }}" alt="" srcset="">
                            {{-- <h2 class="pt-title">Corellia</h2> --}}
                        </a>
                    </div>
                    <!-- /logo -->
                    <div class="navinfo text-left">
                        <!-- pt-menu -->
                        <div class="pt-desctop-parent-menu">
                            <div class="pt-desctop-menu">
                                @include('master.menu')
                            </div>
                        </div>
                        <!-- /pt-menu -->
                    </div>
                    <div class="options">
                        @include('master.component.nav_option')
                    </div>
                </div>
            </div>
        </div>
        <!-- stuck nav -->
        <div class="pt-stuck-nav">
            <div class="container-fluid">
                <div class="pt-header-row">
                    
                    <div class="pt-stuck-parent-menu"></div>
                    <div class="pt-logo-container">
                        <!-- mobile logo -->
                        <div class="pt-logo pt-logo-alignment">
                            <a href="{{ route('home') }}" itemprop="url">
                                <img src="{{ asset('img/logo.png') }}" alt="" srcset="">
                            </a>
                        </div>
                        <!-- /mobile logo -->
                    </div>
                    {{-- <div class="pt-stuck-parent-search pt-parent-box"></div> --}}
				<div class="pt-stuck-parent-account pt-parent-box"></div>
				<div class="pt-stuck-parent-compare pt-parent-box"></div>
				<div class="pt-stuck-parent-wishlist pt-parent-box"></div>
				<div class="pt-stuck-parent-cart pt-parent-box"></div>
                    
                    
                </div>
            </div>
        </div>
    </header>
    <main id="pt-pageContent" class="znPageLoad">
            @yield('content') 
    </main>
   
    <a href="#" id="js-back-to-top" class="pt-back-to-top">
        <span class="pt-icon">
            <svg version="1.1" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"
                xml:space="preserve">
                <g>
                    <polygon fill="currentColor" points="20.9,17.1 12.5,8.6 4.1,17.1 2.9,15.9 12.5,6.4 22.1,15.9 	">
                    </polygon>
                </g>
            </svg>
        </span>
        <span class="pt-text">BACK TO TOP</span>
    </a>


   
    @include('master.component.modal')
    @include('master.script')
</body>
</html>
