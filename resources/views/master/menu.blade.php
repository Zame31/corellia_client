<nav>
    <ul>
        <li id="m1" class="dropdown">
            <a href="{{ route('home') }}?menu=1"><span>HOME </span></a>
        </li>
        <li id="m2" class="dropdown">
            <a href="#"><span>UNDANGAN </span></a>
            <div class="dropdown-menu">
                <div class="container">
                    <div class="row pt-col-list">
                        <div class="col">
                            <h6 class="pt-title-submenu">MENU UNDANGAN</h6>
                            <ul class="pt-megamenu-submenu">
                                <li><a href="{{ route('katalog.index',['type'=>'undangan','type_id'=>'1']) }}?menu=2">Katalog</a></li>
                                {{-- <li><a href="{{ route('undangan.custom_design') }}">Custom Design</a></li> --}}
                                <li>
                                    @if (Auth::user())
                                        <a href="{{ route('undangan.custom_design') }}?menu=2">Design by order</a>
                                    @else
                                        <a onclick="mLoginShow();">Design by order</a>  
                                    @endif
                                    
                                
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li id="m4" class="dropdown pt-megamenu-col-01">
            <a href="#"><span>WEBSITE</span></a>
            <div class="dropdown-menu">
                <div class="container">
                    <div class="row pt-col-list">
                        <div class="col">
                            <h6 class="pt-title-submenu">MENU WEBSITE</h6>
                            <ul class="pt-megamenu-submenu">
                                <li>
                                @if (Auth::user())
                                <a href="{{ route('website.index',['type'=>'website','type_id'=>'5']) }}?menu=4">Create Wedding Website</a>
                                @else
                                    <a onclick="mLoginShow();">Create Wedding Website</a>  
                                @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li id="m3" class="dropdown pt-megamenu-col-01">
            <a href="#"><span>KEBUTUHAN LAINNYA</span></a>
            <div class="dropdown-menu">
                    <div class="container">
                        <div class="row pt-col-list">
                            <div class="col">
                                <h6 class="pt-title-submenu">KEBUTUHAN LAINNYA</h6>
                                <ul class="pt-megamenu-submenu">
                                    <li><a href="{{ route('katalog.index',['type'=>'souvenir','type_id'=>'2']) }}?menu=3">Souvenir</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'seserahan','type_id'=>'4']) }}?menu=3">Seserahan</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'mahar','type_id'=>'3']) }}?menu=3">Mahar</a></li>
                                    <li><a href="{{ route('katalog.index',['type'=>'lainnya','type_id'=>'6']) }}?menu=3">Lainnya</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </li>
    </ul>
</nav>

@php
    if (isset($_GET['menu'])) {
        $getmenu = $_GET['menu'];
    } else {
        $getmenu = '99';
    }
    
@endphp
<script>
    let getmenu = "{{$getmenu}}";
  
    $('#m1').removeClass('selected');
    $('#m2').removeClass('selected');
    $('#m3').removeClass('selected');
    $('#m4').removeClass('selected');
    
    if (getmenu == 1) {
        $('#m1').addClass('selected');
    }else if(getmenu == '2'){
        $('#m2').addClass('selected');
    }else if(getmenu == '3'){
        $('#m3').addClass('selected');
    }else if(getmenu == '4'){
        $('#m4').addClass('selected');
    }

</script>
