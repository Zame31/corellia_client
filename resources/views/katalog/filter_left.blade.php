
                <div class="pt-collapse open">
                        <h3 class="pt-collapse-title">
                            Color
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                        </h3>
                        <div class="pt-collapse-content">
                            <ul class="pt-options-swatch">
                                @php
                                     $design_color = \DB::select("SELECT *
                                    from ref_color order by id");
                                @endphp

                                @foreach ($design_color as $dc)
                                <li>
                                    <a onclick="set_filter_u_color({{$dc->id}}); get_list();" 
                                        class="options-color pt-color-bg-04 pt-border" style="background: {{$dc->code}};" href="#"></a>
                                </li>

                            @endforeach
                                {{-- <li><a class="options-color pt-color-bg-white pt-border" href="#" onclick="set_filter_u_color(1); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-04" style="background:#6f2e2c;" href="#" onclick="set_filter_u_color(2); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-05" href="#" onclick="set_filter_u_color(3); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-06" href="#" onclick="set_filter_u_color(4); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-07" href="#" onclick="set_filter_u_color(5); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-08" href="#" onclick="set_filter_u_color(6); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-09" href="#" onclick="set_filter_u_color(7); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-10" href="#" onclick="set_filter_u_color(8); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-11" href="#" onclick="set_filter_u_color(9); get_list();"></a></li>
                                <li><a class="options-color pt-color-bg-03" href="#" onclick="set_filter_u_color(10); get_list();"></a></li> --}}
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="pt-collapse open">
                        <h3 class="pt-collapse-title">
                            Foil Color
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                        </h3>
                        <div class="pt-collapse-content">
                            <ul class="pt-options-swatch size-middle">
                                <li><a href="#" onclick="set_filter_u_foilcolor(1); get_list();">Blue</a></li>
                                <li ><a href="#" onclick="set_filter_u_foilcolor(2); get_list();">Gold</a></li>
                                <li ><a href="#" onclick="set_filter_u_foilcolor(3); get_list();">White</a></li>
                                <li ><a href="#" onclick="set_filter_u_foilcolor(0); get_list();">Clear</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="pt-collapse open">
                        <h3 class="pt-collapse-title">
                            Shape
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                        </h3>
                        <div class="pt-collapse-content">
                            <ul class="pt-options-swatch size-middle">
                                <li><a href="#" onclick="set_filter_u_shape(1); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape1.png') }}" alt="" srcset=""></a>
                                </li>
                                {{-- <li ><a href="#" onclick="set_filter_u_shape(2); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape2.png') }}" alt="" srcset=""></a>
                                </li> --}}
                                <li ><a href="#" onclick="set_filter_u_shape(3); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape3.png') }}" alt="" srcset=""></a>
                                </li>
                                <li ><a href="#" onclick="set_filter_u_shape(4); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape4.png') }}" alt="" srcset=""></a>
                                </li>
                                {{-- <li ><a href="#" onclick="set_filter_u_shape(5); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape5.png') }}" alt="" srcset=""></a>
                                </li> --}}
                                {{-- <li ><a href="#" onclick="set_filter_u_shape(6); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape6.png') }}" alt="" srcset=""></a>
                                </li> --}}
                                <li ><a href="#" onclick="set_filter_u_shape(7); get_list();" style="padding: 0;height:100%;">
                                    <img width="50px" src="{{ asset('gallery/shape/shape7.png') }}" alt="" srcset=""></a>
                                </li>
                                <li ><a href="#" onclick="set_filter_u_shape(0); get_list();" style="padding: 0;height: 75px;width: 51px;background: #f9f9f9;">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
    
                    <div class="pt-collapse open">
                        <h3 class="pt-collapse-title">
                            Paper
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                        </h3>
                        <div class="pt-collapse-content">
                            <ul class="pt-options-swatch size-middle">
                                @php
                                     $d = DB::table('ref_paper')->get();
                                @endphp
                                @foreach ($d as $item)
                                <li>
                                    <a href="#" onclick="set_filter_u_paper({{$item->id}}); get_list();"><b> {{$item->paper_name}} </b> 
                                       
                                    </a>
                                </li>
                                @endforeach
                              
                            </ul>
                        </div>
                    </div>
                   
                    {{-- <input type="text" class="form-control" id="inputNormal" placeholder="Input your text"> --}}
                    <div class="checkbox-group">
                        <input type="checkbox" value="1" onchange="set_filter_u_waxseal(this.value); get_list();" id="waxseal" name="waxseal">
                        <label for="waxseal">
                            <span class="check"></span>
                            <span class="box"></span>
                            <h3 class="pt-collapse-title">
                                Waxseal
                            </h3>
                        </label>
                    </div>