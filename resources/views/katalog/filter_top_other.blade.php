<div class="pt-filters-options">
        <div class="pt-btn-toggle">
            <a href="#">
                <span class="pt-icon">
                    <svg>
                        <use xlink:href="#icon-filter"></use>
                    </svg>
                </span>
                <span class="pt-text">
                    Filter
                </span>
            </a>
        </div>
     
   
        <div class="pt-quantity">

            <a href="#" class="pt-col-two" data-value="pt-col-two">
                <span class="icon-listing-two">
                    <span></span>
                    <span></span>
                </span>
            </a>
            <a href="#" class="pt-col-three" data-value="pt-col-three">
                <span class="icon-listing-three">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            <a href="#" class="pt-col-four" data-value="pt-col-four">
                <span class="icon-listing-four">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
          
        </div>
        <a href="#" class="pt-grid-switch">
            <span></span><span></span>
        </a>
    </div>