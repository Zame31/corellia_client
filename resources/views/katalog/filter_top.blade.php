<div class="pt-filters-options desctop-no-sidebar">
    <div class="pt-sort pt-not-detach">

        <div class="row">
            <div class="col-md-2">
                <select id="utype" class="zn-dropdown">
                    <option selected value="0">Semua Style</option>
                    @php
                        $getJenis = \DB::select("SELECT * FROM ref_style");
                    @endphp
                    @foreach ($getJenis as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <select id="uformat" class="zn-dropdown">
                    <option value="0">Semua Format</option>
                    @php
                        $getJenis = \DB::select("SELECT * FROM ref_format");
                    @endphp
                    @foreach ($getJenis as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                    {{-- <option value="1">Four Panel</option>
                    <option value="2">All in one</option>
                    <option value="3">Petite</option>
                    <option value="4">Classic</option> --}}
                </select>
            </div>
            <div class="col-md-3">
                    <select id="uori" class="zn-dropdown">
                        <option value="0">Semua Orientatation</option>
                        <option value="1">Potrait</option>
                        <option value="2">Landscape</option>
                        <option value="3">Square</option>
                    </select>
                </div>
            <div class="col-md-3">
                <select id="uharga" class="zn-dropdown">
                    <option value="0">Semua Harga</option>
                    <option value="1">Rp 5.000 - Rp 10.000</option>
                    <option value="2">Rp 10.500 - Rp 20.000</option>
                    <option value="3">Rp 21.000 - Rp 50.000</option>

                </select>
            </div>
        </div>





    </div>
    <div class="pt-quantity">
        <a href="#" class="pt-col-one" data-value="pt-col-one">
            <span class="icon-listing-one">
                <span></span>
                <span></span>
            </span>
        </a>
        <a href="#" class="pt-col-two" data-value="pt-col-two">
            <span class="icon-listing-two">
                <span></span>
                <span></span>
            </span>
        </a>
        <a href="#" class="pt-col-three" data-value="pt-col-three">
            <span class="icon-listing-three">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>
        <a href="#" class="pt-col-four" data-value="pt-col-four">
            <span class="icon-listing-four">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>
        <a href="#" class="pt-col-six" data-value="pt-col-six">
            <span class="icon-listing-six">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>
    </div>
    <a href="#" class="pt-grid-switch">
        <span></span><span></span>
    </a>
</div>
