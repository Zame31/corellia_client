<script>

// set_active_product('{{$type}}');

    
function set_active_product(menu) {
    $("[id^='p_']").removeClass('active');
    $('#p_'+menu).addClass('active');
}
function set_active_price(menu) {
    $("[id^='pr_']").removeClass('active');
    $('#pr_'+menu).addClass('active');
}


var tmp_type = '{{$type}}';
var tmp_type_id = '{{$type_id}}';
var tmp_price = '';
var tmp_price_a = '';
var tmp_price_b = '';


var tmp_u_type = '';
var tmp_u_format = '';
var tmp_u_ori = '';
var tmp_u_harga = '';
var tmp_u_color = '';
var tmp_u_foilcolor = '';
var tmp_u_shape = '';
var tmp_u_paper = '';
var tmp_u_waxseal = '';

function set_filter_type(type,type_id) {
    tmp_type = type;
    tmp_type_id = type_id;
}

function set_filter_price(price_a,price_b) {
    tmp_price_a = price_a;
    tmp_price_b = price_b;
}

function set_filter_u_type(data) {
    console.log(data);
    
    tmp_u_type = data;
}

function set_filter_u_format(data) {
    tmp_u_format = data;
}

function set_filter_u_ori(data) {
    tmp_u_ori = data;
}
function set_filter_u_harga(data) {
    tmp_u_harga = data;
}

function set_filter_u_color(data) {
    tmp_u_color = data;
}
function set_filter_u_foilcolor(data) {
    tmp_u_foilcolor = data;
}

function set_filter_u_shape(data) {
    tmp_u_shape = data;
}

function set_filter_u_paper(data) {
    tmp_u_paper = data;   
}

function set_filter_u_waxseal(data) {
    if($('#waxseal').is(":checked")){
        tmp_u_waxseal = data;   
    }else{
        tmp_u_waxseal = 0;   
    }
    
}

get_list();

function get_list() {

    console.log('waw');

    set_active_product(tmp_type);
    set_active_price(tmp_price_a);


    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_list') }}',
        data: {
            type:tmp_type,
            type_id:tmp_type_id,
            price_a:tmp_price_a,
            price_b:tmp_price_b,
            u_type:tmp_u_type,
            u_format:tmp_u_format,
            u_ori:tmp_u_ori,
            u_harga:tmp_u_harga,
            u_color:tmp_u_color,
            u_foilcolor:tmp_u_foilcolor,
            u_shape:tmp_u_shape,
            u_paper:tmp_u_paper,
            u_waxseal:tmp_u_waxseal





        },
        beforeSend: function () {
            $('#list_katalog').html('<div class="loader">Loading...</div>');
            
        },
        success: function (response) {

            console.log(response.rm);

            $('#list_katalog').html(response.rm);

            // endLoadingPage();

        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });
        

} 


</script>