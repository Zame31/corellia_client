<div class="pt-collapse open">
    <h3 class="pt-collapse-title">
        TIPE PRODUK
        <span class="pt-icon">
            <svg>
                <use xlink:href="#icon-arrow_small_bottom"></use>
            </svg>
        </span>
    </h3>
    <div class="pt-collapse-content" style="">
        <ul class="pt-list-row">
            <li id="p_semua" class="active"><a onclick="set_filter_type('semua','99'); get_list();">Semua</a></li>
            <li id="p_mahar" ><a onclick="set_filter_type('mahar','3'); get_list();">Mahar</a></li>
            <li id="p_souvenir" ><a onclick="set_filter_type('souvenir','2'); get_list();">Souvenir</a></li>
            <li id="p_seserahan"><a onclick="set_filter_type('seserahan','4'); get_list();">Seserahan</a></li>
            <li id="p_seserahan"><a onclick="set_filter_type('lainnya','6'); get_list();">Lainnya</a></li>

        </ul>
    </div>
</div>

<div class="pt-collapse open">
        <h3 class="pt-collapse-title">
            RANGE HARGA
            <span class="pt-icon">
                <svg>
                    <use xlink:href="#icon-arrow_small_bottom"></use>
                </svg>
            </span>
        </h3>
        <div class="pt-collapse-content" style="">
            <ul class="pt-list-row">
                <li id="pr_0" class="active"><a onclick="set_filter_price(0,0); get_list();">Semua Harga</a></li>
                <li id="pr_10000"><a onclick="set_filter_price(100,10000); get_list();">Rp 100 - Rp 10.000</a></li>
                <li id="pr_100001"><a onclick="set_filter_price(10001,50000); get_list();">Rp 10.001 - Rp 50.000</a></li>
                <li id="pr_200001"><a onclick="set_filter_price(50001,100000); get_list();">Rp 50.001 - Rp 100.000</a></li>
                <li id="pr_300000"><a onclick="set_filter_price(100000,9999999); get_list();">Lebih Dari Rp 100.000</a></li>

            </ul>
        </div>
    </div>

