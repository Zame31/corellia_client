@section('content')

<div class="container container-fluid-mobile" style="padding-top:50px;padding-bottom: 150px;font-size: 14px;">
    <div class="row">
        <div class="col-12" style="height: 70px;">
            {{-- <a onclick="setCartlistOther('WS1',5)" class="btn btn-lg" style="color:#ffffff;">
                <div class="pt-icon">
                    <svg>
                        <use xlink:href="#icon-cart_1"></use>
                    </svg>
                </div>
                <span class="pt-text">
                    ADD TO CART
                </span>
            </a> --}}
            <button id="sDesktop" style="position: absolute;" class="btn btn-lg btn-border mb-2 " onclick="setSize()"><i class="la la-desktop" style="font-size: 30px;margin-right: 10px;"></i> CHANGE TO DESKTOP PREVIEW</button>
            <button id="sMobile" style="display:none;position: absolute;" class="btn btn-lg btn-border mb-2  " onclick="setSizeM()"><i class="la la-mobile" style="font-size: 30px;margin-right: 10px;"></i> CHANGE TO MOBILE PREVIEW</button>

        </div>
        <div id="set_size_frame" class="col-6 hidden-xs trans">
            <iframe id="myFrame" style="
            width: 100%;
            height: 900px;
            border: 1px solid white;
        "  src="{{asset('website/'.$data->product_id.'/')}}/index.html"></iframe>
        </div>
        <div class="col-6">
            <div class="pt-product-single-info">
                {{-- <div class="pt-wrapper">
                    <div class="pt-label">
                        <div class="pt-label pt-label-new">Baru</div>
                    </div>
                </div> --}}
                <h1 class="pt-title">{{$data->product_name}}</h1>
                <div class="pt-price" style="font-size: 20px;">
                    Rp {{number_format($data->price,0,",",".")}} <small>/ Minggu</small>
                </div>

                
                <div class="mt-4 text-justify">{{$data->deskripsi}}</div>

                <form id="form-data">

                

                <div class="pt-collapse-block">
                    <div class="pt-item ">
                        <div class="pt-collapse-title">
                            Nama Pengantin Pria & Wanita
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                            <small style="font-weight: 100;display: block;color: #00000085;">Isikan Dengan Lengkap Nama Pengantin Pria & Wanita</small>

                        </div>
                        <div class="pt-collapse-content">
                           

                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Nama Panggilan Pengantin Pria & Wanita</label>
                                <input onkeyup="setToFrame('setName')" type="text" name="setName" class="form-control" id="setName">
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Nama Lengkap Pengantin Pria</label>
                                <input onkeyup="setToFrame('setNamePria')" type="text" name="setNamePria" class="form-control" id="setNamePria"
                                    >
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Deskripsi Pengantin Pria</label>
                                <input onkeyup="setToFrame('setDescPria')" type="text" name="setDescPria" class="form-control" id="setDescPria"
                                    >
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Nama Lengkap Pengantin Wanita</label>
                                <input onkeyup="setToFrame('setNameWanita')" type="text" name="setNameWanita" class="form-control" id="setNameWanita"
                                    >
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Deskripsi Pengantin Wanita</label>
                                <input onkeyup="setToFrame('setDescWanita')" type="text" name="setDescWanita" class="form-control" id="setDescWanita"
                                    >
                            </div>
                        </div>
                    </div>

                    <div class="pt-item">
                        <div class="pt-collapse-title">
                            Tanggal & Alamat Pernikahan
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                            <small style="font-weight: 100;display: block;color: #00000085;">Isikan Dengan Lengkap Tanggal dan Alamat Pernikahan</small>
                        </div>
                        <div class="pt-collapse-content">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Tanggal Resepsi Pernikahan</label>
                                        <input onchange="setToFrame('setTglRes')" type="text" name="setTglRes" class="form-control tgl" id="setTglRes"
                                            >
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Mulai </label>
                                        <input onkeyup="setToFrame('setJam1Res')" type="text" name="setJam1Res" class="form-control" id="setJam1Res"
                                            placeholder="12:00">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Selesai </label>
                                        <input onkeyup="setToFrame('setJam2Res')" type="text" name="setJam2Res" class="form-control" id="setJam2Res"
                                            placeholder="14:00">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Tempat Resepsi Pernikahan</label>
                                <input onkeyup="setToFrame('setTempatRes')" type="text" name="setTempatRes" class="form-control" id="setTempatRes"
                                    >
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Alamat Resepsi Pernikahan</label>
                                <input onkeyup="setToFrame('setAlamatRes')" type="text" name="setAlamatRes" class="form-control" id="setAlamatRes"
                                    >
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Akad Pernikahan</label>
                                        <input onchange="setToFrame('setTglAkad')" type="text" name="setTglAkad" class="form-control tgl" id="setTglAkad"
                                            >
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Mulai </label>
                                        <input onkeyup="setToFrame('setJam1Akad')" type="text" name="setJam1Akad" class="form-control" id="setJam1Akad"
                                            placeholder="12:00">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Selesai </label>
                                        <input onkeyup="setToFrame('setJam2Akad')" type="text" name="setJam2Akad" class="form-control" id="setJam2Akad"
                                            placeholder="14:00">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Tempat Akad Pernikahan</label>
                                <input onkeyup="setToFrame('setTempatAkad')" type="text" name="setTempatAkad" class="form-control" id="setTempatAkad"
                                    >
                            </div>
                            <div class="form-group text-left mt-3">
                                <label for="inputFitstName" style="font-size:14px;">Alamat Akad Pernikahan</label>
                                <input onkeyup="setToFrame('setAlamatAkad')" type="text" name="setAlamatAkad" class="form-control" id="setAlamatAkad"
                                    >
                            </div>
                        </div>
                    </div>
                    <div class="pt-item">
                        <div class="pt-collapse-title">
                            Foto & Gallery
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                            <small style="font-weight: 100;display: block;color: #00000085;">Isikan Foto & Gallery</small>
                        </div>
                        <div class="pt-collapse-content">
                            
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Utama</label>
                                        <input onchange="changeHeader('set_header');" type="file" name="set_header" class="form-control" id="set_header">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('set_header');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Pengantin Pria</label>
                                        <input onchange="changeHeader('foto_pria');" type="file" name="foto_pria" class="form-control" id="foto_pria">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('foto_pria');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Pengantin Wanita</label>
                                        <input onchange="changeHeader('foto_wanita');" type="file" name="foto_wanita" class="form-control" id="foto_wanita">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('foto_wanita');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Gallery 1</label>
                                        <input onchange="changeHeader('gallery_1');" type="file" name="gallery_1" class="form-control" id="gallery_1">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('gallery_1');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Gallery 2</label>
                                        <input onchange="changeHeader('gallery_2');" type="file" name="gallery_2" class="form-control" id="gallery_2">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('gallery_2');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Gallery 3</label>
                                        <input onchange="changeHeader('gallery_3');" type="file" name="gallery_3" class="form-control" id="gallery_3">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('gallery_3');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8 text-left mt-3">
                                        <label for="inputFitstName" style="font-size:14px;">Foto Gallery 4</label>
                                        <input onchange="changeHeader('gallery_4');" type="file" name="gallery_4" class="form-control" id="gallery_4">
                                        <small style="font-size: 12px;color: #afafaf;">Ukuran Maksimal 2 Mb</small>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <a onclick="changeHeader('gallery_4');" class="btn btn-sm btn-border" style="color:#000000;margin-top: 45px;">Simpan</a>
                                    </div> --}}
                                </div>
                           
                            
                        </div>
                    </div>

                    <div class="form-group text-left mt-3">
                        <label for="inputFitstName" style="font-size:14px;">Kata - Kata Mutiara</label>
                        <textarea style="height:150px;" onkeyup="setToFrame('setKata')" name="setKata" class="form-control" id="setKata">"Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu isteri-isteri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir."</textarea>
                    </div>

                    <div class="form-group text-left mt-3">
                        <label for="inputFitstName" style="font-size:14px;">Background Color</label>
                        <input class="form-control" onchange="setToFrame('bg_color')" type="color" name="bg_color" id="bg_color" style="height: 35px;">
                    </div>
                    @php
                        $music = \DB::table('ref_music')->get();
                    @endphp
                    <div class="form-group text-left mt-3">
                        <label for="inputFitstName" style="font-size:14px;">Music</label>
                        <select onchange="set_audio()" class="form-control" name="music" id="music">
                            @foreach ($music as $m)
                                <option data-music="{{asset('gallery/music')}}/{{$m->file}}" value="{{$m->file}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   
                    <audio  id="set_music"  class="fraudio">
                        <source src="{{asset('gallery/music/biw.mp3')}}" type="audio/ogg">
                        <source src="{{asset('gallery/music/biw.mp3')}}" type="audio/mpeg">
                    </audio>

                   

                    <div class="form-group text-left mt-3" style="border-top: 2px dashed #f1f1f1;padding-top: 20px;">
                        <label for="inputFitstName" style="font-size:14px;">Nama Website</label>
                        <div class="input-group mt-2 mb-1"> 
                        <div class="input-group-prepend">
                            <div class="input-group-text" style="
                            font-weight: bold;
                            background: #c07b5b;
                            color: #fff;
                            font-size: 12px;
                        ">Corellia.id/</div>
                        </div>
                        <input type="text" name="nama_website" class="form-control" id="nama_website">
                        </div>
                    </div>
    
                    <div class="form-group text-left mt-3">
                        <label for="inputFitstName" style="font-size:14px;">Tanggal Tayang Website</label>
                        <input type="text" name="date_active" class="form-control tgl" id="date_active">
                    </div>

                    <div class="form-group text-left mt-3">
                        <label for="inputFitstName" style="font-size:14px;">Link Live</label>
                        <input type="text" name="link_live" class="form-control" id="link_live">
                    </div>
                    <style>
                       #map-canvas {
                            height: 100%;
                            margin: 0px;
                            padding: 0px
                        }
                        .controls {
                            margin-top: 16px;
                            border: 1px solid transparent;
                            border-radius: 2px 0 0 2px;
                            box-sizing: border-box;
                            -moz-box-sizing: border-box;
                            height: 32px;
                            outline: none;
                            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                        }
    
                        #pac-input {
                            background-color: #fff;
                            font-family: Roboto;
                            font-size: 15px;
                            font-weight: 300;
                            margin-left: 12px;
                            padding: 0 11px 0 13px;
                            text-overflow: ellipsis;
                            width: 400px;
                        }
    
                        #pac-input:focus {
                            border-color: #4d90fe;
                        }
    
                        .pac-container {
                            font-family: Roboto;
                        }
    
                        #type-selector {
                            color: #fff;
                            background-color: #4d90fe;
                            padding: 5px 11px 0px 11px;
                        }
    
                        #type-selector label {
                            font-family: Roboto;
                            font-size: 13px;
                            font-weight: 300;
                        }
                      </style>
                       <label for="inputFitstName" style="font-size:14px;">Cari Lokasi Pernikahan</label>
                      <input id="pac-input" class="controls" type="text" placeholder="Cari Lokasi">
                      <input id="maps_lat" name="maps_lat" type="hidden" value="">
                      <input id="maps_lng" name="maps_lng" type="hidden" value="">

                    <div style="width: 100%;height: 500px;">    
                    <div  id="map-canvas"></div>
                    </div>

                    <div id="map"></div>
                    <div class='pointer'></div>
                </form>
                </div>

               
                
               
               
                
                <div class="pt-wrapper">
                    <div class="pt-row-custom-01">
                        <div class="col-item">
                            <div class="pt-input-counter style-01">
                                <span class="minus-btn">
                                    <i class="la la-minus"></i> 
                                </span>
                                
                            <input id="set_val_qty" onkeyup="set_qty(this.value);" onchange="set_qty(this.value);" 
                            type="text" value="1"  maxlength="4" size="9999">
                            <span class="plus-btn">
                                <i class="la la-plus"></i> 
                            </span>
                            </div>
                        </div>
                        <div class="col-item">
                            <a onclick="setWishlist('{{$data->product_id}}',5)" class="btn btn-lg btn-border" style="color:#000000;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-wishlist"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO WISHLIST
                                </span>
                            </a>
                        </div>
                        <div class="col-item">
                            <a onclick="setCartlistOther('{{$data->product_id}}',5)" class="btn btn-lg" style="color:#ffffff;">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-cart_1"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    ADD TO CART
                                </span>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="pt-wrapper mt-5 mb-1">
                    <div class="pt-table-03-indent">
                        <table class="pt-table-03">
                            <tbody>
                                <tr>
                                    <td style="width:60%">{{$data->product_name}}</td>
                                    <td width="10px">Rp</td>
                                    <td id="price" class="text-right">{{number_format($data->price,0,",",".")}}</td>
                                </tr>
                                <tr>
                                    <td style="width:60%">Minggu</td>
                                    <td width="10px"></td>
                                    <td class="text-right" id="qty">1</td>
                                </tr>
                                {{-- <tr>
                                    <td style="width:60%;font-weight: bold;color: #c89c7d;">Total Produk</td>
                                    <td width="10px" style="font-weight: bold;color: #c89c7d;">Rp</td>
                                    <td id="tmp_total" class="text-right" style="color: #c89c7d;">{{number_format($data->price,0,",",".")}}</td>
                                </tr> --}}
                                <tr style="display:none;">
                                    <td style="width:60%">Potongan Harga dari Voucher</td>
                                    <td width="10px">Rp</td>
                                    <td class="text-right" id="voucher_amount">0</td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width:60%">Diskon dari Voucher</td>
                                    <td width="10px">Rp</td>
                                    <td class="text-right" id="voucher_percentage">0</td>
                                    <input id="set_percentage" type="hidden" value="">
                                </tr>

                                <tr>
                                    <td style="width:60%;font-weight: bold;color: #c89c7d;">Total Payment</td>
                                    <td width="10px" style="font-weight: bold;color: #c89c7d;">Rp</td>
                                    <td id="total" class="text-right" style="color: #c89c7d;">{{number_format($data->price,0,",",".")}}</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('website.action_detail')

@stop
