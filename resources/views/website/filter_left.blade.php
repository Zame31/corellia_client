
                <div class="pt-collapse open">
                        <h3 class="pt-collapse-title">
                            Style
                            <span class="pt-icon">
                                <svg>
                                    <use xlink:href="#icon-arrow_small_bottom"></use>
                                </svg>
                            </span>
                        </h3>
                        <div class="pt-collapse-content">
                            <ul class="pt-options-swatch size-middle">
                                <li>
                                    <a href="#" onclick="set_filter_u_color(0); get_list();"><b> Semua</b> 
                                       
                                    </a>
                                </li>
                                @php
                                     $d = DB::table('ref_style')->get();
                                @endphp
                                @foreach ($d as $item)
                                <li>
                                    <a href="#" onclick="set_filter_u_color({{$item->id}}); get_list();"><b> {{$item->name}} </b> 
                                       
                                    </a>
                                </li>
                                @endforeach
                              
                            </ul>
                        </div>
                    </div>
                  