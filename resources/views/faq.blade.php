@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">FAQs</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
					
						<dd>
							<p>
								Temukan semua jawaban dari pertanyaan seputar Corellia, Pertanyaan yang sering diajukan
							</p>
						</dd>
						<dt>Apa keunikan dari undangan corellia ini?apakah bisa desain kustom?</dt>
						<dd>
							<p>
								Yup betul undangan kami memiliki fitur untuk membuat desain baru, dimana sebelumnya calon pengantin bisa merasakan terlebih dahulu katalog desain kami yang bisa di costumize di website corellia ini.	
							</p>
						</dd>
						<dt>Jenis dan bahan apa saja yang dipakai untuk pilihan undangan corellia?</dt>
						<dd>
							<p>
								Artpaper, Jasmine, Java, Akasia, Aster, Copen haggen, dll
							</p>
						</dd>
						<dt>Fasilitas apa saja yang didapat jika memesan undangan di Corellia?</dt>
						<dd>
							<p>
								Undangan bisa langsung siap disebar, sudah di pack plastik dan di printkan label nama yg diterima dari calon pengantin dengan format kami. 
							</p>
						</dd>

						<dt>Bagaimana cara pemesanan di Corellia ?</dt>
						<dd>
							<p>
								Pemesanan melalui Website Corellia di www.corellia.id  atau bisa mengunjungi gallery kami
							</p>
						</dd>
						<dt>Berapa lama proses dari awal sampai di Corellia?</dt>
						<dd>
							<p>
								Proses pengerjaan undangan di Corellia berkisar 2-3 Bulan dengan pembuatan desain katalog 2-3 Minggu, desain kustom 3-4 minggu, revisi 1-2 minggu, dan proses naik cetak 3 Minggu. 
							</p>
						</dd>
						<dt>Apakah setelah didesign boleh melakukan revisi?</dt>
						<dd>
							<p>
								Proses Revisi : Revisi Ke-1, Revisi Ke-2 masih gratis, mulai Revisi Ke-3 dan selanjutnya dikenakan Biaya tambahan 100.000,-/revisi
							</p>
						</dd>
						<dt>Bagaimana syarat pembayaran di corellia?</dt>
						<dd>
							<p>
								Tahap I Tanda jadi sebesar 500.000,- <br>
Tahap II Pelunasan sebelum Proses naik cetak,<br>
Transfer melalui rekening<br>
BCA : 437 218 2036 a.n Ratri Cahyasasi<br>
BNI : 016 194 6000 a.n Ratri Cahyasasi<br>
Mandiri : 177 000 698 8098 a.n Ratri Cahyasasi <br>

								
							</p>
						</dd>
						<dt>Apakah pengiriman bisa ke seluruh dunia?</dt>
						<dd>
							<p>
								Ya, sertakan rincian dll
							</p>
						</dd>
						<dt>Selain paket undangan apa saja yang diunggulkan di website ini?</dt>
						<dd>
							<p>
								Kita memiliki fitur “Wedding Website” yang dimana pada masa sekarang undangan yang belum mampu tersampaikan karena jarak dan waktu, calon pengantin bisa membuat website wedding invitation sendiri secara online, jadi hanya mengirimkan Link website yang mengarahkan teman-teman, keluarga jauh mengakses undangan secara online. 
							</p>
						</dd>
						
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop