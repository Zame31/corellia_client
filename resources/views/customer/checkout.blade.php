@section('content')
<main id="pt-pageContent" style="padding-bottom:500px;">
    <div class="container-indent">
        <div class="container">
            <h1 class="pt-title-subpages noborder">CHECKOUT</h1>
            {{-- @php
                dd($cart);
            @endphp --}}
            <div class="pt-account-layout">
                {{-- <h2 class="pt-title-page">{{$zz}}</h2> --}}
                <div class="pt-wrapper">
                    <a href="{{ route('customer.cartlist') }}" class="btn-link btn-lg pt-link-back">
                        <div class="pt-icon">
                            <svg width="24" height="24" viewBox="0 0 24 24">
                                <use xlink:href="#icon-arrow_large_left"></use>
                            </svg>
                        </div>
                        <span class="pt-text">Kembali Ke Cart List</span>
                    </a>
                </div>
                <div class="pt-wrapper">
                    <div class="pt-table-responsive">
                        <table class="pt-table-shop-03">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th class="text-right">Price</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>

                                @php
                                    $total_add_item = 0;
                                    $berat_product = 0;

                                    $stack = array("");

                                @endphp
                                @foreach ($cart['product_name'] as $key => $v)

                                @php
                                    array_push($stack,$cart['info_type'][$key]);

                                    // dd($stack);

                                   
                                @endphp
                                <tr>
                                    <td>
                                        <div style="
                                        margin-top: 20px;
                                        margin-bottom: 20px;
                                        font-size: 16px;
                                        font-weight: 600;
                                    ">{{$cart['product_name'][$key]}}</div>
                                        @if ($cart['info_type'][$key] == "5")
                                            @php
                                                // dd($cart['design_id'][$key]);
                                                $dwebsite = collect(\DB::select("SELECT * from mst_data_website where id::varchar = '".$cart['design_id'][$key]."'"))->first();
                                            @endphp
                                            <br>
                                            <b style="
                                            margin-bottom: 10px;
                                            display: block;
                                            font-size: 14px;
                                            ">corellia.id/{{$dwebsite->nama_website}}</b>
                                            <a target="blank" href="{{route("website.preview",["id"=>"$dwebsite->id","name"=>"$dwebsite->nama_website"])}}" class="btn btn-block" style="display: inline;padding: 10px 20px;">
                                                <span class="pt-text">
                                                    PREVIEW WEBSITE
                                                </span>
                                            </a>

                                        @endif

                                        @if ($cart['info_type'][$key] != "1" && $cart['info_type'][$key] != "5" )
                                            @php
                                                $other = collect(\DB::select("SELECT * from mst_other where product_id = '".$cart['product_id'][$key]."'"))->first();
                                                $berat_product +=$other->berat*$cart['count_product'][$key];
                                            @endphp

                                        @endif
                                        @if ($cart['info_type'][$key] == "1" )
                                        <div class="mt-2" style="font-size:12px;">
                                            @php
                                                  $undangan = collect(\DB::select("SELECT * from mst_undangan where product_id = '".$cart['product_id'][$key]."'"))->first();
                                                  $style_size = collect(\DB::select("SELECT * from ref_type_size where id = '".$cart['type_size_id'][$key]."'"))->first();
                                                  $berat_product +=$undangan->berat*$cart['count_product'][$key];


                                            @endphp

                                            <span class="zn-point">Type & Size : {{$style_size->type}}</span>
                                            <span class="zn-point">Berat : {{$undangan->berat}} g </span>
                                            <span class="zn-point">Warna : {{$cart['info_warna'][$key]}} </span>

                                            {{-- <span class="zn-point"> Waxseal : {{$cart['info_waxseal'][$key]}}</span> --}}
                                            <span class="zn-point"> Foil Color : {{$cart['info_foilcolor'][$key]}}</span>
                                            {{-- <span class="zn-point"> Emboss:{{($cart['emboss'][$key] == 1) ? "Digunakan":"Tidak Digunakan"}}</span> --}}
                                            @if ($cart['info_kertas2'][$key])
                                            <span class="zn-point">Material Inner : {{$cart['info_kertas'][$key]}}  | Material Outer:{{$cart['info_kertas2'][$key]}}</span>

                                            @else
                                            <span class="zn-point">Material : {{$cart['info_kertas'][$key]}}</span>
                                            @endif
                                            @php
                                                 $add_free_finishing = explode("|",$cart['free_finishing_id'][$key]);
                                                 $add_free_additional = explode("|",$cart['free_additional_id'][$key]);
                                                 $len_fin = count($add_free_finishing);
                                                $len_add = count($add_free_additional);
                                                $free_finishing = '';
                                                $free_additional = '';
                                                //  dd($cart);
                                            @endphp
                                            <span class="zn-point"> Free Finishing:
                                                @for ($i=0; $i < $len_fin-1; $i++)
                                                    @if ($key != $len_fin - 1)
                                                    @php
                                                        $adz = collect(\DB::select("SELECT * from ref_finishing where id = '".$add_free_finishing[$i]."'"))->first();
                                                        $free_finishing .= $adz->name.", ";
                                                    @endphp
                                                    @endif
                                                @endfor
                                            {!!$free_finishing!!}
                                                {{-- @for ($i = 0; $i < $len_fin-1; $i++)
                                                    @if ($key != $len_fin - 1)
                                                        {{$cart['free_finishing'][$i]}},
                                                    @endif
                                                @endfor --}}

                                            </span>


                                            <span class="zn-point"> Free Additional Item:
                                                @php
                                                for ($i=0; $i < $len_add-1; $i++) {
                                                   if ($key != $len_add-1) {
                                                       $adz = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_free_additional[$i]."'"))->first();
                                                       $free_additional .= $adz->additional_name.", ";
                                                   }
                                               }
                                           @endphp
                                    {!!$free_additional!!}
                                                {{-- @for ($i = 0; $i < $len_add-1; $i++)
                                                    @if ($key != $len_add - 1)
                                                        {{$cart['free_additional'][$i]}},
                                                    @endif
                                                @endfor --}}

                                            </span>

                                        </div>
                                    </td>
                                        @endif

                    </div>
                    @php
                        if($cart['info_type'][$key] == "1"){
                            // INIT PRICE
                            $disc = 0;
                            if ($cart['count_product'][$key] >= 200 && $cart['count_product'][$key] <= 299) {
                                $disc = 2000;
                            }else if($cart['count_product'][$key] >= 300 && $cart['count_product'][$key] <= 499){
                                $disc = 1000;
                            }else if($cart['count_product'][$key] >= 500 && $cart['count_product'][$key] <= 699){
                                $disc = 500;
                            }else if($cart['count_product'][$key] >= 700){
                                $disc = 0;
                            }
                            $init_price = $cart['price'][$key]+$disc;
                        }
                        else{
                            $init_price = $cart['price'][$key];
                        }

                    @endphp
                    <td class="text-right align-middle">{{number_format($init_price,0,",",".")}}</td>
                    <td class="text-right align-middle">{{$cart['count_product'][$key]}}</td>
                    <td class="text-right align-middle">{{number_format($cart['total_per'][$key],0,",",".")}}</td>
                    </tr>

                    {{-- @php
                        dd($cart);
                    @endphp --}}
                    {{-- ADDITIONAL ITEM --}}
                    @if ($cart['info_type'][$key] == "1" )

                        {{-- WAXSEAL --}}
                        <tr>
                            <td class="text-left">
                                <span style="font-size: 12px;" class="zn-point">Waxseal : {{$cart['info_waxseal'][$key]}}</span>
                                </td>
                            <td class="text-right">{{number_format($cart['harga_waxseal'][$key],0,",",".")}}</td>
                            <td class="text-right">{{$cart['count_product'][$key]}}</td>
                            <td class="text-right">{{number_format($cart['harga_waxseal'][$key]*$cart['count_product'][$key],0,",",".")}}</td>
                        </tr>
                          {{-- WAXSEAL --}}
                        @php
                        $total_add_item += $cart['harga_waxseal'][$key]*$cart['count_product'][$key];

                        $add_item = explode("|",$cart['additional_item'][$key]);
                        $add_item_count = explode("|",$cart['additional_item_count'][$key]);
                        $len = count($add_item);
                        @endphp

                        <tr>
                            <td class="text-left">
                                <span style="font-size: 12px;"><b>Additional Item</b></span>
                                </td>
                            <td class="text-right"></td>
                            <td class="text-right"></td>
                            <td class="text-right"></td>
                        </tr>

                        @foreach ($add_item as $ss => $vitem)

                            @if ($ss != $len - 1)
                                @php

                                $adss = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$ss]."'"))->first();

                                $total_add_item += $adss->price*$cart['count_product'][$key];
                                @endphp
                                <tr>
                                    <td class="text-left">
                                        <span style="font-size: 12px;" class="zn-point">{{$adss->additional_name}}</span>
                                        </td>
                                    <td class="text-right">{{number_format($adss->price,0,",",".")}}</td>
                                    <td class="text-right">{{$cart['count_product'][$key]}}</td>
                                    <td class="text-right">{{number_format($adss->price*$cart['count_product'][$key],0,",",".")}}</td>
                                </tr>
                            @endif

                        @endforeach
                    @endif
                @endforeach
                <tr>
                    <td colspan="4"></td>
                </tr>
                    <tr>
                        <td colspan="3"><strong>Sub Total</strong></td>
                        <td class="text-right"><strong>{{number_format($cart['sub_total']+$total_add_item,0,",",".") }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Potongan Harga</strong></td>
                        <td class="text-right"><strong>{{number_format($cart['voucher_amount'],0,",",".") }}</strong></td>
                    </tr>
                    <tr>
                        @php
                        if ($cart['voucher'] != '') {
                        $get_voucher = DB::table('mst_voucher')->where('code', $cart['voucher'])->first();
                        $persen = $get_voucher->percentage." %";
                        }else {
                        $persen = '';
                        }
                        @endphp
                        <td colspan="3"><strong>Diskon {{$persen}} </strong></td>
                        <td class="text-right"><strong>{{number_format($cart['voucher_percentage'],0,",",".") }}</strong>
                        </td>
                    </tr>

                    {{-- <tr>
                        <td colspan="3"><strong>Ongkos Kirim</strong></td>
                        <td class="text-right">0</td>
                    </tr> --}}
                    <tr>
                        <td colspan="3"><strong> Total</strong></td>
                        <td class="text-right"><strong>{{number_format($cart['total']+$total_add_item,0,",",".") }}</strong></td>
                    </tr>
                    </tbody>
                    </table>
                    <input type="hidden" id="set_total_add_item" value="{{$total_add_item}}">
                </div>
            </div>
            <div class="pt-wrapper">
                <div class="zn-border-delivery"></div>
                <div class="pt-shop-info" style="background: #fbfbfb;padding: 30px;border-radius: 5px;">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="pt-item">
                                <h6 class="pt-title">PENERIMA :</h6>
                                <div id="set_penerima_checkout" class="pt-description">
                                    {{$cust->cust_name}}
                                    <br>
                                    <small
                                        style="margin-top: -5px;display: block;color: #c7c7c7;">{{$cust->email}}</small>
                                    <small>{{$cust->phone_no}}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="pt-item">
                                <h6 class="pt-title">ALAMAT PENGIRIMAN:</h6>
                                <div id="set_alamat_checkout" class="pt-description">
                                    {{$cust->address}}
                                    <br>
                                    {{$cust->postal_code}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-right">
                            <a onclick="mProfilShow('{{Auth::user()->id}}')" class="btn btn-sm btn-border">
                                <div class="pt-icon">
                                    <svg>
                                        <use xlink:href="#icon-user"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">
                                    Pengaturan Profil
                                </span>
                            </a>
                        </div>

                    </div>

                    @if (in_array("5", $stack))
                    <blockquote class="pt-blockquote" style="margin-top:100px;">
                        <span class="pt-description mb-5">
                            Website akan aktif setelah melakukan pembayaran
                        </span>
                    </blockquote>
                    @endif

                    @if (in_array("1", $stack) || in_array("2", $stack) || in_array("3", $stack))

                    <div class="row" style="margin-top:100px;">
                        <div class="col-md-12">
                            <div class="pt-item">
                                <h6 class="pt-title">PILIHAN PENGIRIMAN :</h6>

                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="row mt-3">
                                            <div class="col-md-2">
                                                <label class="radio mt-3">
                                                    <input checked onchange="setPengiriman()" class="pengiriman" id="pengiriman" value="diambil"
                                                        type="radio" name="pengiriman">
                                                    <span class="outer"><span class="inner"></span></span>
                                                </label>
                                            </div>
                                            <div class="col-md-10">
                                                Diambil Di Gallery <br>
                                                <small>Tidak Ada Biaya Tambahan</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row mt-3">
                                            <div class="col-md-2">
                                                <label class="radio mt-3">
                                                    <input onchange="setPengiriman()" class="pengiriman" id="pengiriman" value="dikirim"
                                                        type="radio" name="pengiriman">
                                                    <span class="outer"><span class="inner"></span></span>
                                                </label>
                                            </div>
                                            <div class="col-md-10">
                                                Dikirim <br>
                                                <small>Estimasi Biaya Pengiriman <br>
                                                    Dengan Berat {{$berat_product/1000}} Kg <br>Rp {{number_format((($berat_product/1000)*10000),0,",",".")}} </small>

                                                <input type="hidden" id="set_biaya_ongkir" value="{{($berat_product/1000)*10000}}">
                                                </div>
                                        </div>
                                    </div>



                                </div>



                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row" style="margin-top:100px;">
                        <div class="col-md-12">
                            <div class="pt-item">
                                <h6 class="pt-title">METODE PEMBAYARAN :</h6>

                                <div class="row">
                                    @php
                                    $dbank = \DB::select("SELECT * from ref_bank ");
                                    @endphp

                                    @foreach ($dbank as $key => $v)
                                    <div class="col-md-4">
                                        <div class="row mt-3">
                                            <div class="col-md-2">
                                                <label class="radio mt-3">
                                                    <input {{($key == 0) ? "checked":""}} class="bank" id="bank"
                                                        value="{{$v->id}}" type="radio" name="bank">
                                                    <span class="outer"><span class="inner"></span></span>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <img style="
                                                                margin-top: 15px;
                                                            " src="{{asset('img/')}}/{{$v->bank_img}} " alt="">
                                            </div>
                                            <div class="col-md-6">
                                                <small>a.n {{$v->an}}</small> <br>
                                                {{$v->bank_acc_no}} <br>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach


                                </div>



                            </div>
                        </div>
                    </div>




                </div>
            </div>

            <div class="pt-shopcart-wrapperbox mt-5">
                {{-- <form id="shopcartform" class="form-default" method="post" novalidate="novalidate" action="#"> --}}
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-6 form-default form-layout-01">
                        <div class="pt-shopcart-box">

                            <h6 class="pt-title p-0">Catatan</h6>
                            <small>
                                @if (in_array("1", $cart['info_type']))
                                    Tuliskan tanggal pernikahan dan kapan udangan harus selesai untuk memudahkan pihak
                                    corellia mengkonfirmasi pesananmu.
                                @else
                                    Tuliskan Pesan untuk pihak corellia.
                                @endif
                               </small>
                            <div class="form-default form-wrapper">
                                <textarea style="height: 135px;" name="note" class="form-control" rows="7"
                                    placeholder="Masukan Catatan" id="note"></textarea>
                            </div>
                        </div>





                    </div>


                    <div class="col-12 col-md-8 col-lg-6">
                        <div class="pt-shopcart-total">
                            <div class="pt-price-01">Total Pembayaran</div>
                            <div class="pt-price-02" id="get_total_final">
                                Rp {{number_format($cart['total']+$total_add_item,0,",",".") }}
                            </div>
                            {{-- <div class="checkbox-group">
                                                <input type="checkbox" id="checkBox41" name="checkbox" checked="">
                                                <label for="checkBox41">
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    I agree with the terms and conditions
                                                </label>
                                            </div> --}}
                            <button onclick="set_pembelian()" class="btn btn-block mt-5">
                                <span class="pt-text">
                                    BUAT PESANAN
                                </span>
                            </button>
                        </div>

                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
    </div>
</main>

<div class="modal fade" id="modalProfil" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">
                        <form id="form-profil" class="form-default form-layout-01" method="post">
                            <div class="row">

                                <div class="col-md-12">
                                    <h2 class="pt-title-page text-left">Pengaturan Profil</h2>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Nama Lengkap</label>
                                        <input type="text" name="profil_nama" class="form-control" id="profil_nama"
                                            placeholder="Masukan Nama Lengkap">
                                    </div>
                                    {{-- <div class="form-group text-left">
                                                <label for="inputFitstName">E-Mail</label>
                                                <input type="text" name="profil_email" class="form-control" id="profil_email"
                                                    placeholder="Masukan E-Mail">
                                            </div> --}}
                                    {{-- <div class="form-group text-left">
                                                <label for="inputFitstName">Username</label>
                                                <input type="text" name="profil_username" class="form-control" id="profil_username"
                                                    placeholder="Masukan Username">
                                            </div> --}}
                                    <div class="form-group text-left">
                                        <label for="inputFitstName">No Telepon</label>
                                        <input type="text" onkeyup="convertToNumber(this)" name="profil_telepon"
                                            class="form-control" id="profil_telepon" placeholder="Masukan No Telepon">
                                    </div>

                                    <div class="row-btn">
                                        <button onclick="sendProfil();" class="btn btn-block">SIMPAN</button>
                                        {{-- <button type="submit" class="btn-link btn-block btn-lg"><span class="pt-text">Lost your password?</span></button> --}}
                                    </div>


                                </div>
                                <div class="col-md-6">


                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Kode Pos</label>
                                        <input type="text" onkeyup="convertToNumber(this)" name="profil_pos"
                                            class="form-control" id="profil_pos" placeholder="Masukan Kode Pos">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="inputFitstName">Alamat</label>
                                        <textarea placeholder="Masukan Alamat" class="form-control" name="profil_alamat"
                                            id="profil_alamat" cols="5" rows="5"></textarea>

                                    </div>


                                </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@include('customer.action.checkout_action')
@stop
