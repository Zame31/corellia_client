@section('content')

<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
            <h1 class="pt-title-subpages noborder">Custom Design by Order</h1>
            <small style="text-align: center;display: block;">List semua Custom Design yang kamu ajukan ke pihak corellia.</small>
                <br><br><br>
			<div class="pt-portfolio-masonry">
				<div class="pt-portfolio-content layout-default pt-grid-col-2 pt-add-item">
                    @php
                        $cdesign = \DB::select("SELECT * from mst_design_order where cust_id = ".Auth::user()->id);
                    @endphp
                    @foreach ($cdesign as $ds)
                        <div class="element-item womens">
                            <figure>
                                <img src="{{asset('gallery/design_order/')}}/{{$ds->image}} " alt="">
                                <figcaption>
                                    <h6 class="pt-title"><a>
                                        <small>{{$ds->code}}</small> <br> {{$ds->judul}}</a></h6>
                                    <div class="pt-description">
                                        <p>{{$ds->deskripsi}}</p>
                                    </div>
                                    <div class="pt-btn">
                                        <a href="{{asset('gallery/design_order/')}}/{{$ds->image}}" class="pt-btn-zoom">
                                            <svg>
                                                <use xlink:href="#icon-quick_view"></use>
                                        </svg>
                                        </a>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    @endforeach
					
					
				</div>
			</div>
		</div>
	</div>
</main>
@include('master.component.footer')
@stop