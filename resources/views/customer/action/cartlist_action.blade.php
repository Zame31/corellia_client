<script>
set_total();

function set_qty(value,id) {

    $('#qty').html(value);

    var init_price = $('#price'+id).val();
    var total_per_list = init_price * value;

    $('#t_view'+id).html('Rp '+numFormat(total_per_list));
    $('#total_per'+id).val(total_per_list);

    set_total();
}

function set_qty_undangan(value,id,min) {

    if (value < min) {
        shownotif('Jumlah Pembelian Minimal '+min,'error');
        $('#count_product'+id).val(min);

    }else {
        // set_qty(value,id);
        $('#qty').html(value);

        var disc = 0;

        // JIKA UNDANGAN
        if (value >= 200 && value <= 299) {
            disc = 2000;
        }else if(value >= 300 && value <= 499){
            disc = 1000;
        }else if(value >= 500 && value <= 699){
            disc = 500;
        }else if(value >= 700){
            disc = 0;
        }

        var init_price = $('#price'+id).val();
        var total_per_list = (parseInt(init_price)+disc) * value;
        var harga_per_list = (parseInt(init_price)+disc);

        // WAXSEAL
        var init_price_waxseal = $('#price_waxseal'+id).val();
        var waxseal_harga_per_list = (parseInt(init_price_waxseal)* value);

        // ADDITIONAL
        var ia = 0;
        var tot_additional = 0;
          $("[id^='price_additional"+id+"']").each(function(){
                console.log(this.value);
                $('#vprice_additional'+id+ia).html('Rp '+numFormat(parseInt(this.value)* value));
                ia++;
                tot_additional +=parseInt(this.value)* value;
            });

        $('#tot_additional'+id).val(tot_additional);



        $('#vprice_undangan'+id).html('Rp '+numFormat(harga_per_list));
        $('#vprice_waxseal'+id).html('Rp '+numFormat(waxseal_harga_per_list));

        $('#t_view'+id).html('Rp '+numFormat(total_per_list));
        $('#total_per'+id).val(total_per_list);


        set_total();
    }



}

function set_qty_other(value,id,type) {

    if (type == 2) {

        if (value < 300) {
            shownotif('Jumlah Pembelian Minimal 300','error');
            $('#count_product'+id).val(300);

        }else {
            set_qty(value,id);
        }
    }else {
        set_qty(value,id);
    }


}

function set_total() {

    var results = [];
    var sub_total = 0;

    tmp_checked = [];

    $("input[name='cck[]']:checked").each( function () {
        tmp_checked.push($(this).val());
	});

    for (let ick = 0; ick < tmp_checked.length; ick++) {
        sub_total += parseInt($('#total_per'+tmp_checked[ick]).val());

        if ($('#info_type'+tmp_checked[ick]).val() == 1) {
            var total_additional = $('#tot_additional'+tmp_checked[ick]).val();
            var price_waxseal = $('#price_waxseal'+tmp_checked[ick]).val();
            var count_product = $('#count_product'+tmp_checked[ick]).val();

            console.log(price_waxseal);
            console.log(count_product);


            sub_total += parseInt(total_additional)+(parseInt(price_waxseal)*count_product);
        }
    }


    // $("[id^='total_per']").each(function(){
    //     sub_total += parseInt(this.value);
    // });

    var voucher_amount = $('#voucher_amount').html();
    var voucher_percentage = $('#voucher_percentage').html();
    var s_per = $('#set_percentage').val();

    voucher_amount = clearNumFormat(voucher_amount);
    voucher_percentage = clearNumFormat(voucher_percentage);

    var p_per = (sub_total*s_per)/100;
    var total = sub_total - voucher_amount - p_per;

    $('#voucher_percentage').html(numFormat(p_per));

    $('#tmp_total').html(numFormat(sub_total));
    $('#total').html( numFormat(total));





}

function setCheckout() {

    var price = [];
    var product_name = [];
    var count_product = [];
    var total_per = [];
    var product_id = [];
    var info_warna = [];
    var info_waxseal = [];
    var info_kertas = [];
    var info_foilcolor = [];
    var info_type = [];
    var waxseal_id = [];
    var design_id = [];
    var foilcolor_id = [];

    var emboss = [];
    var free_finishing = [];
    var free_additional = [];
    var additional_item = [];
    var additional_item_count = [];
    var info_kertas2 = [];

    var info_kertas_id = [];
    var info_kertas2_id = [];
    var free_finishing_id = [];
    var free_additional_id = [];
    var type_size_id = [];
    var harga_waxseal = [];

    var sub_total = 0;

    tmp_checked = [];

    $("input[name='cck[]']:checked").each( function () {
        tmp_checked.push($(this).val());
    });


    if (tmp_checked.length == 0) {
        shownotif('Pilih Produk Untuk Melakukan Checkout','error');
    }else{

        for (let ick = 0; ick < tmp_checked.length; ick++) {
            sub_total += parseInt($('#total_per'+tmp_checked[ick]).val());
            info_kertas_id.push($('#info_kertas_id'+tmp_checked[ick]).val());
            info_kertas2_id.push($('#info_kertas2_id'+tmp_checked[ick]).val());
            free_finishing_id.push($('#free_finishing_id'+tmp_checked[ick]).val());
            free_additional_id.push($('#free_additional_id'+tmp_checked[ick]).val());
            type_size_id.push($('#type_size_id'+tmp_checked[ick]).val());
            emboss.push($('#emboss'+tmp_checked[ick]).val());
            free_finishing.push($('#free_finishing'+tmp_checked[ick]).val());
            free_additional.push($('#free_additional'+tmp_checked[ick]).val());
            additional_item.push($('#additional_itemss'+tmp_checked[ick]).val());
            additional_item_count.push($('#additional_item_count'+tmp_checked[ick]).val());
            waxseal_id.push($('#waxseal_id'+tmp_checked[ick]).val());
            design_id.push($('#design_id'+tmp_checked[ick]).val());
            foilcolor_id.push($('#foilcolor_id'+tmp_checked[ick]).val());
            info_type.push($('#info_type'+tmp_checked[ick]).val());
            info_warna.push($('#info_warna'+tmp_checked[ick]).val());
            info_waxseal.push($('#info_waxseal'+tmp_checked[ick]).val());
            info_kertas.push($('#info_kertas'+tmp_checked[ick]).val());
            info_kertas2.push($('#info_kertas2'+tmp_checked[ick]).val());
            info_foilcolor.push($('#info_foilcolor'+tmp_checked[ick]).val());
            product_name.push($('#name'+tmp_checked[ick]).val());
            count_product.push($('#count_product'+tmp_checked[ick]).val());
            price.push($('#price'+tmp_checked[ick]).val());
            total_per.push($('#total_per'+tmp_checked[ick]).val());
            product_id.push($('#product_id'+tmp_checked[ick]).val());
            harga_waxseal.push($('#harga_waxseal'+tmp_checked[ick]).val());

        }

      


        var voucher_amount = $('#voucher_amount').html();
        var voucher_percentage = $('#voucher_percentage').html();
        var s_per = $('#set_percentage').val();

        voucher_amount = clearNumFormat(voucher_amount);
        voucher_percentage = clearNumFormat(voucher_percentage);

        var p_per = (sub_total*s_per)/100;
        var total = sub_total - voucher_amount - p_per;

        $.ajax({
            type: 'POST',
            url: '{{ route('customer.set_checkout') }}',
            data: {
                "_token": "{{ csrf_token() }}",
                p_per:p_per,
                product_name:product_name,
                count_product:count_product,
                price:price,
                total_per:total_per,
                sub_total:sub_total,
                voucher_amount:voucher_amount,
                voucher_percentage:voucher_percentage,
                total:total,
                product_id:product_id,
                info_warna:info_warna,
                info_waxseal:info_waxseal,
                info_kertas:info_kertas,
                info_foilcolor:info_foilcolor,
                info_type:info_type,
                design_id:design_id,
                waxseal_id:waxseal_id,
                foilcolor_id:foilcolor_id,
                emboss:emboss,
                free_finishing:free_finishing,
                free_additional:free_additional,
                additional_item:additional_item,
                additional_item_count:additional_item_count,
                info_kertas2:info_kertas2,
                info_kertas_id:info_kertas_id,
                info_kertas2_id:info_kertas2_id,
                free_finishing_id:free_finishing_id,
                free_additional_id:free_additional_id,
                type_size_id:type_size_id,
                harga_waxseal:harga_waxseal,

                voucher:$('#set_voucher').val()

            },

            beforeSend: function () {

            },
            success: function (response) {
                console.log(response.rm);
            }

        }).done(function (msg) {
            window.location.href = "{{ route('customer.checkout') }}";
            // loadPage('{{ route('customer.checkout') }}');
        }).fail(function (msg) {
            // endLoadingPage();
        });

    }


}

function generateVoucher() {
    var voucher = $('#voucher').val();

    console.log(voucher);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('katalog.get_voucher') }}',
        data: {
            id:voucher
        },

        beforeSend: function () {
            // loadingPage();
        },
        success: function (response) {
            console.log(response.rm);

            var msg = response.rm;

            // endLoadingPage();
            if (response.rc == 99) {
                shownotif(response.rm,'error');
                $('#voucher_desc').html(response.rm);
            }else{
                shownotif('Voucher Berhasil Digunakan','other');

                var tmp_total = $('#tmp_total').html();

                tmp_total = clearNumFormat(tmp_total);

                var p_per = (tmp_total*response.rm.percentage)/100;


                $('#set_percentage').val(response.rm.percentage);

                $('#voucher_amount').html(numFormat(response.rm.amount));
                // $('#voucher_percentage').html(numFormat(p_per));
                $('#voucher_desc').html(response.rm.desc);

                $('#set_voucher').val(voucher);

                set_total();
            }
        }

    }).done(function (msg) {
    }).fail(function (msg) {
        // endLoadingPage();
    });


}
</script>
