@section('content')
<main id="pt-pageContent" style="padding-top:100px;padding-bottom:500px;background: #f9f9f9;">
    <div>
        <div class="container">
            <div class="text-center"><img src="{{ asset('img/logo.png') }}" style="
                width: 100px;
            " alt=""></div>

            <h1 class="pt-title-subpages noborder mt-2" style="
            font-size: 24px;
            margin: 0;
            padding: 0 !important;
        ">KONFIRMASI PEMBAYARAN</h1>
            <strong style="text-align:center;display:block;color: #c89c7d;font-size: 15px;text-transform: uppercase;">
            </strong>

            <div class="row">
                <div class="offset-md-3 col-md-3">
                    <div class="text-center mt-4">
                        <span style="display:block;color: #c89c7d;font-size:16px;font-weight:bold;">KODE
                            PEMBELIAN</span>
                        <span>{{$pembelian->no_pembelian}}</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center mt-4">
                        <span style="display:block;color: #c89c7d;font-size:16px;font-weight:bold;">TOTAL
                            PEMBAYARAN</span>
                        <span>Rp {{number_format($pembelian->billing,0,",",".")}}</span>
                    </div>
                </div>
            </div>

            <div class="pt-account-layout">


                <div class="pt-wrapper">
                    <div class="zn-border-brown"></div>
                    <div class="pt-shop-info" style="padding: 50px 30px;background: white;border-radius: 5px;">
                        <form enctype="multipart/form-data" id="form-pembayaran" class="form-default form-layout-01 bv-form" method="post"
                            novalidate="novalidate"><button type="submit" class="bv-hidden-submit"
                                style="display: none; width: 0px; height: 0px;"></button>
                            <div class="row">

                                <input type="hidden" name="no_pembelian" value="{{$pembelian->no_pembelian}}">

                                <div class="col-md-6">

                                    <div class="form-group text-left">
                                        <label>Bank yang digunakan</label>
                                        <input type="text" name="bank" id="bank" class="form-control">

                                    </div>


                                    <div class="form-group text-left">
                                        <label>Atas Nama</label>
                                        <input type="text" name="atas_nama" class="form-control" id="atas_nama"
                                            placeholder="Masukan Nama Lengkap">
                                    </div>
                                    <div class="form-group text-left">
                                        <label>No Rekening</label>
                                        <input type="text" onkeyup="convertToNumber(this)" name="no_rek" class="form-control" id="no_rek"
                                            placeholder="Masukan No Rekening">
                                    </div>

                                    <div class="form-group text-left">
                                        <label>Nominal Transfer</label>
                                        @php
                                        $nom_bayar = 0;
                                        $is_website = 0;
                                        $is_other = 0;

                                        if ($pembelian->billing == $pembelian->total_pembelian) {

                                            foreach ($pembelian_list as $key => $value) {
                                                if ($value->type_id == 5) {
                                                    $nom_bayar += $value->totalprice;
                                                    $is_website = 1;
                                                } else {
                                                    if (count($pembelian_list) > 1) {
                                                        $nom_bayar +=($pembelian->billing*(25/count($pembelian_list)))/100;
                                                        $is_other = 1;
                                                    }else{
                                                        $nom_bayar +=($pembelian->billing*25)/100;
                                                        $is_other = 1;
                                                    }

                                                }
                                            }

                                        } else {
                                            $nom_bayar = $pembelian->billing;
                                        }

                                        // dd($is_website);

                                        $nom_bayar = number_format($nom_bayar,0,",",".")



                                        @endphp
                                        <input {{($pembelian->billing == $pembelian->total_pembelian) ? "":"readonly"}} {{($is_website == 1) ? "readonly":""}} type="text" onkeyup="convertToRupiah(this);validate_bayar()" name="nominal_transfer" class="form-control" id="nominal_transfer"
                                           value="{{$nom_bayar}}"  placeholder="Masukan Nominal" maxlength="10">

                                           {{-- ONLY WEBSITE --}}
                                           @if ($is_website == 1 && $is_other == 0)

                                           @else
                                            {!!($pembelian->billing == $pembelian->total_pembelian) ? '<small style="color: #d4d4d4;">Minimal Pembayaran Awal Rp'.$nom_bayar.'. Sebanyak 25% dari total pembayaran</small>':""!!}
                                            <div class="invalid-feedback" id="sn">Nominal Transfer Tidak Sesuai</div>
                                           @endif


                                        </div>
                                    <div class="form-group text-left">
                                        <label>Transfer Ke</label>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <img style="margin-top: 15px;"
                                                    src="{{asset('img/')}}/{{$pembelian->bank_img}} " alt="">
                                            </div>
                                            <div class="col-md-8">
                                                {{$pembelian->bank_acc_no}} <br>
                                                <small>a.n {{$pembelian->an}}</small> <br>
                                            </div>
                                        </div>
                                        {{-- <select name="" id="" class="form-control">
                                                    @php
                                                      $dbank =  \DB::select("SELECT * from ref_bank ");
                                                    @endphp

                                                    @foreach ($dbank as $v)
                                                    <option value="">{{$v->bank_name}} | {{$v->bank_acc_no}}</option>
                                        @endforeach
                                        </select> --}}
                                    </div>


                                </div>
                                <div class="col-md-6">

                                    <div class="form-group text-left">
                                        <label>Upload Bukti Transfer</label>
                                        <input type="file" name="bukti_pembayaran" class="form-control" id="bukti_pembayaran"
                                            placeholder="Masukan Nama Lengkap" style="
                                            font-size: 12px;
                                            padding: 5px;
                                        ">
                                        <small style="
                                                    color: #d4d4d4;
                                                ">File yang di upload berupa JPG dan PNG. maksimal 2 Mb</small>
                                    </div>

                                    <label>Catatan</label>
                                    <div class="form-default form-wrapper">
                                        <textarea style="height: 135px;" name="catatan" class="form-control" rows="7"
                                            placeholder="Masukan Pesan" id="catatan"></textarea>
                                    </div>


                                    </div>

                                    <div class="col-md-12">
                                        <div>
                                            <small style="
                                            margin-top: 55px !important;
                                            display: block;
                                            text-align:center;
                                            border: 1px solid #ebebeb;
                                            padding: 10px 20px;
                                            background: #f5f5f5;
                                        ">Terima kasih telah berbelanja di corellia.id. Pembayaran akan di konfirmasi paling lambat 3 x 24 jam.</small>

                                        </div>
                                    </div>

                            </div>
                        </form>




                    </div>
                    <div class="zn-border-brown"></div>
                </div>

                <div class="row">
                    <div class="offset-md-4 col-md-4">
                        <button onclick="sendPembayaran()" class="btn btn-block mt-5">
                            <span class="pt-text">
                                KONFIRMASI SEKARANG
                            </span>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
@include('customer.action.pay_action')
@stop
