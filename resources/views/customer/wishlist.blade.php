@section('content')

@if ($dm == null && $d_undangan == null && $d_website == null)

<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<div class="pt-empty-layout">
				<span class="pt-icon">
					<svg width="94" height="83" viewBox="0 0 94 83" fill="none">
						<use xlink:href="#icon-empty_wishlist"></use>
					</svg>
				</span>
				<h1 class="pt-title">Wishlist</h1>
				<p>Belum ada produk yang kamu masukan kedalam 'Wish List' </p>
				<div class="row-btn">
					<a href="{{ route('home') }}" class="btn btn-border">Lanjut belanja</a>
				</div>
			</div>
		</div>
	</div>
</main>
@else
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">Wishlist</h1>
			<div class="js-init-carousel row arrow-location-center-02 pt-layout-product-item js-align-arrow">
				{{-- @php
					dd($dm);
				@endphp --}}

				{{-- UNDANGAN --}}
				@foreach ($d_undangan as $v)
				@php
					 $dm_img = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id = '".$v->product_id."'"))->first();
				@endphp
				<div class="col-6 col-md-4 col-lg-6">
					<div class="pt-product">
						<div class="pt-image-box">
							<div class="pt-app-btn">
								<a href="#" class="pt-btn-remove">
									<svg width="24" height="24" viewBox="0 0 24 24">
										<use xlink:href="#icon-remove"></use>
									</svg>
								</a>
							</div>
							<a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}">
							<span class="pt-img"><img style="height: 270px;" src="{{asset("gallery/product/$dm_img->img")}}" alt=""></span>
								<span class="pt-label-location">
								</span>
							</a>
						</div>
						<div class="pt-description">
							<div class="pt-col">
								
								<h2 class="pt-title"><a>{{$v->product_name}}</a></h2>
								<ul class="pt-add-info">
									<li>
										<a>By Corellia</a>
									</li>
								</ul>
								<div class="pt-price">
									{{number_format($v->price,0,",",".")}}
								</div>
							</div>
							<div class="pt-col">
								<div class="pt-row-hover">
									<a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}" class="pt-btn-addtocart">
										<div class="pt-icon">
											<svg width="24" height="24" viewBox="0 0 24 24">
												<use xlink:href="#icon-cart_1"></use>
											</svg>
										</div>
										<span class="pt-text">BUY</span>
									</a>
									<div class="pt-price">
										<span class="new-price">Rp {{number_format($v->price,0,",",".")}}</span>
									</div>
									<div class="pt-wrapper-btn">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach

				{{-- OTHER --}}
				@foreach ($dm as $v)
				@php
					 $dm_img = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id = '".$v->product_id."'"))->first();
				@endphp
				<div class="col-6 col-md-4 col-lg-6">
					<div class="pt-product">
						<div class="pt-image-box">
							<div class="pt-app-btn">
								<a href="#" class="pt-btn-remove">
									<svg width="24" height="24" viewBox="0 0 24 24">
										<use xlink:href="#icon-remove"></use>
									</svg>
								</a>
							</div>
							<a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}">
							<span class="pt-img"><img src="{{asset("gallery/product/$dm_img->img")}}" alt=""></span>
								<span class="pt-label-location">
								</span>
							</a>
						</div>
						<div class="pt-description">
							<div class="pt-col">
								
								<h2 class="pt-title"><a>{{$v->product_name}}</a></h2>
								<ul class="pt-add-info">
									<li>
										<a>By Corellia</a>
									</li>
								</ul>
								<div class="pt-price">
									{{number_format($v->price,0,",",".")}}
								</div>
							</div>
							<div class="pt-col">
								<div class="pt-row-hover">
									<a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}" class="pt-btn-addtocart">
										<div class="pt-icon">
											<svg width="24" height="24" viewBox="0 0 24 24">
												<use xlink:href="#icon-cart_1"></use>
											</svg>
										</div>
										<span class="pt-text">BUY</span>
									</a>
									<div class="pt-price">
										<span class="new-price">Rp {{number_format($v->price,0,",",".")}}</span>
									</div>
									<div class="pt-wrapper-btn">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach

				{{-- WEBSITE --}}
				@foreach ($d_website as $v)
			
				<div class="col-6 col-md-4 col-lg-6">
					<div class="pt-product">
						<div class="pt-image-box">
							<div class="pt-app-btn">
								<a href="#" class="pt-btn-remove">
									<svg width="24" height="24" viewBox="0 0 24 24">
										<use xlink:href="#icon-remove"></use>
									</svg>
								</a>
							</div>
							<a href="{{route("website.detail",["id"=>"$v->product_id","type"=>1])}}">
							<span class="pt-img"><img src="{{asset("gallery/product/$v->img")}}" alt=""></span>
								<span class="pt-label-location">
								</span>
							</a>
						</div>
						<div class="pt-description">
							<div class="pt-col">
								
								<h2 class="pt-title"><a>{{$v->product_name}}</a></h2>
								<ul class="pt-add-info">
									<li>
										<a>By Corellia</a>
									</li>
								</ul>
								<div class="pt-price">
									{{number_format($v->price,0,",",".")}}
								</div>
							</div>
							<div class="pt-col">
								<div class="pt-row-hover">
									<a href="{{route("website.detail",["id"=>"$v->product_id","type"=>1])}}" class="pt-btn-addtocart">
										<div class="pt-icon">
											<svg width="24" height="24" viewBox="0 0 24 24">
												<use xlink:href="#icon-cart_1"></use>
											</svg>
										</div>
										<span class="pt-text">BUY</span>
									</a>
									<div class="pt-price">
										<span class="new-price">Rp {{number_format($v->price,0,",",".")}}</span>
									</div>
									<div class="pt-wrapper-btn">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				
				
				
			</div>
		</div>
	</div>
</main>

@endif



@include('master.component.footer')
@stop