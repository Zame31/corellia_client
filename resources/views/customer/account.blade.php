@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">PROFILE</h1>
			<div class="pt-account-layout">
				
				<div class="pt-wrapper">
					<h3 class="pt-title">Detail Profile</h3>
					<div class="pt-table-responsive">
						<table class="pt-table-shop-02">
							<tbody>
								<tr>
									<td>Nama Lengkap:</td>
									<td> {{$profile->cust_name}} </td>
								</tr>
								<tr>
									<td>E-mail:</td>
									<td>{{$profile->email}}</td>
								</tr>
								<tr>
									<td>Alamat:</td>
									<td>{{$profile->address}}</td>
								</tr>
								
								<tr>
									<td>No Telepon:</td>
									<td>{{$profile->phone_no}}</td>
								</tr>
								<tr>
									<td>Kode Pos:</td>
									<td>{{$profile->postal_code}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<a onclick="mProfilShow('{{Auth::user()->id}}')" class="btn btn-border mt-3">Pengaturan Profile</a>
				
				</div>
			</div>
		</div>
	</div>
</main>


@include('master.component.footer')



<div class="modal fade" id="modalProfil" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">

						<div class="row">
							<div class="col-12">
								<div class="pt-offset-35">
									<div class="pt-product-page__tabs pt-tabs pt-tabs-gorizontal-js">
										 <div class="pt-tabs__head">
											<ul>
												<li data-active="true"><span>PENGATURAN PROFILE</span></li>
												<li><span>UBAH PASSWORD</span></li>
											</ul>
											<div class="pt-tabs__border"></div>
										</div>
										<div class="pt-tabs__body">
											<div>
											   <span class="pt-tabs__title">Description</span>
											   <div class="pt-tabs__content">
													
												<form id="form-profil" class="form-default form-layout-01" method="post">
													<div class="row">
														<div class="col-md-6">
							
															<div class="form-group text-left">
																<label for="inputFitstName">Nama Lengkap</label>
																<input type="text" name="profil_nama" class="form-control" id="profil_nama"
																	placeholder="Masukan Nama Lengkap">
															</div>
															{{-- <div class="form-group text-left">
																		<label for="inputFitstName">E-Mail</label>
																		<input type="text" name="profil_email" class="form-control" id="profil_email"
																			placeholder="Masukan E-Mail">
																	</div> --}}
															{{-- <div class="form-group text-left">
																		<label for="inputFitstName">Username</label>
																		<input type="text" name="profil_username" class="form-control" id="profil_username"
																			placeholder="Masukan Username">
																	</div> --}}
															<div class="form-group text-left">
																<label for="inputFitstName">No Telepon</label>
																<input type="text" onkeyup="convertToNumber(this)" name="profil_telepon"
																	class="form-control" id="profil_telepon" placeholder="Masukan No Telepon">
															</div>
							
															<div class="row-btn">
																<button onclick="sendProfil2();" class="btn btn-block">SIMPAN</button>
																{{-- <button type="submit" class="btn-link btn-block btn-lg"><span class="pt-text">Lost your password?</span></button> --}}
															</div>
							
							
														</div>
														<div class="col-md-6">
							
							
															<div class="form-group text-left">
																<label for="inputFitstName">Kode Pos</label>
																<input type="text" onkeyup="convertToNumber(this)" name="profil_pos"
																	class="form-control" id="profil_pos" placeholder="Masukan Kode Pos">
															</div>
															<div class="form-group text-left">
																<label for="inputFitstName">Alamat</label>
																<textarea placeholder="Masukan Alamat" class="form-control" name="profil_alamat"
																	id="profil_alamat" cols="5" rows="5"></textarea>
							
															</div>
							
							
														</div>
													</div>
												</form>
											   </div>
											</div>
											<div>
												<span class="pt-tabs__title">Additional information</span>
												<div class="pt-tabs__content">
													<form id="form-password" class="form-default form-layout-01" method="post">
														<div class="row">
							
															<div class="col-md-12">
								
																<div class="form-group text-left">
																	<label>Password Baru</label>
																	<input type="password" name="pro_r_password" class="form-control" id="pro_r_password"
																		placeholder="Masukan Password Baru">
																</div>
																<div class="form-group text-left">
																	<label>Ketik Ulang Password Baru</label>
																	<input type="password" name="pro_r_repassword" class="form-control" id="pro_r_repassword"
																		placeholder="Ketik Ulang Password Baru">
																</div>
															
								
																<div class="row-btn">
																	<button onclick="passwordbaru();" class="btn btn-block">SIMPAN</button>
																	{{-- <button type="submit" class="btn-link btn-block btn-lg"><span class="pt-text">Lost your password?</span></button> --}}
																</div>
								
								
															</div>
													</form>
												 </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" style="z-index:10000" id="modalPassword" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="icon icon-clear"></span></button>
            </div>
            <div class="modal-body">
                <div class="container-indent">
                    <div class="container">
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>

	
$(document).ready(function () {
$("#form-password").bootstrapValidator({
    excluded: [':disabled'],
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        
        pro_r_password: {
            validators: {
                notEmpty: {
                    message: 'Mohon isi di kolom berikut'
                },
                regexp: {
                    regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[$@!%?&]).{8,}$/,
                    message: 'Password minimal 8 karakter terdiri dari huruf, huruf Kapital, angka dan spesial karakter. Contoh : Corellia2@'
                }
            }
        },
		pro_r_repassword: {
            validators: {
                notEmpty: {
                message: 'Mohon isi di kolom berikut'
            },
                identical: {
                    field: 'pro_r_password',
                    message: 'Tidak Sama Dengan Password Yang Dibuat'
                }
            }
        }
    }
}).on('success.field.bv', function (e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});
});

$('#form-password').on('keyup keypress', function (e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) {
    passwordbaru();
}
});

function passwordbaru() {

	var validateData = $('#form-password').data('bootstrapValidator').validate();
if (validateData.isValid()) {


var formData = document.getElementById("form-password");
var objData = new FormData(formData);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.ajax({
    type: 'POST',
    url: '{{ route('customer.passwordbaru') }}',
    data: objData,
    dataType: 'JSON',
    contentType: false,
    cache: false,
    processData: false,

    beforeSend: function () {
        loadingPage();
        
    },

    success: function (response) {

        if (response.rc == 1) {    
			shownotif('Password Berhasil Disimpan','other');
			location.reload();            
        }
        
    }

}).done(function (msg) {
    endLoadingPage();

}).fail(function (msg) {
    endLoadingPage();
});

}
}
</script>
@stop