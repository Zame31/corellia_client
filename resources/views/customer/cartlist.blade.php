@section('content')
{{-- <style>
    .pt-shopcart-page .pt-item .pt-item-description .pt-col:nth-child(1) {
    -webkit-box-flex: 2;
    -ms-flex: none;
    flex: none;
    padding-right: 15px;
}
</style> --}}

@if ($dm == null && $d_undangan == null && $d_website == null)
<main id="pt-pageContent">
    <div class="container-indent">
        <div class="container">
            <div class="pt-empty-layout">
                <span class="pt-icon">
                    <svg width="100" height="119" viewBox="0 0 100 119">
                        <use xlink:href="#icon-empty_shopping_bag"></use>
                    </svg>
                </span>
                <h1 class="pt-title">Cart List Kosong</h1>
                <p>Belum ada produk yang kamu masukan kedalam 'Cart List'</p>
                <div class="row-btn">
                    <a href="{{ route('home') }}" class="btn btn-border">Lanjut Belanja</a>
                </div>
            </div>
        </div>
    </div>
</main>
@else
<main id="pt-pageContent">
    <div class="container-indent">
        <div class="container">
            <h1 class="pt-title-subpages noborder">Shopping Cart</h1>

            <div class="pt-shopcart-page">

                {{-- UNDANGAN --}}

                @foreach ($d_undangan as $v)
                @php
                    $color = $v->color;
                    $product_id = $v->product_id;
                    $printing = $v->foilcolor;
                    $u_shape = $v->shape;
                    $u_paper = $v->paper;


                    $whereParts = array();

                    if($product_id) { $whereParts[] = "product_id = '$product_id' "; }
                    if($color) { $whereParts[] = "color = $color "; }
                    if($u_shape) { $whereParts[] = "shape = $u_shape "; }
                    if($u_paper) { $whereParts[] = "paper = $u_paper "; }
                    if($printing) { $whereParts[] = "foilcolor = $printing "; }


                    $sql_data = "SELECT * from mst_design ";
                    if(count($whereParts)) {
                        $sql_data .= "WHERE " . implode('AND ', $whereParts);
                    }


                    $data = collect(\DB::select($sql_data))->first();

                    // dd($sql_data);
                    $dm_img = collect(\DB::select("SELECT * from mst_image where id = '".$data->image_id."'"))->first();

                    // $dm_img = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id =
                    // '".$v->product_id."'"))->first();
                @endphp

                <div class="pt-item" style="padding-left: 175px;">
                    <div class="pt-item-btn">
                        <button onclick="mVerifShow({{$v->cartlist_id}},'cart')" class="pt-btn js-remove-item">
                            <svg width="24" height="24" viewBox="0 0 24 24">
                                <use xlink:href="#icon-remove"></use>
                            </svg>
                        </button>
                        <div class="checkbox-group size-md pt-demo-border">
                            <input type="checkbox" onchange="set_total()" id="cck{{$v->product_id}}{{$v->id}}" value="{{$v->product_id}}{{$v->id}}" name="cck[]">
                            <label for="cck{{$v->product_id}}{{$v->id}}">
                                <span class="check"></span>
                                <span class="box"></span>
                            </label>
                        </div>
                    </div>
                    <div class="pt-item-img">
                        <a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}"><img
                                src="{{asset("gallery/product/$dm_img->img")}}" alt=""></a>

                                <div class="pt-input-counter style-01 mt-2">
                                    <span class="minus-btn">
                                        <i class="la la-minus"></i>
                                    </span>
                                    <input id="count_product{{$v->product_id}}{{$v->id}}" onkeyup="set_qty_undangan(this.value,'{{$v->product_id}}{{$v->id}}',{{$v->min_pembelian}});"
                                        onchange="set_qty_undangan(this.value,'{{$v->product_id}}{{$v->id}}',{{$v->min_pembelian}});" type="text" maxlength="3"
                                        value="{{$v->qty}}" size="999">
                                        <span class="plus-btn">
                                            <i class="la la-plus"></i>
                                         </span>
                                </div>
                    </div>

                        @php
                            // INIT PRICE
                            $disc = 0;
                            if ($v->qty >= 200 && $v->qty <= 299) {
                            $disc = 2000;
                            }else if($v->qty >= 300 && $v->qty <= 499){
                                $disc = 1000;
                            }else if($v->qty >= 500 && $v->qty <= 699){
                                $disc = 500;
                            }else if($v->qty >= 700){
                                $disc = 0;
                            }
                            $init_price = $v->price+$disc;
                        @endphp

                    <div class="pt-item-description">
                        <div class="pt-col" style="flex: none;width: 100%;">
                            <h6 class="pt-title"><a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}">{{$v->product_name}}</a></h6>
                            <input type="hidden" id="name{{$v->product_id}}{{$v->id}}"  value="{{$v->product_name}}">
                            <input type="hidden" id="product_id{{$v->product_id}}{{$v->id}}" value="{{$v->product_id}}">
                            <ul class="pt-add-info">
                                <li>
                                    <div style="display: inline-block;width: 60%;">Style & Size : <b > {{$v->style_size}} </b></div>
                                    <table style="display: inline-block;height: 23px;">
                                        <tr>
                                            <td>
                                                {{-- <div id="vprice_undangan{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($init_price,0,",",".")}}</div> --}}
                                                <input id="price{{$v->product_id}}{{$v->id}}" name="price{{$v->product_id}}{{$v->id}}" type="hidden"
                                                value="{{$v->price}}">
                                            </td>
                                            <td>
                                                <div id="t_view{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($init_price*$v->qty,0,",",".")}}
                                                </div>
                                                <input id="total_per{{$v->product_id}}{{$v->id}}" name="total_per{{$v->product_id}}{{$v->id}}" type="hidden"
                                                    value="{{($init_price*$v->qty)}}">
                                            </td>
                                        </tr>
                                    </table></li>
                                <li>Warna : <b > {{$v->color_name}} </b></li>
                                @if ($v->material2)
                                <li>Material Inner : <b> {{$v->material1}} </b></li>
                                <li>Material Outer : <b> {{$v->material2}} </b></li>
                                @else
                                <li>Material : <b> {{$v->material1}} </b></li>
                                @endif

                                @php
                                $add_item = explode("|",$v->additional_user);
                                $add_item_count = explode("|",$v->additional_user_count);
                                $add_free_finishing = explode("|",$v->free_finishing);
                                $add_free_additional = explode("|",$v->free_additional);


                                $len = count($add_item);
                                $len_fin = count($add_free_finishing);
                                $len_add = count($add_free_additional);

                                @endphp

                                {{-- <li>Emboss : <b> {{($v->emboss == 1) ? "Digunakan":"Tidak Digunakan"}} </b></li> --}}

                                <li>Foil Color : <b > {{$v->foilcolor_name}} </b></li>
                                <li><div style="display: inline-block;width: 60%;">Waxseal : <b> {{$v->waxseal_name}} </b></div>
                                    <table style="display: inline-block;height: 23px;">
                                        <tr>
                                            {{-- <td class="pt-price" style="font-size:12px;">Rp {{number_format($v->harga_waxseal,0,",",".")}}</td> --}}
                                            <input type="hidden" id="price_waxseal{{$v->product_id}}{{$v->id}}" value="{{ $v->harga_waxseal }}">
                                            <td id="vprice_waxseal{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($v->harga_waxseal*$v->qty,0,",",".")}}</td>
                                        </tr>
                                    </table>
                                </li>
                                @if (count($add_item) > 1)
                                <li>Additional Item : </li>
                                @endif

                                @php
                                       $tot_additional = 0;
                                @endphp
                                @foreach ($add_item as $key => $vitem)
                                    @php
                                        $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$key]."'"))->first();

                                    @endphp
                                    @if ($key != $len - 1)
                                        <span class="zn-point" style="font-size: 13px;">
                                            <div style="display: inline-block;width: 61%;">{{$ad->additional_name}}</div>
                                        {{-- <table style="float: right;display: inline-block;width: 60%;"> --}}
                                            {{-- <tr> --}}
                                                {{-- <td class="pt-price" style="font-size:12px;">Rp {{number_format($v->harga_waxseal,0,",",".")}}</td> --}}
                                                <input type="hidden" id="price_additional{{$v->product_id}}{{$v->id}}{{$key}}" value="{{ $ad->price }}">
                                                <span style="font-size: 12px;" id="vprice_additional{{$v->product_id}}{{$v->id}}{{$key}}" class="pt-price"
                                                    >Rp {{number_format($ad->price*$add_item_count[$key],0,",",".")}} </span>
                                             {{-- </tr>
                                        </table> --}}
                                        @php
                                            $tot_additional += $ad->price*$add_item_count[$key];
                                        @endphp


                                    </span>
                                    @endif
                                @endforeach
                                <input type="hidden" id="tot_additional{{$v->product_id}}{{$v->id}}" value="{{ $tot_additional }}">
                                <li>Free Finishing : </li>



                                @foreach ($add_free_finishing as $key => $vitem)
                                    @php
                                        $ad = collect(\DB::select("SELECT * from ref_finishing where id::varchar = '".$add_free_finishing[$key]."'"))->first();
                                    @endphp
                                    @if ($key != $len_fin - 1)
                                        <span class="zn-point" style="font-size: 13px;">{{$ad->name}}</span>
                                        <input type="hidden" id="free_finishing{{$v->product_id}}{{$v->id}}" value="{{$ad->name}}">
                                    @endif
                                @endforeach


                                <li>Free Additional Item : </li>
                                @foreach ($add_free_additional as $key => $vitem)
                                    @php
                                        $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_free_additional[$key]."'"))->first();
                                    @endphp
                                    @if ($key != $len_add - 1)
                                        <span class="zn-point" style="font-size: 13px;">{{$ad->additional_name}}</span>
                                        <input type="hidden" id="free_additional{{$v->product_id}}{{$v->id}}" value="{{$ad->additional_name}}">

                                    @endif
                                @endforeach


                                {{-- <li>By Corellia </li> --}}
                                <input type="hidden" id="info_warna{{$v->product_id}}{{$v->id}}" value="{{$v->color_name}}">
                                <input type="hidden" id="info_kertas{{$v->product_id}}{{$v->id}}" value="{{$v->material1}}">
                                <input type="hidden" id="info_kertas2{{$v->product_id}}{{$v->id}}" value="{{$v->material2}}">
                                <input type="hidden" id="info_waxseal{{$v->product_id}}{{$v->id}}" value="{{$v->waxseal_name}}">
                                <input type="hidden" id="info_foilcolor{{$v->product_id}}{{$v->id}}" value="{{$v->foilcolor_name}}">
                                <input type="hidden" id="info_type{{$v->product_id}}{{$v->id}}" value="{{$v->type}}">
                                <input type="hidden" id="design_id{{$v->product_id}}{{$v->id}}" value="{{$v->design_id}}">
                                <input type="hidden" id="waxseal_id{{$v->product_id}}{{$v->id}}" value="{{$v->waxseal}}">
                                <input type="hidden" id="foilcolor_id{{$v->product_id}}{{$v->id}}" value="{{$v->foilcolor}}">
                                <input type="hidden" id="emboss{{$v->product_id}}{{$v->id}}" value="{{$v->emboss}}">
                                <input type="hidden" id="additional_itemss{{$v->product_id}}{{$v->id}}" value="{{$v->additional_user}}">
                                <input type="hidden" id="additional_item_count{{$v->product_id}}{{$v->id}}" value="{{$v->additional_user_count}}">
                                <input type="hidden" id="info_kertas_id{{$v->product_id}}{{$v->id}}" value="{{$v->paper_inner}}">
                                <input type="hidden" id="info_kertas2_id{{$v->product_id}}{{$v->id}}" value="{{$v->paper_outer}}">
                                <input type="hidden" id="free_finishing_id{{$v->product_id}}{{$v->id}}" value="{{$v->free_finishing}}">
                                <input type="hidden" id="free_additional_id{{$v->product_id}}{{$v->id}}" value="{{$v->free_additional}}">
                                <input type="hidden" id="type_size_id{{$v->product_id}}{{$v->id}}" value="{{$v->type_size}}">
                                <input type="hidden" id="min_pembelian{{$v->product_id}}{{$v->id}}" value="{{$v->min_pembelian}}">
                                <input type="hidden" id="harga_waxseal{{$v->product_id}}{{$v->id}}" value="{{$v->harga_waxseal}}">

                            </ul>
                        </div>


                        {{-- <div class="pt-col" style="flex: auto;width: 100%;">

                            <div class="tt-table-responsive-md">
                                <div class="pt-table-size">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="zn-font-table">Produk</th>
                                                <th class="zn-font-table">Harga</th>
                                                <th class="zn-font-table">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="font-size:12px;">{{$v->product_name}}</td>
                                                <td>
                                                    <div id="vprice_undangan{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($init_price,0,",",".")}}</div>
                                                        <input id="price{{$v->product_id}}{{$v->id}}" name="price{{$v->product_id}}{{$v->id}}" type="hidden"
                                                        value="{{$v->price}}">
                                                    </td>
                                                <td>
                                                    <div id="t_view{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($init_price*$v->qty,0,",",".")}}
                                                    </div>
                                                    <input id="total_per{{$v->product_id}}{{$v->id}}" name="total_per{{$v->product_id}}{{$v->id}}" type="hidden"
                                                        value="{{$init_price*$v->qty}}">
                                                </td>
                                            </tr>
                                            @if ($v->waxseal_name != "Tidak Digunakan")
                                            <tr>
                                                <td style="font-size:12px;">{{$v->waxseal_name}}</td>
                                                <td class="pt-price" style="font-size:12px;">Rp {{number_format($v->harga_waxseal,0,",",".")}}</td>
                                                <input type="hidden" id="price_waxseal{{$v->product_id}}{{$v->id}}" value="{{ $v->harga_waxseal }}">
                                                <td id="vprice_waxseal{{$v->product_id}}{{$v->id}}" class="pt-price" style="font-size:12px;">Rp {{number_format($v->harga_waxseal*$v->qty,0,",",".")}}</td>
                                            </tr>
                                            @endif

                                            @foreach ($add_item as $key => $vitem)
                                                @php
                                                    $ad = collect(\DB::select("SELECT * from ref_additional where id = '".$add_item[$key]."'"))->first();
                                                @endphp
                                                @if ($key != $len - 1)
                                                <tr>
                                                <td style="font-size:12px;">{{$ad->additional_name}}</td>
                                                <td class="pt-price" style="font-size:12px;">Rp {{number_format($ad->price,0,",",".")}}</td>
                                                <input type="hidden" id="price_additional{{$v->product_id}}{{$v->id}}{{$key}}" value="{{ $ad->price }}">
                                                <td id="vprice_additional{{$v->product_id}}{{$v->id}}{{$key}}" class="pt-price" style="font-size:12px;">Rp {{number_format($ad->price*$add_item_count[$key],0,",",".")}} </td>
                                            </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div> --}}
                        {{-- <div class="pt-col">
                            <div id="vprice_undangan{{$v->product_id}}{{$v->id}}" class="pt-price">Rp {{number_format($init_price,0,",",".")}}</div>
                            <input id="price{{$v->product_id}}{{$v->id}}" name="price{{$v->product_id}}{{$v->id}}" type="hidden"
                            value="{{$v->price}}">
                        </div> --}}


                        {{-- <div class="pt-col">
                            <div id="t_view{{$v->product_id}}{{$v->id}}" class="pt-price">Rp{{number_format($init_price*$v->qty,0,",",".")}}
                            </div>
                            <input id="total_per{{$v->product_id}}{{$v->id}}" name="total_per{{$v->product_id}}{{$v->id}}" type="hidden"
                                value="{{$init_price*$v->qty}}">
                        </div> --}}

                    </div>
                </div>
                @endforeach

                {{-- OTHER --}}

                @foreach ($dm as $v)
                @php
                $dm_img = collect(\DB::select("SELECT * from mst_image where seq = 1 and product_id
                ='".$v->product_id."'"))->first();
                @endphp
                <input type="hidden" id="info_type{{$v->product_id}}" value="{{$v->type}}">

                <div class="pt-item">
                    <div class="pt-item-btn">
                        <button onclick="mVerifShow({{$v->cartlist_id}},'cart')" class="pt-btn js-remove-item">
                            <svg width="24" height="24" viewBox="0 0 24 24">
                                <use xlink:href="#icon-remove"></use>
                            </svg>
                        </button>
                        <div class="checkbox-group size-md pt-demo-border">
                            <input type="checkbox" onchange="set_total()" id="cck{{$v->product_id}}" value="{{$v->product_id}}" name="cck[]">
                            <label for="cck{{$v->product_id}}">
                                <span class="check"></span>
                                <span class="box"></span>
                            </label>
                        </div>
                    </div>
                    <div class="pt-item-img">
                        <a href="{{route("katalog.detail",["id"=>"$v->product_id","type"=>"$v->type"])}}"><img
                                src="{{asset("gallery/product/$dm_img->img")}}" alt=""></a>
                    </div>
                    <div class="pt-item-description">
                        <div class="pt-col">
                            <h6 class="pt-title"><a href="#">{{$v->product_name}}</a></h6>
                            <input type="hidden" id="name{{$v->product_id}}"  value="{{$v->product_name}}">
                            <input type="hidden" id="product_id{{$v->product_id}}" value="{{$v->product_id}}">
                            <ul class="pt-add-info">
                                <li>By Corellia</li>
                            </ul>
                        </div>
                        <div class="pt-col">
                            <div class="pt-price">Rp {{number_format($v->price,0,",",".")}}</div>
                            <input id="price{{$v->product_id}}" name="price{{$v->product_id}}" type="hidden"
                                value="{{$v->price}}">
                        </div>
                        <div class="pt-col">
                            <div class="pt-input-counter style-01">
                                <span class="minus-btn">
                                    <i class="la la-minus"></i>
                                </span>
                                <input id="count_product{{$v->product_id}}" onkeyup="set_qty_other(this.value,'{{$v->product_id}}','{{$v->type}}');"
                                    onchange="set_qty_other(this.value,'{{$v->product_id}}','{{$v->type}}');" type="text" maxlength="3"
                                    value="{{$v->qty}}" size="999">
                                    <span class="plus-btn">
                                        <i class="la la-plus"></i>
                                    </span>
                            </div>
                        </div>
                        <div class="pt-col">
                            <div id="t_view{{$v->product_id}}" class="pt-price">Rp{{number_format($v->price*$v->qty,0,",",".")}}
                            </div>
                            <input id="total_per{{$v->product_id}}" name="total_per{{$v->product_id}}" type="hidden"
                                value="{{$v->price*$v->qty}}">
                        </div>
                    </div>
                </div>
                @endforeach

                @php
                    // dd($d_website);
                @endphp

                @foreach ($d_website as $v)

                <input type="hidden" id="info_type{{$v->product_id}}" value="5">

                <div class="pt-item">
                    <div class="pt-item-btn">
                        <button onclick="mVerifShow({{$v->id}},'cart')" class="pt-btn js-remove-item">
                            <svg width="24" height="24" viewBox="0 0 24 24">
                                <use xlink:href="#icon-remove"></use>
                            </svg>
                        </button>
                        <div class="checkbox-group size-md pt-demo-border">
                            <input type="checkbox" onchange="set_total()" id="cck{{$v->product_id}}" value="{{$v->product_id}}" name="cck[]">
                            <label for="cck{{$v->product_id}}">
                                <span class="check"></span>
                                <span class="box"></span>
                            </label>
                        </div>
                    </div>
                    <div class="pt-item-img">
                        <a href="{{route("website.detail",["id"=>"$v->product_id","type"=>"5"])}}"><img
                                src="{{asset("gallery/product/$v->img")}}" alt=""></a>
                    </div>
                    <div class="pt-item-description">
                        <div class="pt-col">
                            <h6 class="pt-title"><a href="#">{{$v->product_name}}</a></h6>
                            <input type="hidden" id="name{{$v->product_id}}"  value="{{$v->product_name}}">
                            <input type="hidden" id="product_id{{$v->product_id}}" value="{{$v->product_id}}">
                            <input type="hidden" id="design_id{{$v->product_id}}" value="{{$v->id_website}}">

                            <ul class="pt-add-info mb-2">
                                {{-- <li>By Corellia</li> --}}
                                <li>Nama Website : <b></b></li>
                            </ul>

                            <span class="zn-point" style="font-size: 13px;">Corellia.id/{{$v->nama_website}}</span>
                        <br><br>
                            <a target="blank" href="{{route("website.preview",["id"=>"$v->id_website","name"=>"$v->nama_website"])}}" class="btn btn-block" style="display: inline;padding: 10px 20px;">
                                <span class="pt-text">
                                    PREVIEW WEBSITE
                                </span>
                            </a>
                        </div>
                        <div class="pt-col">
                            <div class="pt-price">Rp {{number_format($v->price,0,",",".")}}</div>
                            <input id="price{{$v->product_id}}" name="price{{$v->product_id}}" type="hidden"
                                value="{{$v->price}}">
                        </div>
                        <div class="pt-col">
                            <div class="pt-input-counter style-01">
                                <span class="minus-btn">
                                    <i class="la la-minus"></i>
                                </span>
                                <input id="count_product{{$v->product_id}}" onkeyup="set_qty_other(this.value,'{{$v->product_id}}','{{$v->type}}');"
                                    onchange="set_qty_other(this.value,'{{$v->product_id}}','{{$v->type}}');" type="text" maxlength="3"
                                    value="{{$v->qty}}" size="999">
                                    <span class="plus-btn">
                                        <i class="la la-plus"></i>
                                    </span>
                            </div>
                        </div>
                        <div class="pt-col">
                            <div id="t_view{{$v->product_id}}" class="pt-price">Rp{{number_format($v->price*$v->qty,0,",",".")}}
                            </div>
                            <input id="total_per{{$v->product_id}}" name="total_per{{$v->product_id}}" type="hidden"
                                value="{{$v->price*$v->qty}}">
                        </div>
                    </div>
                </div>
                @endforeach



                <div class="pt-shopcart-btn">
                    {{-- <div class="pt-col">
                            <a href="#" class="btn-link btn-lg">
                                <div class="pt-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24">
                                        <use xlink:href="#icon-arrow_large_left"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">Continue shopping</span>
                            </a>
                        </div>
                        <div class="pt-col">
                            <a href="#" class="btn-link btn-lg">
                                <div class="pt-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24">
                                        <use xlink:href="#icon-remove"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">Clear shopping cart</span>
                            </a>
                            <a href="#" class="btn-link btn-lg">
                                <div class="pt-icon">
                                    <svg width="24" height="24" viewBox="0 0 24 24">
                                        <use xlink:href="#icon-update"></use>
                                    </svg>
                                </div>
                                <span class="pt-text">Update cart</span>
                            </a>
                        </div> --}}
                </div>
            </div>
            <div class="pt-shopcart-wrapperbox">
                {{-- <form id="shopcartform" class="form-default" method="post" novalidate="novalidate" action="#"> --}}
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-6">
                        <div class="pt-shopcart-box">
                            <h6 class="pt-title">Estimasi Pengiriman</h6>
                            <p>
                                Pengiriman paling lambat akan diproses 1x24 Jam
                            </p>
                        </div>

                    </div>
                    <div class="col-12 col-md-8 col-lg-6">
                        <div class="pt-shopcart-total">
                            <div class="row">
                                <div class="mt-0 col-md-5">Sub Total Produk</div>
                                <div class="mt-0 col-md-1">Rp</div>
                                <div id="tmp_total" class="mt-0 col-md-6 text-right"></div>
                            </div>
                            <div class="row">
                                    <div class="mt-0 col-md-5">Potongan Harga</div>
                                    <div class="mt-0 col-md-1">Rp</div>
                                    <div class="mt-0 col-md-2 text-right">-</div>
                                    <div id="voucher_amount" class="mt-0 col-md-4 text-right">0</div>
                            </div>
                            <div class="row">

                                    <div class="mt-0 col-md-5">Diskon</div>
                                    <div class="mt-0 col-md-1">Rp</div>
                                    <div class="mt-0 col-md-2 text-right">-</div>
                                    <div id="voucher_percentage" class="mt-0 col-md-4 text-right">0</div>

                            </div>
                            {{-- <div class="row">

                                    <div class="mt-0 col-md-5">Ongkos Kirim</div>
                                    <div class="mt-0 col-md-1">Rp</div>
                                    <div class="mt-0 col-md-6 text-right">0</div>

                            </div> --}}
                            <input id="set_percentage" type="hidden" value="">

                            <div class="row" style="
                            border-top: 2px solid #ffffff1f;
                            margin-top: 15px;
                            padding-top: 15px;
                        ">

                                    <div class="mt-0 col-md-5">Grand Total Produk</div>
                                    <div class="mt-0 col-md-1">Rp</div>
                                    <div id="total" class="mt-0 col-md-6 text-right">0</div>

                            </div>


                            {{-- <div class="pt-price-01 mt-5">Total</div>
                            <div  class="pt-price-02">
                                Rp 0
                            </div> --}}
                            <input id="total_final" type="hidden" value="">

                            <div class="checkbox-group">
                                <input type="checkbox" id="checkBox41" name="checkbox" checked="">
                                {{-- <label for="checkBox41">
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            I agree with the terms and conditions
                                        </label> --}}
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-9">
                                <form id="form-voucher" class="form-default form-layout-01" method="post"
                                    novalidate="novalidate" action="#">
                                    <div class="form-group">
                                        {{-- <div class="pt-required mb-3">* Masukan Kode Voucher Untuk Mendapatkan Potongan Harga</div> --}}
                                        <input type="text" maxlength="20" id="voucher" name="voucher"
                                            class="form-control" id="creatFitstName" placeholder="Kode Voucher">
                                        <input type="hidden" id="set_voucher" value="">
                                        <div id="voucher_desc" style="font-size:12px;margin-bottom:5px;color:#c89c7d;">

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-3 text-right">
                                <a onclick="generateVoucher()" class="btn btn-sm btn-border">
                                    <span class="pt-text">
                                        PAKAI
                                    </span>
                                </a>
                            </div>

                            <div class="col-md-12">
                                <button onclick="setCheckout();" class="btn btn-block">
                                    <span class="pt-text">
                                        PROCEED TO CHECKOUT
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
</main>


@endif
@include('customer.action.cartlist_action')

@include('master.component.footer')
@stop
