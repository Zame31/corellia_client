<!DOCTYPE html>
<html lang="en">
    @include('master.head')
<body >
    {{-- <div id="loader-wrapper" class="zn-loader">
        <div class="loader">Loading...</div>
    </div> --}}
</div>
<main id="login_pat">
	<div class="pt-maintence">
		<div class="pt-maintence-header">
			<div class="container-fluid">
				<div class="pt-maintence-logo">
					<a href="https://corellia.id/">
						<h2 class="pt-title">Corellia</h2>
					</a>
				</div>
			</div>
		</div>
		<div class="pt-maintence-layout">
			<div class="container">
				<div class="pt-row row">
					<div class="pt-col col-lg-6">
						<div class="pt-box-top">
							<div class="pt-countdown_box_02">
								<div class="pt-countdown_inner">
									<div class="pt-countdow-title">Coming Soon</div>
									<div class="pt-countdown"
										data-date="2019-11-04"
										data-year="Yrs"
										data-month="Mths"
										data-week="Wk"
										data-day="DAYS"
										data-hour="HRS"
										data-minute="MINS"
										data-second="SECS"></div>
								</div>
							</div>
						</div>
						<div class="pt-box-center">
							<h1 class="pt-title">
								Your New Website Will Be Available Soon
							</h1>
							{{-- <strong class="pt-base-dark-color">Silahkan Selesaikan Pembayaranmu</strong> --}}

						</div>

					</div>
                <div class="pt-col col-lg-6"><img src="{{asset('img/logo.png')}}" alt=""></div>
				</div>
			</div>
		</div>
		<div class="pt-maintence-footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-lg-8">
						<div class="pt-maintence-copyright">
							&copy; 2019 Corellia
						</div>
					</div>
					<div class="col-12 col-lg-auto">
						<div class="pt-social-box">
							<ul class="pt-social-box-icon">
								<li><a target="_blank" href="http://www.facebook.com/">
									<svg width="11" height="18" viewBox="0 0 11 18">
										<use xlink:href="#icon-social_icon_facebook"></use>
								   </svg>
								</a></li>
								<li><a target="_blank" href="https://instagram.com/">
									<svg width="18" height="18" viewBox="0 0 18 18">
										<use xlink:href="#icon-social_icon_instagram"></use>
								   </svg>
								</a></li>
								<li><a target="_blank" href="#">
									<svg width="18" height="19" viewBox="0 0 18 19">
										<use xlink:href="#icon-social_icon_1"></use>
								   </svg>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<div class="modal fade" id="modalGetAccess" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-body noindent">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
				</div>
				<div class="pt-modal-get-access">
					<form class="form-default" method="post" novalidate="novalidate" action="#">
						<div class="form-group">
							<label for="inputPassword">Password:</label>
							<input type="password" name="name" class="form-control" id="inputPassword" placeholder="Password">
						</div>
						<button type="submit" class="btn btn-block">ENTER</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
