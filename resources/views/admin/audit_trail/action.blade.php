<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

var act_url = '{{ route('audit.data_audit_trail') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        // { data: 'id', name: 'id' },
        { data: 'event', name: 'event' },
        { data: 'auditable_type', name: 'auditable_type' },
        { data: 'new_values', name: 'new_values' },
        { data: 'old_values', name: 'old_values' },
        { data: 'created_at', name: 'created_at' }
      

    ]
});

</script>
