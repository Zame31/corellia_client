

<div class="modal fade" id="modal_trace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Progres Pembelian</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <h6>No Pembelian</h6>
            <div id="no_pembelian" style="font-weight:bold;"></div>
            <br>
            <div class="kt-list-timeline">
                <div class="kt-list-timeline__items" id="list_trace">
                </div>
            </div>
        </div>
      
            <div class="modal-footer">
                <div class="row" style="width: 100%;">
                    <div class="col-md-8">
                        <textarea maxlength="200" placeholder="Tulis Status Pengiriman Terbaru" class="form-control"
                        id="note_trace" name="note_trace" rows="3"></textarea>

                    </div>
                    <div class="col-md-4">
                        <button onclick="addTrace()" type="button" class="btn btn-primary">Tambahkan</button>
                    </div>
                </div>
            </div>

      </div>
    </div>
  </div>
