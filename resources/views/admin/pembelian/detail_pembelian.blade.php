@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Detail Pembelian </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Pembelian </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>

            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">


            <a  onclick="loadNewPage(`{{ route('pembelian.index') }}?type={{$type}}`)"
                    class="btn kt-subheader__btn-secondary" aria-expanded="false" style="color:#ffffff;">
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>



<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--head-lg">

        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-5">
                    {{-- <h4 class="mb-4 mt-5" style="color: #1dc9b7;">Detail Pembelian</h4> --}}
                    <div class="row mt-4" style="
                    color: #404040;
                ">
                        <div class="col-md-6">
                            <div class="mb-4">
                                <h6>Kode Pembelian</h6>
                                <h5> {{$data->no_pembelian}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Pembelian</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($data->created_at))}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Jatuh Tempo</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($data->end_date))}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Pengiriman</h6>
                                <h5> {{($data->pengiriman == 'dikirim') ? "Dikirim":"Diambil di Gallery"}}</h5>
                            </div>
                            <div class="mb-4">
                                <h6>Ongkos Kirim</h6>
                                <h5>Rp {{number_format($data->ongkir,0,",",".")}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Potongan Harga</h6>
                                <h5>Rp {{number_format($data->discount,0,",",".")}} </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-4">
                                <h6>Transfer Ke Bank</h6>
                                <h5> {{$data->bank_name}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Nama Customer</h6>
                                <h5> {{$data->cust_name}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>No Telpon</h6>
                                <h5> {{$data->phone_no}} </h5>
                            </div>

                            <div class="mb-4">
                                <h6>Total Pembayaran</h6>
                                <h5>Rp {{number_format($data->total_pembelian,0,",",".")}} </h5>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Pembelian
                                </h6>
                                <h5 style="font-size:15px;">
                                    {{($data->note) ? $data->note:"-"}}
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Corellia
                                </h6>
                                <h5 style="font-size:15px;">
                                    {{($data->response_admin) ? $data->response_admin:"-"}}
                                </h5>
                            </div>
                        </div>
                        @if ($type == 2 || $type == 3)
                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Undangan
                                </h6>
                                <textarea name="note_undangan" id="note_undangan"
                                class="form-control" cols="30" rows="10">{!!$data->note_undangan!!}</textarea>
                                <button onclick="simpanNote('{{$data->no_pembelian}}')" type="button" class="mt-2 btn btn-success btn-elevate btn-elevate-air" style="
                                width: 100%;
                            ">Simpan Catatan</button>
                            </div>
                            <div class="mb-4">
                                @if ($data->designer)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="zn-head-list">Designer :  {{$data->designer_name}} </h6>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="zn-head-list">Pilih Designer</h6>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="pilihdesigner" id="pilihdesigner">
                                                @php
                                                    $list_designer = \DB::table('admin')->where('role',3)->get();
                                                @endphp
                                                @foreach ($list_designer as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">

                                            <button onclick="simpanDesigner('{{$data->no_pembelian}}')" type="button" class=" btn btn-success btn-elevate btn-elevate-air" style="
                                                width: 100%;
                                            ">Pilih Designer</button>
                                        </div>
                                    </div>
                                @endif


                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-7">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th class="text-right">Harga Produk</th>
                                <th class="text-right">Jumlah</th>
                                <th class="text-right" style="width:150px;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            @foreach ($d_undangan as $v)
                            @php
                            if ($v->qty >= 200 && $v->qty <= 299) {
                                $disc = 2000;
                            }else if($v->qty >= 300 && $v->qty <= 499){
                                $disc = 1000;
                            }else if($v->qty >= 500 && $v->qty <= 699){
                                $disc = 500;
                            }else if($v->qty >= 700){
                                $disc = 0;
                            }
                        @endphp
                            @php
                                    $free_finishing = '';
                                    $free_additional = '';
                                    $add_free_finishing = explode("|",$v->free_finishing);
                                    $add_free_additional = explode("|",$v->free_additional);
                                    $len_fin = count($add_free_finishing);
                                    $len_add = count($add_free_additional);

                                    for ($i=0; $i < $len_fin-1; $i++) {
                                        if ($i != $len_fin-1) {
                                            $adz = collect(\DB::select("SELECT * from ref_finishing where id = '".$add_free_finishing[$i]."'"))->first();
                                            $free_finishing .= "<br>- ".$adz->name;
                                        }
                                    }

                                    for ($i=0; $i < $len_add-1; $i++) {
                                        if ($i != $len_add-1) {
                                            $adz = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_free_additional[$i]."'"))->first();
                                            $free_additional .= "<br>- ".$adz->additional_name;
                                        }
                                    }
                            @endphp
                                <tr>
                                    <td>
                                        <b>{{$v->product_name}}</b>
                                    <br>
                                    <br>
                                    <span class="mt-2 btn btn-secondary btn-sm">
                                        {{$v->style_size}} </span>
                                    <span class="mt-2 btn btn-secondary btn-sm">Warna :
                                        {{$v->color_name}} </span><br>
                                    {{-- <span class="mt-2 btn btn-secondary btn-sm">Waxseal :
                                        {{$v->waxseal_name}} </span> --}}

                                    <span class="mt-2 btn btn-secondary btn-sm">Foil Color :
                                        {{$v->foilcolor_name}} </span>
                                    <br>
                                    {{-- <span class="mt-2 btn btn-secondary btn-sm">
                                        Emboss:{{($v->emboss == 1) ? "Digunakan":"Tidak Digunakan"}}</span> --}}

                                    @if ($v->material2)
                                    <span class="mt-2 btn btn-secondary btn-sm">Material
                                        Inner:<b>{{$v->material1}}</b></span>
                                    <span class="mt-2 btn btn-secondary btn-sm">Material
                                        Outer:<b>{{$v->material2}}</b></span>
                                    @else
                                    <span class="mt-2 btn btn-secondary btn-sm">Material:{{$v->material1}}</span>
                                    @endif
                                    <br><span class="mt-2 btn btn-secondary btn-sm text-left"> Free
                                        Finishing: {!!$free_finishing!!}</span>
                                    <span class="mt-2 btn btn-secondary btn-sm text-left"> Free Additional
                                        Item: {!!$free_additional!!}</span>
                                    </td>
                                    <td class="text-right"> Rp {{number_format($v->price+$disc,0,",",".")}}  </td>
                                    <td class="text-right"> {{number_format($v->qty,0,",",".")}}  </td>
                                    <td class="text-right"> Rp {{number_format($v->totalprice,0,",",".")}}  </td>
                                </tr>
                                <tr>
                                    <td class="text-left">
                                        <span style="font-size: 12px;" class="zn-point">Waxseal : {{$v->waxseal_name}}</span>
                                        </td>
                                        <td class="text-right"> Rp {{number_format($v->harga_waxseal,0,",",".")}}  </td>
                                        <td class="text-right"> {{number_format($v->qty,0,",",".")}}  </td>
                                        <td class="text-right"> Rp {{number_format($v->harga_waxseal*$v->qty,0,",",".")}}  </td>
                                </tr>
                                @php
                            $add_item = explode("|",$v->additional_user);
                            $add_item_count = explode("|",$v->additional_user_count);
                            $len = count($add_item);
                            @endphp

                                @foreach ($add_item as $key => $vitem)
                                    @php

                                    $ad = collect(\DB::select("SELECT * from
                                    ref_additional
                                    where id::varchar = '".$add_item[$key]."'"))->first();
                                    @endphp
                                    @if ($key != $len - 1)
                                    <tr>
                                        <td>{{$ad->additional_name}}</td>
                                        <td class="text-right">Rp {{number_format($ad->price,0,",",".")}}</td>
                                        <td class="text-right">{{$add_item_count[$key]}}</td>
                                        <td class="text-right">Rp {{number_format($ad->price*$add_item_count[$key],0,",",".")}}
                                        </td>
                                    </tr>

                                    @endif

                                @endforeach

                                    @php
                                         $add_item = explode("|",$v->additional);
                                        $len = count($add_item);
                                        $additional = '';
                                    @endphp

                                    @foreach ($add_item as $key => $vitem)
                                    @php
                                           $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$key]."'"))->first();
                                    @endphp
                                    @if ($key != $len - 1)
                                    <tr>
                                        <td>{{$ad->additional_name}}</td>
                                        <td class="text-right">Rp {{number_format($ad->price,0,",",".")}}</td>
                                        <td class="text-right">1</td>
                                        <td class="text-right"><del> Rp {{number_format($ad->price,0,",",".")}}</del></td>
                                    </tr>
                                    @endif
                                    @endforeach


                            @endforeach

                            @foreach ($d_other as $v)
                            <tr>
                                <td>
                                    {{$v->product_name}}
                                </td>
                                <td class="text-right"> Rp {{number_format($v->price,0,",",".")}}  </td>
                                <td class="text-right"> {{number_format($v->qty,0,",",".")}}  </td>
                                <td class="text-right"> Rp {{number_format($v->totalprice,0,",",".")}}  </td>
                            </tr>

                        @endforeach
                            {{-- @php
                                dd($d_design_order);
                            @endphp --}}
                        @foreach ($d_design_order as $v)
                            <tr>
                                <td>
                                   ({{$v->product_id}}) {{$v->judul}}
                                </td>
                                <td class="text-right"> Rp {{number_format($v->totalprice/$v->qty,0,",",".")}}  </td>
                                <td class="text-right"> {{number_format($v->qty,0,",",".")}}  </td>
                                <td class="text-right"> Rp {{number_format($v->totalprice,0,",",".")}}  </td>
                            </tr>

                        @endforeach

                        @foreach ($d_website as $v)
                            <tr>
                                <td style="padding-bottom: 20px;">{{$v->product_name}}<br><br>
                                    <a target="blank" href="{{route("website.preview",["id"=>"$v->id_website","name"=>"$v->nama_website"])}}"
                                        class="btn btn-label btn-label-info btn-sm btn-bold">Preview Website
                                    </a>
                                </td>

                                <td class="text-right"> Rp {{number_format($v->totalprice/$v->qty,0,",",".")}}  </td>
                                <td class="text-right"> {{number_format($v->qty,0,",",".")}}  </td>
                                <td class="text-right"> Rp {{number_format($v->totalprice,0,",",".")}}  </td>
                            </tr>

                        @endforeach


                        </tbody>
                    </table>


                </div>

            </div>
        </div>
    </div>
</div>
<script>

function simpanNote(no_pembelian) {


$.ajax({
    type: 'POST',
    url: '{{ route('customer.set_note_undangan') }}',
    data: {
        note_undangan:$('#note_undangan').val(),
        no_pembelian:no_pembelian,
        is_admin:'t'
    },

    beforeSend: function () {
        loadingPage();
    },
    success: function (response) {
        endLoadingPage();
        toastr.success("Berhasil Menyimpan Catatan Undangan");

    }

}).done(function (msg) {
}).fail(function (msg) {
    endLoadingPage();
});

}

function simpanDesigner(no_pembelian) {


$.ajax({
    type: 'POST',
    url: '{{ route('staff.set_designer') }}',
    data: {
        pilihdesigner:$('#pilihdesigner').val(),
        no_pembelian:no_pembelian
    },

    beforeSend: function () {
        loadingPage();
    },
    success: function (response) {

        endLoadingPage();
        toastr.success("Berhasil Memilih Designer");
        location.reload();
    }

}).done(function (msg) {
}).fail(function (msg) {
    endLoadingPage();
});

}

</script>
@stop
