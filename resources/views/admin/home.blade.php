@section('content_admin')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Dashboard </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboard</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>

            </div>
        </div>
 
    </div>
</div>
<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
    <div class="kt-portlet__head kt-portlet__head--noborder">
        <div class="kt-portlet__head-label">
            {{-- <h3 class="kt-portlet__head-title">
                Grafik Penjualan
            </h3> --}}
        </div>
        <div class="kt-portlet__head-toolbar">
           
        </div>
    </div>
    <div class="kt-portlet__body">


        <!--begin::Widget 6-->
        <div class="kt-widget15">
            {{-- <div class="kt-widget15__chart">
                <canvas id="kt_chart_sales_stats" style="height:160px;"></canvas>
            </div> --}}
            <div class="crt" class="col-md-12">
                <canvas id="crt" height="100"></canvas>

            </div>
            <div class="kt-widget15__items kt-margin-t-40">
                <div class="row">
                    <div class="col">
                        <div class="kt-widget15__item">
                            <span id="d_undangan" class="kt-widget15__stats">
                                0
                            </span>
                            <span class="kt-widget15__text">
                                Penjualan Undangan 
                            </span>
                            <div class="kt-space-10"></div>
                            <div class="progress kt-widget15__chart-progress--sm">
                                <div class="progress-bar " role="progressbar" style="width: 100%;background-color: #00c5dc !important;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="kt-widget15__item">
                            <span id="d_produk_lain" class="kt-widget15__stats">
                                0
                            </span>
                            <span class="kt-widget15__text">
                                Penjualan Produk Lain
                            </span>
                            <div class="kt-space-10"></div>
                            <div class="progress kt-progress--sm">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="kt-widget15__item">
                            <span id="d_website" class="kt-widget15__stats">
                                0
                            </span>
                            <span class="kt-widget15__text">
                                Penjualan Website
                            </span>
                            <div class="kt-space-10"></div>
                            <div class="progress kt-progress--sm">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                  
                    <div class="col">
                        <div class="kt-widget15__item">
                            <span id="d_pembayaran" class="kt-widget15__stats">
                                0
                            </span>
                            <span class="kt-widget15__text">
                                Total Pembayaran Lunas
                            </span>
                            <div class="kt-space-10"></div>
                            <div class="progress kt-progress--sm">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col">
                        <div class="kt-widget15__item">
                            <span id="d_proses" class="kt-widget15__stats">
                                0
                            </span>
                            <span class="kt-widget15__text">
                                Jumlah Pesanan Dalam Proses
                            </span>
                            <div class="kt-space-10"></div>
                            <div class="progress kt-progress--sm">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

        <!--end::Widget 6-->
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Pembelian
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    
                </div>
            </div>
            <div class="kt-portlet__body">
        
                <!--begin::widget 12-->
                <div class="kt-widget4">
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-file kt-font-info"></i>
                        </span>
                        <a onclick="loadNewPage('{{ route('pembelian.index') }}?type=0')" style="cursor: pointer;" class="kt-widget4__title kt-widget4__title--light">
                            Pembelian Baru
                        </a>
                        <span id="d_p1" class="kt-widget4__number kt-font-info">0</span>
                    </div>
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-file  kt-font-success"></i>
                        </span>
                        <a onclick="loadNewPage('{{ route('pembelian.index') }}?type=5')" style="cursor: pointer;" class="kt-widget4__title kt-widget4__title--light">
                            Menunggu Pembayaran.
                        </a>
                        <span id="d_p2" class="kt-widget4__number kt-font-success">0</span>
                    </div>
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-file kt-font-danger"></i>
                        </span>
                        <a onclick="loadNewPage('{{ route('pembelian.index') }}?type=1')" style="cursor: pointer;" class="kt-widget4__title kt-widget4__title--light">
                            Menunggu Konfirmasi Pembayaran
                        </a>
                        <span id="d_p3" class="kt-widget4__number kt-font-danger">0</span>
                    </div>
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-file kt-font-warning"></i>
                        </span>
                        <a onclick="loadNewPage('{{ route('pembelian.index') }}?type=2')" style="cursor: pointer;" class="kt-widget4__title kt-widget4__title--light">
                            Dibayar Sebagian
                        </a>
                        <span id="d_p4" class="kt-widget4__number kt-font-warning">0</span>
                    </div>
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-file kt-font-brand"></i>
                        </span>
                        <a onclick="loadNewPage('{{ route('pembelian.index') }}?type=3')" style="cursor: pointer;" class="kt-widget4__title kt-widget4__title--light">
                            Dibayar Lunas
                        </a>
                        <span id="d_p5" class="kt-widget4__number kt-font-brand">0</span>
                    </div>
                </div>
        
                <!--end::Widget 12-->
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Produk Terlaris
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    
                </div>
            </div>
            <div class="kt-portlet__body">
                <div id="list_terlaris" class="kt-widget4">
                </div>
            </div>
        </div>
    </div>
</div>


<script src={{asset('js/chart.js')}}></script>

<script>
	get_dashboard("all");


	function get_dashboard(filter) {
		var act_url = '{{ route('data.dashboard', ':id') }}';
        act_url = act_url.replace(':id', filter);

        $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingPage();
           },
        success: function (res) {

			var data = $.parseJSON(res);

			$('#d_undangan').html('Rp '+numFormat(data['d_undangan']['d']));
			$('#d_produk_lain').html('Rp '+numFormat(data['d_produk_lain']['d']));
			$('#d_proses').html(numFormat(data['d_proses']['d']));
			$('#d_pembayaran').html('Rp '+numFormat(data['d_pembayaran']['d']));
			$('#d_website').html('Rp '+numFormat(data['d_website']['d']));
			$('#d_p1').html(numFormat(data['d_p1']['d']));
			$('#d_p2').html(numFormat(data['d_p2']['d']));
			$('#d_p3').html(numFormat(data['d_p3']['d']));
			$('#d_p4').html(numFormat(data['d_p4']['d']));
			$('#d_p5').html(numFormat(data['d_p5']['d']));

            var terlaris = data['d_terlaris'];

            // console.log(terlaris);
            
            $('#list_terlaris').html('');

            for (let ii = 0; ii < terlaris.length; ii++) {

                $('#list_terlaris').append(`
                    <div class="kt-widget4__item">
                        <span class="kt-widget4__icon">
                            <i class="flaticon2-shopping-cart-1 kt-font-success"></i>
                        </span>
                        <span class="kt-widget4__title kt-widget4__title--light">
                            `+terlaris[ii]['product_name']+`
                        </span>
                        <span class="kt-widget4__number kt-font-success">`+terlaris[ii]['d']+` Pembelian</span>
                    </div>
                `);
                
            }
		
            var data_graf =[];
			var data_color = ["#00C5DC","#F4516C","#FFB822","#8859E0","#0C5484","#66BB6A","#00838F","#e57373"];
			var data_tanggal = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];


            var color_i = 0;

                $.each(data['grafik'], function (k,v) {

                data_graf.push({

                    label: k,
                    data: v,
                    fill: false,
                    backgroundColor: data_color[color_i],
                    borderColor: data_color[color_i],
                });

                color_i++;
            });



			var ctx = document.getElementById("crt");

			var myChart = new Chart(ctx, {
				type: 'line',
				data: {
					labels: data_tanggal,
					datasets: data_graf
				},
				options: {
							responsive: true,
							title: {
								display: true,
								text: 'Grafik Jumlah Pembelian'
							},
							tooltips: {
								mode: 'index',
								intersect: false,
							},
							hover: {
								mode: 'nearest',
								intersect: true
							},
							scales: {
								xAxes: [{
									display: true,
									scaleLabel: {
										display: true,
										labelString: 'Bulan'
									}
								}],
								yAxes: [{
									display: true,
									scaleLabel: {
										display: true,
										labelString: 'Frekuensi Transaksi'
									}
								}]
							}
						}
			});

        }
    }).done(function( msg ) {
        endLoadingPage();
    });
	}




</script>

@stop
