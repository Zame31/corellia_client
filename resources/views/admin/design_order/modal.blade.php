<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Undangan</label>
                                <select class="form-control" name="product_id" id="product_id">
                                    <option value="">Pilih Undangan</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_undangan");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->product_id}}">{{$item->product_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                          
                            <div class="form-group">
                                <label>Warna</label>
                                <select class="form-control" name="color" id="color">
                                    <option value="">Pilih Warna</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_color");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}"> {{$item->color}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-2">
                                <label>Shape</label>
                            </div>
                            <div class="row">
                                
                                
                                @for ($i = 1; $i <= 7; $i++)
                                    <div class="col-md-4">
                                        <label class="kt-radio kt-radio--bold kt-radio--success">
                                        <input  value="{{$i}}" type="radio" name="shape">
                                            <img width="50px" src="{{ asset('gallery/shape/shape'.$i.'.png') }}" alt="" srcset="">
                                            <span style="margin-top: 25px;"></span>
                                        </label>
                                    </div>
                                @endfor
                               
                            </div>

                        </div>
                        <div class="col-md-6">
                          
                            <div class="col-md-12 mb-5 mt-3">
                                <h5>Upload Gambar Undangan</h5>
                                <div>Format Gambar PNG,JPG maksimal 2 Mb.</div>          
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                            <div id="ava1" class="kt-avatar__holder" style="width: 360px;
                                            height: 360px;background-size: 360px 360px;background-image: url({{asset('admin/img/no_img.png')}})"></div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Ubah Gambar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="img1" id="img1">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Hapus Gambar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                       
                                </div>
                            </div>

                        </div>
                     
                    </div>
                   


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
