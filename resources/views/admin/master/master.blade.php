<!DOCTYPE html>
<html lang="en">
@include('admin.master.head')
<body
    style="background-image: url({{asset('admin/img/bg.jpg')}}); background-size: 100%;
    background-position-y: -350px;"
    class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="index&demo=demo4.html">
                <img alt="Logo" src="{{asset('assets/media/logos/logo-4-sm.png')}}" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more-1"></i></button>
        </div>
    </div>

      <!-- Loading -->
      <div id="loading">
        <div class="lds-facebook">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    @include('admin.master.component')

    @include('admin.pembelian.modal_acc_pembelian')
    
    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                    <div class="kt-container ">

                        <!-- begin:: Brand -->
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            {{-- <a class="kt-header__brand-logo" href="?page=index&demo=demo4">
                                <img alt="Logo" src="assets/media/logos/logo-4.png"
                                    class="kt-header__brand-logo-default" />
                                <img alt="Logo" src="assets/media/logos/logo-4-sm.png"
                                    class="kt-header__brand-logo-sticky" />
                            </a> --}}
                        </div>
                        <!-- end:: Brand -->
                        <!-- begin: Header Menu -->
                        @include('admin.master.menu')
                        <!-- end: Header Menu -->

                        <!-- begin:: Header Topbar -->
                        <div class="kt-header__topbar kt-grid__item">
                            <!--begin: Notifications -->
                            <div class="kt-header__topbar-item dropdown">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    <span onclick="read_notif({{{Auth::user()->id}}})" class="kt-header__topbar-icon kt-pulse kt-pulse--light">
                                    

                                        <i class="flaticon2-bell-alarm-symbol"></i>
                                        <span class="kt-pulse__ring"></span>
                                    </span>

                                    <span id="count_notif_admin" class="kt-badge kt-badge--light notif-num" >0</span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                                    <form>

                                        <!--begin: Head -->
                                        <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url({{asset('admin/assets/media/misc/bg-1.jpg')}})">
                                            <h3 class="kt-head__title " style="padding-bottom: 30px;">
                                                Notifications
                                                &nbsp;
                                                <span class="btn btn-success btn-sm btn-bold btn-font-md" id="count_notif_admin_in">0</span>
                                            </h3>
                                        </div>

                                        <!--end: Head -->
                                        <div class="tab-content">
                                        <div id="list_notif_admin" class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" 
                                        data-scroll="true" data-height="300" data-mobile-height="200">
                                                   
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!--end: Notifications -->
                            <!--begin: User bar -->
                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    
                                    <span class="kt-header__topbar-welcome">Hello,</span>
                                    <span class="kt-header__topbar-username">{{{Auth::user()->username}}} </span>
                                    <span class="kt-header__topbar-icon"><b><i class="flaticon2-user"></i></b></span>
                                    <img alt="Pic" src="{{asset('admin/assets/media/users/300_21.jpg')}}" class="kt-hidden" />
                                </div>
                                <div
                                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                    <!--begin: Head -->
                                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                        style="background-image: url({{asset('admin/assets/media/misc/bg-1.jpg')}})">
                                        <div class="kt-user-card__avatar">
                                            <img class="kt-hidden" alt="Pic" src="{{asset('admin/assets/media/users/300_25.jpg')}}" />
                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                            <span
                                                class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">S</span>
                                        </div>
                                        <div class="kt-user-card__name">
                                                {{{Auth::user()->name}}}
                                        </div>
                                    </div>
                                    <!--end: Head -->
                                    <!--begin: Navigation -->
                                    <div class="kt-notification">

                                        <div class="kt-notification__custom kt-space-between">
                                            <form id="formlogout" action="{{ route('admin.logout') }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="submit" value="Sign Out" class="btn btn-label btn-label-brand btn-sm btn-bold">
                                           </form>

                                           <button onclick="changePassword('Ubah Password','Ubah password demi keamanan akun anda')" class="btn btn-label btn-label-danger btn-sm btn-bold">Change Password</button>
                            
                                        </div>
                                    </div>
                                    <!--end: Navigation -->
                                </div>
                            </div>
                            <!--end: User bar -->
                        </div>
                        <!-- end:: Header Topbar -->
                    </div>
                </div>

                <!-- end:: Header -->
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                    id="kt_body">
                    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                        id="kt_content">
                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            <div id="pageLoadAdmin">
                                @yield('content_admin')
                            </div>
                        </div>

                        <!-- end:: Content -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <script type="text/javascript">
    getNotifAdmin({{{Auth::user()->id}}});
      
        var pusher = new Pusher('b82d8d49fc44a567b6fe', {
          cluster: 'ap3',
          forceTLS: true
        });
    
        // Subscribe to the channel we specified in our Laravel Event
        var channel = pusher.subscribe('status-liked');
    
        // Bind a function to a Event (the full Laravel class)
        channel.bind('App\\Events\\StatusLiked', function(data) {

           
            getNotifAdmin({{{Auth::user()->id}}});
            
        });


        function getNotifAdmin(id) {
            
            $.ajax({
                type: 'POST',
                url: '{{ route('admin_notif.get') }}',
                data: {id : id},
                beforeSend: function () {
                    // loadingModal();
                },
                success: function (response) {
                    
                    toastr.success(response.jumlah+" Pemberitahuan Baru");
                    if (response.jumlah == 0) {
                        $('#count_notif_admin').fadeOut();
                    }else {
                        $('#count_notif_admin').fadeIn();
                    }

                    $('#count_notif_admin').html(response.jumlah);
                    $('#count_notif_admin_in').html(response.jumlah+" Baru");

                    $('#list_notif_admin').html('');

                    $.each(response.rm, function (k,v) {
                        
                        console.log(v.type);
                        
                        var link = '';
                        if (v.type == '77') {
                            link = "{{ route('design_fix.index') }}";
                        }else {
                            link = "{{ route('pembelian.index') }}?type="+v.type;
                        }
                        
                
                    $('#list_notif_admin').append(`<a style="cursor: pointer;" onclick="loadNewPage('`+link+`')"  class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-line-chart kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                `+v.notif+`
                            </div>
                            <div class="kt-notification__item-time">
                                `+convertDateFormat(v.created_at)+`
                            </div>
                        </div>
                    </a>`);
                    });
        
                    console.log(response.rm);
                   
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                // endLoadingModal();
                // $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
            });
            
        }

        
        function read_notif(id) {

            $.ajax({
            type: 'POST',
            url: '{{ route('admin_notif.read_notif') }}',
            data: {id : id},
            beforeSend: function () {
                // loadingPage();
            },

                success: function (response) {
                    console.log(response);
                    $('#count_notif_admin').html(response.jumlah);
                    $('#count_notif_admin_in').html(response.jumlah+" Baru");

                    if (response.jumlah == 0) {
                        $('#count_notif_admin').fadeOut();
                    }else {
                        $('#count_notif_admin').fadeIn();
                    }
                }

                }).done(function (msg) {
                
                }).fail(function (msg) {
                });

        }
      </script>

</body>
</html>
