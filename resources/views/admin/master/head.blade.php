<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Corellia</title>
    <meta name="description" content="Akuntansi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- <link rel="shortcut icon" href="{{asset('admin/img/logo.jpg')}}" /> --}}
    <link href="{{ asset('admin/css/fonts.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/custom/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/css/pages/invoices/invoice-1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet" >
    <link href="{{asset('admin/css/myStyle.css')}}" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css'>

    <script src="{{asset('admin/assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('admin/assets/plugins/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/js/global/integration/plugins/datatables.init.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/jszip/dist/jszip.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/pdfmake/build/pdfmake.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/pdfmake/build/vfs_fonts.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-buttons/js/buttons.print.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
	<script src="{{asset('admin/assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/chartjs.js')}}"></script>
    <script src="{{asset('admin/js/_global.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/js/jspdf.debug.js')}}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js'></script>
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script>
        $(document).on("click", '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        var base_url = "{{ url('/') }}"+"/";
        console.log(base_url);
        var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#591df1",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
            };
         
            function changePassword(head,textPas) {
                var text = textPas+`
                <br><br>
                <form id="identicalForm" class="form-horizontal">
                    <div class="form-group">

                        <div class="col-sm-12">
                            <input type="password" placeholder="Password Baru" class="form-control" name="newpassword" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="password" placeholder="Ketik Ulang Password Baru"  class="form-control" name="confirmPassword" />
                        </div>
                    </div>
                </form>
                `;
                var action = `<button onclick="znIconboxClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Close</button>
                <button onclick="storePassword()" type="submit"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Simpan</button>`;

                znIconbox(head,text,action,'password');

                $('#identicalForm').bootstrapValidator({
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            newpassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'Tidak Boleh Kosong'
                                    },
                                    regexp: {
                                        regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[$@!%?&]).{8,}$/,
                                        message: 'Password minimal 8 karakter terdiri dari huruf, huruf Kapital, angka dan spesial karakter. Contoh : Admin2019@'
                                    }
                                }
                            },
                            confirmPassword: {
                                validators: {
                                    notEmpty: {
                                    message: 'Tidak Boleh Kosong'
                                },
                                    identical: {
                                        field: 'newpassword',
                                        message: 'Tidak Sama Dengan Password Yang Dibuat'
                                    }
                                }
                            }
                        }
                    });

                    $('#identicalForm').on('keyup keypress', function (e) {
                        var keyCode = e.keyCode || e.which;
                        if (keyCode === 13) {
                            storePassword();
                        }
                    });
            }

          

            function storePassword() {
                var validateProduk = $('#identicalForm').data('bootstrapValidator').validate();
                if (validateProduk.isValid()) {

                    var formData = document.getElementById("identicalForm");
                    var objData = new FormData(formData);

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('user.changePassword') }}',
                        data: objData,
                        dataType: 'JSON',
                        contentType: false,
                        cache: false,
                        processData: false,

                        beforeSend: function () {
                            loadingModal();
                        },
                        success: function (response) {
                            endLoadingModal();
                            znIconboxClose();
                            toastr.success('Berhasil Merubah Password');
                            $( "#formlogout" ).submit();
                        }

                    }).done(function (msg) {
                    }).fail(function (msg) {
                        endLoadingModal();
                        znIconboxClose();
                        toastr.error("Terjadi Kesalahan");
                    });
                }
            }

            function checkPassword() {
                $.ajax({
                    url: '{{ route('password.check') }}',
                    type: 'GET',
                    beforeSend: function() {
                    },
                    success: function (res) {
                        var data = $.parseJSON(res);

                        console.log(data);

                        if (data == "1") {
                            changePassword('Selamat Datang','Anda masih menggunakan password default dari sistem, ubah password demi keamanan Akun anda');
                        }
                    }
                }).done(function( msg ) {
                });
            }

            $(document).ready(function() {
                checkPassword();
            });
    </script>
</head>
