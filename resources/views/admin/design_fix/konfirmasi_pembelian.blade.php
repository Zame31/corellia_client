@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Detail Design Fix </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Design Fix </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>

            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @if ($data->status_pengerjaan_design == 1)
                @else
                <a style="color:#ffffff;"
                    onclick="confAct('design_selesai','design_selesai?no_pembelian={{$data->no_pembelian}}','{{ route('design_fix.index') }}');"
                    class="btn btn-success kt-subheader__btn-options" aria-expanded="false">
                    Konfirmasi Pengerjaan Selesai
                </a>
                @endif


                <a style="color:#ffffff;" onclick="loadNewPage(`{{ route('design_fix.index') }}`)"
                    class="btn kt-subheader__btn-secondary" aria-expanded="false">
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--head-lg">

        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12">
                    @if ($data->status_pengerjaan_design == 1)
                    <span class="btn btn-label-success">Pengerjaan Design Telah Selesai</span>
                    @endif
                </div>
                <div class="col-md-5">
                    {{-- <h4 class="mb-4 mt-5" style="color: #1dc9b7;">Detail Pembelian</h4> --}}
                    <div class="row mt-4" style="
                    color: #404040;
                ">
                        <div class="col-md-6">
                            <div class="mb-4">
                                <h6>Kode Pembelian</h6>
                                <h5> {{$data->no_pembelian}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>Tanggal Pembelian</h6>
                                <h5> {{date('d-m-Y H:i:s',strtotime($data->created_at))}} </h5>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-4">
                                <h6>Nama Customer</h6>
                                <h5> {{$data->cust_name}} </h5>
                            </div>
                            <div class="mb-4">
                                <h6>No Telpon</h6>
                                <h5> {{$data->phone_no}} </h5>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Pembelian
                                </h6>
                                <h5 style="font-size:15px;">
                                    {{$data->note}}
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Corellia
                                </h6>
                                <h5 style="font-size:15px;">
                                    {{($data->response_admin) ? $data->response_admin:"-"}}
                                </h5>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="mb-4">
                                <h6 class="zn-head-list">Catatan Undangan
                                </h6>
                                <h5 style="font-size:15px;">
                                    <textarea readonly class="form-control" style="height:600px;" cols="30" rows="10">{!!$data->note_undangan!!}</textarea>

                                </h5>


                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-7">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                <th>Produk</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($d_undangan as $v)
                            @php
                                $additional_list = '';
                                $add_item = explode("|",$v->additional_user);
                                $add_item_count = explode("|",$v->additional_user_count);
                                $len = count($add_item);
                            @endphp

                            @foreach ($add_item as $key => $vitem)
                                @php
                                $ad = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_item[$key]."'"))->first();

                                if ($key != $len - 1) {
                                    $additional_list .= "<br>- ".$ad->additional_name;
                                }
                                @endphp
                            @endforeach

                            @php
                                $free_finishing = '';
                                $free_additional = '';
                                  $add_free_finishing = explode("|",$v->free_finishing);
                                    $add_free_additional = explode("|",$v->free_additional);
                                    $len_fin = count($add_free_finishing);
                                    $len_add = count($add_free_additional);

                                    for ($i=0; $i < $len_fin-1; $i++) {
                                        if ($i != $len_fin-1) {
                                            $adz = collect(\DB::select("SELECT * from ref_finishing where id = '".$add_free_finishing[$i]."'"))->first();
                                            $free_finishing .= "<br>- ".$adz->name;
                                        }
                                    }

                                    for ($i=0; $i < $len_add-1; $i++) {
                                        if ($i != $len_add-1) {
                                            $adz = collect(\DB::select("SELECT * from ref_additional where id::varchar = '".$add_free_additional[$i]."'"))->first();
                                            $free_additional .= "<br>- ".$adz->additional_name;
                                        }
                                    }
                            @endphp
                                <tr>
                                    <td>
                                        <b>{{$v->product_name}}</b>
                                    <br>
                                    <br>
                                    <span class="mt-2 btn btn-secondary btn-sm">
                                        {{$v->style_size}} </span>
                                    <span class="mt-2 btn btn-secondary btn-sm">Warna :
                                        {{$v->color_name}} </span>
                                    <span class="mt-2 btn btn-secondary btn-sm">Waxseal :
                                        {{$v->waxseal_name}} </span>

                                    <span class="mt-2 btn btn-secondary btn-sm">Foil Color :
                                        {{$v->foilcolor_name}} </span>

                                    {{-- <span class="mt-2 btn btn-secondary btn-sm">
                                        Emboss:{{($v->emboss == 1) ? "Digunakan":"Tidak Digunakan"}}</span> --}}

                                    @if ($v->material2)
                                    <span class="mt-2 btn btn-secondary btn-sm">Material
                                        Inner:<b>{{$v->material1}}</b></span>
                                    <span class="mt-2 btn btn-secondary btn-sm">Material
                                        Outer:<b>{{$v->material2}}</b></span>
                                    @else
                                    <span class="mt-2 btn btn-secondary btn-sm">Material:{{$v->material1}}</span>
                                    @endif
                                    <br><span class="mt-2 btn btn-secondary btn-sm text-left"> Free
                                        Finishing: {!!$free_finishing!!}</span>
                                    <span class="mt-2 btn btn-secondary btn-sm text-left"> Free Additional
                                        Item: {!!$free_additional!!}</span>
                                    <span class="mt-2 btn btn-secondary btn-sm text-left"> Additional
                                        Item: {!!$additional_list!!}</span>

                                    <br>
                                    <br>
                                    @php
                                        // $d_image = \DB::select("SELECT * from mst_image where product_id = '".$v->product_id."'");
                                        $data = collect(\DB::select("SELECT * from mst_design where id = '".$v->design_id."'"))->first();

                                        // dd($data);
                                        $img1 = '';
                                        $img2 = '';
                                        $img3 = '';
                                        $img4 = '';
                                        if ($data->image_id) {
                                            $img1 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image_id."'"))->first();
                                        }
                                        if ($data->image2) {
                                            $img2 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image2."'"))->first();
                                        }
                                        if ($data->image3) {
                                            $img3 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image3."'"))->first();
                                        }
                                        if ($data->image4) {
                                            $img4 = collect(\DB::select("SELECT * from mst_image where id = '".$data->image4."'"))->first();
                                        }
                                    @endphp
                                    <div class="row">
                                        @if ($img1)
                                        <a href="{{asset('gallery/product')}}/{{$img1->img}}" data-toggle="lightbox" data-gallery="gallery" style="display:inline-block;" class="col-md-3 mt-3">
                                            <img class="zn-fit-center" src="{{asset('gallery/product')}}/{{$img1->img}}" class="img-fluid rounded">
                                        </a>
                                        @endif
                                        @if ($img2)
                                        <a href="{{asset('gallery/product')}}/{{$img2->img}}" data-toggle="lightbox" data-gallery="gallery" style="display:inline-block;" class="col-md-3 mt-3">
                                            <img class="zn-fit-center" src="{{asset('gallery/product')}}/{{$img2->img}}" class="img-fluid rounded">
                                        </a>
                                        @endif
                                        @if ($img3)
                                        <a href="{{asset('gallery/product')}}/{{$img3->img}}" data-toggle="lightbox" data-gallery="gallery" style="display:inline-block;" class="col-md-3 mt-3">
                                            <img class="zn-fit-center" src="{{asset('gallery/product')}}/{{$img3->img}}" class="img-fluid rounded">
                                        </a>
                                        @endif
                                        @if ($img4)
                                        <a href="{{asset('gallery/product')}}/{{$img4->img}}" data-toggle="lightbox" data-gallery="gallery" style="display:inline-block;" class="col-md-3 mt-3">
                                            <img class="zn-fit-center" src="{{asset('gallery/product')}}/{{$img4->img}}" class="img-fluid rounded">
                                        </a>
                                        @endif
                                    </div>



                                        {{-- <img src="{{asset('gallery/product')}}/{{$item->img}}" width="150px" alt=""> --}}
                                    {{-- @endforeach --}}
                                    </td>

                                </tr>


                            @endforeach



                        </tbody>
                    </table>




                </div>

            </div>
        </div>
    </div>
</div>
@stop
