<script>

$("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                type: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                product_id: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                
                custid: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                bank: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                pengiriman: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                ongkir: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                qty: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },

            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

$('#custid').select2({
        placeholder:"Silahkan Pilih"
    });
    $('#product_id').select2({
        placeholder:"Silahkan Pilih"
    });

$('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });


function set_field_harga(type) {
    console.log(type);
    
    if (type == 99) {
        $('#totalprice').prop('readonly', false); 
        $('#totalprice').val(0);
    }else {
        $('#totalprice').prop('readonly', true);
    }
}

function setHarga() {
    
    let harga = $('#product_id').find(':selected').data('harga');
    
    $('#totalprice').val(numFormat(harga));    
}

function setTotal() {
    let ongkir = parseInt(clearNumFormat($('#ongkir').val()));
    let total = (clearNumFormat($('#totalprice').val())*$('#qty').val())+ongkir;
    $('#total_pembelian').val(numFormat(total));
}

function setOngkir(type) {
    if (type == 'dikirim') {
        $('#ongkir').prop('readonly', false); 
    }else {
        $('#ongkir').prop('readonly', true);
        $('#ongkir').val(0);
    }
    
}

function getData(type,id) {
   
    var act_url = '{{ route('invoice.get_data',["type"=>":type","id"=>":id"]) }}';
    act_url = act_url.replace(':type', type);
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            $('#'+res.type).html('');
            $('#'+res.type).append(`<option value="">Pilih</option>`);
            
            if (res.id == 99) {
                $.each(res.rm.d, function (k,v) {
                        $('#'+res.type).append(`<option data-harga="0" value="`+v.code+`">`+'('+v.code+') '+v.judul+`</option>`);
                    });
            }else {
                if (res.type == 'product_id') {
                    $.each(res.rm.d, function (k,v) {
                        $('#'+res.type).append(`<option data-harga="`+v.price+`" value="`+v.product_id+`">`+v.product_name+`</option>`);
                    });
                }
            }

            
          


        }
    }).done(function( msg ) {
        endLoadingModal();
    }).fail(function (msg) {
        endLoadingModal();
        toastr.warning("Data Tidak Ditemukan");
    });
}



function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('invoice.store_other') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                        window.location.href = "{{ route('pembelian.index') }}?type=5";
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingModal();
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

</script>
