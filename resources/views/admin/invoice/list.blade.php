@section('content_admin')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Buat Invoice Undangan </h3>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Buat Invoice Undangan </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Invoice Undangan </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="saveData()" class="btn kt-subheader__btn-secondary">
                    Simpan Invoice Undangan
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Buat Invoice Undangan
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="form-data">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label>Pilih Produk</label>
                                <select onchange="
                                getData('type_size',this.value);
                                getData('color',this.value);
                                getData('shape',this.value);" class="form-control" name="product_id" id="product_id">
                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Type & Size</label>
                                <select onchange="setHarga();setMaterial();setTotal();" class="form-control" name="type_size" id="type_size">
                                    
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Warna</label>
                                <select class="form-control" name="color" id="color">
                                  
                                </select>
                            </div>
                            <div class="mb-2">
                                <label>Pilih Shape</label>
                            </div>
                            <div id="shape" class="row">
                            
                               
                            </div>
                            <div class="form-group">
                                <label>Pilih Foil Color</label>
                                <select class="form-control" name="foilcolor" id="foilcolor">
                                    <option value="">Pilih Data</option>
                                    @php
                                    $dw = \DB::select('select * from ref_foilcolor');
                                @endphp
                                <option value="0">Normal</option>
                                @foreach ($dw as $vv)
                                    <option value="{{$vv->id}}">{{$vv->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Waxseal</label>
                                <select class="form-control" name="waxseal" id="waxseal">
                                    <option value="">Pilih  Data</option>
                                    @php
                                    $dw = \DB::select('select * from ref_waxseal');
                                @endphp
                                @foreach ($dw as $vv)
                                    <option value="{{$vv->id}}">{{$vv->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Emboss</label>
                                <select class="form-control" name="emboss" id="emboss">
                                    <option value="0">Tidak Digunakan</option>
                                   <option value="1">Digunakan</option>
                                </select>
                            </div>
                            <div id="material_inner" class="form-group">
                                <label id="tittle_material">Material Inner</label>
                                <select class="form-control" name="paper_inner" id="paper_inner">
                                    @php
                                    $d_paper = \DB::select('select * from ref_paper');
                                @endphp
                                    @foreach ($d_paper as $dp)
                                        <option value="{{$dp->id}}">{{$dp->paper_name}} </option>
                                    @endforeach
                                   
                                </select>
                            </div>
                            <div id="material_outer_sh" class="form-group">
                                <label>Material Outer</label>
                                <select class="form-control" name="paper_outer" id="paper_outer">
                                    @php
                                    $d_paper = \DB::select('select * from ref_paper');
                                @endphp
                                    @foreach ($d_paper as $dp)
                                        <option value="{{$dp->id}}">{{$dp->paper_name}} </option>
                                    @endforeach
                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Free Finishing</label>
                                @php
                                $dv = \DB::select("SELECT * from ref_finishing");
                                @endphp
                                <select class="form-control" name="free_finishing" id="free_finishing">
                                <option value="0">Tidak Digunakan</option>
                                @foreach ($dv as $item)
                                <option  value="{{$item->id}}">{{$item->name}}</option>
                            
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Free Additional</label>
                                @php
                                $dv = \DB::select("SELECT * from ref_additional where price = 0");
                            @endphp
                            <select class="form-control" name="free_additional" id="free_additional">
                                <option value="0">Tidak Digunakan</option>
                                @foreach ($dv as $item)
                                <option value="{{$item->id}}">{{$item->additional_name}}</option>
                              
                                @endforeach
                            </select>
                            </div>
                          
                           
                        </div>
                        <div class="col-md-6">
                          
                            <div class="form-group">
                                <label>Pilih Customer</label>
                                <select class="form-control" name="custid" id="custid">
                                    <option value="">Pilih  Customer</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM mst_customer ");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->cust_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Di Transfer Ke Bank</label>
                                <select class="form-control" name="bank" id="bank">
                                    <option value="">Pilih  Bank</option>
                                    @php
                                        $getJenis = \DB::select("SELECT * FROM ref_bank ");
                                    @endphp
                                    @foreach ($getJenis as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}} / {{$item->bank_acc_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Pengiriman</label>
                                <select onchange="setOngkir(this.value);setTotal();" class="form-control" name="pengiriman" id="pengiriman">
                                    <option value="diambil">Diambil Di Gallery</option>
                                    <option value="dikirim">Dikirim</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ongkos Kirim</label>
                                <input value="0" readonly type="text" maxlength="7" class="form-control" onkeyup="convertToRupiah(this);setTotal();" name="ongkir" id="ongkir">
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="text" readonly class="form-control zn-readonly" onkeyup="convertToRupiah(this)" name="totalprice" id="totalprice">
                            </div>
                            <div class="form-group">
                                <label>Jumlah</label>
                                <input type="text" value="1" maxlength="7" class="form-control" onkeyup="convertToNumber(this);setTotal();" name="qty" id="qty">
                            </div>
                            <div class="form-group">
                                <label>Total Bayar</label>
                                <input type="text" readonly class="form-control zn-readonly" name="total_pembelian" id="total_pembelian">
                            </div>
                        </div>
                     
                    </div>


                </form>
            </div>
        </div>
    </div>
@include('admin.invoice.modal')

@include('admin.invoice.action')
@stop
