<script>

$('#promo_head').val('{{$data->promo_head}}');
$('#promo_subhead1').val('{{$data->promo_subhead1}}');
$('#promo_subhead2').val('{{$data->promo_subhead2}}');
$('#type1').val('{{$data->type1}}');
$('#type2').val('{{$data->type2}}');
$('#type3').val('{{$data->type3}}');

getData('product_id','{{$data->type1}}','product_id1');
getData('product_id','{{$data->type2}}','product_id2');
getData('product_id','{{$data->type3}}','product_id3');



var imageUrl = "{{asset('gallery/home')}}/{{$data->promo_banner_img1}}";
$('#ava1').css("background-image", "url(" + imageUrl + ")");

var imageUrl2 = "{{asset('gallery/home')}}/{{$data->promo_banner_img2}}";
$('#ava2').css("background-image", "url(" + imageUrl2 + ")");

var imageUrl3 = "{{asset('gallery/home')}}/{{$data->promo_banner_img3}}";
$('#ava3').css("background-image", "url(" + imageUrl3 + ")");          
       

    $('.product_id').select2({
        placeholder:"Silahkan Pilih"    
    });

    $('.type').select2({
        placeholder:"Silahkan Pilih"
    });
$("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                promo_head: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                promo_subhead1: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                promo_subhead2: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                promo_banner_desc1: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                promo_banner_desc2: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                promo_banner_desc3: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        },
                    stringLength: {
                          max:200,
                          message: 'Maksimal 200 Karakter'
                      }
                    }
                },
                img1: {
                validators: {
                    file: {
                            extension: 'jpg,jpeg,png',
                            type: 'image/jpg,image/jpeg,image/png',
                            maxSize: 2 * (1024*1024),
                            message: 'File Tidak Sesuai'
                        }
                    }
                },
                img2: {
                    validators: {
                        file: {
                                extension: 'jpg,jpeg,png',
                                type: 'image/jpg,image/jpeg,image/png',
                                maxSize: 2 * (1024*1024),
                                message: 'File Tidak Sesuai'
                            }
                    }
                },
                img3: {
                    validators: {
                        file: {
                                extension: 'jpg,jpeg,png',
                                type: 'image/jpg,image/jpeg,image/png',
                                maxSize: 2 * (1024*1024),
                                message: 'File Tidak Sesuai'
                            }
                    }
                },
                type1: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                type2: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                type3: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                product_id1: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                product_id2: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },
                product_id3: {
                    validators: {
                        notEmpty: {
                            message: 'Tidak Boleh Kosong'
                        }
                    }
                },

            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });



$('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });


function setHarga() {
    
    let harga = $('#type_size').find(':selected').data('harga');
    
    $('#totalprice').val(numFormat(harga));    
}

function setTotal() {
    let ongkir = parseInt(clearNumFormat($('#ongkir').val()));
    let total = (clearNumFormat($('#totalprice').val())*$('#qty').val())+ongkir;
    $('#total_pembelian').val(numFormat(total));
}

function setOngkir(type) {
    if (type == 'dikirim') {
        $('#ongkir').prop('readonly', false); 
    }else {
        $('#ongkir').prop('readonly', true);
        $('#ongkir').val(0);
    }
    
}

function setMaterial() {
    
    let material = $('#type_size').find(':selected').data('material');
    
    if (material == 1) {
        $('#tittle_material').html('Material');
        $('#material_outer_sh').hide();
        $('#paper_inner').removeClass('zn-readonly');
    }else if (material == 2) {
        $('#tittle_material').html('Material Inner');
        $('#material_outer_sh').show();
        $('#paper_inner').removeClass('zn-readonly');
    // ONLY ARTPAPER
    } else {
        $('#tittle_material').html('Material');
        $('#material_outer_sh').hide();
        $('#paper_inner').val(1);
        $('#paper_inner').addClass('zn-readonly');

    }
}

function getData(type,id,select) {
   
    var act_url = '{{ route('invoice.get_data',["type"=>":type","id"=>":id"]) }}';
    act_url = act_url.replace(':type', type);
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            $('#'+select).html('');
            $('#'+select).append(`<option value="">Pilih</option>`);
            
            if (res.type == 'product_id') {
                $.each(res.rm.d, function (k,v) {
                    $('#'+select).append(`<option value="`+v.product_id+`">`+v.product_name+`</option>`);
                });
            }

            if (select == 'product_id1') {
                $('#'+select).val('{{$data->product_id1}}');
            }else if (select == 'product_id2') {
                $('#'+select).val('{{$data->product_id2}}');
            }else if (select == 'product_id3') {
                $('#'+select).val('{{$data->product_id3}}');
            }
          

           

        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}



function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('mst_home.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingModal();
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

</script>
