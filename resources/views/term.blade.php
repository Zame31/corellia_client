@section('content')
<main id="pt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="pt-title-subpages noborder">Terms and Conditions</h1>
			<div class="row justify-content-md-center">
				<div class="col-lg-10 col-xl-8">
					<dl class="pt-type-01">
						<div style="text-align: justify;">
							<dt>Registrasi Online</dt>
							<dd>
								<p>
									Pendaftaran diperlukan untuk beberapa layanan atau fitur yang tersedia di situs Corellia. Kami membutuhkan informasti detail pernikahan pasangan untuk kami jadikan desain undangan. Jika anda mengubah informasi mengenai hal tersebut, anda harus memberikan informasi terbaru segera ke email by.corellia@gmail.com  Password akun yang anda miliki harus unik dan aman, anda juga harus segera memberitahu tim Corellia jika terjadi pelanggaran keamanan atau penggunaan yang tidak sah dari akun anda.</p>
							</dd>
							<dt>Kelayakan untuk Pembelian</dt>
							<dd>
								<p>
									Untuk menyelesaikan transaksi di website kami, anda harus memberikan data diri anda seperti nama asli, nomor kontak, alamat email, dan informasi lainnya seperti yang ditunjukkan. Anda juga akan diminta untuk memberikan rincian pembayaran yang valid, benar, dan harus melakukan konfirmasi jika anda adalah orang yang dimaksud dalam informasi penagihan yang disediakan. Kami hanya akan memproses transaksi dengan akun yang telah memenuhi persyaratan kelayakan kami dan memiliki akun bank resmi. Selain itu, anda juga menyetujui jika kami dapat menggunakan informasi pribadi yang diberikan oleh anda untuk melakukan pengecekan. Informasi pribadi yang anda berikan kepada kami akan aman dan tidak akan ada penyalahgunaan.
								</p>
							</dd>
							<dt>Pembayaran</dt>
							<dd>
								<p>
									Calon customer wajib memberikan tanda jadi min Rp 500.000,- ( lima ratus ribu rupiah ) untuk dapat lanjut ke proses design, dan melakukan pelunasan saat fix desain untuk turun cetak.
								</p>
							</dd>
							<dt>Design</dt>
							<dd>
								<ol>
									<li>Proses desain dilakukan dalam waktu 2-3 Minggu dari awal pengajuan , proses desain kustom by oeder 3-4 minggu
									</li>
									<li>Revisi dilakukan max sebanyak 2x lebih dari itu dikenakan biaya sebesear Rp.100.000,- (seratus ribu rupiah) / revisi dikerjakan dalam waktu sekitar 1-2 minggu, dan proses naik cetak setelah fix desain di 3 Minggu.
									</li>
									<li>Penulisan nama, tanggal, gelar dan lainnya menjadi tanggung jawab custumer, Corellia tidak bertanggungjawab atas isi undangan setelah desain tersebut disetujui.</li>
								
								</ol>
							</dd>
						</div>
						
					</dl>
				</div>
			</div>
		</div>
	</div>
</main>

@include('master.component.footer')
@stop